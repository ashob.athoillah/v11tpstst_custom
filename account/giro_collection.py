# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp 
from odoo.exceptions import UserError
from itertools import chain

    
class GiroCollection(models.Model):
    _name = 'giro.collection'
    _order = "value_date DESC, create_date DESC"
    
    name = fields.Char('DDA',required=True,size=64)
    value_date = fields.Date('Value Date', required=True)
    partner_id = fields.Many2one('res.partner', 'Partner', required=True)
    line_ids = fields.One2many('account.move.line','giro_collection_id','Lines',readonly=True)
    payment_term_type = fields.Selection([
        ('giro','Giro'),
        ('cda','CDA')], string="Is Giro / CDA")
    collected = fields.Boolean('Already Collected', readonly=True, copy=False, default=False)
    amount = fields.Float(string='Amount', digits=dp.get_precision('Account'), readonly=True, copy=False)