# -*- encoding: utf-8 -*-
import time
from odoo.addons import decimal_precision as dp
from datetime import datetime, date
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.addons.v11_tpstst_custom.membership import membership_application as membership


class MembershipRenewal(models.Model):
    _name = "membership.renewal"
    _description = 'Membership Renewal'
    _inherit = ['mail.thread']

    @api.model
    def _default_account(self):
        company = self.env.user.company_id
        account_id = company.niche_income_account_id and company.niche_income_account_id.id or False

        return account_id

    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.membership_journal_id and company.membership_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    @api.model
    def _default_funds(self):
        company = self.env.user.company_id
        funds_id = company.default_membership_fund_id and company.default_membership_fund_id.id or False

        return funds_id

    # General
    reg_date = fields.Date('Renewal Date',  default=lambda self: fields.datetime.now(), track_visibility='onchange')
    reg_no = fields.Char('Renewal Number', copy=False, track_visibility='onchange')

    partner_id = fields.Many2one('res.partner', domain=[('member', '=', True), ('membership_type', '=', 'ordinary')], string="Member", track_visibility='onchange')
    membership_type = fields.Selection(membership.MEMBERSHIP_TYPE, default='ordinary', string="Membership Type", track_visibility='onchange')
    membership_period_id = fields.Many2one('membership.period', default=lambda self: self._get_default_membership_period(), string="Membership Period", track_visibility='onchange')
    state = fields.Selection([
            ('draft', 'Draft'), 
            ('confirm', 'Confirm'), 
            ('paid', 'Paid'), 
            ], string="State", default='draft', track_visibility='onchange')
    
    account_id = fields.Many2one('account.account','Account', required=True, default=_default_account)
    journal_id = fields.Many2one('account.journal', string="Journal", default=_default_journal, track_visibility='onchange')
    membership_fee_id = fields.Many2one('product.product', compute='_get_membership_fee', string="Membership Fee", copy=False, track_visibility='onchange')
    total_membership_fee = fields.Float('Membership Fee', compute="_amount_all", store=True, track_visibility='onchange')
    move_line_ids = fields.One2many('account.move.line', 'membership_renewal_id')
    funds_id = fields.Many2one('pool.of.funds','Funds', default=_default_funds, required=True, domain=[('state','=', 'confirm')])
    voucher_ids = fields.One2many('account.voucher', 'membership_renewal_id')
   
    residual = fields.Float('Payable Amount', track_visibility='onchange')
    amount_untaxed = fields.Float(string='Untaxed Fee', store=True,
                                  compute='_amount_all', track_visibility='onchange')
    amount_tax = fields.Float(string='GST', store=True, readonly=False, compute='_amount_all', track_visibility='onchange')
    amount_total = fields.Float(string='Total Amount', store=True,
                                compute='_amount_all', track_visibility='onchange')
    amount_paid = fields.Float(string='Amount Paid', store=True,
                                track_visibility='onchange')
    accept_term_and_condition = fields.Boolean('I\'m willing to accept Terms & Conditions', track_visibility='onchange')

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.name_set()
        self.state = 'confirm'

    @api.multi
    def action_paid(self):
        pool_of_funds = self.env['pool.of.funds'].search([('name','=', self.funds_id.name)])
        funds_vals = []
        vals = {
                    'funds_id': pool_of_funds.id,
                    'amount': self.amount_total,
                    'log_for': 'donation',
                    'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                    'account': self.account_id.user_type_id.name,
                }
        funds_vals.append((0, 0, vals))
        pool_of_funds.write({'log_ids':funds_vals})
        self.action_update_membership()
        self.mail_paid_email()
        self.state = 'paid'

    # Membership Related

    @api.multi
    def _get_default_membership_period(self) :
        obj_period = self.env['membership.period']
        get_period = obj_period.search([('default', '=', True)], limit=1)
        return get_period

    @api.multi
    def action_update_membership(self) :
        membership_type = self.membership_type
        partner = self.partner_id
        today = datetime.now().date()

        data = {}
        data['membership_state']    = 'active'
        if membership_type == 'ordinary' :
            data['membership_start']    = today
            data['membership_previous_validity'] = datetime.strptime(partner.membership_stop, "%Y-%m-%d")

            if datetime.strptime(partner.membership_stop, "%Y-%m-%d").date() > datetime.now().date() :
                data['membership_stop']     = datetime.strptime(partner.membership_stop, "%Y-%m-%d") + relativedelta(years=self.membership_period_id.period)
            else :
                data['membership_stop']     = today + relativedelta(years=self.membership_period_id.period)

        if partner :
            partner.write(data)
        else :
            raise UserError(_("Can't found member data, may recheck your application"))
    

    # Payment Related
    @api.depends('membership_type', 'membership_period_id')
    @api.multi
    def _get_membership_fee(self):
        obj_product = self.env['product.product']
        for rec in self :
            fee = False
            membership_type = rec.membership_type
            membership_fee = False
            membership_period = rec.membership_period_id

            if membership_type and membership_type == 'ordinary' :
                fee = obj_product.search([
                    ('active', '=', True), 
                    ('membership', '=', True), 
                    ('type', '=', 'service'), 
                    ('membership_fee_type', '=', 'renewal'),
                    ('membership_type', '=', membership_type), 
                    ('membership_period_id', '=', membership_period.id)
                    ], limit=1)
            else :
                fee = obj_product.search([
                    ('active', '=', True), 
                    ('membership', '=', True), 
                    ('membership_fee_type', '=', 'renewal'),
                    ('type', '=', 'service'),
                    ('membership_type', '=', membership_type)
                    ], limit=1)

            if fee :
                membership_fee = fee

            # else :
            #     raise UserError(_("No membership fee founds, May go Product Module to create the records."))

            rec.membership_fee_id = membership_fee

    @api.depends('membership_fee_id', 'membership_fee_id.valid_price')
    @api.multi
    def _amount_all(self) :
        for rec in self :
            membership_fee = rec.membership_fee_id
            list_taxes = []
            member_amount = 0.00
            total_product_amount = 0.00

            if membership_fee :
                member_amount = membership_fee.valid_price
                taxes_member = [float("%.2f" % (member_amount * (taxes.amount / 100))) for taxes in membership_fee.taxes_id if membership_fee.taxes_id]
                list_taxes += taxes_member

            amount_untaxed = member_amount + total_product_amount
            amount_tax = sum(list_taxes)

            rec.amount_untaxed = amount_untaxed
            rec.amount_tax = amount_tax
            rec.amount_total = amount_untaxed + amount_tax

            rec.total_membership_fee = membership_fee.valid_price
            # rec.write({'amount_untaxed' : amount_untaxed, 'amount_tax' : amount_tax, 'amount_total' : amount_untaxed + amount_tax})

    # ETC
    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no :
            self.reg_no = self.env['ir.sequence'].next_by_code('membership.renewal.seq')
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.reg_no or _('/')
            result.append((rec.id, name))

        return result

    #mail related

    @api.multi
    def mail_paid_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_ordinary_renewal_paid') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        member = self.partner_id

        if member.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : member.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()
    
    
    @api.multi
    def call_membership_renewal_payment_wizard(self):
        vals = {}
        journal_id = self._default_journal() or False
        vals['journal_id'] = journal_id
        return {
            'name': _('Membership Renewal Payment'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.voucher.donation',
            'target': 'new',
            'context': {'default_membership_renewal_id': self.id, 'default_amount': self.amount_total, 'default_funds_id': self.funds_id.id, 'default_reg_no': self.reg_no, 'default_journal_id': vals['journal_id'],'default_partner_id':self.partner_id.id}
        }