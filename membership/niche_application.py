# -*- encoding: utf-8 -*-
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from datetime import datetime, date
import base64
import io
import xlwt


class NicheApplication(models.Model):
    _name = "niche.application"
    _description = 'Columbarium Niche Application'
    _inherit = ['mail.thread']

    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.niche_journal_id and company.niche_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    @api.model
    def _default_funds(self):
        company = self.env.user.company_id
        funds_id = company.default_niche_fund_id and company.default_niche_fund_id.id or False

        return funds_id

    @api.model
    def _default_account(self):
        company = self.env.user.company_id
        account_id = company.niche_income_account_id and company.niche_income_account_id.id or False

        return account_id

    # General
    reg_date = fields.Date('Date of Application / 申请日期', default=lambda self: fields.datetime.now(), track_visibility='onchange')
    reg_no = fields.Char('Registration Number /  编号', copy=False, track_visibility='onchange')

    remarks = fields.Text('Remark / 备考',  track_visibility='onchange')

    # Applicant
    partner_id = fields.Many2one('res.partner', string="Member", track_visibility='onchange')
    state = fields.Selection([
            ('draft', 'Draft'), 
            ('confirm', 'Confirm'), 
            ('approve', 'Approved'), 
            ('paid', 'Paid'),
            ('reject', 'Rejected'),
            ('cancel', 'Cancelled')], string="State", default='draft', track_visibility='onchange')

    deceased_info_ids = fields.One2many('niche.deceased.info', 'niche_application_id', string="Deceased Info", track_visibility='onchange')

    # Payment
    residual = fields.Float('Payable Amount',  track_visibility='onchange')
    amount_untaxed = fields.Float(string='Untaxed Fee', store=True,
                                  compute='_amount_all', track_visibility='onchange')
    amount_tax = fields.Float(string='GST', store=True, readonly=False, compute='_amount_all', track_visibility='onchange')
    amount_total = fields.Float(string='Total Amount', store=True,
                                compute='_amount_all', track_visibility='onchange')
    amount_paid = fields.Float(string='Amount Paid', store=True,
                                 track_visibility='onchange')

    move_line_ids = fields.One2many('account.move.line', 'niches_id')
    voucher_ids = fields.One2many('account.voucher', 'niche_id')
    account_id = fields.Many2one('account.account','Account', required=True, default=_default_account)
    funds_id = fields.Many2one('pool.of.funds','Funds', required=True, default=_default_funds, domain=[('state','=', 'confirm')])

    # Product
    product_id = fields.Many2one('product.product', string="Location", domain=[('columbarium_product', '=', True), ('sold_out', '=', False)],  track_visibility='onchange')
    product_template_id = fields.Many2one('product.template', string="Product", domain=[('columbarium_product', '=', True), ('sold_out', '=', False)])
    level_id = fields.Many2one('level.detail.list', track_visibility='onchange')
    journal_id = fields.Many2one('account.journal', string="Journal", default=_default_journal, track_visibility='onchange')

    responsible_user_id = fields.Many2one('res.users', copy=False, string='Attended by / 经手人', track_visibility='onchange')
    purchase_advance_id = fields.Many2one('purchase.in.advance', string='Purchase in Advance', track_visibility='onchange')
    accept_term_and_condition = fields.Boolean('I\'m willing to accept Terms & Conditions', track_visibility='onchange')
    term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self: self._get_default_term_condition(), string='Term & Conditions', track_visibility='onchange')
    signature = fields.Binary("Applicant’s Signature / 申请人签名", copy=False, track_visibility='onchange')
    
    @api.multi
    def _get_default_term_condition(self) :
        company = self.env.user.company_id

        term_and_condition = False
        
        if company.default_niche_term_id :
            term_and_condition = company.default_niche_term_id and company.default_niche_term_id.id or False

        return term_and_condition
    
    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id
    
    @api.onchange('product_id')
    @api.multi
    def sync_data_product(self) :
        if self.product_id :
            self.product_template_id = self.product_id.product_tmpl_id and self.product_id.product_tmpl_id.id
        else :
            self.product_template_id = False

        self.level_id = False     

    @api.multi
    def get_application_record(self) :
        app_obj = self.env['membership.application']
        application = app_obj.search([('niche_application_id', '=', self.id)], limit=1)
        if application :
            application.action_donated()
        # else :
        #     raise Warning(_('You\'re need to became associate member to able purchase niche'))

    # @api.onchange('partner_id')
    # @api.multi
    # def sync_data_partner(self) :
    #     partner = self.partner_id
    #     if partner :
    #         self.title_id       = partner.title and partner.title.id
    #         self.name           = partner.name
    #         self.chinese_name   = partner.chinese_name
    #         self.identity_no    = partner.identity_no
    #         self.color_id       = partner.color_id
    #         self.mobile         = partner.mobile
    #         self.email          = partner.email
    #         self.street         = partner.street
    #         self.street2        = partner.street2
    #         self.zip            = partner.zip
    #         self.country_id     = partner.country_id and partner.country_id.id
    #         self.company_id     = partner.parent_id and partner.parent_id.id
    #     else :
    #         self.title_id       = False
    #         self.name           = False
    #         self.chinese_name   = False
    #         self.identity_no    = False
    #         self.color_id       = False
    #         self.mobile         = False
    #         self.email          = False
    #         self.street         = False
    #         self.street2        = False
    #         self.zip            = False
    #         self.country_id     = False
    #         self.company_id     = False

    # button

    @api.multi
    def action_confirm(self) :
        self.get_responsible_user()
        self.validation_deceased_info()
        self.action_add_booked_item()
        self.mail_confirm_email()
        self.name_set()
        self.state = 'confirm'

    @api.multi
    def action_paid(self) :
        pool_of_funds = self.env['pool.of.funds'].search([('name','=', self.funds_id.name)])
        funds_vals = []
        vals = {
                    'funds_id': pool_of_funds.id,
                    'amount': self.amount_total,
                    'log_for': 'donation',
                    'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                    'account': self.account_id.user_type_id.name,
                }
        funds_vals.append((0, 0, vals))
        pool_of_funds.write({'log_ids':funds_vals})

        if self.partner_id.membership_type == 'associate' :
            self.get_application_record()
        self.action_write_status_item()
        self.state = 'paid'


    @api.multi
    def action_approve(self) :
        self.get_responsible_user()
        self.mail_approve_email()
        self.state = 'approve'
    
    @api.multi
    def action_reject(self) :
        self.action_remove_booked_item()
        self.get_responsible_user()
        self.state = 'reject'

    @api.multi
    def action_cancel(self) :
        self.get_responsible_user()
        if self.state == 'confirm' or self.state == 'approve' :
            self.action_remove_booked_item()

        self.state = 'cancel'

    @api.multi
    def action_add_booked_item(self) :
        for rec in self :
            product= rec.product_template_id
            level = rec.level_id
            if not rec.product_id.sold_out :
                if level.slot > (level.slot_used + level.slot_booked) and not level.is_sold :
                    level.slot_booked += 1
                    level._compute_overall_tablet_slot()
                    product._compute_overall_slot()
                else :
                    raise Warning(_('This level is not available anymore.'))
            else :
                raise Warning(_('This location and level is not available anymore.'))

    @api.multi
    def action_remove_booked_item(self) :
        for rec in self :
            level = rec.level_id
            level.slot_booked -= 1

    @api.multi
    def action_write_status_item(self) :
        for rec in self :
            product= rec.product_template_id
            level = rec.level_id
            if not rec.product_id.sold_out :
                if level.slot >= (level.slot_used + level.slot_booked) and not level.is_sold :
                    level.slot_used += 1
                    level.slot_booked -= 1
                    level._compute_overall_tablet_slot()
                    product._compute_overall_slot()
                else :
                    raise Warning(_('This level is not available anymore.'))
            else :
                raise Warning(_('This location and level is not available anymore.'))

    @api.multi
    def validation_deceased_info(self) :
        total = len(self.deceased_info_ids)
        if total > 1 :
            raise Warning(_('System only allow 1 Person / Application'))
        # elif total < 1 :
        #     raise Warning(_('Please insert info for deceased person'))

    #payment related

    @api.depends('product_id', 'level_id', 'level_id.price', 'level_id.slot_used', 'state')
    @api.multi
    def _amount_all(self) :
        for rec in self :
            product = rec.product_id
            level = rec.level_id
            state = rec.state
            amount_untaxed = 0.00
            list_taxes = []
            amount_tax= 0.00
            amount_paid = 0.00
            residual = 0.00
            #check whether sold to avoid overlapping 
            if product and level :
                if (product.sold_out or level.is_sold) and state not in ['approve', 'paid'] :
                    raise Warning(_('Location/Level for application %s already FULL please to choose other location/level') % (str(rec.partner_id.name) + ' - ' + str(product.name) + ' - ' + str(level.name)) )
                    
                amount_untaxed = level.price
                list_taxes = [float("%.2f" % (amount_untaxed * (taxes.amount / 100))) for taxes in product.taxes_id if product.taxes_id]
                amount_tax = amount_untaxed + sum(list_taxes)


            rec.amount_untaxed = amount_untaxed
            rec.amount_tax = sum(list_taxes)
            rec.amount_total = amount_tax

    #ETC
    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no :
            self.reg_no = self.env['ir.sequence'].next_by_code('niche.application.seq')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.reg_no or _('New Niche Application')
            result.append((rec.id, name))

        return result

    # @api.model
    # def create(self, vals):
    #     vals['reg_no'] = self.env['ir.sequence'].next_by_code('niche.application.seq')
    #     return super(NicheApplication, self).create(vals)

    # Mail related 

    @api.multi
    def mail_confirm_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_niche_confirm') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        member = self.partner_id

        if member.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : member.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

    @api.multi
    def mail_approve_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_niche_approve') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        member = self.partner_id

        if member.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : member.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

    @api.multi
    def print_col_lot_excel(self):
        """Method to print a default Excel Report for Columbarium Niche Lot Number Block"""
        style_money = xlwt.easyxf('font: name Calibri, color black, height 220; align : wrap 1, vert centre, horz center;')
        style_lot = xlwt.easyxf('font: name Calibri, color black, height 220; align : wrap 1, horz center, rotation 90;')
        all_border = xlwt.easyxf('borders: top thin, right thin, bottom thin, left thin;')
        upper_bold = xlwt.easyxf('font: name Arial, color black, height 220, bold on; align : vert centre;')
        right_style = xlwt.easyxf('font: name Calibri, color black, height 120; align: horz  right; borders: top thin, right thin, bottom thin, left thin;')
        grey = xlwt.easyxf('pattern: pattern solid, fore_color gray40;')
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Sheet 1')

        # Lot A in range 1643 - 1631s
        row = 1
        ws.row(row + 1).height = 900
        ws.col(1).width = 1700
        lot_1643 = 1644 # Lot A start from 1643 but declare as 1644 because use a enumerate loop
        lot_1631 = 1631 # Lot A end in 1631
        a1 = 1
        for i, j in enumerate(range(lot_1631, lot_1643), 2):
            ws.write(2, i, lot_1643-a1, style_lot)
            ws.write(29, i + 15, lot_1643-a1, right_style)
            ws.col(i).width = 1700
            ws.col(i + 15).width = 1700
            if lot_1643 - a1 == 1631:
                for k in range(7):
                    ws.write(29 + k, i + 16, "", grey)
                ws.col(i + 16).width = 1700

            a1 += 1
        

        seq_a = 8 # seq_a stand for sequence Lot A (there's 8 lot in Location A based on the given report)
        cell_to_border_a = 2 # Cell to be brodered in LOT A
        for i, j in enumerate(range(15), 3):
            for k, l in enumerate(range(13),2):
                ws.write(i, k, "", all_border)
            if i % 2 != 0:
                ws.row(i).height = 600
                ws.write(i, 1, seq_a, style_money)
                if seq_a > 6 or seq_a == 3:
                    ws.write(i, 0, "$12,000", style_money)
                elif seq_a in range(4, 8):
                    ws.write(i, 0, "$15,000", style_money)
                elif seq_a == 2:
                    ws.write(i, 0, "$9,000", style_money)
                else:
                    ws.write(i, 0, "$6,000", style_money)
                if seq_a == 1:
                    ws.row(i+1).height = 500
                    ws.write(i+1, 4, "LOCATION", upper_bold)
                    ws.write(i+1, 6, "4A", upper_bold)
                    ws.write(i+1, 7, "（向前）", upper_bold)
                seq_a -= 1
            else:
                ws.row(i).height = 100
        

        # Lot A in range 1618 - 1630

        row2 = 20

        ws.row(row2).height = 900
        lot_1618 = 1618
        lot_1630 = 1631
        a2 = 1
        for i, k in enumerate(range(lot_1618, lot_1630), 2):
            ws.write(20, i, k, style_lot)
            ws.write(35, i + 15, k, right_style)
            ws.col(i).width = 1700
            ws.col(i + 15).width = 1700

        seq_a2 = 8 # seq_a2 stand for sequence Lot A (there's 8 lot in Location A based on the given report)
        cell_to_border_a = 2 # Cell to be brodered in LOT A
        for i, j in enumerate(range(15), 21):
            for k, l in enumerate(range(13),2):
                ws.write(i, k, "", all_border)
            if i % 2 != 0:
                ws.row(i).height = 600
                ws.write(i, 1, seq_a2, style_money)
                if seq_a2 > 6 or seq_a2 == 3:
                    ws.write(i, 0, "$11,000", style_money)
                elif seq_a2 in range(4, 8):
                    ws.write(i, 0, "$14,000", style_money)
                elif seq_a2 == 2:
                    ws.write(i, 0, "$8,000", style_money)
                else:
                    ws.write(i, 0, "$5,000", style_money)
                if seq_a2 == 1:
                    ws.row(i+1).height = 500
                    ws.write(i+1, 4, "LOCATION", upper_bold)
                    ws.write(i+1, 6, "4A", upper_bold)
                    ws.write(i+1, 7, "（向前）", upper_bold)
                seq_a2 -= 1
            else:
                ws.row(i).height = 100



        fp = io.BytesIO()
        wb.save(fp)
        fp.seek(0)
        data = fp.read()
        fp.close()
        file = base64.encodestring(data)
        module_rec = self.env['columbarium.lot.no.wizard'].create({'name': 'Columbarium Lot Number Block.xls', 'file' : file})
        return {
          'name': _('Columbarium Lot Number Block'),
          'res_id' : module_rec.id,
          'view_type': 'form',
          "view_mode": 'form',
          'res_model': 'columbarium.lot.no.wizard',
          'type': 'ir.actions.act_window',
          'target': 'new',
        }

    @api.multi
    def call_niches_payment_wizard(self):
        vals = {}
        return {
            'name': _('Niche Payment'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.voucher.donation',
            'target': 'new',
            'context': {'default_niche_id': self.id, 'default_amount': self.amount_total, 'default_funds_id': self.funds_id.id, 'default_reg_no': self.reg_no, 'default_journal_id': self.journal_id.id, 'default_partner_id':self.partner_id.id}
        }

class NicheDeceasedInfo(models.Model):
    _name = "niche.deceased.info"
    _description = 'Niche Deceased Info'
    _inherit = ['mail.thread']

    title_id = fields.Many2one('res.partner.title', string="Title / 称号", track_visibility='onchange')
    name = fields.Char('English Name / 英文姓名', track_visibility='onchange', required=True)
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility='onchange') 
    identity_no = fields.Char('NRIC No. / 新居民证号码', index=True, size=12, copy=False)
    identity_no_mask = fields.Char('NRIC No. / 新居民证号码', copy=False, store=True, compute="_compute_identity_no_mask")
    show_identity_no = fields.Char('Show ?', copy=False, default=False)
    origin = fields.Char('Origin', track_visibility='onchange')
    death_certificate_no = fields.Char(string='Death Certificate Number', track_visibility='onchange')
    birthdate = fields.Date(string='Date of Birth / 出生日期', track_visibility='onchange')
    date_of_death = fields.Date(string='Date of Death', track_visibility='onchange')
    cremated_place = fields.Char(string='Place where deceased was cremated', track_visibility='onchange')
    permit_no = fields.Char('Permit No.', track_visibility='onchange')
    niche_application_id = fields.Many2one('niche.application', string='Niche Application Info', track_visibility='onchange')
    relationship_id = fields.Many2one('partner.relationship', string='Relationship', track_visibility='onchange')
    identity_no_type = fields.Selection([
                        ('bc', 'BC'),
                        ('nric', 'NRIC'),
                        ('passport', 'Passport'),
                        ('fin', 'FIN')
                        ], default='nric')

    identity_type = fields.Selection([
                        ('sc', 'Singapore Citizen'),
                        ('pr', 'Permanent Resident'),
                        ('other', 'Others'),
                        ], string='Identity Type')
    citizenship_id = fields.Many2one('hr.citizenship', string="Citizenship", track_visibility='onchange')
    purchase_advance_list_id = fields.Many2one('purchase.advance.list', string="Related Purchase Advance List Form")
    status = fields.Selection([
                ('living', 'Living'),
                ('deceased', 'Deceased')
            ], default='living', string="Status")

    @api.multi
    def show_identity(self):
        for rec in self:
            rec.show_identity_no = True

    @api.multi
    def hide_identity(self):
        for rec in self:
            rec.show_identity_no = False

    @api.depends('identity_no', 'show_identity_no')
    @api.multi
    def _compute_identity_no_mask(self):
        for rec in self:
            if rec.identity_no:
                if rec.show_identity_no:
                    rec.identity_no_mask = rec.identity_no
                else:
                    rec.identity_no_mask = rec.convert_string(rec.identity_no)    
            else :
                pass
    
    @api.multi
    def convert_string(self, val):
        if val:
            res = len(val[:-4]) * "*" + val[-4:]
            return res
        return False


    def check_nric_formula(self, nric, birthdate=False):
        sc = False
        pr = False
        identity_type = ''
        nric_len = 9
        sg_first_char = ['s', 'S', 't', 'T']
        pr_first_char = ['f', 'F', 'g', 'G']
        special_val = ['g', 'G', 't', 'T']
        digit_count = [2,7,6,5,4,3,2]
        count_amount = 0
        mod_amount = 0
        last_char = ''
        sg_check_letter = {'0' : 'J',
                           '1' : 'Z',
                           '2' : 'I',
                           '3' : 'H',
                           '4' : 'G',
                           '5' : 'F',
                           '6' : 'E',
                           '7' : 'D',
                           '8' : 'C',
                           '9' : 'B',
                           '10' : 'A',
                           }
        pr_check_letter = {'0' : 'X',
                           '1' : 'W',
                           '2' : 'U',
                           '3' : 'T',
                           '4' : 'R',
                           '5' : 'Q',
                           '6' : 'P',
                           '7' : 'N',
                           '8' : 'M',
                           '9' : 'L',
                           '10' : 'K',
                           }
        
        if len(str(nric)) != nric_len:
            raise UserError(_('Error in Identity Number'))
            
        if birthdate:
            birthyear = datetime.strptime(birthdate, '%Y-%m-%d').year
            if birthyear > 1968:
                nric_birthyear = str(birthyear)[-2:]
                if (nric[1] + nric[2]) != nric_birthyear:
                    raise UserError(_('Error in Identity Number'))

        if nric[0] in sg_first_char:
            sc = True
        elif nric[0] in pr_first_char:
            pr = True
        else:
            raise UserError(_('Error in Identity Number'))
            
        count_amount += int(digit_count[0]) * int(nric[1])
        count_amount += int(digit_count[1]) * int(nric[2])
        count_amount += int(digit_count[2]) * int(nric[3])
        count_amount += int(digit_count[3]) * int(nric[4])
        count_amount += int(digit_count[4]) * int(nric[5])
        count_amount += int(digit_count[5]) * int(nric[6])
        count_amount += int(digit_count[6]) * int(nric[7])

        if nric[0] in special_val:
            mod_amount = (count_amount + 4) % 11
        else:
            mod_amount = count_amount % 11

        if sc == True:
            identity_type = 'sc'
            last_char = sg_check_letter[str(mod_amount)]
        elif pr == True:
            identity_type = 'pr'
            last_char = pr_check_letter[str(mod_amount)]

        if str(nric[8]) != str(last_char):
            raise UserError(_('Error in Identity Number'))
            
        return identity_type

    @api.onchange('identity_no')
    def onchange_identity_no(self):
        warning = {}
        vals = {}
        if self.identity_no :
            identity_no = str(self.identity_no).upper().strip()
            vals['identity_no'] = identity_no
            user_id = self.env['res.partner'].search([('identity_no', '=ilike', identity_no)])
            sg_citizen = self.env['hr.citizenship'].search([('is_singaporean', '=', True)])
            
            if not self.birthdate:
                self.identity_no = False
                return {'warning': {
                    'title': "Warning",
                    'message': "Please fill in birthdate first",
                    }
                }
            
            if self.identity_no_type in ['bc', 'nric']:
                check_nric = self.check_nric_formula(identity_no, self.birthdate)
                if check_nric[0:2] == 'sc':
                    self.citizenship_id = sg_citizen[0]
                    self.identity_type = 'sc'
                elif check_nric[0:2] == 'pr':
                    self.citizenship_id = False
                    self.identity_type = 'pr'
            else:
                self.identity_type = 'other'
            # add check by len for case user accidentally change the identity no then want to change it back.
            # if user create new partner with same identity no, system would prompt warning from constraint, so there will no duplicated partner.
            if len(user_id) > 1 : 
                title = ("Warning")
                message = "Identity Number(%s) already exists!" % identity_no
                warning = {
                        'title': title,
                        'message': message,
                }
                vals['identity_no'] = False
            return {
                    'value':vals,
                    'warning':warning}
                    
        else:
            self.citizenship_id = False
            self.identity_type = False

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name) + ' ( ' + str(rec.chinese_name) + ' )'
            result.append((rec.id, name))

        return result