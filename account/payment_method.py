# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class PaymentMethod(models.Model):
    _name = 'custom.payment.method'
    _description = "Payment Method"
    _order = "sequence"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _sql_constraints = [('name_unique', 'unique(name, type)', 'This Payment method is already exists.')]

    name = fields.Char(string="Name", required=True,track_visibility='onchange')
    sequence = fields.Integer(string="Sequence",track_visibility='onchange')
    journal_id = fields.Many2one('account.journal', string="Journal",track_visibility='onchange')
    partner_id = fields.Many2one('res.partner', string="Partner", related="journal_id.company_id.partner_id",track_visibility='onchange')
    type = fields.Selection([
        ('paynow','PayNow'),
        ('cash', 'Cash'),
        ('net', 'Nets'),
        ('cheque', 'Cheque'),
        ('other', 'Miscellaneous')
    ], string="Type", required=True,track_visibility='onchange')
    charges = fields.Float(string="Charges (%)",track_visibility='onchange')
    description = fields.Text(string="Description",track_visibility='onchange')
    pos_payment_ref = fields.Boolean('POS Payment Ref', default=False)