# -*- encoding: utf-8 -*-
from datetime import datetime, date
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError

class TabletApplication(models.Model):
    _name = "tablet.application"
    _description = 'Tablets Application'
    _inherit = ['mail.thread']
    

    @api.model
    def _default_account(self):
        company = self.env.user.company_id
        account_id = company.tablet_income_account_id and company.tablet_income_account_id.id or False

        return account_id

    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.tablet_journal_id and company.tablet_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    @api.model
    def _default_funds(self):
        company = self.env.user.company_id
        funds_id = company.default_tablet_fund_id and company.default_tablet_fund_id.id or False

        return funds_id

    # General
    reg_date = fields.Date('Date of Application / 申请日期', default=lambda self: fields.datetime.now(), track_visibility='onchange')
    reg_no = fields.Char('Registration Number /  编号', track_visibility='onchange')

    remarks = fields.Text('Remark / 备考',  track_visibility='onchange')

    # Applicant
    partner_id = fields.Many2one('res.partner', string="Member", track_visibility='onchange')
    tablet_ids = fields.One2many('tablet.list', 'tablet_application_id', string="Tablet Applied For")
    signature = fields.Binary("Applicant’s Signature / 申请人签名", track_visibility='onchange')
    state = fields.Selection([
            ('draft', 'Draft'), 
            ('confirm', 'Confirm'), 
            ('approve', 'Approved'), 
            ('paid', 'Paid'),
            ('reject', 'Rejected'),
            ('cancel', 'Cancelled')], string="State", default='draft', track_visibility='onchange')

    # Payment
    journal_id = fields.Many2one('account.journal', string="Journal", default=_default_journal, track_visibility='onchange')
    
    residual = fields.Float('Payable Amount',  track_visibility='onchange')
    amount_untaxed = fields.Float(string='Untaxed Fee', store=True,
                                  compute='_amount_all', track_visibility='onchange')
    amount_tax = fields.Float(string='GST', store=True, readonly=False, compute='_amount_all', track_visibility='onchange')
    amount_total = fields.Float(string='Total Amount', store=True,
                                compute='_amount_all', track_visibility='onchange')
    amount_paid = fields.Float(string='Amount Paid', store=True,
                                 track_visibility='onchange')

    move_line_ids = fields.One2many('account.move.line', 'tablets_id')
    voucher_ids = fields.One2many('account.voucher', 'tablet_id')
    funds_id = fields.Many2one('pool.of.funds','Funds', required=True, default=_default_funds, domain=[('state','=', 'confirm')])
    account_id = fields.Many2one('account.account','Account', required=True, default=_default_account)

    # Product
    product_id = fields.Many2one('product.product', domain=[('tablet_product', '=', True), ('sold_out', '=', False)],  track_visibility='onchange')
    level_id = fields.Many2one('level.detail.list', string="Alloment Area", track_visibility='onchange')
    product_template_id = fields.Many2one('product.template', string="Product", domain=[('tablet_product', '=', True), ('sold_out', '=', False)])
    unit_id = fields.Many2one('side.detail.list', string="Unit No.", track_visibility='onchange')
    unit_level_id = fields.Many2one('side.level.list', string="Unit No. Level", track_visibility='onchange')

    #etc
    responsible_user_id = fields.Many2one('res.users', copy=False, string='Attended by / 经手人', track_visibility='onchange')
    purchase_advance_id = fields.Many2one('purchase.in.advance', string='Purchase in Advance', track_visibility='onchange')
    accept_term_and_condition = fields.Boolean('I\'m willing to accept Terms & Conditions', track_visibility='onchange')
    term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self: self._get_default_term_condition(), string='Term & Conditions', track_visibility='onchange')
    tablet_applied_name_reg_no = fields.Char('Tablet Applied No.')

    @api.multi
    def _get_default_term_condition(self) :
        company = self.env.user.company_id
        term_and_condition = False
        if company.default_tablet_term_id :
            term_and_condition = company.default_tablet_term_id and company.default_tablet_term_id.id or False

        return term_and_condition

    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id
    
    @api.onchange('product_id')
    @api.multi
    def sync_data_product(self) :
        if self.product_id :
            self.product_template_id = self.product_id.product_tmpl_id and self.product_id.product_tmpl_id.id
        else :
            self.product_template_id = False

        self.level_id = False

    @api.multi
    def get_application_record(self) :
        app_obj = self.env['membership.application']
        application = app_obj.search([('tablet_application_id', '=', self.id)], limit=1)
        if application :
            application.action_donated()
        # else :
        #     raise Warning(_('You\'re need to became associate member to able purchase tablet'))

    @api.multi
    def action_confirm(self) :
        self.validation_tablet()
        self.name_set()
        self.state = 'confirm'

    @api.multi
    def action_paid(self) :
        pool_of_funds = self.env['pool.of.funds'].search([('name', '=', self.funds_id.name)])
        funds_vals = []
        vals = {
                    'funds_id': pool_of_funds.id,
                    'amount': self.amount_total,
                    'log_for': 'donation',
                    'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                    'account': self.account_id.user_type_id.name,
                }
        funds_vals.append((0, 0, vals))
        pool_of_funds.write({'log_ids': funds_vals})
        if self.partner_id.membership_type == 'associate' :
            application = self.get_application_record()
        self.action_write_status_item()
        self.state = 'paid'

    @api.multi
    def action_approve(self) :
        self.get_responsible_user()
        self.action_add_booked_item()
        # self.mail_approve_email()
        self.state = 'approve'
    
    @api.multi
    def action_reject(self) :
        self.get_responsible_user()
        self.state = 'reject'

    @api.multi
    def action_cancel(self) :
        self.get_responsible_user()
        self.action_remove_booked_item()
        self.state = 'cancel'
    
    @api.multi
    def action_add_booked_item(self) :
        for rec in self :
            product = rec.product_template_id
            level = rec.unit_level_id
            if not product.sold_out :
                if level.slot > (level.slot_used + level.slot_booked) and not level.is_sold :
                    level.slot_booked += 1
                    level._compute_slot_tablet()
                    product._compute_overall_slot()
                else :
                    raise Warning(_('This level is not available anymore.'))
            else :
                raise Warning(_('This location and level is not available anymore.'))

    @api.multi
    def action_remove_booked_item(self) :
        for rec in self :
            if rec.state == 'approve' :
                level = rec.level_id
                level.slot_booked -= 1

    @api.multi
    def action_write_status_item(self) :
        for rec in self :
            product = rec.product_template_id
            level = rec.unit_level_id
            if not product.sold_out :
                if level.slot >= (level.slot_used + level.slot_booked) and not level.is_sold :
                    level.slot_used += 1
                    level.slot_booked -= 1
                    level._compute_slot_tablet()
                    product._compute_overall_slot()
                else :
                    raise Warning(_('This level is not available anymore.'))
            else :
                raise Warning(_('This location and level is not available anymore.'))

    @api.multi
    def validation_tablet(self) :
        total = len(self.tablet_ids)
        if total < 1 :
            raise Warning(_('You have to insert at least 1 person to proceed this application'))
        elif total > 5 :
            raise Warning(_('You can only have up to 5 person to proceed this application'))

    #payment related
    @api.depends('product_id', 'level_id', 'level_id.price', 'level_id.slot_used', 'state')
    @api.multi
    def _amount_all(self) :
        for rec in self :
            product = rec.product_id
            level = rec.level_id
            state = rec.state
            amount_untaxed = 0.00
            list_taxes = []
            amount_tax= 0.00
            amount_paid = 0.00
            residual = 0.00
            #check whether sold to avoid overlapping 
            if product and level :
                if (product.sold_out or level.is_sold) and state not in ['approve', 'paid'] :
                    raise Warning(_('Product/Level for application %s already sold please to choose other location/leve') % (str(rec.partner_id.name) + ' - ' + str(product.name) + ' - ' + str(level.name)) )
                    
                amount_untaxed = level.price
                list_taxes = [float("%.2f" % (amount_untaxed * (taxes.amount / 100))) for taxes in product.taxes_id if product.taxes_id]
                amount_tax = amount_untaxed + sum(list_taxes)


            rec.amount_untaxed = amount_untaxed
            rec.amount_tax = sum(list_taxes)
            rec.amount_total = amount_tax

    #etc
    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no :
            self.reg_no = self.env['ir.sequence'].next_by_code('tablet.application.seq')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.reg_no or _('New Tablet Application')
            result.append((rec.id, name))

        return result

    #mail related

    @api.multi
    def mail_confirm_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_tablet_confirm') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        member = self.partner_id

        if member.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : member.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

    @api.multi
    def mail_approve_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_tablet_approve') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        member = self.partner_id

        if member.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : member.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()
    
    @api.multi
    def tablet_row(self):
        default_row_in_report = 5
        tablet_row = []
        if self.tablet_ids:
            len_tablet = len(self.tablet_ids)
            for no, rec in enumerate(self.tablet_ids, start=1):
                val = {}
                val['no'] = no
                val['name'] = rec.name + ' / ' + rec.chinese_name
                val['relationship_id'] = rec.relationship_id.name
                val['dob'] = rec.birthdate
                val['dod'] = rec.date_of_death
                tablet_row.append(val)
        return tablet_row

    @api.multi
    def call_tablet_payment_wizard(self):
        vals = {}
        journal_id = self._default_journal() or False
        vals['journal_id'] = journal_id
        return {
            'name': _('Tablet Payment'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.voucher.donation',
            'target': 'new',
            'context': {'default_tablet_id': self.id, 'default_location': self.product_id.name, 'default_amount': self.amount_total, 'default_funds_id': self.funds_id.id, 'default_reg_no': self.reg_no,'default_journal_id':vals['journal_id'],'default_partner_id':self.partner_id.id}
        }

class TabletList(models.Model):
    _name = "tablet.list"
    _description = 'Tablet List'
    _inherit = ['mail.thread']

    reg_no = fields.Char('No.', track_visibility='onchange')
    name = fields.Char('English Name / 英文姓名', track_visibility='onchange', required=True)
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility='onchange') 
    relationship_id = fields.Many2one('partner.relationship',string='Relationship', track_visibility='onchange')
    birthdate = fields.Date(string='Date of Birth', track_visibility='onchange')
    date_of_death = fields.Date(string="Date of Death", track_visibility='onchange')
    status = fields.Selection([('living', 'Living'), ('deceased', 'Deceased')], 'Status', track_visibility='onchange')
    tablet_application_id = fields.Many2one('tablet.application', string='Tablets Application Info', track_visibility='onchange')
    purchase_advance_list_id = fields.Many2one('purchase.advance.list', string="Related Purchase Advance List Form")
    prev_reg_no = fields.Char('Prev No.')
    passed_year = fields.Char('Passed Year', track_visibility='onchange')
    entry_year = fields.Char('Entry Year', track_visibility='onchange')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result
    
    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        reg_no = ''
        for rec in self :
            if rec.tablet_application_id and not rec.tablet_application_id.tablet_applied_name_reg_no :
                reg_no = rec.env['ir.sequence'].next_by_code('tablet.particular.deceased.seq')
                rec.tablet_application_id.tablet_applied_name_reg_no = reg_no
            elif rec.purchase_advance_list_id and rec.purchase_advance_list_id.purchase_advance_type == 'tablet' and not rec.purchase_advance_list_id.tablet_applied_name_reg_no :
                reg_no = rec.env['ir.sequence'].next_by_code('tablet.particular.deceased.seq')
                rec.purchase_advance_list_id.tablet_applied_name_reg_no = reg_no
            else :
                if rec.tablet_application_id :
                    reg_no = rec.tablet_application_id.tablet_applied_name_reg_no
                elif rec.purchase_advance_list_id :
                    reg_no = rec.purchase_advance_list_id.tablet_applied_name_reg_no
                else :
                    raise Warning(_('Something went wrong with the generated sequence number, please contact the technical team to futher investigation.'))

            rec.reg_no = reg_no

    @api.model
    def create(self, vals):
        tablet = super(TabletList, self).create(vals)
        tablet.name_set()
        return tablet