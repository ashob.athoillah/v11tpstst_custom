# -*- coding: utf-8 -*-

import json
from pprint import pprint
from threading import Thread
from datetime import date, datetime

from odoo.tools import float_is_zero, float_compare
from odoo import api, fields, models, _
from odoo.exceptions import except_orm, AccessError, UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp
import math

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    membership_application_id = fields.Many2one('membership.application', string="Related Application", track_visibility='onchange')
    event_application_id = fields.Many2one('participant.registration', string="Related Application", track_visibility='onchange')
    columbarium_niche_application_id = fields.Many2one('niche.application', string="Related Application", track_visibility='onchange')
    tablet_application_id = fields.Many2one('tablet.application', string="Related Application", track_visibility='onchange')
    donation_id = fields.Many2one('donation.donation', string="Donation", track_visibility='onchange')
    invoice_cn_id = fields.Many2one('account.invoice', string='Credit Notes', track_visibility='onchange')
    event_id = fields.Many2one('event.event', string="Related Application", track_visibility='onchange')
    voucher_ids = fields.Many2many('advance.account.voucher', 'account_invoice_advance_voucher_rel', string='Vouchers', compute='_compute_vouchers', store=False)
    membership_renewal_id = fields.Many2one('membership.renewal', string="Related Application", track_visibility='onchange')
    advance_application_id = fields.Many2one('purchase.in.advance', string="Related Application", track_visibility='onchange')

    @api.one
    @api.depends('state', 'payment_move_line_ids', 'residual', 'amount_total')
    def _compute_vouchers(self):
        vline_obj = self.env['advance.account.voucher.line']
        voucher_obj = self.env['advance.account.voucher']

        inv_move_lines = []
        inv_move_line_ids = self.move_id.line_ids
        for inv_move_line in inv_move_line_ids:
            if inv_move_line.id not in inv_move_lines:
                inv_move_lines.append(inv_move_line.id)

        vouchers = []
        voucher_l_ids = vline_obj.search([('move_line_id', 'in', inv_move_lines),('amount', '!=', 0.00)])
        for voucher_l in voucher_l_ids:
            if voucher_l.voucher_id:
                voucher_id = voucher_l.voucher_id.id
                if voucher_id not in vouchers:
                    vouchers.append(voucher_id)

        for voucher in voucher_obj.browse(vouchers):
            if self.residual != 0.00:
                for line_cr in voucher.line_cr_ids:
                    if line_cr.move_line_id.move_id.id == self.move_id.id:
                        reconcile = False
                        if self.residual == line_cr.amount:
                            reconcile = True
                        line_cr.write({'amount_unreconciled': self.residual, 'reconcile': reconcile})


                for line_dr in voucher.line_dr_ids:
                    if line_dr.move_line_id.move_id.id == self.move_id.id:
                        reconcile = False
                        if self.residual == line_dr.amount:
                            reconcile = True
                        line_dr.write({'amount_unreconciled': self.residual, 'reconcile': reconcile})

        self.voucher_ids = voucher_obj.browse(vouchers)

    @api.multi
    def check_e_voucher(self):
        evoucher_id = []

        if self.bok_sub_app_id and self.bok_sub_app_id.e_voucher_id :
            evoucher_id.append(self.bok_sub_app_id.e_voucher_id)

        if evoucher_id :
            for voucher in evoucher_id:
                voucher_id = voucher
                voucher_limit = voucher_id.limit
                voucher_use_count = voucher_id.use_count
                membership_period = None
                if voucher_limit > 0 :
                    if voucher_limit <= voucher_use_count:
                        raise UserError("Can't proceed with payment: voucher usage limit reached !")
                    else :
                        now = datetime.now().date()
                        for invoice in self:
                            if invoice.invoice_seq_number_type == 'membership' :
                                if invoice.membership_application_id :
                                    membership_period = invoice.membership_application_id.membership_period
                                elif invoice.membership_renewal_id :
                                    membership_period = invoice.membership_renewal_id.membership_period

                            invoice.env['electronic.voucher.history'].create({
                                        'name': invoice.number + ' E-Voucher History',
                                        'member_id': invoice.partner_id and invoice.partner_id.id,
                                        'e_voucher_id':voucher_id and voucher_id.id,
                                        'e_voucher_use_date': now,
                                        'membership_period': membership_period,
                                })
                elif voucher_limit == 0:
                    now = datetime.now().date()
                    for invoice in self:
                        if invoice.invoice_seq_number_type == 'membership' :
                            if invoice.membership_application_id :
                                membership_period = invoice.membership_application_id.membership_period
                            elif invoice.membership_renewal_id :
                                membership_period = invoice.membership_renewal_id.membership_period

                        invoice.env['electronic.voucher.history'].create({
                                    'name': invoice.number + ' E-Voucher History',
                                    'member_id': invoice.partner_id and invoice.partner_id.id,
                                    'e_voucher_id':voucher_id and voucher_id.id,
                                    'e_voucher_use_date': now,
                                    'membership_period': membership_period,
                                })

    @api.multi
    def action_invoice_paid(self):
        # self.check_e_voucher()
        # lots of duplicate calls to action_invoice_paid, so we remove those already paid
        to_pay_invoices = self.filtered(lambda inv: inv.state != 'paid')
        if to_pay_invoices.filtered(lambda inv: inv.state != 'open'):
            raise UserError(_('Invoice must be validated in order to set it to register payment.'))
        if to_pay_invoices.filtered(lambda inv: not inv.reconciled):
            raise UserError(_('You cannot pay an invoice which is partially paid. You need to reconcile payment entries first.'))

        if not self.invoice_cn_id :
            if self.membership_application_id :
                membership_app = self.membership_application_id
                if membership_app.state == 'donated' :
                    membership_app.action_paid()

            elif self.event_application_id:
                event_app = self.event_application_id
                if event_app.state == 'vertified' :
                    event_app.action_paid()

            elif self.columbarium_niche_application_id :
                niche_app = self.columbarium_niche_application_id
                if niche_app.state == 'approve' :
                    niche_app.action_paid()

            elif self.tablet_application_id :
                tablet = self.tablet_application_id
                if tablet.state == 'approve' :
                    tablet.action_paid()
                    
            elif self.membership_renewal_id :
                member_renewal = self.membership_renewal_id
                if member_renewal.state == 'confirm' :
                    member_renewal.action_paid()


        return to_pay_invoices.write({'state': 'paid'})
