from odoo import api, models, fields

class TPSTSTAccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    payment_method_id = fields.Many2one('custom.payment.method', string="Payment Method", copy=False)


class TPSTSTAccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    order_ids = fields.One2many('pos.order', 'session_id',  string='Orders')
    payment_method_id = fields.Many2one('custom.payment.method', string="Payment Method", copy=False)
    pos_payment_ref = fields.Boolean(related='payment_method_id.pos_payment_ref', readonly=True)
    payment_ref = fields.Char('Bank Name')
    
    @api.onchange('pos_statement_id')
    def fill_info_from_pos_order(self):
        context = self._context
        pos_order = self.pos_statement_id
        if pos_order:
            self.date = pos_order.date_order
            self.name = '{}:'.format(pos_order.name)
            self.ref = pos_order.session_id.name
            self.amount = pos_order.amount_total
        bank_statement = self.env['account.bank.statement'].browse([context.get('bank_statement')])
        orders = bank_statement.pos_session_id.order_ids
        domain = {}
        if bank_statement:
            domain['pos_statement_id'] = [('id', 'in', orders.ids)]
        return {'domain': domain}