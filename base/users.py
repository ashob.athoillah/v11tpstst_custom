from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil import relativedelta
from lxml import etree

class ResUsers(models.Model):
    _inherit = 'res.users'

    partner_id = fields.Many2one(string="Name / 姓名",track_visibility='onchange')
    internal = fields.Boolean(string='Internal User')
    groups_id = fields.Many2many('res.groups', 'res_groups_users_rel', 'uid', 'gid', string='Groups', default=lambda self : self._default_groups())
    non_en_pass = fields.Char(string="Non-Encryption Password",track_visibility='onchange', export_invisible=True)
    password = fields.Char(string="Password",track_visibility='onchange', export_invisible=True)
    portal_user = fields.Boolean(string='Portal User') 
    digital_signature = fields.Binary(string="Applicant’s Signature / 申请人签名")

    @api.multi
    def _default_groups(self):
        context = self._context
        if context.get('no_share', False):
            default_user = self.env.ref('base.default_user', raise_if_not_found=False)
        else:
            default_user = self.env.ref('base.public_user', raise_if_not_found=False)
        return [(6, 0, (default_user or self.env['res.users']).groups_id.ids)]
    
    @api.multi
    def toggle_active(self):
        for user in self:
            if not user.active and not self.env.user.has_group('base.group_erp_manager'):
                raise UserError("Unable to activate the user, please contact the administrator")
        super(ResUsers, self).toggle_active()

    @api.model
    def create(self, vals):
        context = self._context
        if context.get('no_share', False):
            default_user = self.env.ref('base.default_user', raise_if_not_found=False)
        else:
            default_user = self.env.ref('base.public_user', raise_if_not_found=False)
        for id in (default_user or self.env['res.users']).groups_id.ids:
            vals.update({
                'in_group_' + str(id): True
            })
        return super(ResUsers, self).create(vals)

    # def check_password(self, password):
    #     if password:
    #         for rec in self:
    #             try:
    #                 self.sudo(rec.id).check_credentials(password)
    #                 return False
    #             except:
    #                 return True

    @api.multi
    def mail_create_user(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('auth_signup', 'mail_template_user_signup_account_created') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : self.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

class TigernixAdminSupport(models.Model):    
    _name = 'tigernix.admin.support'
    
    name   = fields.Char('Name',size=64)
    email  = fields.Char('Email',size=128)
    company_id = fields.Many2one('res.company', 'Company', required=False)