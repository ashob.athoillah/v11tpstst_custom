# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from lxml import etree
from itertools import chain
from datetime import datetime


class ResCompany(models.Model):
    _name = 'res.company'
    _inherit = ['res.company', 'mail.thread', 'mail.activity.mixin']

    street = fields.Char(track_visibility='onchange')
    street2 = fields.Char(string="Building Name", track_visibility='onchange')
    fax = fields.Char(string="Fax", track_visibility='onchange')
    max_users = fields.Char(string="Max User", track_visibility='onchange')
    gst_no = fields.Char(string="GST No.", track_visibility='onchange')
    uen_no = fields.Char(string="UEN No.", track_visibility='onchange')
    charity_no = fields.Char(string="Chairty No.", track_visibility='onchange')
    chinese_company_name = fields.Char(string="Chairty No.", track_visibility='onchange')

    donation_journal_id = fields.Many2one('account.journal', string="Donation Journal",
                                          help="Default journal for donation transactions", track_visibility='onchange')
    donation_receivable_account_id = fields.Many2one('account.account', string="Donation Receivable Account",
                                                     help="Account for billing receipts for monthly donations using the giro / mastercard payment method",
                                                     track_visibility='onchange')
    donation_income_account_id = fields.Many2one('account.account', string="Donation Income Account",
                                                 help="Account for the source of the donation income",
                                                 track_visibility='onchange')
    membership_receivable_account_id = fields.Many2one('account.account', string="Membership Receivable Account",
                                                     help="Account for billing receipts for monthly donations using the giro / mastercard payment method",
                                                     track_visibility='onchange')
    membership_income_account_id = fields.Many2one('account.account', string="Membership Income Account",
                                                 help="Account for the source of the donation income",
                                                 track_visibility='onchange')
    niche_receivable_account_id = fields.Many2one('account.account', string="Niche Receivable Account",
                                                     help="Account for billing receipts for monthly donations using the giro / mastercard payment method",
                                                     track_visibility='onchange')
    niche_income_account_id = fields.Many2one('account.account', string="Niche Income Account",
                                                     help="Account for billing receipts for monthly donations using the giro / mastercard payment method",
                                                     track_visibility='onchange')
    tablet_income_account_id = fields.Many2one('account.account', string="Tablet Income Account",
                                                 help="Account for the source of the donation income",
                                                 track_visibility='onchange')
    tablet_receivable_account_id = fields.Many2one('account.account', string="Tablet Income Account",
                                                 help="Account for billing receipts for monthly donations using the giro / mastercard payment method",
                                                 track_visibility='onchange')
    event_receivable_account_id = fields.Many2one('account.account', string="Event Receivable Account",
                                                     help="Account for billing receipts for monthly donations using the giro / mastercard payment method",
                                                     track_visibility='onchange')
    event_income_account_id = fields.Many2one('account.account', string="Event Income Account",
                                                 help="Account for the source of the donation income",
                                                 track_visibility='onchange')
    purchase_adv_income_account_id = fields.Many2one('account.account', string="Purchase in Advance Tablet Income Account",
                                                 help="Account for the source of the donation income",
                                                 track_visibility='onchange')
    purchase_adv_receivable_account_id = fields.Many2one('account.account', string="Purchase in Advance Income Account",
                                                 help="Account for the source of the donation income",
                                                 track_visibility='onchange')

    membership_journal_id = fields.Many2one('account.journal', string="Membership Journal", track_visibility='onchange')
    event_journal_id = fields.Many2one('account.journal', string="Event Journal", track_visibility='onchange')
    niche_journal_id = fields.Many2one('account.journal', string="Niche Journal", track_visibility='onchange')
    tablet_journal_id = fields.Many2one('account.journal', string="Tablet Journal", track_visibility='onchange')
   
    membership_email = fields.Char('Membership Department Email', help="Determine who's responsible for recieve email membership", track_visibility='onchange')
    event_email = fields.Char('Event Department Email', help="Determine who's responsible for recieve email event", track_visibility='onchange')
    donation_email = fields.Char('Donation Department Email', help="Determine who's responsible for recieve email donation", track_visibility='onchange')
    term_and_conditions = fields.Text('Term & Conditions', track_visibility='onchange')


    default_treasurer_id = fields.Many2one('res.users', 'Treasurer',required=True)
    default_auditor_id = fields.Many2one('res.users', 'Auditor',required=True)
    default_niche_fund_id = fields.Many2one('pool.of.funds', 'Niche',required=True, domain=[('state','=','confirm')])
    default_membership_fund_id = fields.Many2one('pool.of.funds', 'Member',required=True, domain=[('state','=','confirm')])
    default_tablet_fund_id = fields.Many2one('pool.of.funds', 'Tablet',required=True, domain=[('state','=','confirm')])
    default_event_fund_id = fields.Many2one('pool.of.funds', 'Event',required=True, domain=[('state','=','confirm')])
    default_trunk_fund_id = fields.Many2one('pool.of.funds', 'Trunk',required=True, domain=[('state','=','confirm')])
    default_charity_fund_id = fields.Many2one('pool.of.funds', 'Charity / Coffin',required=True, domain=[('state','=','confirm')])
    default_medical_fund_id = fields.Many2one('pool.of.funds', 'Medical',required=True, domain=[('state','=','confirm')])
    default_festival_fund_id = fields.Many2one('pool.of.funds', 'Festival',required=True, domain=[('state','=','confirm')])
    default_other_fund_id = fields.Many2one('pool.of.funds', 'Others',required=True, domain=[('state','=','confirm')])

    default_associate_membership_term_id = fields.Many2one('term.and.condition', string="Associate Membership", track_visibility='onchange')
    default_membership_term_id = fields.Many2one('term.and.condition', string="Membership", track_visibility='onchange')
    default_event_term_id = fields.Many2one('term.and.condition', string="Event", track_visibility='onchange')
    default_donation_term_id = fields.Many2one('term.and.condition', string="Donation", track_visibility='onchange')
    default_tablet_term_id = fields.Many2one('term.and.condition', string="Tablet", track_visibility='onchange')
    default_niche_term_id = fields.Many2one('term.and.condition', string="Niche", track_visibility='onchange')
    default_tablet_in_advance_term_id = fields.Many2one('term.and.condition', string="Tablet in Advance", track_visibility='onchange')
    default_niche_in_advance_term_id = fields.Many2one('term.and.condition', string="Niche in Advance", track_visibility='onchange')
    daily_cashier_payment_method_ids = fields.Many2many('account.journal', 'company_daily_cashier_payment_method_rel','company_id','payment_method_id',string='Daily Cashier Payment Methods')

class TermAndConditions(models.Model):
    _name="term.and.condition"
    _description = "Term & Conditions"

    name = fields.Char('Name')
    description = fields.Html('Term & Conditions', sanitize_attributes=False)

    