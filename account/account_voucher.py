# -*- coding: utf-8 -*-

import pprint
import logging

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import OrderedSet, pycompat
from itertools import chain
from datetime import datetime
from dateutil import relativedelta
from lxml import etree
from odoo.models import fix_import_export_id_paths

STATE = [
    ('draft', 'Draft'),
    ('cancel', 'Cancelled'),
    ('proforma', 'Waiting for Validation'),
    ('posted', 'Posted')
]

DONATION_STATE = [
    ('draft', 'Draft'),
    ('confirm', 'Confirm'),
    ('success', 'Sucessful'),
    ('post', 'Posted'),
    ('cancel', 'Cancel'),
]

def voucher_state(*args):
    list_1 = []
    in_list_1 = set(list_1)
    for x in args:
        in_list_2 = set(x)
        not_in_list_2 = in_list_2 - in_list_1
        list_1 = list_1 + list(not_in_list_2)
        in_list_1 = set(list_1)
    return list_1


VOUCHER_STATE = voucher_state(STATE, DONATION_STATE)

class AccountVoucher(models.Model):
    _inherit = 'account.voucher'
    _order = 'number desc'
    _sql_constraints = [
        ('donation_transaction_number_unique', 'unique(donation_transaction_number)', 'The donation transaction no. is already exist.'),
    ]

    @api.model
    def _default_journal(self):
        custom_voucher_type = self._context.get('custom_voucher_type', False)
        voucher_type = self._context.get('voucher_type', 'sale')
        if custom_voucher_type in ['donation']:
            voucher_type = 'general'
        company_id = self._context.get('company_id', self.env.user.company_id.id)
        domain = [
            ('type', '=', voucher_type),
            ('company_id', '=', company_id),
        ]
        journal_id = False
        if custom_voucher_type in ['donation']:
            journal_id = self.env.user.company_id.donation_journal_id
        if not journal_id or voucher_type != 'general':
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    # DEFAULT FIELDS
    name = fields.Char(string="Reference",track_visibility='onchange')
    number = fields.Char(readonly=False,track_visibility='onchange')
    partner_id = fields.Many2one('res.partner', string="Name / 姓名",track_visibility='onchange')
    state = fields.Selection(STATE, 'Voucher Status', readonly=False, copy=False, default='draft', track_visibility=None)
    paid = fields.Boolean(compute='_check_paid', help="The Voucher has been totally paid.", store=False)
    move_line_ids = fields.One2many('account.move.line', 'move_id', string="Journal Items", related="move_id.line_ids", readonly=True)
    date = fields.Date(string="Date",track_visibility='onchange')
    pay_now = fields.Selection([('pay_now', 'Pay Directly'), ('pay_later','Pay Later')], string="Payment Type",track_visibility='onchange')

    # CUSTOM FIELDS
    voucher_state = fields.Selection(VOUCHER_STATE, 'Status', readonly=True, track_visibility='onchange', copy=False,
                                     default='draft', compute="_compute_voucher_state")

    custom_voucher_type = fields.Selection([
        ('donation', 'Donation'),
    ], string="Custom Voucher Type",track_visibility='onchange')
    
    paid_date = fields.Date(string="Paid Date")
    advance_voucher_id = fields.Many2one('advance.account.voucher', string="Related Payment", readonly=1, copy=False)

    # DONATION FIELDS
    ext_transaction_number = fields.Char('External Transaction Number')
    donation_transaction_number = fields.Char(string="Donation Transaction No.", related="number", readonly=True, store=True)
    donation_state = fields.Selection(DONATION_STATE, 'Donation Status', copy=False, default='draft')
    type_donation = fields.Boolean(string='Type Donation', invisible=1)
    type_other_donation = fields.Boolean(string='Type Non Donation', invisible=1)
    donation_id = fields.Many2one('donation.donation', string="Donation", readonly=1)
    event_id = fields.Many2one('participant.registration', string="Event", readonly=1)
    purchase_in_advance_id = fields.Many2one('purchase.in.advance', string="Donation", readonly=1)
    tablet_id = fields.Many2one('tablet.application', string="Tablet", readonly=1)
    niche_id = fields.Many2one('niche.application', string="Niche", readonly=1)
    membership_id = fields.Many2one('membership.application', string="Membership", readonly=1)
    membership_renewal_id = fields.Many2one('membership.renewal', string="Membership", readonly=1)
    donation_interval_id = fields.Many2one('donation.interval', related="donation_id.donation_interval_id", string="Donation Interval", readonly=1, track_visibility='onchange')
    interval = fields.Integer(string="Frequency Interval number", related="donation_id.interval",
                                      readonly=1)
    
    donation_amount = fields.Monetary(string="Donation Amount", compute="_compute_donation_transaction_amount", store=True)
    total_donation_amount = fields.Monetary(string="Received Amount", compute="_compute_donation_transaction_amount", store=True)
    fee_amount = fields.Monetary(string="Fee Amount", compute="_compute_donation_transaction_amount", store=True)

    campaign_id = fields.Many2one('donation.campaign', related='donation_id.campaign_id', string="Campaign", readonly=1, store=True)

    journal_id = fields.Many2one('account.journal', 'Journal',
                                 required=True, readonly=True, states={'draft': [('readonly', False)]},
                                 default=_default_journal, track_visibility='onchange')
    payment_journal_id = fields.Many2one('account.journal', string="Payment Journal",track_visibility='onchange')
    donation_payment_method_ids = fields.Many2many('custom.payment.method', string="Donation Payment Method", readonly=True)
    payment_method_id = fields.Many2one('account.journal', string="Payment Method",
                                        readonly=1, track_visibility='onchange')
    payment_method_type = fields.Selection([], string="Payment Method Type", related="payment_method_id.type", 
                                           readonly=1, store=True)

    cheque_no = fields.Char(string="Cheque No.", readonly=True, track_visibility='onchange')
    paynow_no = fields.Char(string="PayNow No.", readonly=True, track_visibility='onchange')

    active = fields.Boolean(string="Active", related="partner_id.active", store=True,track_visibility='onchange', readonly=True)

    @api.multi
    def write(self, vals):
        for rec in self:
            if vals.get('custom_voucher_type', rec.custom_voucher_type) in ['donation']:
                self.env['lock.period'].action_check_date(date=vals.get('date', rec.date), name=rec.number)
        return super(AccountVoucher, self).write(vals)

    @api.multi
    @api.depends('number', 'custom_voucher_type')
    def name_get(self):
        ctx = self._context
        res = []
        for item in self:
            if item.number:
                name = item.number
            elif item.custom_voucher_type == 'donation':
                name = "New Donation"
            else:
                name = "New Voucher"

            res.append((item.id, name))
        return res

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = [('donation_transaction_number', operator, name)]
        res = self.search(domain + args, limit=limit)
        return res.name_get()

    @api.multi
    def unlink(self):
        for voucher in self:
            if voucher.custom_voucher_type in ['donation']:
                self.env['lock.period'].action_check_date(date=voucher.date, name=voucher.number)
            if voucher.custom_voucher_type == 'donation':
                if voucher.donation_transaction_number:
                    raise ValidationError("Unable to delete the donation transaction has number.")

        return super(AccountVoucher, self).unlink()

    @staticmethod
    def strptime(date):
        date = date and datetime.strptime(str(date), "%Y-%m-%d") or False
        return date

    @api.model
    def default_get(self, fields_list):
        res = super(AccountVoucher, self).default_get(fields_list)
        voucher_type = self._context.get('voucher_type', False)
        custom_voucher_type = self._context.get('custom_voucher_type', voucher_type)
        res['custom_voucher_type'] = custom_voucher_type

        return res


    @api.depends('amount', 'donation_state', 'line_ids', 'line_ids.price_subtotal', 'line_ids.type', 'donation_id.state')
    @api.multi
    def _compute_donation_transaction_amount(self):
        total_donation = []
        for rec in self:
            total_donation = [donation.price_subtotal if donation.type == 'donation' else -abs(donation.price_subtotal) for donation in rec.line_ids]

            rec.donation_amount = sum([amount for amount in total_donation if amount > 0])
            rec.fee_amount = abs(sum([amount for amount in total_donation if amount < 0]))
            rec.total_donation_amount = sum(total_donation)

    @api.one
    @api.depends('move_id.line_ids.reconciled', 'move_id.line_ids.account_id.internal_type')
    def _check_paid(self):
        if self.pay_now != 'pay_now':
            paid = any([((line.account_id.internal_type, 'in', ('receivable', 'payable')) and line.reconciled) for line in
                 self.move_id.line_ids])
        else:
            paid = (self.donation_state in ['success']) and True or False

        self.paid = paid
        if paid:
            self.paid_date = datetime.today()

    @api.depends('state', 'donation_state')
    def _compute_voucher_state(self):
        for rec in self:
            if rec.custom_voucher_type == 'donation':
                rec.voucher_state = rec.donation_state
            else:
                rec.voucher_state = rec.state

    @api.depends('company_id', 'pay_now', 'account_id', 'payment_method_id')
    def _compute_payment_journal_id(self):
        context = self._context
        if context.get('create_move_line', True):
            for voucher in self:
                if voucher.pay_now != 'pay_now':
                    continue
                domain = [
                    ('type', 'in', ('bank', 'cash')),
                    ('company_id', '=', voucher.company_id.id),
                ]
                journal_id = voucher.payment_method_id or False
                default_debit_account = journal_id and journal_id.default_debit_account_id or False
                default_debit_account_id = default_debit_account and default_debit_account.id or False
                default_credit_account = journal_id and journal_id.default_credit_account_id or False
                default_credit_account_id = default_credit_account and default_credit_account.id or False
                if voucher.account_id and voucher.account_id.internal_type == 'liquidity':
                    field = 'default_debit_account_id' if voucher.voucher_type == 'sale' else 'default_credit_account_id'
                    domain.append((field, '=', voucher.account_id.id))
                    if voucher.voucher_type == 'sale':
                        journal_account_id = default_debit_account_id
                    else:
                        journal_account_id = default_credit_account_id
                    if journal_account_id != voucher.account_id.id:
                        journal_id = self.env['account.journal'].search(domain, limit=1)

                voucher.payment_journal_id = journal_id

    @api.onchange('payment_method_id')
    def _inverse_payment_journal_id(self):
        for voucher in self:
            if voucher.pay_now != 'pay_now':
                continue
            if voucher.voucher_type == 'sale':
                voucher.account_id = voucher.payment_journal_id.default_debit_account_id
            else:
                voucher.account_id = voucher.payment_journal_id.default_credit_account_id

    @api.model
    def create(self, vals):
        if vals.get('custom_voucher_type', False) in ['donation']:
            self.env['lock.period'].action_check_date(date=vals.get('date', False))

        res = super(AccountVoucher, self).create(vals)
        return res

    @api.multi
    def proforma_voucher(self):
        context = self._context
        # if context.get('create_move_line', True):
        #     self.action_move_line_create()
        for rec in self:
            if not rec.account_date:
                rec.account_date = rec.date
            rec.number = self.env['ir.sequence'].next_by_code('donation.transaction')
            rec._compute_donation_transaction_amount()
            if rec.pay_now != 'pay_now':
                rec.donation_state = 'confirm'
            else:
                rec.success_donation()
                rec.donation_state = 'success'

    @api.multi
    def first_move_line_get(self, move_id, company_currency, current_currency):
        debit = credit = 0.0
        if self.voucher_type == 'purchase':
            credit = self._convert_amount(self.amount)
        elif self.voucher_type == 'sale':
            debit = self._convert_amount(self.amount)
        if debit < 0.0: debit = 0.0
        if credit < 0.0: credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        # set the first line of the voucher
        move_line = {
            'voucher_id': self.id,
            'name': self.name or '/',
            'debit': debit,
            'credit': credit,
            'account_id': self.account_id.id,
            'move_id': move_id,
            'journal_id': self.journal_id.id,
            'partner_id': self.partner_id.commercial_partner_id.id,
            'currency_id': company_currency != current_currency and current_currency or False,
            'amount_currency': (sign * abs(self.amount)  # amount < 0 for refunds
                                if company_currency != current_currency else 0.0),
            'date': self.account_date,
            'date_maturity': self.date_due,
            'payment_id': self._context.get('payment_id'),
            'payment_method_id': self.payment_method_id and self.payment_method_id.id or False,
        }
        return move_line

    @api.multi
    def action_move_line_create(self):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        '''
        for voucher in self:
            if voucher.custom_voucher_type not in ['donation']:
                local_context = dict(self._context, force_company=voucher.journal_id.company_id.id)
                if voucher.move_id:
                    continue
                company_currency = voucher.journal_id.company_id.currency_id.id
                current_currency = voucher.currency_id.id or company_currency
                # we select the context to use accordingly if it's a multicurrency case or not
                # But for the operations made by _convert_amount, we always need to give the date in the context
                ctx = local_context.copy()
                ctx['date'] = voucher.account_date
                ctx['check_move_validity'] = False
                # Create a payment to allow the reconciliation when pay_now = 'pay_now'.
                if voucher.pay_now == 'pay_now' and voucher.amount > 0:
                    ctx['payment_id'] = self.env['account.payment'].create(self.voucher_pay_now_payment_create()).id
                # Create the account move record.
                move = self.env['account.move'].create(voucher.account_move_get())
                # Get the name of the account_move just created
                # Create the first line of the voucher
                move_line = self.env['account.move.line'].with_context(ctx).create(
                    voucher.with_context(ctx).first_move_line_get(move.id, company_currency, current_currency))
                line_total = move_line.debit - move_line.credit
                if voucher.voucher_type == 'sale':
                    line_total = line_total - voucher._convert_amount(voucher.tax_amount)
                elif voucher.voucher_type == 'purchase':
                    line_total = line_total + voucher._convert_amount(voucher.tax_amount)
                # Create one move line per voucher line where amount is not 0.0
                line_total = voucher.with_context(ctx).voucher_move_line_create(line_total, move.id, company_currency,
                                                                                current_currency)

                # Add tax correction to move line if any tax correction specified
                if voucher.tax_correction != 0.0:
                    tax_move_line = self.env['account.move.line'].search(
                        [('move_id', '=', move.id), ('tax_line_id', '!=', False)], limit=1)
                    if len(tax_move_line):
                        tax_move_line.write(
                            {'debit': tax_move_line.debit + voucher.tax_correction if tax_move_line.debit > 0 else 0,
                             'credit': tax_move_line.credit + voucher.tax_correction if tax_move_line.credit > 0 else 0})

                values = {
                    'move_id': move.id,
                    'state': 'posted',
                    'number': move.name
                }

                # We post the voucher.
                voucher.write(values)
                move.post()
        return True

    @api.multi
    def success_donation(self):
        for rec in self:
            if rec.donation_state not in ['success', 'cancel'] :
                rec.donation_state = 'success'

    def unsuccess_donation(self):
        for rec in self:
            rec.donation_state = 'unsuccess'

    def cancel_voucher(self):
        # user_id = self.env.user
        for rec in self:
            if rec.custom_voucher_type == 'donation' and rec.donation_state != 'draft':
                raise ValidationError("Unable to cancel donation transaction. Donation transaction are not in state draft.")

        return super(AccountVoucher, self).cancel_voucher()

    def action_cancel_draft(self):
        res = super(AccountVoucher, self).action_cancel_draft()
        for rec in self:
            if rec.state in ['draft']:
                rec.donation_state = 'draft'

        return res

    @api.multi
    def account_move_get(self):
        if self.number:
            name = self.number
        elif self.custom_voucher_type == 'donation':
            name = self.env['ir.sequence'].next_by_code('donation.transaction')
        elif self.journal_id.sequence_id:
            if not self.journal_id.sequence_id.active:
                raise UserError(_('Please activate the sequence of selected journal !'))
            name = self.journal_id.sequence_id.with_context(ir_sequence_date=self.date).next_by_id()
        else:
            raise UserError(_('Please define a sequence on the journal.'))

        move = {
            'name': name,
            'journal_id': self.journal_id.id,
            'narration': self.narration,
            'date': self.account_date,
            'ref': self.reference,
        }
        return move

    def action_success(self):
        for rec in self:
            rec.success_donation()

    def donation_advance_writeoff(self):
        for rec in self:
            rec.unsuccess_donation()

    def action_send_success_donation_email(self):
        self.ensure_one()
        ctx = self._context.copy()
        company_id = self.env.user.company_id
        # email_template_id = company_id.thank_you_donate_template
        # compose_view_id = self.env.ref('mail.email_compose_message_wizard_form')
        # if not compose_view_id:
        #     raise ValidationError(_("Unable to open email compose wizard"))
        # if not email_template_id:
        #     raise ValidationError(_("Please configure failed duduction email template in company configuration"))

        # ctx.update({
        #     'default_res_id': self.id,
        #     'default_model': 'account.voucher',
        #     'default_use_template': True,
        #     'default_template_id': email_template_id and email_template_id.id,
        #     'default_composition_mode': 'comment',
        # })

        # return {
        #     'name': "Send Thank You Letter",
        #     'type': 'ir.actions.act_window',
        #     'view_type': 'form',
        #     'view_mode': 'form',
        #     'res_model': 'mail.compose.message',
        #     'views': [(compose_view_id.id, 'form')],
        #     'view_id': compose_view_id.id,
        #     'target': 'new',
        #     'context': ctx,
        # }

    @api.multi
    def amount_to_text(self):
        self.ensure_one()
        # 200 = Administrative fee
        # 259 = GST at 7%
#        invoice_subtotal = self.invoice_line_ids.price_subtotal + 200 + 259
        amount_due = self.amount
        amount_i, amount_d = divmod(amount_due, 1)
        words = self.currency_id.with_context(lang=self.partner_id.lang or 'es_ES').amount_to_text(amount_i)
        invoice_words = '%(words)s' % dict(words=words)
        return invoice_words

class AccountVoucherLine(models.Model):
    _inherit = 'account.voucher.line'

    type = fields.Selection([
        ('donation', 'Donation'),
        ('transaction_fee', 'Transaction Fee'),
    ], string="Type")