from re import S
import time
from odoo.addons import decimal_precision as dp
from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from itertools import chain
from odoo.addons.v11_tpstst_custom.membership import membership_application as membership

class ProductPriceList(models.Model):
    _name = "prod.price.list"
    _order = "effective_date asc"

    name = fields.Float("Price")
    effective_date = fields.Date("Effective Date")
    product_id = fields.Many2one('product.product', 'Product')


class LevelPosition(models.Model) :
    _name= "level.position"
    _description = "Level Position"

    name = fields.Char('Name')