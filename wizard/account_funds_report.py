# -*- coding: utf-8 -*-
# Part of Tigernix. See LICENSE file for full copyright and licensing details.

from hmac import compare_digest
import time

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
import dateutil.relativedelta
import datetime
from datetime import datetime
import calendar
import pytz
from pytz import timezone
import xlwt 
from xlwt import Workbook , easyxf, Formula
from xlsxwriter.utility import xl_range
import base64
import io
import pytz
from PIL import Image

class SchoolFeesReportFile(models.TransientModel):
    _name = "account.funds.report.file"

    file = fields.Binary(string="Excel File", readonly=True)
    name = fields.Char(string="File Name")

class SchoolFeesReport(models.TransientModel):
    _name = "account.funds.report"

    funds_ids = fields.Many2many('pool.of.funds', 'account_funds_id', 'funds_report_id', 'account_id',  string='Funds')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    compared_year = fields.Selection([(y, str(y)) for y in range((datetime.now().year - 5), datetime.now().year )], 'Year')

    @api.one
    def get_date_format(self, date):
        if date:
            new_date = datetime.strptime(date, "%Y-%m-%d").strftime("%d-%m-%Y")
        else:
            new_date = ''
        return new_date

    @api.multi
    def print_report(self):
        funds_obj = self.env['pool.of.funds']
        funds_log_obj = self.env['pool.of.funds.log']
        company_id = self.env.user.company_id
        config = self.env['res.company'].search([('id','=',company_id.id)])
        
        if self.start_date > self.end_date:
            raise UserError(_('Start Date cannot higher than End Date!'))
        
        workbook = xlwt.Workbook()
        
        # set styles to be used in cells
        xlwt.add_palette_colour("custom_colour", 0x21)
        workbook.set_colour_RGB(0x21, 230, 230, 230)
        header = xlwt.easyxf('font: bold 1, height 200, color black, name Arial; align: vert centre, horz centre, wrap yes;\
                    borders: top_color black, bottom_color black, left_color black, right_color black,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')
                    
        header_funds = xlwt.easyxf('font: bold 1, height 200, color black, name Arial; align: vert centre, horz centre, wrap yes;\
                    borders: top_color black, bottom_color black, left_color black, right_color black,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color custom_colour;')
                    
        header2 = xlwt.easyxf('font: bold 1, height 200, color black, name Arial; align: vert centre, horz centre, wrap yes;\
                    borders: top_color black, bottom_color black, left_color black, right_color black,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color custom_colour;')
                    
        value_text = xlwt.easyxf('font: bold 0, height 200, color black, name Arial; align: vert centre, horz left, wrap yes;\
                    borders: top_color gray25, bottom_color gray25, left_color gray25, right_color gray25,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')
                    
        value_change = xlwt.easyxf('font: bold 0, height 200, color black, name Arial; align: vert centre, horz right, wrap yes;\
                    borders: top_color gray25, bottom_color gray25, left_color gray25, right_color gray25,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')

        value_change_subtotal = xlwt.easyxf('font: bold 1, height 200, color black, name Arial; align: vert centre, horz right, wrap yes;\
                    borders: top_color black, bottom_color black, left_color black, right_color black,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')
                    
        value = xlwt.easyxf('font: bold 0, height 200, color black, name Arial; align: vert centre, horz right, wrap yes;\
                    borders: top_color gray25, bottom_color gray25, left_color gray25, right_color gray25,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')
        value.num_format_str = '$#,##0.00'
                    
        value_subtotal = xlwt.easyxf('font: bold 1, height 200, color black, name Arial; align: vert centre, horz right, wrap yes;\
                    borders: top_color black, bottom_color black, left_color black, right_color black,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')
        value_subtotal.num_format_str = '$#,##0.00'
                    
        value_total_left = xlwt.easyxf('font: bold 0, height 200, color black, name Arial; align: vert centre, horz right, wrap yes;\
                    borders: top_color gray25, bottom_color gray25, left_color gray25, right_color black,\
                              top thin, bottom thin, left thin, right thin;\
                    pattern: pattern solid, fore_color white;')
        value_total_left.num_format_str = '$#,##0.00'
        
        funds_ids = self.funds_ids
        if not funds_ids:
            funds_ids = funds_obj.search([])
        
        sheet_name = 'Funds Report'
        
        worksheet = workbook.add_sheet(sheet_name)
        
        worksheet.row(0).height = 500
        worksheet.row(1).height = 500
        worksheet.write_merge(0, 0, 0, 0, company_id.name or '', header)
        
        report_range_str = ''
        start_date_month = datetime.strptime(self.start_date, "%Y-%m-%d").strftime("%B %Y")
        end_date_month = datetime.strptime(self.end_date, "%Y-%m-%d").strftime("%B %Y")
        report_range_str = 'Funds Report from ' + start_date_month + ' to ' + end_date_month
        worksheet.write_merge(1, 1, 0, 0, report_range_str, header)
        
        worksheet.set_panes_frozen(True)
        worksheet.set_horz_split_pos(3)
        worksheet.set_vert_split_pos(1)
        worksheet.row(2).height = 1200
        
        write_unres = 1
        write_res = 1
        column_no = 0
        worksheet.write(2, column_no, 'Particulars', header)
        worksheet.col(column_no).width = 13500
        column_no += 1
        fund_identify = {}
        first_column = 0
        
        for fund in funds_ids:
            if fund.type_of_fund == "restricted":
                write_res = 0

                if first_column == 0:
                    first_column = column_no

                worksheet.write(2, column_no, fund.name, header_funds)
                fund_identify[fund.id] = column_no
                worksheet.col(column_no).width = 5500
                column_no += 1

        if write_res == 0:
            worksheet.write_merge(0, 1, first_column, column_no-1, "Restricted", header)
        if write_res == 1:
            column_no = column_no - 1
            
        first_column = 0
        worksheet.col(column_no).width = 500
        column_no += 1

        for fund in funds_ids:
            if fund.type_of_fund == "unrestricted":
                write_unres = 0

                if first_column == 0:
                    first_column = column_no

                worksheet.write(2, column_no, fund.name, header_funds)
                fund_identify[fund.id] = column_no
                worksheet.col(column_no).width = 5500
                column_no += 1

        if write_unres == 0:
            worksheet.write_merge(0, 1, first_column, column_no-1, "Unrestricted", header)
        
        if write_unres == 1:
            column_no = column_no - 1

        worksheet.col(column_no).width = 500
        worksheet.write(2, column_no, '', value_text)
        column_no += 1
        worksheet.col(column_no).width = 5500
        worksheet.write(2, column_no, 'Total Now', header_funds)
        fund_identify['total'] = column_no
        column_no += 1
        receipt_row = 3

        #Opening Balance
        # worksheet.write(receipt_row, 0, '', header)
        worksheet.write(receipt_row, 0, 'OPENING BALANCE', header)
        total_opening_bal = 0
        opening_bal_vals = {}
        
        for fund in funds_ids:
            query = """
            SELECT sum(amount) FROM pool_of_funds_log
            WHERE funds_id = %s
            AND transaction_date < '%s';
            """ %(fund.id, self.start_date)
            self._cr.execute(query)
            query_res = self._cr.dictfetchall()
            opening_bal = 0

            for bal in query_res:
                if not bal['sum']:
                    opening_bal += 0
                else:
                    opening_bal += bal['sum']
                    
            funds_column = fund_identify[fund.id]
            worksheet.write(receipt_row, funds_column, opening_bal, value_subtotal)
            opening_bal_vals[fund.id] = opening_bal
            total_opening_bal += opening_bal

        worksheet.write(receipt_row, fund_identify['total'], total_opening_bal, value_subtotal)
        receipt_row += 1
        
        #Receipts
        # worksheet.write(receipt_row, 0, '', header)
        worksheet.write(receipt_row, 0, 'INCOME', header2)
        worksheet.write(receipt_row, fund_identify['total'], '', value_total_left)
        receipt_row += 1
        
        if len(funds_ids) == 1:
            query = """
            SELECT id FROM pool_of_funds_log
            WHERE funds_id in (%s)
            AND transaction_date BETWEEN '%s' AND '%s'
            AND amount > 0;
            """ %(funds_ids[0].id, self.start_date, self.end_date)
        else:
            query = """
            SELECT id FROM pool_of_funds_log
            WHERE funds_id in %s
            AND transaction_date BETWEEN '%s' AND '%s'
            AND amount > 0;
            """ %(funds_ids._ids, self.start_date, self.end_date)

        self._cr.execute(query)
        query_res = self._cr.dictfetchall()
        log_list = []

        for query_log in query_res:
            log_list.append(query_log['id'])

        funds_log_ids = funds_log_obj.browse(log_list)
        receipt_subtotal = {}
        account_list = []

        # listing the used account
        for log in funds_log_ids:
            duplicate = 0
            for list in account_list:
                if list == log.account_id.name:
                    duplicate = 1
            if duplicate == 0:
                account_list.append(log.account_id.name)
        
        #income
        account_check = 0
        list_total_income = []
        all_account_obj = self.env['account.account'].search([])
        list_amount_compare = []
        list_total_compare = []
        
        for all_account in all_account_obj:
            compare = 0
            total_compare = 0
            if all_account.user_type_id.name == 'Income':
                for account in account_list:
                    if account == all_account.name:
                        account_check = 1
                        funds_amount = 0
                        amount = 0
                        old_funds_column = ""
                        
                        if account != False :
                            for log in funds_log_ids:
                                funds_id = log.funds_id
                                if account == log.account_id.name :
                                    subtotal = 0
                                    partner = '-'
                                    date = '-'
                                    amount = amount + log.amount
                                    funds_amount = funds_amount + log.amount
                                    
                                    if log.move_id:
                                        partner = log.move_id.partner_id.name
                                        date = log.move_id.date
                                    elif log.voucher_id:
                                        partner = log.voucher_id.partner_id.name
                                        date = log.voucher_id.date
                                    else:
                                        partner = log.account
                                        date = log.transaction_date
                                        if not date:
                                            date = log.create_date

                                    funds_column = fund_identify[funds_id.id]

                                    if funds_amount != 0 and old_funds_column != "" and old_funds_column != funds_column:
                                        worksheet.write(receipt_row, old_funds_column, funds_amount, value)
                                        funds_amount = 0

                                    old_funds_column = funds_column
                                
                            # worksheet.write(receipt_row, 0, self.get_date_format(date), value_text)
                            worksheet.write(receipt_row, 0, account, value_text)
                            funds_column = fund_identify[funds_id.id]

                            if funds_amount != 0:
                                worksheet.write(receipt_row, funds_column, funds_amount, value)

                            worksheet.write(receipt_row, fund_identify['total'], amount, value_total_left)
                            list_total_income.append(amount)
                            receipt_row += 1
                                
                            if receipt_subtotal.get(funds_id.id):
                                subtotal = amount + float(receipt_subtotal[funds_id.id])
                            else:
                                subtotal = amount
                            receipt_subtotal[funds_id.id] = subtotal

                            compare += log.amount
                            total_compare += log.amount 

                if account_check == 0:
                    worksheet.write(receipt_row, 0, all_account.name, value_text)
                    worksheet.write(receipt_row, fund_identify['total'], 0, value_total_left)
                    list_total_income.append(0)
                    receipt_row += 1

                account_check = 0
                list_amount_compare.append(compare)
                list_total_compare.append(total_compare)
                
        # worksheet.write(receipt_row, 0, '', header)
        worksheet.write(receipt_row, 0, 'SUBTOTAL', header2)
        total_receipt = 0

        for funds in funds_ids:
            if receipt_subtotal.get(funds.id):
                subtotal = receipt_subtotal[funds.id]
                total_receipt += subtotal
            else:
                subtotal =  0
                total_receipt += 0

            funds_column = fund_identify[funds.id]
            worksheet.write(receipt_row, funds_column, subtotal, value_subtotal)

        worksheet.write(receipt_row, fund_identify['total'], total_receipt, value_subtotal)
        receipt_row += 1
        worksheet.write(receipt_row, fund_identify['total'], '', value_total_left)
        receipt_row += 1
        worksheet.write(receipt_row, fund_identify['total'], '', value_total_left)
        receipt_row += 1
        
        #Payments
        # worksheet.write(receipt_row, 0, '', header)
        worksheet.write(receipt_row, 0, 'PAYMENTS', header2)
        worksheet.write(receipt_row, fund_identify['total'], '', value_total_left)
        receipt_row += 1
        
        if len(funds_ids) == 1:
            query = """
            SELECT id FROM pool_of_funds_log
            WHERE funds_id in (%s)
            AND transaction_date BETWEEN '%s' AND '%s'
            AND amount < 0;
            """ %(funds_ids[0].id, self.start_date, self.end_date)

        else:
            query = """
            SELECT id FROM pool_of_funds_log
            WHERE funds_id in %s
            AND transaction_date BETWEEN '%s' AND '%s'
            AND amount < 0;
            """ %(funds_ids._ids, self.start_date, self.end_date)

        self._cr.execute(query)
        query_res = self._cr.dictfetchall()
        log_list = []

        for query_log in query_res:
            log_list.append(query_log['id'])

        funds_log_ids = funds_log_obj.browse(log_list)
        payment_subtotal = {}
        for log in funds_log_ids:
            partner = '-'
            date = '-'
            amount = log.amount
            funds_id = log.funds_id

            if log.inv_id:
                partner = log.inv_id.partner_id.name
                date = log.inv_id.date_invoice
                if not partner:
                    partner = log.inv_id.student_id.name
                if not partner:
                    partner = log.inv_id.name

            elif log.move_id:
                partner = log.move_id.partner_id.name
                date = log.move_id.date
                if not partner:
                    partner = log.move_id.student_id.name
                if not partner:
                    partner = log.move_id.name + ' - ' + (log.move_id.ref or '')

            elif log.voucher_id:
                partner = log.voucher_id.partner_id.name
                date = log.voucher_id.date
                if not partner:
                    partner = log.voucher_id.student_id.name
                if not partner:
                    partner = str(log.voucher_id.number) + ' - ' + (log.voucher_id.reference or '')

            else:
                partner = log.account_id.name
                date = log.transaction_date
                if not date:
                    date = log.create_date
            
            worksheet.write(receipt_row, 0, self.get_date_format(date), value_text)
            worksheet.write(receipt_row, 1, partner, value_text)
            funds_column = fund_identify[funds_id.id]
            worksheet.write(receipt_row, funds_column, amount, value)
            worksheet.write(receipt_row, fund_identify['total'], amount, value_total_left)
            receipt_row += 1
            
            if payment_subtotal.get(funds_id.id):
                subtotal = amount + float(payment_subtotal[funds_id.id])
            else:
                subtotal = amount
                
            payment_subtotal[funds_id.id] = subtotal
            
        # worksheet.write(receipt_row, 0, '', header)
        worksheet.write(receipt_row, 0, 'SUBTOTAL', header2)
        total_payment = 0

        for funds in funds_ids:
            if payment_subtotal.get(funds.id):
                subtotal = payment_subtotal[funds.id]
                total_payment += subtotal
            else:
                subtotal = 0
                total_payment += 0

            funds_column = fund_identify[funds.id]
            worksheet.write(receipt_row, funds_column, subtotal, value_subtotal)

        worksheet.write(receipt_row, fund_identify['total'], total_payment, value_subtotal)
        receipt_row += 1
        
        #Total
        # worksheet.write(receipt_row, 0, '', header)
        worksheet.write(receipt_row, 0, 'CLOSING BALANCE', header2)
        total_all = 0
        
        for funds in funds_ids:
            total_receipt = 0
            total_payment = 0
            funds_column = fund_identify[funds.id]
            opening_bal_value = opening_bal_vals.get(funds.id, 0)

            if receipt_subtotal.get(funds.id):
                total_receipt = receipt_subtotal[funds.id]

            if payment_subtotal.get(funds.id):
                total_payment = payment_subtotal[funds.id]

            if opening_bal_value + total_receipt + total_payment == 0:
                worksheet.write(receipt_row, funds_column, '-', value_subtotal)
            else:
                worksheet.write(receipt_row, funds_column, opening_bal_value + total_receipt + total_payment, value_subtotal)

            total_all += opening_bal_value + total_receipt + total_payment

        worksheet.write(receipt_row, fund_identify['total'], total_all, value_subtotal)
        receipt_row += 2
        worksheet.write(receipt_row, 0, "Treasurer :", header)
        worksheet.write(receipt_row, fund_identify['total'], "Auditor :", header)
        receipt_row += 3
        worksheet.write(receipt_row, 0, config.default_treasurer_id.name, header)
        worksheet.write(receipt_row, fund_identify['total'], config.default_auditor_id.name, header)
        
        #computing the compare year
        if self.compared_year != False:
            worksheet.col(column_no).width = 5500
            worksheet.col(column_no+1).width = 5500
            total_change = 0
            start = str(self.compared_year) + "-01-01"
            end = str(self.compared_year) + "-12-31"
            # end = "2022-02-05"
            worksheet.write(2, column_no, "Total " + str(self.compared_year), header_funds)
            worksheet.write(2, column_no+1, "% CHANGE", header_funds)
            receipt_row = 4

            query = """
            SELECT id FROM pool_of_funds_log
            WHERE transaction_date BETWEEN '%s' AND '%s';
            """ %(start, end)

            self._cr.execute(query)
            query_res = self._cr.dictfetchall()
            log_list = []

            for query_log in query_res:
                log_list.append(query_log['id'])

            for compare in list_amount_compare:

                if list != False:
                    receipt_row += 1
                    worksheet.write(receipt_row, column_no, compare, value)
                    for total_income in list_total_income:
                        print (total_income, compare, "compare func ======================================")
                        if compare == 0:
                            change = 0
                        else:
                            change = (total_income - compare)/compare
                        total_change += change
                        worksheet.write(receipt_row, column_no+1, ("%.2f" % change), value_change)
                        list_total_income.pop(0)
                        break

            receipt_row += 1
            worksheet.write(receipt_row, column_no+1, float("%.2f" % total_change), value_change_subtotal)
            worksheet.write(receipt_row, column_no, total_compare, value_subtotal)
    
        fp = io.BytesIO()
        workbook.save(fp)
        fp.seek(0)
        data = fp.read()
        fp.close()
        res = base64.encodestring(data)
        filename = 'Funds Report' + '.xls'
        module_rec = self.env['account.funds.report.file'].create({'name': filename, 'file' : res})
        
        return {
          'name': _('Funds Report'),
          'res_id' : module_rec.id,
          'view_type': 'form',
          "view_mode": 'form',
          'res_model': 'account.funds.report.file',
          'type': 'ir.actions.act_window',
          'target': 'new',
          }




