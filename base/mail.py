# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import time
import dateutil.relativedelta as relativedelta
import logging
from urllib.parse import urlparse

from urllib.parse import urlencode, quote as quote
from pytz import timezone
from email.utils import formataddr

from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError
from odoo.tools import html_escape as escape, posix_to_ldml, safe_eval, float_utils, format_date, pycompat
from email.utils import formataddr

_logger = logging.getLogger(__name__)

class MailTemplate(models.Model):
    _inherit = "mail.template"
    
    
    tpstst_email = fields.Boolean(string="TPSTST Email")
    