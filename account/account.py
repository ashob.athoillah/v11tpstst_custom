# -*- coding: utf-8 -*-

import time

from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp 
from odoo.exceptions import UserError, except_orm
from itertools import chain
from datetime import datetime

from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import dateutil.relativedelta
import pytz
from pytz import timezone

def to_local_datetime(dt, context, from_format="%Y-%m-%d %H:%M:%S", to_format="%Y-%m-%d %H:%M:%S"):
    tz_name = context.get('tz')
    local_tz = False
    if tz_name:
        local_tz = timezone(tz_name)
    local_date = datetime.strptime(dt, from_format)
    if local_tz:
        local_date = pytz.utc.localize(local_date).astimezone(local_tz) 
    local_date = datetime.strftime(local_date, to_format)
    return local_date

def to_utc_datetime(dt, context, from_format="%Y-%m-%d %H:%M:%S", to_format="%Y-%m-%d %H:%M:%S"):
    tz_name = context.get('tz')
    local_tz = False
    if tz_name:
        local_tz = timezone(tz_name)
    local_date = datetime.strptime(dt, from_format)
    utc_date = local_date
    if local_tz:
        utc_date = local_tz.localize(utc_date).astimezone(pytz.utc)
    utc_date = datetime.strftime(utc_date, to_format)
    return utc_date
    
class TypeOfFunds(models.Model):
    _name = 'type.of.funds'
    _order = 'name'
    
    name = fields.Char('Type of Funds',required=True,size=64)
    funds_ids = fields.One2many('pool.of.funds','type_id','Funds',readonly=True)
    account_ids = fields.Many2many('account.account',string='Accounts')

class PoolOfFunds(models.Model):
    _name = 'pool.of.funds'
    _inherit = ['mail.thread', 'utm.mixin']
    _order = 'name'
    _sql_constraints = [('name_unique', 'unique(name)', 'This Funds is already exists.')]

    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        return super(PoolOfFunds, self.with_context(show_balance=True)).name_search(name, args=args, operator=operator, limit=limit)

    @api.multi
    def name_get(self):
        context = self._context
        if not context.get('show_balance'):
            return super(PoolOfFunds, self).name_get()
        res = []
        digits=dp.get_precision('Account')(self._cr)
        precision = '{:,.'+str(digits[1])+'f}'
        for fund in self:
            fund_name = "%s: %s"%(fund.name,precision.format(fund.total_amount))
            res.append((fund.id,fund_name))
        return res

    @api.multi
    def unlink(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        for fund in self:
            if not finance_head_user:
                raise UserError(_("Only Finance Head Can Delete Funds."))
            if fund.log_ids:
                raise UserError(_('You cannot remove funds with log!'))
            if fund.state != 'cancel':
                raise UserError(_('Please cancel before remove this Funds.'))
        return super(PoolOfFunds, self).unlink()

    def _get_log_total_amount(self):
        args = 'where funds_id=%s '%str(self.id)
        if self._context:
            if self._context.get('date_from'):
                args += "\n and pflog.create_date >= '%s' "%self._context['date_from']
            if self._context.get('date_to'):
                args += "\n and pflog.create_date <= '%s' "%self._context['date_to']
        query = '''
                select sum(coalesce(amount,0.0)) 
                from pool_of_funds_log pflog
                %s
            '''%(args)
        self._cr.execute(query)
        query_result = self._cr.fetchall()
        total_amount = query_result and query_result[0] and query_result[0][0] or 0.0
        self._cr.execute(query+"\n and log_for='utilized' ")
        query_result = self._cr.fetchall()
        total_utilized = query_result and query_result[0] and query_result[0][0] or 0.0
        self._cr.execute(query+"\n and log_for='donation' ")
        query_result = self._cr.fetchall()
        total_donation = query_result and query_result[0] and query_result[0][0] or 0.0
        return {
                'total_amount':total_amount,
                'total_utilized':total_utilized,
                'total_donation':total_donation,
                }

    @api.multi
    @api.depends('log_ids','log_ids.amount','log_ids.log_for')
    def _get_total_amount(self):
        if type(self.ids) not in [list, tuple] or not tuple(self.ids):
            return
        
        for fund in self:
            fund_res = fund._get_log_total_amount()
            fund.total_amount = fund_res['total_amount']
            fund.total_utilized = fund_res['total_utilized']
            fund.total_donation = fund_res['total_donation']
                
    type_of_fund = fields.Selection([('restricted','Restricted Funds'),('unrestricted','Unrestricted Funds')],string='Type of Funds', required=True)
    type_id = fields.Many2one('type.of.funds','Type of Funds', track_visibility='onchange')
    name = fields.Char('Name', size=64, required=True, readonly=False)
    company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env.user.company_id.id)
    total_utilized = fields.Float(string='Utilized Funds', digits=dp.get_precision('Account'), multi='total', compute='_get_total_amount',store=True)
    total_donation = fields.Float(string='Total Donation', digits=dp.get_precision('Account'), multi='total', compute='_get_total_amount',store=True)
    total_amount = fields.Float(string='Balance', digits=dp.get_precision('Account'), multi='total', compute='_get_total_amount',store=True)
    over_utilize_limit = fields.Float(string='Over Utilize Limit', digits=dp.get_precision('Account'),default=lambda*self:0.0, track_visibility='onchange')
    description = fields.Text('Description',help='Donors criteria of judgement')
    state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('cancel','Cancelled')], string='Status', default='draft', copy=False, track_visibility='onchange')
    log_ids = fields.One2many('pool.of.funds.log','funds_id','Logs',readonly=True)
#    journal_id = fields.Many2one('account.journal',required=True,string='Journal')
#    donation_account_id = fields.Many2one('account.account', string='Donation Account',required=False,
#        domain=[('deprecated', '=', False)])
#    account_income_id = fields.Many2one('account.account', string='Income Account',required=True,
#        domain=[('deprecated', '=', False),('internal_type','=','other')])
#    account_expense_id = fields.Many2one('account.account', string='Expense Account',required=True,
#        domain=[('deprecated', '=', False),('internal_type','=','other')])
    move_ids = fields.One2many('pool.of.funds.log','funds_id','Entries',readonly=True)
    move_line_ids = fields.One2many('account.move.line','funds_id','Entries',readonly=True)
    partner_ids = fields.Many2many('res.partner', 'pool_funds_partner_rel','funds_id','partner_id',string='Donors')
    pof_approved_template = fields.Many2one('mail.template',string='FAS Approved Template')
    pof_rejected_template = fields.Many2one('mail.template',string='FAS Rejected Template')
    start_date  = fields.Date(string='Start Date', track_visibility='onchange')
    end_date  = fields.Date(string='End Date', track_visibility='onchange')
    # programme_categ_ids = fields.Many2many('product.category', 'pool_funds_product_category','funds_id','product_categ_id',string='Programmes',domain=[('product_type','=', 'course')], readonly=False)
    analytic_tag_id = fields.Many2one('account.analytic.tag', string='Analytic Tag', required=True)
    
    @api.onchange('name')
    def onchange_name(self):
        if self.name:
            upper = self.name.upper()
            self.name = upper
    
    @api.multi
    def button_confirm(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        if not finance_head_user:
            raise UserError(_("Only Finance Head Can Confirm Funds."))
        
        for fund in self:
            fund.state = 'confirm'
        return True
    
    @api.multi
    def button_cancel(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        if not finance_head_user:
            raise UserError(_("Only Finance Head Can Cancel Funds."))
        
        for fund in self:
            fund.state = 'cancel'
        return True
    
    @api.multi
    def button_set_to_draft(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        if not finance_head_user:
            raise UserError(_("Only Finance Head Can Set Funds to Draft."))
        
        for fund in self:
            fund.state = 'draft'
        return True
    
    def _get_move_vals(self, journal=None, date=False, communication=False, log_id=False):
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _('The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _('The sequence of journal %s is deactivated.') % journal.name)
        if not date:
            date = fields.Date.context_today(self)
        name = self.name or journal.with_context(ir_sequence_date=date).sequence_id.next_by_id()
        return {
            #'name': name,
            'date': date,
            'ref': name or '',
            'narration': communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
            'funds_id': self.id,
            'funds_log_id':log_id,
        }

    def _get_shared_move_line_vals(self, debit, credit, amount_currency, move_id, funds_id=False):
        return {
            'partner_id': False, # self.payment_type in ('inbound', 'outbound') and self.env['res.partner']._find_accounting_partner(self.partner_id).id or False,
            'funds_id': self.id, #invoice_id and invoice_id.id or False,
#            'invoice_id': False, #invoice_id and invoice_id.id or False,
            'move_id': move_id,
            'debit': debit,
            'credit': credit,
            'amount_currency': amount_currency or False,
        }

    def _get_counterpart_move_line_vals(self):
        return {
            'name': self.name,
            'account_id': self.account_income_id.id,
            'journal_id': self.journal_id.id,
            'currency_id': False, #self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
            'funds_id': self.id,
        }
        
        
    def _get_liquidity_move_line_vals(self, amount):
        name = self.name
        vals = {
            'name': name,
            'account_id': amount < 0.0 and self.journal_id.default_debit_account_id.id or self.journal_id.default_credit_account_id.id,
#            'payment_id': self.id,
            'journal_id': self.journal_id.id,
            'currency_id': False,#self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
        }

        return vals

    def _create_payment_entry(self, amount, date=False, remarks='', log_id=False):
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        if not date:
            date = fields.Date.context_today(self)
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=date).compute_amount_fields(amount, False, self.company_id.currency_id, 0.0)

        move = self.env['account.move'].create(self._get_move_vals(date=date, communication=remarks, log_id=log_id))

        #Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(self._get_counterpart_move_line_vals())
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)

        liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
        aml_obj.create(liquidity_aml_dict)

        move.post()
        return move

    @api.multi
    def open_wizard_amend_funds(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        if not finance_head_user:
            raise UserError(_("Only Finance Head Can Adjust and Transfer Funds."))
        
        ir_model_data = self.env['ir.model.data']
        act = False
        try:
            act = ir_model_data.xmlid_to_object('v11_tpstst_custom.action_pool_of_funds_amend')
        except ValueError:
            act = False
        result = act.read()[0]
        result['context'] = self._context
        default_type = ''
        if self._context and self._context.get('default_type',''):
            default_type = self._context['default_type']
        if default_type == 'adjust':
            result['name'] = 'Adjust'
        elif default_type == 'move':
            result['name'] = 'Transfer Balance'
        return result
    
class PoolOfFundsLog(models.Model):
    _name = 'pool.of.funds.log'
    _order = 'transaction_date desc'
    
    create_uid = fields.Many2one('res.users','Modified by',readonly=True)
    create_date = fields.Datetime('Date Validated',readonly=True)
    transaction_date = fields.Date('Transaction Date',readonly=True)
    funds_id = fields.Many2one('pool.of.funds','Funds',required=True,ondelete='cascade')
    transfer_funds_id = fields.Many2one('pool.of.funds','Transferred From/To')
    log_for = fields.Selection([('donation','Donations'),('utilized','Utilized Fund'),('transfer','Transfer')], required=True, default='utilized')
    amount = fields.Float(string='Amount', digits=dp.get_precision('Account'), readonly=True)
    account = fields.Char('Account', size=64, required=True, readonly=False)
    remarks = fields.Text('Remarks')
    move_ids = fields.One2many('account.move','funds_log_id',string='Journal Entries', readonly=True)
    move_id = fields.Many2one('account.move',string='Journal Entries', readonly=True)
    voucher_id = fields.Many2one('account.voucher', string="Sales Receipt/Direct Payment", readonly=True)
    adv_voucher_id = fields.Many2one('advance.account.voucher', string="Payment", readonly=True)
    inv_id = fields.Many2one('account.invoice', string="Invoice", readonly=True)
    account_id = fields.Many2one('account.account', 'Account', required = True)
    
    @api.multi
    def unlink(self):
        track_obj = self.env['fund.track']
        for fund in self:
            track_obj.create({
                            'name' : fund.remarks,
                            'amount' : fund.amount,
                            'delete_by' : self.env.user.id,
                            'delete_date' : fields.Date.context_today(self),
                            'move_id' : fund.move_id.id,
                            'voucher_id' : fund.voucher_id.id,
                            'inv_id' : fund.inv_id.id
                            })
        return super(PoolOfFundsLog, self).unlink()
    
class FundTrack (models.Model):
    _name = "fund.track"
    
    name = fields.Char('Funds Name')
    amount = fields.Float(string='Amount')
    delete_by = fields.Many2one('res.users', 'Delete By')
    delete_date = fields.Datetime('Delete Date Time')
    move_id = fields.Many2one('account.move',string='Journal Entries', readonly=True)
    voucher_id = fields.Many2one('account.voucher', string="Sales Receipt/Direct Payment", readonly=True)
    inv_id = fields.Many2one('account.invoice', string="Invoice", readonly=True)
    
class AccountAccount(models.Model):
    _name = 'account.account'
    _inherit = ['account.account','mail.thread', 'utm.mixin']
    _readonly_view = True
    _nodelete_restrict = True
    
    #is_deferred_account = fields.Boolean('Deferred Account',default=lambda*a:False)
    deferred_account_id = fields.Many2one('account.account','Deferred Account', track_visibility='onchange')
    deferred_for = fields.Selection([('reg','Registration'),('bursary','FAS')],string='Deferred Account for', track_visibility='onchange')
    #deferred_source_account_id = fields.Many2one('account.account','Source Account',domain=[('is_deferred_account','=',False)])
    government_subsidy  = fields.Boolean(string='Subsidy from Government', default=False, help="Check this box if this account is used for government subsidies", track_visibility='onchange')
    remarks = fields.Text('Remarks', track_visibility='onchange')
    
    name = fields.Char(required=True, index=True, track_visibility='onchange')
    currency_id = fields.Many2one('res.currency', string='Account Currency',
        help="Forces all moves for this account to have this account currency.", track_visibility='onchange')
    code = fields.Char(size=64, required=True, index=True, track_visibility='onchange')
    deprecated = fields.Boolean(index=True, default=False, track_visibility='onchange')
    user_type_id = fields.Many2one('account.account.type', string='Type', required=True, oldname="user_type", track_visibility='onchange',
        help="Account Type is used for information purpose, to generate country-specific legal reports, and set the rules to close a fiscal year and generate opening entries.")
    internal_type = fields.Selection(related='user_type_id.type', string="Internal Type", store=True, readonly=True)
    #has_unreconciled_entries = fields.Boolean(compute='_compute_has_unreconciled_entries',
    #    help="The account has at least one unreconciled debit and credit since last time the invoices & payments matching was performed.")
    last_time_entries_checked = fields.Datetime(string='Latest Invoices & Payments Matching Date', readonly=True, copy=False, track_visibility='onchange',
        help='Last time the invoices & payments matching was performed on this account. It is set either if there\'s not at least '\
        'an unreconciled debit and an unreconciled credit Or if you click the "Done" button.')
    reconcile = fields.Boolean(string='Allow Reconciliation', default=False, track_visibility='onchange',
        help="Check this box if this account allows invoices & payments matching of journal items.")
    tax_ids = fields.Many2many('account.tax', 'account_account_tax_default_rel',
        'account_id', 'tax_id', string='Default Taxes', track_visibility='onchange')
    note = fields.Text('Internal Notes', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', required=True,
        default=lambda self: self.env['res.company']._company_default_get('account.account'), track_visibility='onchange')
    tag_ids = fields.Many2many('account.account.tag', 'account_account_account_tag', string='Tags', help="Optional tags you may want to assign for custom reporting", track_visibility='onchange')
    group_id = fields.Many2one('account.group', track_visibility='onchange')
    dont_show_report = fields.Boolean("Don't Show on Aged Report", default=False)
    
    
    @api.model
    def _search_test(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        invoice_ids = []
        move_line_obj = self.env['account.move.line']
        voucher_line_obj = self.env['advance.account.voucher.line']
        voucher_lines = (context.get('sort_voucher_cr_inv_acc',[]) or []) + (context.get('sort_voucher_dr_inv_acc',[]) or [])
        if voucher_lines:
            for line in voucher_lines:
                move_line = False
                if (line[2] or {}).get('move_line_id'):
                    move_line = move_line_obj.browse(line[2]['move_line_id'])
                if not move_line and line[1]:
                    vline = voucher_line_obj.browse(line[1])
                    if vline:
                        move_line = vline.move_line_id
                if move_line and move_line.invoice_id:
                    invoice_ids.append(move_line.invoice_id.id)
        results = []
        if count:
            results = 0
        if invoice_ids:
            query = """
                with account_ids as(
                                select distinct on (account_id) 
                                        account_id, 
                                        case when invline.component_id is null then 1 else 0 end as null_component, 
                                        comp.is_programme_fee, 
                                        comp.id as comp_id, 
                                        acc.name as acc_name
                                from account_invoice_line invline
                                join account_account acc on acc.id=invline.account_id
                                left join product_component_type comp on comp.id = invline.component_id
                                where invoice_id in (%s)
                                order by account_id, null_component, comp.is_programme_fee desc, comp.id
                                )
                 select account_id
                 from account_ids
                 order by null_component, is_programme_fee desc,comp_id
            """%(str(invoice_ids)[1:-1])
                #query order by boolean desc means True came first then False
            self._cr.execute(query)
            query_result = self._cr.fetchall()
            account_ids = list(chain.from_iterable(query_result))
            if account_ids:
                account_ids2 = super(AccountAccount, self)._search(args+[('id','in',account_ids)], offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
                #if not count:
                results += account_ids2
                #print ('LIMIT',limit)
                #total result would not correct
                if limit and limit-len(account_ids2) > 0:
                    limit -= len(account_ids2)
                args += [('id','not in',account_ids)]
        results += super(AccountAccount, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
        #result2 = super(AccountAccount, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
        #return result2
        return results
    
    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        return super(AccountAccount, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)

class AccountTax(models.Model):
    _name = 'account.tax'
    _inherit = ['account.tax','mail.thread', 'utm.mixin']
    _description = 'Tax'
    _readonly_view = True
    _nodelete_restrict = True
    
    active = fields.Boolean(default=True, help="Set active to false to hide the tax without removing it.", track_visibility='onchange')

    @api.multi
    def compute_all(self, price_unit, currency=None, quantity=1.0, product=None, partner=None):
        """ Returns all information required to apply taxes (in self + their children in case of a tax goup).
            We consider the sequence of the parent for group of taxes.
                Eg. considering letters as taxes and alphabetic order as sequence :
                [G, B([A, D, F]), E, C] will be computed as [A, D, F, C, E, G]

        RETURN: {
            'total_excluded': 0.0,    # Total without taxes
            'total_included': 0.0,    # Total with taxes
            'taxes': [{               # One dict for each tax in self and their children
                'id': int,
                'name': str,
                'amount': float,
                'sequence': int,
                'account_id': int,
                'refund_account_id': int,
                'analytic': boolean,
            }]
        } """
        if len(self) == 0:
            company_id = self.env.user.company_id
        else:
            company_id = self[0].company_id
        if not currency:
            currency = company_id.currency_id
        taxes = []
        # By default, for each tax, tax amount will first be computed
        # and rounded at the 'Account' decimal precision for each
        # PO/SO/invoice line and then these rounded amounts will be
        # summed, leading to the total amount for that tax. But, if the
        # company has tax_calculation_rounding_method = round_globally,
        # we still follow the same method, but we use a much larger
        # precision when we round the tax amount for each line (we use
        # the 'Account' decimal precision + 5), and that way it's like
        # rounding after the sum of the tax amounts of each line
        custom_precision = self._context.get('custom_precision')
        prec = currency.decimal_places
        if custom_precision:
            prec = custom_precision or currency.decimal_places

        # In some cases, it is necessary to force/prevent the rounding of the tax and the total
        # amounts. For example, in SO/PO line, we don't want to round the price unit at the
        # precision of the currency.
        # The context key 'round' allows to force the standard behavior.
        round_tax = False if company_id.tax_calculation_rounding_method == 'round_globally' else True
        round_total = True
        if 'round' in self.env.context:
            round_tax = bool(self.env.context['round'])
            round_total = bool(self.env.context['round'])

        if not round_tax:
            prec += 5

        base_values = self.env.context.get('base_values')
        if not base_values:
            total_excluded = total_included = base = round(price_unit * quantity, prec)
        else:
            total_excluded, total_included, base = base_values

        # Sorting key is mandatory in this case. When no key is provided, sorted() will perform a
        # search. However, the search method is overridden in account.tax in order to add a domain
        # depending on the context. This domain might filter out some taxes from self, e.g. in the
        # case of group taxes.
        for tax in self.sorted(key=lambda r: r.sequence):
            if tax.amount_type == 'group':
                children = tax.children_tax_ids.with_context(base_values=(total_excluded, total_included, base))
                ret = children.compute_all(price_unit, currency, quantity, product, partner)
                total_excluded = ret['total_excluded']
                base = ret['base'] if tax.include_base_amount else base
                total_included = ret['total_included']
                tax_amount = total_included - total_excluded
                taxes += ret['taxes']
                continue

            tax_amount = tax._compute_amount(base, price_unit, quantity, product, partner)
            if not round_tax:
                tax_amount = round(tax_amount, prec)
            else:
                if custom_precision:
                    tax_amount = round(tax_amount, custom_precision)
                else:
                    tax_amount = currency.round(tax_amount)

            if tax.price_include:
                total_excluded -= tax_amount
                base -= tax_amount
            else:
                total_included += tax_amount

            # Keep base amount used for the current tax
            tax_base = base

            if tax.include_base_amount:
                base += tax_amount

            taxes.append({
                'id': tax.id,
                'name': tax.with_context(**{'lang': partner.lang} if partner else {}).name,
                'amount': tax_amount,
                'base': tax_base,
                'sequence': tax.sequence,
                'account_id': tax.account_id.id,
                'refund_account_id': tax.refund_account_id.id,
                'analytic': tax.analytic,
                'price_include': tax.price_include,
            })

        total_excluded_res = total_excluded
        total_included_res = total_included
        if round_total:
            total_excluded_res = currency.round(total_excluded)
            total_included_res = currency.round(total_included) 
            
        return {
            'taxes': sorted(taxes, key=lambda k: k['sequence']),
            'total_excluded': total_excluded_res,
            'total_included': total_included_res,
            'base': base,
        }

class AccountFiscalPosition(models.Model):
    _inherit = 'account.fiscal.position'
    _description = 'Fiscal Position'
    _readonly_view = True
    _nodelete_restrict = True

class AccountAccountType(models.Model):
    _inherit = "account.account.type"
    _description = "Account Type"
    _readonly_view = True
    _nodelete_restrict = True

class AccountAccountTag(models.Model):
    _name = 'account.account.tag'
    _inherit = ['account.account.tag','mail.thread', 'utm.mixin']
    _description = 'Account Tag'
    _readonly_view = True
    _nodelete_restrict = True
    
    active = fields.Boolean(default=True, help="Set active to false to hide the tag without removing it.", track_visibility='onchange')

class AccountGroup(models.Model):
    _inherit = "account.group"
    _readonly_view = True
    _nodelete_restrict = True
    
class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    active = fields.Boolean('Active', help="If the active field is set to False, it will allow you to hide the account without removing it.", default=True, track_visibility='onchange')
    amount_crossover_budget = fields.Monetary('Budget Amount', compute='_get_amount_crossover_budget', readonly=True)
    remaining_crossover_budget = fields.Monetary('Remaining Budget', compute='_get_amount_crossover_budget', readonly=True)
    # committee_ids = fields.Many2many('hr.committee', 'committee_analytic_acc_rel', 'analytic_acc', 'committee_id', string='Committees')
    
    @api.multi
    def _get_amount_crossover_budget(self):
        context = dict(self._context)
        for analytic in self:
            by_date = False
            if context.get('by_date'):
                by_date = context['by_date']
            amount_crossover_budget = 0
            used_crossover_budget = 0
            for budget_line in analytic.crossovered_budget_line:
                amount_crossover_budget += budget_line.planned_amount
                used_crossover_budget += budget_line.with_context(wizard_date_to=by_date).practical_amount
            analytic.update({'amount_crossover_budget': amount_crossover_budget,
                             'remaining_crossover_budget': amount_crossover_budget + used_crossover_budget})

    @api.one
    #@api.depends('budget_ids')
    def _get_amount_crossover_budgetOLD(self):
        res = {}
        today = by_date = fields.Date.context_today(self)
        company = self.env.user.company_id
        context = dict(self._context)
        cur_obj=self.env['res.currency']
        #period_obj = self.env['account.period']
        crossover_budget_line_obj = self.env['crossovered.budget.lines']
        if not context.get('tz', False):
            context['tz'] = self.env.user.tz
        from_date = context.get('from_date', False)
        to_date = context.get('to_date', False)
        fiscalyear_dates = {}
        fiscalyear_date_from = False
        fiscalyear_date_to = False
        if context.get('by_date'):
            fiscalyear_dates = company.compute_fiscalyear_dates(datetime.strptime(context['by_date'],DEFAULT_SERVER_DATE_FORMAT))
            fiscalyear_date_from = fiscalyear_dates['date_from'].strftime(DEFAULT_SERVER_DATE_FORMAT)
            fiscalyear_date_to = fiscalyear_dates['date_to'].strftime(DEFAULT_SERVER_DATE_FORMAT)
            by_date = context['by_date']
        if not fiscalyear_dates:
            fiscalyear_dates = company.compute_fiscalyear_dates(datetime.strptime(today,DEFAULT_SERVER_DATE_FORMAT))
            fiscalyear_date_from = fiscalyear_dates['date_from'].strftime(DEFAULT_SERVER_DATE_FORMAT)
            fiscalyear_date_to = fiscalyear_dates['date_to'].strftime(DEFAULT_SERVER_DATE_FORMAT)
        if not context.get('from_date') or not context.get('to_date'):
            if not from_date or fiscalyear_date_from < from_date:
                from_date = fiscalyear_date_from
            if not to_date or fiscalyear_date_to > to_date:
                to_date = fiscalyear_date_to
            context.update({
                'from_date': from_date,
                'to_date': to_date,
            })
            
        relativedelta = dateutil.relativedelta.relativedelta
        from_date = context['from_date']
        to_date = context['to_date']
        from_datetime = to_utc_datetime(from_date, context,from_format="%Y-%m-%d")
        to_datetime = (datetime.strptime(to_utc_datetime(to_date,context, from_format='%Y-%m-%d'), '%Y-%m-%d %H:%M:%S')+relativedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')
        
        #print 'FROM datetime',from_datetime,to_datetime
#        po_ids = po_obj.sudo().search([('state','not in',['cancel','draft','sent']),
#                            ('invoice_method','=','picking'),('date_order','>',from_datetime),('date_order','<',to_datetime)])
        for ac in self:
            res[ac.id] = {
                'amount_bugdet': 0.0,
                'remaining_budget': 0.0,
            }
            account_analytic_ids = [ac.id]
            child_ids = [child.id for child in ac.child_ids]
            if child_ids:
                account_analytic_ids = child_ids
            budgets = crossover_budget_line_obj.search( 
                            [('date_from','<=',by_date),('date_to','>=',by_date),('analytic_account_id','in',account_analytic_ids)])
            amount_budget_list = [budget.planned_amount for budget in budgets]
            amount_budget = sum(amount_budget_list)
            inv_line_total = 0.0
            po_line_total = 0.0
            pr_line_total = 0.0
            if account_analytic_ids:
                #price_subtotal_signed : if it is credit note, would be -, if it is customer invoice or vendor bills would be +
                query = '''
                    select sum(coalesce(line.price_subtotal_signed,0.0))
                    from account_invoice_line line
                    join account_invoice inv on inv.id=line.invoice_id
                    where 
                        inv.state = 'draft'
                        and inv.type = 'in_invoice'
                        and line.account_analytic_id in (%s)
                        and to_char(coalesce(coalesce(inv.date,inv.date_invoice),inv.create_date),'YYYY-MM-DD') >= '%s'
                        and to_char(coalesce(coalesce(inv.date,inv.date_invoice),inv.create_date),'YYYY-MM-DD') <= '%s'
                '''%(str(account_analytic_ids)[1:-1],from_date,to_date)
                self._cr.execute(query)
                query_res = self._cr.fetchall()
                inv_line_total += (query_res and query_res[0] and query_res[0][0] or 0.0)
                
                query = '''
                    select sum(coalesce(line.price_subtotal,0.0))
                    from purchase_requisition_line line
                    join purchase_requisition pr on pr.id=line.requisition_id
                    where 
                        pr.state not in ('cancel','in_progress','open','done')
                        and line.account_analytic_id in (%s)
                        and to_char(coalesce(pr.ordering_date,pr.create_date),'YYYY-MM-DD') >= '%s'
                        and to_char(coalesce(pr.ordering_date,pr.create_date),'YYYY-MM-DD') <= '%s'
                '''%(str(account_analytic_ids)[1:-1],from_date,to_date)
                self._cr.execute(query)
                query_res = self._cr.fetchall()
                pr_line_total += (query_res and query_res[0] and query_res[0][0] or 0.0)

                #any changes, please check to wizard/budget_analysis_reporting
                po_line_query = """
                    with po_line_ids as (
                        select distinct on (po_line.id) po_line.id
                        from purchase_order_line as po_line
                        join purchase_order as po on po.id = po_line.order_id
                        left join account_invoice_purchase_order_rel as po_inv on po_inv.purchase_order_id = po.id
                        left join account_invoice as inv on inv.id = po_inv.account_invoice_id
                        where po.state not in ('cancel','draft','sent')
                        and to_char(po.date_order,'YYYY-MM-DD HH24:MI:SS') > '%(from_datetime)s'
                        and to_char(po.date_order,'YYYY-MM-DD HH24:MI:SS') < '%(to_datetime)s'
                        and po_line.account_analytic_id in (%(account_analytic_ids)s)
                        and ( (inv.id is null)
                            or (po.id not in (
                                        select purchase.id 
                                        from account_invoice as invoice 
                                        join account_invoice_purchase_order_rel as po_invoice_rel on po_invoice_rel.account_invoice_id = invoice.id
                                        join purchase_order as purchase on purchase.id = po_invoice_rel.purchase_order_id
                                        where invoice.state != 'cancel'))
                            )
                        group by po_line.id
                        order by po_line.id
                    )
                    select sum(coalesce(line.price_subtotal,0.0))
                    from purchase_order_line line
                    join purchase_order po on po.id=line.order_id
                    where 
                        line.id in (select id from po_line_ids)
                    """%({'from_datetime':from_datetime, 'to_datetime':to_datetime,'account_analytic_ids':str(account_analytic_ids)[1:-1]})
                self._cr.execute(po_line_query)
                query_res = self._cr.fetchall()
                po_line_total += (query_res and query_res[0] and query_res[0][0] or 0.0)
                
                    
            remaining_budget = amount_budget + (ac.balance * -1) - inv_line_total - pr_line_total - po_line_total
            if ac.currency_id:
                ac.amount_crossover_budget = ac.currency_id.round(amount_budget)
                ac.remaining_crossover_budget = ac.currency_id.round(remaining_budget)
            else:
                ac.amount_crossover_budget = amount_budget
                ac.remaining_crossover_budget = remaining_budget
        return res
    
class AccountModel(models.Model):
    _inherit = 'account.model'

    @api.multi
    def unlink(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        for account_model in self:
            if not finance_head_user:
                raise UserError(_("Only Finance Head Can Delete Recurring Model!"))
        return super(AccountModel, self).unlink()
    
    @api.multi
    def generate(self, data=None, context=None):
        if data is None:
            data = {}
        move_ids = []
        entry = {}
        account_move_obj = self.env['account.move']
        pt_obj = self.env['account.payment.term']

        if context is None:
            context = {}

        if data.get('date', False):
            context = dict(context)
            context.update({'date': data['date']})

        move_date = context.get('date', time.strftime('%Y-%m-%d'))
        move_date = datetime.strptime(move_date,"%Y-%m-%d")
        for model in self:
            ctx = context.copy()
            ctx.update({'company_id': model.company_id.id})
            ctx.update({'journal_id': model.journal_id.id})
            try:
                entry['name'] = model.name%{'year': move_date.strftime('%Y'), 'month': move_date.strftime('%m'), 'date': move_date.strftime('%Y-%m')}
            except:
                raise except_orm(_('Wrong Model!'), _('You have a wrong expression "%(...)s" in your model!'))
            move_vals = {
                'ref': entry['name'],
                'journal_id': model.journal_id.id,
                'date': context.get('date', fields.Date.context_today(self))
            }
            lines = []
            for line in model.line_ids:
                analytic_account_id = False
                if line.analytic_account_id:
#                    if not model.journal_id.analytic_journal_id:
#                        raise except_orm(_('No Analytic Journal!'),_("You have to define an analytic journal on the '%s' journal!") % (model.journal_id.name,))
                    analytic_account_id = line.analytic_account_id.id
                line_vals = {
                    'journal_id': model.journal_id.id,
                    'analytic_account_id': analytic_account_id
                }

                date_maturity = context.get('date',time.strftime('%Y-%m-%d'))
                if line.date_maturity == 'partner':
                    if not line.partner_id:
                        raise except_orm(_('Error!'), _("Maturity date of entry line generated by model line '%s' of model '%s' is based on partner payment term!" \
                                                                "\nPlease define partner on it!")%(line.name, model.name))

                    payment_term_id = False
                    if model.journal_id.type in ('purchase', 'purchase_refund') and line.partner_id.property_supplier_payment_term:
                        payment_term_id = line.partner_id.property_supplier_payment_term.id
                    elif line.partner_id.property_payment_term:
                        payment_term_id = line.partner_id.property_payment_term.id
                    if payment_term_id:
                        pterm_list = pt_obj.compute(cr, uid, payment_term_id, value=1, date_ref=date_maturity)
                        if pterm_list:
                            pterm_list = [l[0] for l in pterm_list]
                            pterm_list.sort()
                            date_maturity = pterm_list[-1]
                            
                line_vals.update({
                    'name': line.name,
                    'quantity': line.quantity,
                    'debit': line.debit,
                    'credit': line.credit,
                    'account_id': line.account_id.id,
                    'funds_id': line.funds_id.id,
                    'analytic_tag_id': line.analytic_tag_id.id,
                    'partner_id': line.partner_id.id,
                    'date': context.get('date', fields.Date.context_today(self)),
                    'date_maturity': date_maturity
                })
                lines.append((0,0,line_vals))
                
            move_vals.update({
                'line_ids' : lines
            })
            move_id = account_move_obj.create(move_vals)
            move_ids.append(move_id)
                
        return move_ids
    
class AccountModelLine(models.Model):
    _inherit = 'account.model.line'
    
    analytic_tag_id = fields.Many2one('account.analytic.tag', string='Analytic Tag', required=True)
    funds_id = fields.Many2one('pool.of.funds','Funds', required=True, domain=[('state','=', 'confirm')])
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', ondelete="cascade", required=True)

    @api.onchange('funds_id')
    def onchange_funds_id(self):
        for fund in self:
            if fund.funds_id:
                analytic_tag_fund = fund.funds_id.analytic_tag_id
                self.analytic_tag_id = analytic_tag_fund

class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    @api.multi
    def unlink(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        for analytic_line in self:
            if not finance_head_user:
                raise UserError(_("Only Finance Head Can Delete This Record!"))
        return super(AccountAnalyticLine, self).unlink()

    tag_id = fields.Many2one('account.analytic.tag', string='Analytic Tag', copy=True, required=True)

class accountSubscription(models.Model):
    _inherit = "account.subscription"

    @api.multi
    def unlink(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        for account_subs in self:
            if not finance_head_user:
                raise UserError(_("Only Finance Head Can Delete Recurring Entries!"))
        return super(accountSubscription, self).unlink()


# class AccountPayment(models.Model):
#     _inherit = 'account.payment'

#     @api.model
#     def create(self, vals):
#         account_payment = super(AccountPayment, self).create(vals)
#         print (account_payment.payment_token_id,"=================================1")

#         account_payment._do_payment()
#         return account_payment

#     def _do_payment(self):
#         if self.payment_token_id.acquirer_id.capture_manually:
#             raise ValidationError(_('This feature is not available for payment acquirers set to the "Authorize" mode.\n'
#                                   'Please use a token from another provider than %s.') % self.payment_token_id.acquirer_id.name)
#         reference = "P-%s-%s" % (self.id, datetime.now().strftime('%y%m%d_%H%M%S'))
#         tx = self.env['payment.transaction'].create({
#             'amount': self.amount,
#             'acquirer_id': self.payment_token_id.acquirer_id.id,
#             'type': 'server2server',
#             'currency_id': self.currency_id.id,
#             'reference': reference,
#             'payment_token_id': self.payment_token_id.id,
#             'partner_id': self.partner_id.id,
#             'partner_country_id': self.partner_id.country_id.id,
#         })

#         s2s_result = tx.s2s_do_transaction()

#         if not s2s_result or tx.state != 'done':
#             raise ValidationError(_("Payment transaction failed (%s)") % tx.state_message)

#         self.payment_transaction_id = tx