# -*- coding: utf-8 -*-
# Part of TigernixERP. See LICENSE file for full copyright and licensing details.

from re import S
import time
import random
import string
from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from odoo.addons.v11_tpstst_custom.membership import membership_application as membership

STATE_MEMBER = [
    ('active', 'Active'),
    ('inactive', 'Not Active'),
    ('terminate', 'Terminated'),
]

class Membership(models.Model):
    _inherit = "res.partner"

    membership_type = fields.Selection(membership.MEMBERSHIP_TYPE, string="Membership Type", track_visibility='onchange')

    member = fields.Boolean(string='Member', help="Being indicator as a member in tpstst because on default module it's has to be indicator for free membership")
    membership_state = fields.Selection(STATE_MEMBER,
        string='Membership Status', default="inactive", track_visibility='onchange')

    member_since = fields.Date(string ='Member Since',
        help="Date from first time membership becomes active.", track_visibility='onchange')

    membership_start = fields.Date(string ='Member Validity',
        help="Date from which membership becomes active.", track_visibility='onchange')

    membership_stop = fields.Date(string ='Member Expiry',
        help="Date until which membership remains active.", track_visibility='onchange')

    membership_cancel = fields.Date(string='Terminate Date',
        help="Date on which membership has been cancelled")

    membership_previous_validity = fields.Date(string='Previous Validity Date')
    
    # total_payment = fields.Monetary(string='Total Payments', store=True, compute='compute_total_amount_payment')

    membership_status = fields.Selection(STATE_MEMBER,
        string='Membership Status', compute="_get_membership_status", store=True)
    membership_number = fields.Char('Membership ID', copy=False, track_visibility='onchange')
    # New 
    donation_ids = fields.One2many('donation.donation', 'partner_id', string="Donation Sheet", track_visibility='onchange')
    is_terminate = fields.Boolean('Is terminate ?') 
    status = fields.Selection([
                                ('alive', 'Alive'),
                                ('deceased', 'Deceased'),
                            ], string="Status / 状态", default="alive", track_visibility='onchange')
    tablet_ids = fields.One2many('tablet.application', 'partner_id', string="Tablet Sheet", track_visibility='onchange')
    niche_ids = fields.One2many('niche.application', 'partner_id', string="Columbarium Niche Sheet", track_visibility='onchange')
    # beneficiary_list_ids = fields.One2many('beneficiary.list', 'membership_id', string="Deceased Info", track_visibility='onchange')
    

    @api.depends('membership_start', 'membership_cancel', 'membership_stop', 'is_terminate', 'membership_state', 'membership_number')
    @api.multi
    def _get_membership_status(self):
        today = datetime.now().date()
        membership_status = 'inactive'
        membership_stop = False
        membership_start = False
        for rec in self :
            if rec.membership_stop :
                membership_stop = datetime.strptime(rec.membership_stop, "%Y-%m-%d").date() or False
            if rec.membership_start :
                membership_start = datetime.strptime(rec.membership_start, "%Y-%m-%d").date() or False

            if rec.member and rec.membership_type == 'ordinary' :
                if membership_start <= today <= membership_stop and not self.is_terminate:
                    membership_status = 'active'
                elif self.is_terminate :
                    membership_status = 'terminate'
            
            if rec.membership_type == 'associate' and rec.membership_number :
                membership_status = 'active'
                    
            rec.membership_status = membership_status
            rec.get_membership_state(membership_status)

    @api.multi
    def get_membership_state(self, membership_status):
        self.write({'membership_state' : membership_status})
        
    @api.multi
    def generate_member_id(self) :
        for rec in self :
            rec._generate_membership_id(rec.membership_type)
        
    @api.multi
    def _generate_membership_id(self, membership_type):
        if not self.membership_number :
            if membership_type == 'associate' :
                self.membership_number = self.env['ir.sequence'].next_by_code('associate.membership.number.seq')
            elif membership_type == 'ordinary' :
                self.membership_number = self.env['ir.sequence'].next_by_code('ordinary.membership.number.seq')
            else :
                return {
                    'warning': {
                        'title': "Warning",
                        'message': "We can't find membership type that match with our selection in database.\n Make your membership type are selected and not empty",
                    }
                }
        else :
            return {
                'warning': {
                    'title': "Warning",
                    'message': "You already have a Membership ID.",
                }
            }
                
            

    @api.onchange('membership_start', 'membership_stop')
    @api.multi
    def check_membership_date(self) :
        if self.membership_start and self.membership_stop :
            membership_start = datetime.strptime(self.membership_start, "%Y-%m-%d").date()
            membership_stop = datetime.strptime(self.membership_stop, "%Y-%m-%d").date()

            if membership_start > membership_stop : 
                self.membership_start = False
                self.membership_stop = False
                return {
                    'warning': {
                        'title': "Warning",
                        'message': "Membership Validity cannot more than Membership Expiry",
                    }
                }


    @api.multi
    def button_membership_renewal(self):
    	# view_id = self.env.ref('v11_tpstst_custom.membership_renewal_form_view').id
        context = {'default_partner_id' : self.id}
        return {
            'name': _('Membership Renewal'),
            'view_type':'form',
            'view_mode':'form',
            'res_model': 'generate.voucher.donation',
            'view_id': False,
            'type':'ir.actions.act_window',
            'target':'new',
            'context' : context,
        }
        