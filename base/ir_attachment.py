# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from pprint import pprint
from odoo import fields, models, api, _

class IrAttachmentCategory(models.Model):
    _name = "ir.attachment.category"
    
    name = fields.Char('Category Name', required=True)

class IrAttachment(models.Model):
    """Attachments are used to link binary files or url to any openerp document.

    External attachment storage
    ---------------------------

    The computed field ``datas`` is implemented using ``_file_read``,
    ``_file_write`` and ``_file_delete``, which can be overridden to implement
    other storage engines. Such methods should check for other location pseudo
    uri (example: hdfs://hadoppserver).

    The default implementation is the file:dirname location that stores files
    on the local filesystem using name based on their sha1 hash
    """
    _inherit = "ir.attachment"

    category_id = fields.Many2one('ir.attachment.category', string='Category')
    daily_closing_id = fields.Many2one('bank.acc.rec.statement', string='Daily Closing')
    
    @api.model
    def create(self, values):
        # remove computed field depending of datas
        for field in ('file_size', 'checksum'):
            values.pop(field, False)
        values = self._check_contents(values)
        self.browse().check('write', values=values)
        return super(IrAttachment, self).create(values)
    