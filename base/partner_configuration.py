##############################################################################
import time
from datetime import datetime, date
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta


class HRCitizenshp(models.Model):
    _name = 'hr.citizenship'
    _description = 'Citizenship'
    _order = "is_singaporean desc, name asc"
    _readonly_view = True
    _nodelete_restrict = True

    name = fields.Char(string='Citizenship', required=True, size=64)
    is_singaporean = fields.Boolean('Singaporean?')
    default = fields.Boolean('Default')

    @api.multi
    def write(self, vals):
        if vals.get('default', True):
            get_default = self.env['hr.citizenship'].search([('default', '=', True)], limit=1)
            if get_default:
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(HRCitizenshp, self).write(vals)

    @api.model
    def create(self, vals):
        if vals.get('default', True):
            get_default = self.env['hr.citizenship'].search([('default', '=', True)], limit=1)
            if get_default:
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(HRCitizenshp, self).create(vals)

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result


class PartnerRank(models.Model):
    _name = "partner.rank"
    _description = 'Partner Rank'

    name = fields.Char('Rank')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result


class PartnerRelationship(models.Model):
    _name = "partner.relationship"
    _description = 'Partner Relationship'

    name = fields.Char('Relationship')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result