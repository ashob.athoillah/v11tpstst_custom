# -*- coding: utf-8 -*-
# Part of TigernixERP. See LICENSE file for full copyright and licensing details.


from . import partner
from . import users
from . import company
from . import mail
from . import ir_attachment
from . import partner_configuration