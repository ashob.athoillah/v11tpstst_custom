# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class AccountJournal(models.Model):
    _inherit = "account.journal"
    _readonly_view = True
    _nodelete_restrict = True
    
    @api.multi
    # do not depend on 'refund_sequence_id.date_range_ids', because
    # refund_sequence_id._get_current_sequence() may invalidate it!
    @api.depends('dn_sequence_id.use_date_range', 'dn_sequence_id.number_next_actual')
    def _compute_dn_seq_number_next(self):
        '''Compute 'sequence_number_next' according to the current sequence in use,
        an ir.sequence or an ir.sequence.date_range.
        '''
        for journal in self:
            if journal.dn_sequence_id :#and journal.dn_sequence:
                sequence = journal.dn_sequence_id._get_current_sequence()
                journal.dn_sequence_number_next = sequence.number_next_actual
            else:
                journal.dn_sequence_number_next = 1

    @api.multi
    def _inverse_dn_seq_number_next(self):
        '''Inverse 'dn_sequence_number_next' to edit the current sequence next number.
        '''
        for journal in self:
            if journal.dn_sequence_id and journal.dn_sequence_number_next: # and journal.dn_sequence
                sequence = journal.dn_sequence_id._get_current_sequence()
                sequence.number_next = journal.dn_sequence_number_next

    active = fields.Boolean(default=True, track_visibility='onchange')
    journal_skillsfuture = fields.Boolean(string='Journal SkillsFuture')
    funds_journal = fields.Boolean(string='Funds Journal', help="If this indicator is true, account move with this journal will affect to the funds")
    refund_code = fields.Char(string='Refund Code', size=16, required=False)
    dn_code = fields.Char(string='Debit Note Code', size=16, required=False)
    code = fields.Char(string='Short Code', size=16, required=True, help="The journal entries of this journal will be named using this prefix.")
    dn_sequence_id = fields.Many2one('ir.sequence', string='Debit Note Entry Sequence',
        help="This field contains the information related to the numbering of the debit note entries of this journal.", copy=False)
    dn_sequence_number_next = fields.Integer(string='Debit Notes: Next Number',
        help='The next sequence number will be used for the next credit note.',
        compute='_compute_dn_seq_number_next',
        inverse='_inverse_dn_seq_number_next')
    type = fields.Selection([
            ('sale', 'Sale'),
            ('purchase', 'Purchase'),
            ('cash', 'Cash'),
            ('bank', 'Bank'),
            ('bursary', 'Bursary'),
            ('general', 'Miscellaneous'),
        ], required=True,
        help="Select 'Sale' for customer invoices journals.\n"\
        "Select 'Purchase' for vendor bills journals.\n"\
        "Select 'Cash' or 'Bank' for journals that are used in customer or vendor payments.\n"\
        "Select 'Bursary' for journals that are used in FAS Distributions.\n"\
        "Select 'General' for miscellaneous operations journals.")
    transaction_no = fields.Boolean(default=False, string="Transaction Number")
    
    @api.multi
    def _check_journal_name(self):
        for rec in self:
            records = self.search([('name','=',rec.name)])
            if len(records._ids) > 1:
                return False
        return True
    
    _constraints = [
        (_check_journal_name, 'Journal name already exists!', ['name']),
    ]
    
    @api.model
    def _get_sequence_prefix(self, code, refund=False, refund_code=False, dn=False, dn_code=False):
        prefix = code.upper()
        if refund:
            if not refund_code:
                prefix = 'R' + prefix
            else:
                prefix = refund_code.upper()
        if dn and dn_code:
            prefix = dn_code.upper()
        return prefix + '/'

    @api.model
    def _create_sequence(self, vals, refund=False, dn=False):
        """ Create new no_gap entry sequence for every new Journal"""
        refund_code = vals.get('refund_code','')
        dn_code = vals.get('dn_code','')
        prefix = self._get_sequence_prefix(vals['code'], refund, refund_code=refund_code, dn=dn, dn_code=dn_code)
        refund_string =  _(': Refund')
        dn_string =  _(': Debit Note')
        if self.type == 'sale':
            refund_string =  _(': Credit Note')
        name_string = vals['name']
        code_string = ''
        if refund:
            name_string = vals['name'] + refund_string
            code_string = refund_code
        if dn:
            name_string = vals['name'] + dn_string
            code_string = dn_code
        seq = {
            'name': name_string,
            'code' : code_string,
            'implementation': 'no_gap',
            'prefix': prefix,
            'padding': 6,
            'number_increment': 1,
            'use_date_range': True,
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        seq = self.env['ir.sequence'].create(seq)
        seq_date_range = seq._get_current_sequence()
        seq_date_range.number_next = refund and vals.get('refund_sequence_number_next', 1) or vals.get('sequence_number_next', 1)
        return seq

    @api.multi
    def write(self, vals):
        result = super(AccountJournal, self).write(vals)
        #after write then
        for journal in self:
            if ('refund_code' in vals or 'code' in vals): #need to add 'code' since super method write will overwrite the prefix based on 'code'
                refund_prefix = ''
                refund_code = journal.refund_code or ''
                if refund_code:
                    refund_prefix = self._get_sequence_prefix(refund_code, refund=True, refund_code=refund_code)
                if refund_code:
                    if not journal.refund_sequence_id:
                        if journal.type in ('sale', 'purchase') and journal.refund_sequence:
                            refund_vals = {
                                'name':journal.name,
                                'code':refund_code,
                                'refund_code':refund_code,
                            }
                            refund_sequence_id = self.sudo()._create_sequence(refund_vals, refund=True).id
                            write_res = super(AccountJournal, self).write({'refund_sequence_id': refund_sequence_id})
                    else:
                        #if self.env['account.move'].search([('journal_id', 'in', self.ids)], limit=1):
                        #    raise UserError(_('This journal already contains items, therefore you cannot modify its short name.'))
                        journal.refund_sequence_id.write({'prefix': refund_prefix})
            if ('dn_code' in vals):
                dn_prefix = ''
                dn_code = journal.dn_code or ''
                if dn_code:
                    dn_prefix = self._get_sequence_prefix(dn_code, dn=True, dn_code=dn_code)
                if dn_code:
                    if not journal.dn_sequence_id:
                        if journal.type in ('sale', 'purchase') : #and journal.dn_sequence
                            dn_vals = {
                                'name':journal.name,
                                'code':dn_code,
                                'dn_code':dn_code,
                            }
                            dn_sequence_id = self.sudo()._create_sequence(dn_vals, dn=True).id
                            write_res = super(AccountJournal, self).write({'dn_sequence_id': dn_sequence_id})
                    else:
                        #if self.env['account.move'].search([('journal_id', 'in', self.ids)], limit=1):
                        #    raise UserError(_('This journal already contains items, therefore you cannot modify its short name.'))
                        journal.dn_sequence_id.write({'prefix': dn_prefix})
        return result

    
    