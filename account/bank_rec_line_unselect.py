
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp

import time
import datetime
from dateutil.relativedelta import relativedelta

class BankRecLineUnselect(models.TransientModel):
    _name = 'bank.rec.line.unselect'

    statement_id = fields.Many2one('bank.acc.rec.statement') 
    type = fields.Selection([('dr', 'Debit'), ('cr', 'Credit')], 'Cr/Dr')
    rec_lines = fields.Many2many('bank.acc.rec.statement.line',string='Items')
    
    
    @api.model
    def default_get(self,fields):
        res = {}  
        res = super(BankRecLineUnselect,self).default_get(fields)
        res['statement_id'] = self._context.get('statement_ids',[]) and self._context.get('statement_ids',[])[0] or False
        res['type'] = self._context.get('recline_type') or ''
        return res
    
    
    @api.multi
    def action_update(self):
        rec_line_ids = list(self.rec_lines._ids)
        if not rec_line_ids:
            raise UserError(_("Please select at least one item!"))
        if self.statement_id:
            self.statement_id.unclear_bank_rec_line(recline_ids=rec_line_ids)
        return {'type': 'ir.actions.act_window_close'}