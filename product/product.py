from re import S
import time
from odoo.addons import decimal_precision as dp
from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from itertools import chain, product
from odoo.addons.v11_tpstst_custom.membership import membership_application as membership


class ProductProduct(models.Model):
    _inherit = "product.product"

    valid_price = fields.Float('Price', store=True, compute='_get_curr_price', track_visibility='onchange')
    price_list_ids = fields.One2many('prod.price.list', 'product_id',  string="Price List", track_visibility='onchnage')
    
    
    @api.multi
    def product_info_columbarium(self) :
        for rec in self :
            for level in rec.level_ids :
                level.product_info = 'columbarium'
    
    @api.multi
    def product_info_tablet(self) :
        for rec in self :
            for level in rec.level_ids :
                level.product_info = 'tablet'

    @api.depends('price_list_ids', 'price_list_ids.name', 'price_list_ids.effective_date')
    @api.multi
    def _get_curr_price(self):
        now = datetime.now().date()
        for product in self:
            current_valid_price = 0.00
            latest_effective_date = False
            for price in product.price_list_ids:
                if datetime.strptime(price.effective_date, '%Y-%m-%d').date() <= now:
                    if not latest_effective_date :
                        latest_effective_date = price.effective_date
                    else:
                        if price.effective_date > latest_effective_date:
                            latest_effective_date = price.effective_date
                            
            current_valid_price = product.price_list_ids.filtered(lambda price: price.effective_date == latest_effective_date)
            for price in current_valid_price:
                if price.name > 0.00 :
                    current_valid_price = price.name
                else :
                    current_valid_price = 0.00
            product.valid_price = current_valid_price


class ProductTemplate(models.Model):
    _inherit = "product.template"

    name = fields.Char(string='Name', required=True, translate=False, track_visibility='onchange')
    
    membership_type = fields.Selection(membership.MEMBERSHIP_TYPE, string='Membership Type', track_visibility='onchange')
    membership = fields.Boolean(string="Membership", track_visibility='onchange')
    product_analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account', track_visibility='onchange')

    floor_number = fields.Char('Floor', track_visibility='onchange')
    block_number = fields.Char('Block No.', track_visibility='onchange')
    direction = fields.Char('Direction', track_visibility='onchange')
    level_ids = fields.One2many('level.detail.list', 'product_template_id', string="Level / Block Details", track_visibility='onchange')
    columbarium_product = fields.Boolean('Columbarium', track_visibility='onchange')
    sold_out = fields.Boolean('Sold out ?', track_visibility='onchange')
    tablet_product = fields.Boolean('Tablet', track_visibility='onchange')
    event_product = fields.Boolean('Event', track_visibility='onchange')
    list_price = fields.Float('Sales Price', track_visibility='onchange')
    min_membership_donation = fields.Float('Minimun Donation Required', track_visibility='onchange')
    
    membership_period_id = fields.Many2one('membership.period', string="Membership Period", track_visibility='onchange')
    membership_fee_type = fields.Selection([
        ('application', 'Application'),
        ('renewal', 'Renewal'),
    ], string="Membership Fee Type", track_visibility='onchange')

    slot_total = fields.Integer('All Slot',  store=True, compute="_compute_overall_slot", track_visibility='onchange')
    slot_used = fields.Integer('All Slot Used', store=True, compute="_compute_overall_slot", track_visibility='onchange')
    slot_left = fields.Integer('All Slot Left',  store=True, compute="_compute_overall_slot", strack_visibility='onchange')
    slot_booked = fields.Integer('All Slot Booked', store=True, strack_visibility='onchange')
    # qty_available = fields.Float(
    #     'Quantity On Hand', store=True, compute='_compute_quantities', search='_search_qty_available',
    #     digits=dp.get_precision('Product Unit of Measure'),
    #     help="Current quantity of products.\n"
    #          "In a context with a single Stock Location, this includes "
    #          "goods stored at this Location, or any of its children.\n"
    #          "In a context with a single Warehouse, this includes "
    #          "goods stored in the Stock Location of this Warehouse, or any "
    #          "of its children.\n"
    #          "stored in the Stock Location of the Warehouse of this Shop, "
    #          "or any of its children.\n"
    #          "Otherwise, this includes goods stored in any Stock Location "
    #          "with 'internal' type.")
    
    @api.multi
    def toggle_is_sold(self) :
        if self.slot_left > 0 :
            self.sold_out = False
        else :
            self.sold_out = True

    @api.multi
    def get_current_item_status(self, sold_out) :
        for rec in self :
            rec.sold_out = sold_out

    @api.depends('level_ids', 'level_ids.slot', 'level_ids.slot_used', 'level_ids.slot_booked')
    @api.multi
    def _compute_overall_slot(self) :
        for rec in self :
            slot = 0
            slot_left = 0
            slot_used = 0
            sold_out = False
            slot_booked = 0
            for level in rec.level_ids :
                slot += level.slot
                slot_left += level.slot_left
                slot_used += level.slot_used
                slot_booked += level.slot_booked

            if slot_left == 0 and slot_booked == 0 :
                sold_out = True

            rec.slot_total = slot
            rec.slot_left = slot_left
            rec.slot_used = slot_used
            rec.slot_booked = slot_booked
            rec.get_current_item_status(sold_out)

class LevelDetailList(models.Model) :
    _name = "level.detail.list"
    _description = "Level Detail List"
    _order = "level_order desc"

    name = fields.Char(string='Name', required=True, translate=False, track_visibility='onchange')
    ref_number = fields.Char('Ref', store=True, compute="_get_ref_number", track_visibility='onchange')
    price = fields.Float('Price', track_visibility='onchange')
    is_sold = fields.Boolean('Is sold ?', track_visibility='onchange')
    slot = fields.Integer('Total slot', store=True, default=1, compute="_compute_overall_tablet_slot", track_visibility='onchange')
    slot_used = fields.Integer('Total slot used', store=True, compute="_compute_overall_tablet_slot", inverse="_set_niche_slot", track_visibility='onchange')
    slot_left = fields.Integer('Total slot left', store=True, compute="_compute_overall_tablet_slot", track_visibility='onchange')
    slot_booked = fields.Integer('Total Slot Booked', store=True, compute="_compute_overall_tablet_slot", inverse="_set_niche_slot", track_visibility='onchange')
    product_template_id = fields.Many2one('product.template', string="Related Product", track_visibility='onchange')
    side_position_id = fields.Many2one('level.position', string="Related Position",  track_visibility='onchange')
    product_info = fields.Selection([
                                    ('columbarium', 'Columbarium'),
                                    ('tablet', 'Tablet')
                                    ], string="Product Info")
                                    
    side_detail_ids = fields.One2many('side.detail.list', 'side_id', string="Unit No. Detail")
    
    level_order = fields.Char(string='Name', store=True, compute="_compute_level_order", translate=False)
    
    @api.depends('name', 'product_template_id.block_number', 'product_template_id.name', 'product_info')
    @api.multi
    def _get_ref_number(self) :
        for rec in self :
            ref = ""
            if rec.product_info == 'columbarium' and rec.name :
                ref = str(rec.product_template_id.block_number) + '-' + (rec.name if len(rec.name) >= 2 else '0' + rec.name) + '-' + str(rec.product_template_id.name)
            rec.ref_number = ref
    
    # to differenciate order between tablet and niche
    @api.depends('name', 'product_info')
    @api.multi
    def _compute_level_order(self) :
        level_order = False
        for rec in self :
            if rec.product_info == 'columbarium' :
                level_order = rec.name
            
            rec.level_order = level_order
                
                                    
    @api.onchange('product_template_id')
    def check_product_info(self) :  
        for rec in self :
            product = rec.product_template_id
            product_info = rec.product_info
            if product :
                if product.columbarium_product :
                    product_info = 'columbarium'
                else :
                    product_info = 'tablet'

            rec.product_info = product_info

    @api.onchange('slot', 'slot_used', 'slot_booked')
    @api.multi
    def slot_validation(self) :
        for rec in self :
            slot = rec.slot
            slot_used = rec.slot_used
            slot_booked = rec.slot_booked
            if slot_used > slot or slot_booked > slot or (slot_used + slot_booked) > slot :
                return {'warning': {
                        'title': "Warning",
                        'message': "Slot used and/or booked cannot more than the total slot itself!",
                        }
                    }
            
    @api.multi
    def toggle_is_sold(self) :
        if self.is_sold :
            self.is_sold = False
        else :
            self.is_sold = True
    

    # @api.depends('slot', 'slot_used', 'slot_booked')
    # @api.multi
    # def _compute_slot_tablet(self) :
    #     slot_left = 0
    #     is_sold = False
    #     for rec in self :
    #         slot = rec.slot
    #         slot_used = rec.slot_used
    #         slot_booked = rec.slot_booked
    #         if slot > 0  :
    #             slot_left = slot - (slot_used + slot_booked)
    #             if slot_left == 0 and slot_booked == 0 :
    #                 is_sold = True
    #         else :
    #             slot_left = 1
    #
    #         rec.is_sold = is_sold
    #         rec.slot_left = slot_left
    #         rec.slot_validation()

    def _set_niche_slot(self):
        pass

    @api.depends('side_detail_ids', 'side_detail_ids.slot', 'side_detail_ids.slot_used', 'side_detail_ids.slot_booked')
    @api.multi
    def _compute_overall_tablet_slot(self):
        for rec in self:
            if rec.product_info == 'tablet':
                slot = 0
                slot_left = 0
                slot_used = 0
                sold_out = False
                slot_booked = 0
                for level in rec.side_detail_ids:
                    slot += level.slot
                    slot_left += level.slot_left
                    slot_used += level.slot_used
                    slot_booked += level.slot_booked

                if slot_left == 0 and slot_booked == 0:
                    sold_out = True

                rec.slot = slot
                rec.slot_left = slot_left
                rec.slot_used = slot_used
                rec.slot_booked = slot_booked
                rec.get_current_item_status(sold_out)
            else :
                is_sold = False
                slot = rec.slot
                slot_used = rec.slot_used
                slot_booked = rec.slot_booked
                if slot > 0  :
                    slot_left = slot - (slot_used + slot_booked)
                    if slot_left == 0 and slot_booked == 0 :
                        is_sold = True
                else :
                    slot_left = 1

                rec.is_sold = is_sold
                rec.slot_left = slot_left
                rec.slot_validation()

    @api.multi
    def get_current_item_status(self, sold_out):
        for rec in self:
            rec.sold_out = sold_out

# @api.multi
    # def get_current_unit_slot_total(self) :
    #     for rec in self :
    #         if rec.product_info == 'tablet' :
    #             total_slot_used = 0
    #             total_slot_booked = 0
    #             total_slot = 0
    #             for unit in rec.side_detail_ids :
    #                 total_slot_used += unit.slot_used
    #
    #             if rec.slot_used != total_slot_used :
    #                 rec.slot_used = total_slot_used

    

class SideDetailList(models.Model) :
    _name = "side.detail.list"
    _description = "Side Detail List"

    name = fields.Char(string='Unit No.', required=True, translate=False, track_visibility='onchange')
    is_sold = fields.Boolean('Is available ?', track_visibility='onchange')
    slot = fields.Integer('Total slot', store=True, default=1, compute="_compute_overall_slot", track_visibility='onchange')
    slot_used = fields.Integer('Total slot used', store=True, compute="_compute_overall_slot", track_visibility='onchange')
    slot_left = fields.Integer('Total slot left', store=True, compute="_compute_overall_slot",track_visibility='onchange')
    slot_booked = fields.Integer('Total Slot Booked', store=True, compute="_compute_overall_slot", track_visibility='onchange')
    side_id = fields.Many2one('level.detail.list', string="Related Block No.", track_visibility='onchange')
    unit_level_ids = fields.One2many('side.level.list', 'unit_id', string="Unit No. Level Detail")

    @api.onchange('slot', 'slot_used', 'slot_booked')
    @api.multi
    def slot_validation(self) :
        for rec in self :
            slot = rec.slot
            slot_used = rec.slot_used
            slot_booked = rec.slot_booked
            if slot_used > slot or slot_booked > slot or (slot_used + slot_booked) > slot :
                return {'warning': {
                        'title': "Warning",
                        'message': "Slot used and/or booked cannot more than the total slot itself!",
                        }
                    }
            
    @api.multi
    def toggle_is_sold(self) :
        if self.is_sold :
            self.is_sold = False
        else :
            self.is_sold = True
    
    @api.multi
    def get_current_item_status(self, sold_out) :
        for rec in self :
            rec.sold_out = sold_out

    @api.depends('unit_level_ids', 'unit_level_ids.slot', 'unit_level_ids.slot_used', 'unit_level_ids.slot_booked')
    @api.multi
    def _compute_overall_slot(self) :
        for rec in self :
            slot = 0
            slot_left = 0
            slot_used = 0
            sold_out = False
            slot_booked = 0
            for level in rec.unit_level_ids :
                slot += level.slot
                slot_left += level.slot_left
                slot_used += level.slot_used
                slot_booked += level.slot_booked

            if slot_left == 0 and slot_booked == 0 :
                sold_out = True

            rec.slot = slot
            rec.slot_left = slot_left
            rec.slot_used = slot_used
            rec.slot_booked = slot_booked
            rec.get_current_item_status(sold_out)
            
class SideLevelList(models.Model) :
    _name = "side.level.list"
    _description = "Side Level List"

    name = fields.Char(string='Level', required=True, translate=False, track_visibility='onchange')
    is_sold = fields.Boolean('Is available ?', track_visibility='onchange')
    slot = fields.Integer('Total slot', store=True, default=1, track_visibility='onchange')
    slot_used = fields.Integer('Total slot used', store=True, track_visibility='onchange')
    slot_left = fields.Integer('Total slot left', store=True, compute="_compute_slot_tablet",track_visibility='onchange')
    slot_booked = fields.Integer('Total Slot Booked', store=True, track_visibility='onchange')
    unit_id = fields.Many2one('side.detail.list', string="Related Unit No.", track_visibility='onchange')

    @api.onchange('slot', 'slot_used', 'slot_booked')
    @api.multi
    def slot_validation(self) :
        for rec in self :
            slot = rec.slot
            slot_used = rec.slot_used
            slot_booked = rec.slot_booked
            if slot_used > slot or slot_booked > slot or (slot_used + slot_booked) > slot :
                return {'warning': {
                        'title': "Warning",
                        'message': "Slot used and/or booked cannot more than the total slot itself!",
                        }
                    }
            
    @api.multi
    def toggle_is_sold(self) :
        if self.is_sold :
            self.is_sold = False
        else :
            self.is_sold = True
    

    @api.depends('slot', 'slot_used', 'slot_booked', 'is_sold')
    @api.multi
    def _compute_slot_tablet(self) :
        slot_left = 0
        is_sold = False
        for rec in self :
            slot = rec.slot
            slot_used = rec.slot_used
            slot_booked = rec.slot_booked
            if slot > 0  :
                slot_left = slot - (slot_used + slot_booked)
                if slot_left == 0 and slot_booked == 0 :
                    is_sold = True
            else :
                slot_left = 1
                
            rec.is_sold = is_sold
            rec.slot_left = slot_left
            rec.slot_validation()