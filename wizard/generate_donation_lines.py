from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from datetime import datetime, timedelta

class GenerateDonationLines(models.Model):
    _name = "generate.donation.line"
    _description = 'Generate Donation Line'

    amount = fields.Float('Amount')
    charity = fields.Boolean('Charity / Coffin', default=False)
    festival = fields.Boolean('Festival', default=False)
    medical = fields.Boolean('Medical', default=False)
    trunk = fields.Boolean('Trunk', default=False)
    other = fields.Boolean('Other', default=False)
    donation_id = fields.Many2one('donation.donation', string="Donation")

    @api.multi
    def action_donation_list(self) :
        company = self.env.user.company_id
        config = self.env['res.company'].search([('id','=',company.id)])
        
        if self.amount > 0 :
            donation = self.env['donation.donation'].search([('id','=', self.donation_id.id)])
            if self.charity:
                charity_fund = config.default_charity_fund_id.id
            else:
                charity_fund = None
            if self.festival:
                festival_fund = config.default_festival_fund_id.id
            else:
                festival_fund = None
            if self.medical:
                medical_fund = config.default_medical_fund_id.id
            else:
                medical_fund = None
            if self.trunk:
                trunk_fund = config.default_trunk_fund_id.id
            else:
                trunk_fund = None
            if self.other:
                other_fund = config.default_trunk_fund_id.id
            else:
                other_fund = None
            if donation and not self.charity and not self.festival and not self.medical and not self.trunk and not self.other:
                donation.generate_donation_list(config.default_charity_fund_id.id, config.default_other_fund_id.id, config.default_festival_fund_id.id, config.default_medical_fund_id.id, config.default_trunk_fund_id.id,True, True, True, True, True, self.amount)
            else: 
                donation.generate_donation_list(charity_fund,festival_fund,medical_fund,trunk_fund,other_fund,self.charity, self.festival, self.medical, self.trunk, self.other, self.amount)
        else :
            raise ValidationError(_("Amount must be more than 0"))