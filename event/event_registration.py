from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.exceptions import ValidationError, UserError

import pytz
from pytz import timezone
from itertools import chain

class EventRegistration(models.Model):
    _inherit = 'event.registration'

    #Applicant Data
    partner_id = fields.Many2one('res.partner', string="Partner", track_visibility='onchange')
    
    reg_no = fields.Char('Registration Number /  编号', track_visibility='onchange')
    reg_date = fields.Date(string='Reg Date', default=lambda self: fields.datetime.now(), track_visibility='onchange')

    event_type = fields.Selection([('free','Free'),('paid','Paid')], string="Event Type", track_visibility='onchange')

    title_id = fields.Many2one('res.partner.title', string="Title / 称号", track_visibility='onchange')
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility='onchange')

    event_start_date = fields.Date('Event Start Date', track_visibility='onchange')
    event_end_date = fields.Date('Event End Date', track_visibility='onchange')
    event_begin_date 	= fields.Datetime(string="Event Start Date", related='event_id.date_begin',readonly=True)
    event_end_date 	= fields.Datetime(string="Event End Date", related='event_id.date_end',readonly=True)

    app_id = fields.Many2one('participant.registration', 'Related Form')

    birthdate = fields.Date('Date of Birth / 出生日期', track_visibility='onchange')
    mobile = fields.Char('Mobile / 手机', track_visibility='onchange')
    phone = fields.Char('Tel / 电话', track_visibility='onchange')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string="Gender / 性别", track_visibility='onchange')
    street = fields.Char(string='Street', track_visibility='onchange')
    street2 = fields.Char(string='Street 2', track_visibility='onchange')
    zip = fields.Char(string='Postal Code', track_visibility='onchange')
    dialect_ids = fields.Many2many('dialect.list', 'evreg_dialect_list_rel', string="Dialect / 籍贯", track_visibility='onchange')
    occupation = fields.Char('Occupation / 职业', track_visibility='onchange')
    mailing_address = fields.Char('Mailing Address / 邮寄地址', track_visibility='onchange')
    country_id = fields.Many2one('res.country', string='Country', track_visibility='onchange')


    responsible_user_id = fields.Many2one('res.users', copy=False, string='Attended by / 经手人', track_visibility='onchange')
    no_of_people = fields.Integer('Total Person Accompanied', defaull=1, track_visibility='onchange') 
    session_id = fields.Many2one('event.session', 'Session', track_visibility='onchange')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.reg_no:
                name = str(rec.name) + '[' + (rec.reg_no) + ']'
            else:
                name = str(rec.name)
            result.append((rec.id, name))

        return result

    @api.multi
    def action_confirm(self) :
        self.name_set()

    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no :
            self.reg_no = self.env['ir.sequence'].next_by_code('event.registration.seq')
    
    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id

    @api.one
    def button_reg_close(self):
        """ Close Registration """
        today = fields.Datetime.now()
        self.get_responsible_user()
        if self.event_id.date_begin <= today and self.event_id.state == 'confirm':
            self.write({'state': 'done', 'date_closed': today})
        elif self.event_id.state == 'draft':
            raise UserError(_("You must wait the event confirmation before doing this action."))
        else:
            raise UserError(_("You must wait the event starting day before doing this action."))