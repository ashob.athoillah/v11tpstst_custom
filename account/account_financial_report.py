# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import copy
from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval
from odoo.tools.misc import formatLang, format_date
from odoo.tools import float_is_zero
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo.addons.account_reports.models.account_financial_report import FormulaLine, FormulaContext


import calendar
import copy
import json
import io
import logging
import lxml.html

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    # TODO saas-17: remove the try/except to directly import from misc
    import xlsxwriter


_logger = logging.getLogger(__name__)


class report_account_general_ledger(models.AbstractModel):
    _inherit = "account.general.ledger"
    
    filter_account = None
#    filter_show_zero = None
    
    def get_columns_name(self, options):
        return [{'name': 'Document Ref No'},
                {'name': _("Journal Entry No")},
                {'name': _("Date"), 'class': 'date'},
                {'name': _("Description")},
                {'name': _("Partner")},
                {'name': _("Currency"), 'class': 'number'},
                {'name': _("Debit"), 'class': 'number'},
                {'name': _("Credit"), 'class': 'number'},
                {'name': _("Balance"), 'class': 'number'}]


    @api.model
    def get_lines(self, options, line_id=None):
        lines = []
        context = self.env.context
        account_ids = []
        account_obj = self.env['account.account']
        if options.get('account'):
            account_ids = [acc.get('id') for acc in options.get('account') if acc.get('selected')]
            if account_ids:
                accounts = account_obj.browse(account_ids)
                self = self.with_context(
                                        account_ids=accounts, 
                                        )
        company_id = self.env.user.company_id
        dt_from = options['date'].get('date_from')
        line_id = line_id and int(line_id.split('_')[1]) or None
        aml_lines = []
        # Aml go back to the beginning of the user chosen range but the amount on the account line should go back to either the beginning of the fy or the beginning of times depending on the account
        grouped_accounts = self.with_context(date_from_aml=dt_from, date_from=dt_from and company_id.compute_fiscalyear_dates(datetime.strptime(dt_from, "%Y-%m-%d"))['date_from'] or None).group_by_account_id(options, line_id)
        sorted_accounts = sorted(grouped_accounts, key=lambda a: a.code)
        unfold_all = context.get('print_mode') and len(options.get('unfolded_lines')) == 0
        total_credit = total_debit = total_balance = 0.0
        currencies = self.env['res.currency'].search([])
        currency_dict = {}
        for currency in currencies:
            currency_dict[currency.id] = currency
        used_currency = self.env.user.company_id.currency_id
        for account in sorted_accounts:
            debit = grouped_accounts[account]['debit']
            credit = grouped_accounts[account]['credit']
            balance = grouped_accounts[account]['balance']
            total_credit += credit
            total_debit += debit
            total_balance += balance
            amount_currency = '' if not account.currency_id else self.format_value(grouped_accounts[account]['amount_currency'], currency=account.currency_id)
            lines.append({
                'id': 'account_%s' % (account.id,),
                'name': account.code + " " + account.name,
                'columns': [{'name': v} for v in [amount_currency, self.format_value(debit), self.format_value(credit), self.format_value(balance)]],
                'level': 2,
                'unfoldable': True,
                'unfolded': 'account_%s' % (account.id,) in options.get('unfolded_lines') or unfold_all,
                'colspan': 5,
            })
            if 'account_%s' % (account.id,) in options.get('unfolded_lines') or unfold_all:
                initial_debit = grouped_accounts[account]['initial_bal']['debit']
                initial_credit = grouped_accounts[account]['initial_bal']['credit']
                initial_balance = grouped_accounts[account]['initial_bal']['balance']
                initial_currency = '' if not account.currency_id else self.format_value(grouped_accounts[account]['initial_bal']['amount_currency'], currency=account.currency_id)
                domain_lines = [{
                    'id': 'initial_%s' % (account.id,),
                    'class': 'o_account_reports_initial_balance',
                    'name': _('Initial Balance'),
                    'parent_id': 'account_%s' % (account.id,),
                    'columns': [{'name': v} for v in ['','', '', '', initial_currency, self.format_value(initial_debit), self.format_value(initial_credit), self.format_value(initial_balance)]],
                }]
                progress = initial_balance
                amls = amls_all = grouped_accounts[account]['lines']
                too_many = False
                if len(amls) > 80 and not context.get('print_mode'):
                    amls = amls[:80]
                    too_many = True
                for line in amls:
                    if options.get('cash_basis'):
                        line_debit = line['debit_cash_basis']
                        line_credit = line['credit_cash_basis']
                    else:
                        line_debit = line['debit']
                        line_credit = line['credit']
                    company_currency = currency_dict.get('company_currency_id') or False
                    if company_currency:
                        line_debit = company_currency.compute(line_debit, used_currency)
                        line_credit = company_currency.compute(line_credit, used_currency)
                    progress = progress + line_debit - line_credit
                    currency = "" if not line['currency_id'] else self.with_context(no_format=False).format_value(line['amount_currency'], currency=currency_dict.get(line['currency_id'],None))
                    name = []
                    name = line['name'] and line['name'] or ''
                    if line['ref']:
                        name = name and name + ' - ' + line['ref'] or line['ref']
                    if len(name) > 35 and not self.env.context.get('no_format'):
                        name = name[:32] + "..."
                    partner_name = line['partner_name']
                    if partner_name and len(partner_name) > 35  and not self.env.context.get('no_format'):
                        partner_name = partner_name[:32] + "..."
                    caret_type = 'account.move'
                    invoice_id = False
                    if line['invoice_id']:
                        invoice_id = self.env['account.invoice'].browse(line['invoice_id'])
                        caret_type = 'account.invoice.in' if line['inv_type'] in ('in_refund', 'in_invoice') else 'account.invoice.out'
                    elif line['payment_id']:
                        caret_type = 'account.payment'

                    # request to have invoice no in the expense account
                    expense_account_type_id = self.env.ref('account.data_account_type_expenses')
                    move_name = line['move_name'] if line['move_name'] else '/'
                    if expense_account_type_id.id == account.user_type_id.id:
                        if invoice_id and invoice_id.type in ['in_invoice','in_refund'] and invoice_id.reference:
                            move_name = invoice_id.reference or (line['move_name'] if line['move_name'] else '/') or '/'

                    line_value = {
                        'id': line['id'],
                        'caret_options': caret_type,
                        'parent_id': 'account_%s' % (account.id,),
                        'name': move_name,
                        'columns': [{'name': v} for v in 
                                        [
                                        line['je_no'],
                                        format_date(self.env, line['date']), name, partner_name, currency,
                                        line_debit != 0 and self.format_value(line_debit) or '',
                                        line_credit != 0 and self.format_value(line_credit) or '',
                                        self.format_value(progress)]],
                        'level': 4,
                    }
                    aml_lines.append(line['id'])
                    domain_lines.append(line_value)
                domain_lines.append({
                    'id': 'total_' + str(account.id),
                    'class': 'o_account_reports_domain_total',
                    'parent_id': 'account_%s' % (account.id,),
                    'name': _('Total '),
                    'columns': [{'name': v} for v in ['','', '', '', amount_currency, self.format_value(debit), self.format_value(credit), self.format_value(balance)]],
                })
                if too_many:
                    domain_lines.append({
                        'id': 'too_many' + str(account.id),
                        'parent_id': 'account_%s' % (account.id,),
                        'name': _('There are more than 80 items in this list, click here to see all of them'),
                        'colspan': 8,
                        'columns': [{}],
                        'action': 'view_too_many',
                        'action_id': 'account,%s' % (account.id,),
                    })
                lines += domain_lines

        journals = [j for j in options.get('journals') if j.get('selected')]
        if len(journals) == 1 and journals[0].get('type') in ['sale', 'purchase'] and not line_id:
            #total = self._get_journal_total()
            total = {
                'debit':total_debit,
                'credit':total_credit,
                'balance':total_balance,
            }
            lines.append({
                'id': 0,
                'class': 'total',
                'name': _('Total'),
                'columns': [{'name': v} for v in ['','', '', '', '', self.format_value(total['debit']), self.format_value(total['credit']), self.format_value(total['balance'])]],
                'level': 1,
                'unfoldable': False,
                'unfolded': False,
            })
            lines.append({
                'id': 0,
                'name': _('Tax Declaration'),
                'columns': [{'name': v} for v in ['','', '', '', '', '', '', '']],
                'level': 1,
                'unfoldable': False,
                'unfolded': False,
            })
            lines.append({
                'id': 0,
                'name': _('Name'),
                'columns': [{'name': v} for v in ['','', '', '', '', _('Base Amount'), _('Tax Amount'), '']],
                'level': 2,
                'unfoldable': False,
                'unfolded': False,
            })
            for tax, values in self._get_taxes(journals[0]).items():
                lines.append({
                    'id': '%s_tax' % (tax.id,),
                    'name': tax.name + ' (' + str(tax.amount) + ')',
                    'caret_options': 'account.tax',
                    'unfoldable': False,
                    'columns': [{'name': v} for v in ['','', '', '', '', values['base_amount'], values['tax_amount'], '']],
                    'level': 4,
                })

        if self.env.context.get('aml_only', False):
            return aml_lines
        return lines

    def apply_account_filter(self, options):
        if not options.get('account'):
            return options
        options_filter = options['account'].get('filter')
        if not options_filter:
            return options
        account_obj = self.env['account.account']
        if options_filter == 'custom':
            account_id = options['account'].get('account', False)
            if account_id:
                account = account_obj.browse(account_id)
                if account:
                    options['account']['string'] = account.name
            return options
        options['account']['string'] = 'Accounts'
        return options
    
    def _get_filter_journal(self):
        return self.env['account.journal'].search([])

    @api.model
    def get_journal(self):
        journal_read = self._get_filter_journal()
        journals = []
        for journal in journal_read:
            journals.append({'id': journal.id, 'name': journal.name, 'code': journal.code, 'selected': False})
        return journals

    def _get_filter_account(self):
        return self.env['account.account'].search([])

    @api.model
    def get_account(self):
        account_read = self._get_filter_account()
        accounts = []
        for acc in account_read:
            accounts.append({'id': acc.id, 'name': acc.name, 'code': acc.code, 'selected': False})
        return accounts

    def group_by_account_id(self, options, line_id):
        accounts = {}
        results = self.do_query(options, line_id)
        initial_bal_date_to = datetime.strptime(self.env.context['date_from_aml'], "%Y-%m-%d") + timedelta(days=-1)
        initial_bal_results = self.with_context(date_to=initial_bal_date_to.strftime('%Y-%m-%d')).do_query(options, line_id)
        unaffected_earnings_xml_ref = self.env.ref('account.data_unaffected_earnings')
        unaffected_earnings_line = True  # used to make sure that we add the unaffected earning initial balance only once
        if unaffected_earnings_xml_ref:
            #compute the benefit/loss of last year to add in the initial balance of the current year earnings account
            last_day_previous_fy = self.env.user.company_id.compute_fiscalyear_dates(datetime.strptime(self.env.context['date_from_aml'], "%Y-%m-%d"))['date_from'] + timedelta(days=-1)
            unaffected_earnings_results = self.with_context(date_to=last_day_previous_fy.strftime('%Y-%m-%d'), date_from=False).do_query_unaffected_earnings(options, line_id)
            unaffected_earnings_line = False
        context = self.env.context
        base_domain = [('date', '<=', context['date_to']), ('company_id', 'in', context['company_ids'])]
        if context.get('journal_ids'):
            base_domain.append(('journal_id', 'in', context['journal_ids']))
        if context['date_from_aml']:
            base_domain.append(('date', '>=', context['date_from_aml']))
        if context['state'] == 'posted':
            base_domain.append(('move_id.state', '=', 'posted'))
        if context.get('account_tag_ids'):
            base_domain += [('account_id.tag_ids', 'in', context['account_tag_ids'].ids)]
        if context.get('analytic_tag_ids'):
            base_domain += ['|', ('analytic_account_id.tag_ids', 'in', context['analytic_tag_ids'].ids), ('analytic_tag_ids', 'in', context['analytic_tag_ids'].ids)]
        if context.get('analytic_account_ids'):
            base_domain += [('analytic_account_id', 'in', context['analytic_account_ids'].ids)]
        for account_id, result in results.items():
            domain = list(base_domain)  # copying the base domain
            domain.append(('account_id', '=', account_id))
            account = self.env['account.account'].browse(account_id)
            accounts[account] = result
            accounts[account]['initial_bal'] = initial_bal_results.get(account.id, {'balance': 0, 'amount_currency': 0, 'debit': 0, 'credit': 0})
            if account.user_type_id.id == self.env.ref('account.data_unaffected_earnings').id and not unaffected_earnings_line:
                #add the benefit/loss of previous fiscal year to the first unaffected earnings account found.
                unaffected_earnings_line = True
                for field in ['balance', 'debit', 'credit']:
                    accounts[account]['initial_bal'][field] += unaffected_earnings_results[field]
                    accounts[account][field] += unaffected_earnings_results[field]
            #use query_get + with statement instead of a search in order to work in cash basis too
            aml_ctx = {}
            if context.get('date_from_aml'):
                aml_ctx = {
                    'strict_range': True,
                    'date_from': context['date_from_aml'],
                }
            aml_ids = self.with_context(**aml_ctx)._do_query(options, account_id, group_by_account=False)
            aml_ids = [x[0] for x in aml_ids]
            aml_res = []
            if aml_ids:
                aml_query = '''
                    select 
                            aml.id,
                            mv.je_no,
                            coalesce(aml.debit_cash_basis,0.0) as debit_cash_basis,
                            coalesce(aml.credit_cash_basis,0.0) as credit_cash_basis,
                            coalesce(aml.debit,0.0) as debit,
                            coalesce(aml.credit,0.0) as credit,
                            aml.currency_id,
                            coalesce(aml.amount_currency,0.0) as amount_currency,
                            aml.name,
                            aml.ref,
                            aml.partner_id,
                            partner.name as partner_name,
                            aml.invoice_id,
                            inv.type as inv_type,
                            aml.payment_id,
                            aml.move_id,
                            mv.name as move_name,
                            aml.date,
                            aml.company_id,
                            comp.currency_id as company_currency_id
                    from account_move_line aml
                    join account_move mv on mv.id=aml.move_id
                    left join res_partner partner on partner.id=aml.partner_id
                    left join account_invoice inv on inv.id=aml.invoice_id
                    left join account_payment payment on payment.id=aml.payment_id
                    left join res_company comp on comp.id=aml.company_id
                    where aml.id in (%s)
                    ORDER BY aml.date,aml.id
                '''%(str(aml_ids)[1:-1])
                self._cr.execute(aml_query)
                aml_res = self._cr.dictfetchall()
            accounts[account]['lines'] = aml_res#self.env['account.move.line'].browse(aml_ids)
        #if the unaffected earnings account wasn't in the selection yet: add it manually
        user_currency = self.env.user.company_id.currency_id
        if not unaffected_earnings_line and not float_is_zero(unaffected_earnings_results['balance'], precision_digits=user_currency.decimal_places):
            #search an unaffected earnings account
            unaffected_earnings_account = self.env['account.account'].search([
                ('user_type_id', '=', self.env.ref('account.data_unaffected_earnings').id), ('company_id', 'in', self.env.context.get('company_ids', []))
            ], limit=1)
            if unaffected_earnings_account and (not line_id or unaffected_earnings_account.id == line_id):
                accounts[unaffected_earnings_account[0]] = unaffected_earnings_results
                accounts[unaffected_earnings_account[0]]['initial_bal'] = unaffected_earnings_results
                accounts[unaffected_earnings_account[0]]['lines'] = []
        return accounts

    @api.model
    def get_options(self, previous_options={}):
        if not previous_options:
            previous_options = {}
        #if 'show_zero' not in previous_options:
        #    previous_options['show_zero'] = False
        #filter_show_zero = previous_options.get('show_zero',False)
        #self.filter_show_zero = filter_show_zero
        self.filter_account = {'account': False} #, 'filter': user centre
        date_from = self._context.get('wizard_date_from')
        date_to = self._context.get('wizard_date_to')
        asof_date = self._context.get('asof_date')
        if date_from or date_to:
            if not previous_options.get('date'):
                date_dict = {
                            'filter': 'custom', 
                            'string':'Custom filter'
                            }
                if date_from:
                    date_dict['date_from'] = date_from
                if date_to:
                    date_dict['date_to'] = date_to
                previous_options['date'] = date_dict
        elif asof_date and not previous_options.get('date'):
            to_format = '%d/%m/%Y'
            lang = self.env.user.lang
            lang_obj = self.env['res.lang']
            if lang:
                langs = lang_obj.search([('code','=',lang)])
                if langs:
                   to_format = langs[0].date_format
            formatted_asof_date = datetime.strptime(asof_date,'%Y-%m-%d').strftime(to_format)
            previous_options['date'] = {'filter': 'custom', 'string': 'As of %s'%formatted_asof_date, 'date': asof_date}
            
        if self._context.get('journal') and not previous_options.get('journals'):
            previous_options['journals'] = self._context['journal']
        if  not previous_options.get('journals'):
            previous_options['journals'] = self.get_journal()
        if self._context.get('account') and not previous_options.get('account'):
            previous_options['account'] = self._context['account']
        if  not previous_options.get('account'):
            previous_options['account'] = self.get_account()
#        if  not previous_options.get('show_zero'):
#            previous_options['show_zero'] = False
        res =  super(report_account_general_ledger, self).get_options(previous_options)
        #filter_show_zero = res.get('show_zero',False)
        #self.filter_show_zero = filter_show_zero
        #self.filter_account = self.get_account()
        return res


class ReportAccountFinancialReport(models.Model):
    _inherit = "account.financial.html.report"
    _readonly_view = True
    _nodelete_restrict = True

    show_zero = fields.Boolean('Show Zero Amount')
    show_all_funds = fields.Boolean('Show All Funds Column')
    filter_funds = None
    
    def get_columns_name(self, options):
        columns = [{'name': ''}]
        options['date']['filter'] = 'custom'
        if self.debit_credit and not options.get('comparison', {}).get('periods', False):
            columns += [{'name': _('Debit'), 'class': 'number'}, {'name': _('Credit'), 'class': 'number'}]
        dt_to = options['date'].get('date_to') or options['date'].get('date')
        columns += [{'name': self.format_date(dt_to, options['date'].get('date_from', False), options), 'class': 'number'}]
        if options.get('comparison') and options['comparison'].get('periods'):
            for period in options['comparison']['periods']:
                columns += [{'name': period.get('string'), 'class': 'number'}]
            if options['comparison'].get('number_period') == 1:
                columns += [{'name': '%', 'class': 'number'}]
        return columns
    
    def apply_funds_filter(self, options):
        if not options.get('funds'):
            return options
        options_filter = options['funds'].get('filter')
        if not options_filter:
            return options
        funds_type_obj = self.env['type.of.funds']
        if options_filter == 'custom':
            funds_type_id = options['funds'].get('funds', False)
            if funds_type_id:
                funds_type = funds_type_obj.browse(funds_type_id)
                if funds_type:
                    options['funds']['string'] = funds_type.name
            return options
        options['funds']['string'] = 'Funds'
        return options


    def _get_filter_funds(self):
        return self.env['type.of.funds'].search([])

    @api.model
    def get_funds(self):
        funds_read = self._get_filter_funds()
        funds = []
        funds.append({'id': False, 'name': 'All', 'code': 'All', 'selected': True,'total':True})
        for f in funds_read:
            funds.append({'id': f.id, 'name': f.name, 'code': f.name, 'selected': False,'total':False})
        return funds

    @api.model
    def get_options(self, previous_options={}):
        self.filter_show_zero = False
        if self.show_zero:
            self.filter_show_zero = True
        self.filter_show_all_funds = False
        if self.show_all_funds:
            self.filter_show_all_funds = True
        
        if not previous_options:
            previous_options = {}
            
        date_from = self._context.get('wizard_date_from')
        date_to = self._context.get('wizard_date_to')
        if date_from or date_to:
            if not previous_options.get('date'):
                date_dict = {
                            'filter': 'custom', 
                            'string':'Custom filter'
                            }
                if date_from:
                    date_dict['date_from'] = date_from
                if date_to:
                    date_dict['date_to'] = date_to
                previous_options['date'] = date_dict
                
        self.filter_funds = {'funds': False} #, 'filter': funds
        if self._context.get('fund') and not previous_options.get('funds'):
            previous_options['funds'] = self._context['fund']
        if  not previous_options.get('funds'):
            previous_options['funds'] = self.get_funds()
        filter_show_all_funds = previous_options.get('show_all_funds') or self.show_all_funds
        if filter_show_all_funds:
            for index, fd in enumerate(previous_options['funds']):
                previous_options['funds'][index]['selected'] = True
        self.filter_comparison = {'date_from': '', 'date_to': '', 'filter': 'no_comparison', 'number_period': 1, 'number_period_month':1}
        res =  super(ReportAccountFinancialReport, self).get_options(previous_options)
        return res

#    @api.multi
#    def get_html(self, options, line_id=None, additional_context={}):
#        selected_funds = [f for f in (options.get('funds', []) or []) if f['selected']]
##        selected_funds = [{'id': False, 'name': '', 'code': 'total', 'selected': True}] + selected_funds
#        additional_context.update(selected_funds=selected_funds)
#        res = super(ReportAccountFinancialReport, self).get_html(options, line_id=None, additional_context=additional_context)
#        return res

    @api.multi
    def get_html(self, options, line_id=None, additional_context=None):
        templates = self.get_templates()
        report_manager = self.get_report_manager(options)
        report = {'name': self.get_report_name(),
                'summary': report_manager.summary,
                'company_name': self.env.user.company_id.name,}
        lines = self.with_context(self.set_context(options)).get_lines(options, line_id=line_id)
        selected_funds = [f for f in (options.get('funds', []) or []) if f['selected']]
        if options.get('hierarchy'):
            lines = self.create_hierarchy(lines)

        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_columns_name(options), 'selected_funds': selected_funds ,'lines': lines},
                    'options': options,
                    'context': self.env.context,
                    'model': self,
                }
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        render_template = templates.get('main_template', 'account_reports.main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k,v in self.replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>', self.get_html_footnotes(footnotes_to_render))
        return html
        
    @api.multi
    def get_lines(self, options, line_id=None):

        self = self.with_context(show_zero=options.get('show_zero') or self.show_zero, 
#                                search_funds_type_ids=funds_type_ids
                                )
        line_obj = self.line_ids
        if line_id:
            line_obj = self.env['account.financial.html.report.line'].search([('id', '=', line_id)])
        if options.get('comparison') and options.get('comparison').get('periods'):
            line_obj = line_obj.with_context(periods=options['comparison']['periods'])
        currency_table = self._get_currency_table()
        
        funds_type_ids = []
        if options.get('funds'):
            funds_type_ids = [f.get('id') for f in options.get('funds') if f.get('selected')]
        len_funds = len(funds_type_ids) or 1
        linesDicts = [{} for _ in range(0, len((options.get('comparison') or {}).get('periods') or [1])*len_funds + len_funds)]
        res = line_obj.with_context(
            cash_basis=options.get('cash_basis') or self.cash_basis,
        ).get_lines(self, currency_table, options, linesDicts)
        return res
    
    @api.multi
    def get_lines_obsolete(self, options, line_id=None):
#        funds_type_ids = []
#        if options.get('funds'):
#            funds_type_ids = [f.get('id') for f in options.get('funds') if f.get('selected')]
        self = self.with_context(show_zero=options.get('show_zero') or self.show_zero, 
#                                search_funds_type_ids=funds_type_ids
                                )
        res = super(ReportAccountFinancialReport,self).get_lines(options, line_id=line_id)
        return res
    
    def _query_get_select_sum(self, currency_table):
        res = super(ReportAccountFinancialReport, self)._query_get_select_sum(currency_table)
        select = res[0]
        extra_params = res[1]
        #if self.env.context.get('show_zero'):
        #    for field in ['debit', 'credit', 'balance']:
        #        #replace the columns selected but not the final column name (... AS <field>)
        #        number_of_occurence = len(select.split(field)) - 1
        #        select = select.replace(field, field + '_cash_basis', number_of_occurence - 1)
        return select, extra_params

    def get_xlsx(self, options, response):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self.get_report_name()[:31])

        def_style = workbook.add_format({'font_name': 'Arial', 'num_format': '#,##0.00'})
        def_bold_style = workbook.add_format({'font_name': 'Arial', 'bold': True})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'pattern': 1, 'font_color': '#FFFFFF'})
        level_0_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'left': 2, 'pattern': 1, 'font_color': '#FFFFFF'})
        level_0_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'right': 2, 'pattern': 1, 'font_color': '#FFFFFF', 'num_format': '#,##0.00'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2})
        level_1_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'left': 2})
        level_1_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'right': 2, 'num_format': '#,##0.00'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2})
        level_2_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2, 'left': 2})
        level_2_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2, 'right': 2, 'num_format': '#,##0.00'})
        level_3_style = def_style
        level_3_style_left = workbook.add_format({'font_name': 'Arial', 'left': 2})
        level_3_style_right = workbook.add_format({'font_name': 'Arial', 'right': 2, 'num_format': '#,##0.00'})
        domain_style = workbook.add_format({'font_name': 'Arial', 'italic': True})
        domain_style_left = workbook.add_format({'font_name': 'Arial', 'italic': True, 'left': 2})
        domain_style_right = workbook.add_format({'font_name': 'Arial', 'italic': True, 'right': 2})
        upper_line_style = workbook.add_format({'font_name': 'Arial', 'top': 2})

        sheet.set_column(0, 0, 60) #  Set the first column width to 70
        sheet.set_column(1, 1, 20) #  Set the second column width to 30

        super_columns = self._get_super_columns(options)
        # y_offset = bool(super_columns.get('columns')) and 1 or 0
        y_offset = 0

        sheet.write(y_offset, 0, '', title_style)

        # Amy request to have this header
        sheet.write(y_offset, 0, 'TPSTST', def_bold_style)
        y_offset += 1
        options_date = options.get('date', False) and options['date'].get('date_to', False)
        if not options_date:
            options_date = options.get('date', False) and options['date'].get('date', False)
        if options_date:
            options_date = datetime.strptime(options_date, '%Y-%m-%d').strftime("%d %B %Y")
        sheet.write(y_offset, 0, self.get_report_name() + ' for the period ended ' + (options_date or '-'),
                    def_bold_style)
        y_offset += 2

        # Todo in master: Try to put this logic elsewhere
        x = super_columns.get('x_offset', 0)
        for super_col in super_columns.get('columns', []):
            cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
            x_merge = super_col.get('merge') or super_columns.get('merge')
            if x_merge and x_merge > 1:
                sheet.merge_range(y_offset, x, y_offset, x + (x_merge - 1), cell_content, super_col_style)
                x += x_merge
            else:
                sheet.write(0, x, cell_content, super_col_style)
                x += 1
        if super_columns:
            y_offset += 1

        selected_funds = [f for f in (options.get('funds', []) or []) if f['selected']]
        len_funds = len(selected_funds)
        x = 0

        for column in self.get_columns_name(options):
            if len_funds > 1 and column.get('name', '').replace('<br/>', '').replace('&nbsp;', ''):
                sheet.merge_range(y_offset, (1+(x-1)*len_funds), y_offset, (1+(x-1)*len_funds) + (len_funds - 1), column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' '), title_style)
            else:
                sheet.write(y_offset, x, column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' '), title_style)
                
            x += 1
        y_offset += 1

        if selected_funds:
            x = 0
            for column in self.get_columns_name(options):
                if column.get('name', '').replace('<br/>', '').replace('&nbsp;', ''):
                    for funds_type in selected_funds:
                        sheet.write(y_offset, x, (funds_type.get('code', '') or funds_type.get('name', '')).replace('<br/>', ' ').replace('&nbsp;', ' '), title_style)
                        x += 1
                else:
                    sheet.write(y_offset, x, column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' '), title_style)
                    x += 1
            y_offset += 1

        sheet.write(y_offset, 1, 'S$', super_col_style)
        y_offset += 1

        ctx = self.set_context(options)
        ctx.update({'no_format':True, 'print_mode':True})
        lines = self.with_context(ctx).get_lines(options)

        if options.get('hierarchy'):
            lines = self.create_hierarchy(lines)

        if lines:
            max_width = max([len(l['columns']) for l in lines])

        first_line = True
        for y in range(0, len(lines)):
            if lines[y].get('level') == 0:
                if not first_line:
                    for x in range(0, len(lines[y]['columns']) + 1):
                        sheet.write(y + y_offset, x, None, upper_line_style)
                    y_offset += 1
                style_left = level_0_style_left
                style_right = level_0_style_right
                style = level_0_style
            elif lines[y].get('level') == 1:
                if not first_line:
                    for x in range(0, len(lines[y]['columns']) + 1):
                        sheet.write(y + y_offset, x, None, upper_line_style)
                    y_offset += 1
                style_left = level_1_style_left
                style_right = level_1_style_right
                style = level_1_style
            elif lines[y].get('level') == 2:
                style_left = level_2_style_left
                style_right = level_2_style_right
                style = level_2_style
            elif lines[y].get('level') == 3:
                style_left = level_3_style_left
                style_right = level_3_style_right
                style = level_3_style
            # elif lines[y].get('type') != 'line':
            #     style_left = domain_style_left
            #     style_right = domain_style_right
            #     style = domain_style
            else:
                style = def_style
                style_left = def_style
                style_right = def_style

            sheet.write(y + y_offset, 0, lines[y]['name'], style_left)
            for x in range(1, max_width - len(lines[y]['columns']) + 1):
                sheet.write(y + y_offset, x, None, style)
            for x in range(1, len(lines[y]['columns']) + 1):
                # if isinstance(lines[y]['columns'][x - 1], tuple):
                    # lines[y]['columns'][x - 1] = lines[y]['columns'][x - 1][0]
                if x < len(lines[y]['columns']):
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, lines[y]['columns'][x - 1].get('name', ''), style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, lines[y]['columns'][x - 1].get('name', ''), style_right)
            if 'total' in lines[y].get('class', '') or lines[y].get('level') == 0:
                for x in range(len(lines[0]['columns']) + 1):
                    sheet.write(y + 1 + y_offset, x, None, upper_line_style)
                y_offset += 1
            first_line = False
        if lines:
            for x in range(max_width + 1):
                sheet.write(len(lines) + y_offset, x, None, upper_line_style)

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()

class AccountFinancialReportLine(models.Model):
    _inherit = "account.financial.html.report.line"
    #_readonly_view = True
#    _nodelete_restrict = True #should not block, it only for reports, it does not have any link to transaction data


    #domain = fields.Char(default=None)
    #account_account_ids = fields.Many2many('account.account.type','financial_report_line_account_type_rel', 'report_id','type_id',string='Account Types')
    account_account_ids = fields.Many2many('account.account','financial_report_line_account_account_rel', 'report_id','account_id',string='Accounts')

    @api.multi
    def report_move_lines_action(self):
        domain = safe_eval(self.domain)
        if self.account_account_ids:
            account_account_ids = [acc.id for acc in self.account_account_ids]
            domain = expression.AND([domain, [('account_id', 'in', account_account_ids)]])
        if 'date_from' in self.env.context.get('context', {}):
            if self.env.context['context'].get('date_from'):
                domain = expression.AND([domain, [('date', '>=', self.env.context['context']['date_from'])]])
            if self.env.context['context'].get('date_to'):
                domain = expression.AND([domain, [('date', '<=', self.env.context['context']['date_to'])]])
            if self.env.context['context'].get('state', 'all') == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            if self.env.context['context'].get('company_ids'):
                domain = expression.AND([domain, [('company_id', 'in', self.env.context['context']['company_ids'])]])
        return {'type': 'ir.actions.act_window',
                'name': 'Journal Items (%s)' % self.name,
                'res_model': 'account.move.line',
                'view_mode': 'tree,form',
                'domain': domain,
                }

    def _get_sum(self, currency_table, financial_report, field_names=None):
        ''' Returns the sum of the amls in the domain '''
        if not field_names:
            field_names = ['debit', 'credit', 'balance', 'amount_residual']
        res = dict((fn, 0.0) for fn in field_names)
        if self.domain or self.account_account_ids:
            date_from, date_to, strict_range = \
                self._compute_date_range()
            domain = self.domain and safe_eval(self.domain) or []
            if self.account_account_ids:
                account_account_ids = [acc.id for acc in self.account_account_ids]
                domain = expression.AND([domain, [('account_id', 'in', account_account_ids)]])
                
            res = self.with_context(strict_range=strict_range, date_from=date_from, date_to=date_to)._compute_line(currency_table, financial_report, group_by=self.groupby, domain=domain)
            #res = self.with_context(strict_range=strict_range, date_from=period_from, date_to=period_to)._compute_line(currency_table, financial_report, group_by=self.groupby, domain=domain)
        return res


    def _eval_formula(self, financial_report, debit_credit, currency_table, linesDict):
        debit_credit = debit_credit and financial_report.debit_credit
        formulas = self._split_formulas()
        if self.code and self.code in linesDict:
            res = linesDict[self.code]
        elif formulas and formulas['balance'].strip() == 'count_rows' and self.groupby:
            return {'line': {'balance': self.get_rows_count()}}
        elif formulas and formulas['balance'].strip() == 'from_context':
            return {'line': {'balance': self.get_value_from_context()}}
        else:
            res = FormulaLine(self, currency_table, financial_report, linesDict=linesDict)
        vals = {}
        vals['balance'] = res.balance
        if debit_credit:
            vals['credit'] = res.credit
            vals['debit'] = res.debit
        show_zero = self._context.get('show_zero',False) or False

        results = {}
        if (self.domain or self.account_account_ids) and self.groupby and self.show_domain != 'never':
            domain = self.domain and safe_eval(self.domain) or []
            if self.account_account_ids:
                account_account_ids = [acc.id for acc in self.account_account_ids]
                domain = expression.AND([domain, [('account_id', 'in', account_account_ids)]])
            aml_obj = self.env['account.move.line']
            tables, where_clause, where_params = aml_obj._query_get(domain=domain)
            sql, params = self._get_with_statement(financial_report)
            if financial_report.tax_report:
                where_clause += ''' AND "account_move_line".tax_exigible = 't' '''

            groupby = self.groupby or 'id'
            if groupby not in self.env['account.move.line']:
                raise ValueError(_('Groupby should be a field from account.move.line'))
            select, select_params = self._query_get_select_sum(currency_table)
            params += select_params
            sql = sql + "SELECT \"account_move_line\"." + groupby + ", " + select + " FROM " + tables + " WHERE " + where_clause + " GROUP BY \"account_move_line\"." + groupby

            params += where_params
            self.env.cr.execute(sql, params)
            results = self.env.cr.fetchall()
            results = dict([(k[0], {'balance': k[1], 'amount_residual': k[2], 'debit': k[3], 'credit': k[4]}) for k in results])
            if show_zero and self.account_account_ids:
                for acc in self.account_account_ids:
                    if acc.id not in results:
                        results[acc.id] = {
                            'balance':0.0,
                            'amount_residual':0.0,
                            'debit':0.0,
                            'credit':0.0,
                        }
            c = FormulaContext(self.env['account.financial.html.report.line'], linesDict, currency_table, financial_report, only_sum=True)
            if formulas:
                for key in results:
                    c['sum'] = FormulaLine(results[key], currency_table, financial_report, type='not_computed')
                    c['sum_if_pos'] = FormulaLine(results[key]['balance'] >= 0.0 and results[key] or {'balance': 0.0}, currency_table, financial_report, type='not_computed')
                    c['sum_if_neg'] = FormulaLine(results[key]['balance'] <= 0.0 and results[key] or {'balance': 0.0}, currency_table, financial_report, type='not_computed')
                    for col, formula in formulas.items():
                        if col in results[key]:
                            results[key][col] = safe_eval(formula, c, nocopy=True)
            to_del = []
            if not show_zero:
                for key in results:
                    if self.env.user.company_id.currency_id.is_zero(results[key]['balance']):
                        to_del.append(key)
            for key in to_del:
                del results[key]

        results.update({'line': vals})
        return results
    
    @api.multi
    def get_lines(self, financial_report, currency_table, options, linesDicts):
        final_result_table = []
        comparison_table = [options.get('date')]
        comparison_table += options.get('comparison') and options['comparison'].get('periods', []) or []
        comparison_funds = []
        comparison_funds += [f for f in (options.get('funds', []) or []) if f['selected']]
        funds_type_ids = []
        for funds_type in comparison_funds:
            if funds_type['id']:
                funds_type_ids.append(funds_type['id'])
#        if not comparison_funds:
#            comparison_funds += ['total']
        len_funds = len(comparison_funds)
        currency_precision = self.env.user.company_id.currency_id.rounding
        # build comparison table
        for line in self:
            res = []
            debit_credit = len(comparison_table) == 1 and len_funds == 1
            domain_ids = {'line'}
            k = 0
            for period in comparison_table:
                date_from = period.get('date_from', False)
                date_to = period.get('date_to', False) or period.get('date', False)
                date_from, date_to, strict_range = line.with_context(date_from=date_from, date_to=date_to)._compute_date_range()
                for funds_type in comparison_funds:
                    if funds_type['total']:
                        r = line.with_context(date_from=date_from, date_to=date_to, strict_range=strict_range, search_funds_type_ids=[])._eval_formula(financial_report, debit_credit, currency_table, linesDicts[k])
                    else:
                        funds_type_id = funds_type['id']
                        r = line.with_context(date_from=date_from, date_to=date_to, strict_range=strict_range, search_funds_type_ids=[funds_type_id])._eval_formula(financial_report, debit_credit, currency_table, linesDicts[k])
                    debit_credit = False
                    res.append(r)
                    domain_ids.update(r)
                    k += 1
            res = line._put_columns_together(res, domain_ids)
            if line.hide_if_zero and all([float_is_zero(k, precision_rounding=currency_precision) for k in res['line']]):
                continue

            # Post-processing ; creating line dictionnary, building comparison, computing total for extended, formatting
            vals = {
                'id': line.id,
                'name': line.name,
                'level': line.level,
                'columns': [{'name': l} for l in res['line']],
                'unfoldable': len(domain_ids) > 1 and line.show_domain != 'always',
                'unfolded': line.id in options.get('unfolded_lines', []) or line.show_domain == 'always',
            }

            if line.action_id:
                vals['action_id'] = line.action_id.id
            domain_ids.remove('line')
            lines = [vals]
            groupby = line.groupby or 'aml'
            if line.id in options.get('unfolded_lines', []) or line.show_domain == 'always':
                if line.groupby:
                    domain_ids = sorted(list(domain_ids), key=lambda k: line._get_gb_name(k))
                for domain_id in domain_ids:
                    name = line._get_gb_name(domain_id)
                    vals = {
                        'id': domain_id,
#                        'name': name and len(name) >= 45 and name[0:40] + '...' or name,
                        'name': name, #Above code was change to this so that the datas' string will be shown fully in one line, not cut on 40th character.
                        'level': 4,
                        'parent_id': line.id,
                        'columns': [{'name': l} for l in res[domain_id]],
                        'caret_options': groupby == 'account_id' and 'account.account' or groupby,
                    }
                    if line.financial_report_id.name == 'Aged Receivable':
                        vals['trust'] = self.env['res.partner'].browse([domain_id]).trust
                    lines.append(vals)
                if domain_ids:
                    lines.append({
                        'id': 'total_'+str(line.id),
                        'name': _('Total') + ' ' + line.name,
                        'class': 'o_account_reports_domain_total',
                        'parent_id': line.id,
                        'columns': copy.deepcopy(lines[0]['columns']),
                    })

            for vals in lines:
                if len(comparison_table) == 2:
                    for j,funds_type in enumerate(comparison_funds):
                        vals['columns'].append(line._build_cmp(vals['columns'][(len_funds*0) + j]['name'], vals['columns'][(len_funds*1) + j]['name']))
                    for i in range(len_funds*2):
                        vals['columns'][i] = line._format(vals['columns'][i])
                else:
                    vals['columns'] = [line._format(v) for v in vals['columns']]
                if not line.formulas:
                    vals['columns'] = [{'name': ''} for k in vals['columns']]

            if len(lines) == 1:
                new_lines = line.children_ids.get_lines(financial_report, currency_table, options, linesDicts)
                if new_lines and line.level > 0 and line.formulas:
                    divided_lines = self._divide_line(lines[0])
                    result = [divided_lines[0]] + new_lines + [divided_lines[1]]
                else:
                    result = []
                    if line.level > 0:
                        result += lines
                    result += new_lines
                    if line.level <= 0:
                        result += lines
            else:
                result = lines
            final_result_table += result

        return final_result_table
