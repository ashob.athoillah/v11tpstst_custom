# -*- coding: utf-8 -*-
import time
from odoo import fields, models, api, _
from odoo.exceptions import UserError

class ClosingBankIn(models.TransientModel):
    """
    This wizard will create bank-in from cashier closing
    """

    _name = "closing.bank.in"
    _description = "Bank-In the selected cashier closing"
    
    journal_id = fields.Many2one('account.journal', 'Journal', required = True, domain=[('type','=','bank')])
    #any changes to journal domain, please also update at module bursary
    account_id = fields.Many2one('account.account', 'Account', required = True)
    date = fields.Date(string='Date', required = True, default=fields.Date.context_today)
    
    @api.model
    def default_get(self,fields):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for record in self.env['bank.acc.rec.statement'].browse(active_ids):
            if record.state != 'confirm':
                raise UserError(_("Selected statement(s) be must in 'Waiting for Finance Confirm' state."))
            if record.bank_in:
                raise UserError(_("Selected statement(s) already have Bank-In generated."))
            if record.ending_balance == 0:
                raise UserError(_("Selected statement(s) amount can not be 0 to generate bank-in."))
            if not record.daily_cashier:
                raise UserError(_("This function is only for Daily Cashier Closing."))
        
        res = {}  
        res = super(ClosingBankIn,self).default_get(fields)
        return res
    
    @api.onchange('journal_id')
    def onchange_journal_id(self):
        val = {'value': {'account_id': False}}
        if self.journal_id:
            val['value']['account_id'] = self.journal_id.default_debit_account_id and self.journal_id.default_debit_account_id.id or False
        return val

    @api.multi
    def bank_in(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        
        closing_ids = []
        total = 0
        for record in self.env['bank.acc.rec.statement'].browse(active_ids):
            record.check_closing_difference_balance()
            total += record.ending_balance
            closing_ids.append(record.id)
            
        date = self.date
        bankin_sequence = False
        if bankin_sequence:
            sequence = bankin_sequence.with_context(ir_sequence_date=date).next_by_id()
        else:
            sequence = self.env['ir.sequence'].next_by_code('bank.in')
        values = {
            'name' : sequence or '/',
            'date' : date,
            'journal_id' : self.journal_id and self.journal_id.id or False,
            'account_id' : self.account_id and self.account_id.id or False,
            'total_amount' : total,
            'daily_closing_ids' : [(6,0,closing_ids)]
        }
        bank_rec = self.env['bank.in.statement'].create(values)
        
        for record in self.env['bank.acc.rec.statement'].browse(active_ids):
            record.write({'bank_in_id':bank_rec.id, 'bank_in':True})
            
        return {'type': 'ir.actions.act_window',
            'name': 'Bank-In',
            'res_model': 'bank.in.statement',
            'res_id' : bank_rec.id,
            'view_type': 'tree,form',
            'view_mode': 'form',
        }