# -*- encoding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime
from dateutil import relativedelta

class LockPeriod(models.Model):
    _name = 'lock.period'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Lock Periods"
    _order = "name desc"

    def _default_year(self):
        return datetime.strptime(fields.Date.context_today(self.sudo()), "%Y-%m-%d").year

    name = fields.Char(string="Year", required=True, copy=False, default=_default_year, track_visibility='onchange')
    lock_period_item_ids = fields.One2many('lock.period.item', 'lock_period_id', string="Lock Period Items", copy=False, ondelete="cascade")
    state = fields.Selection([('draft','Draft'), ('confirm','Confirm')], string="State", required=True, readonly=True, copy=False, default="draft", track_visibility='onchange')

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state == 'confirm':
                raise UserError("Unable to delete the lock period has been confirmed.")
        return super(LockPeriod, self).unlink()
            

    @api.constrains('name')
    def check_name(self):
        for rec in self:
            lock_periods = self.search([('name','=', rec.name)], limit=2)
            if lock_periods and len(lock_periods._ids) > 1:
                raise UserError("Lock period with this year is already exists")

    def action_check_date(self, date=False, name=False):
        if date:
            if type(date) == str:
                date_month = datetime.strptime(date, "%Y-%m-%d").strftime("%Y-%m")
            else:
                date_month = date.strftime("%Y-%m")

            query = '''
                SELECT item.is_lock as is_lock
                FROM lock_period_item item
                LEFT JOIN lock_period lperiod ON lperiod.id=item.lock_period_id
                WHERE to_char(item.end_date, 'YYYY-MM') = '%s'
                AND lperiod.state = 'confirm'
                LIMIT 1
            ''' % (date_month)
            self._cr.execute(query)
            results = self._cr.dictfetchall()
            for result in results:
                if result['is_lock']:
                    raise UserError("You cannot add/modify data due to the selected date has been lock.")

    def action_confirm(self):
        for rec in self:
            rec.state = 'confirm'
            rec.action_generate_date()

    def action_get_date(self,year=False,month=False):
        month_string = str(month)
        if len(month_string) < 2:
            month_string = '0' + month_string
        start_date_string = str(year) + '-' + month_string + '-01'
        try:
            start_date = datetime.strptime(start_date_string, "%Y-%m-%d")
        except:
            raise UserError("The year is invalid.")

        next_month = start_date + relativedelta.relativedelta(months=1)
        end_date = next_month - relativedelta.relativedelta(days=1)
        return {
            'start_date': start_date,
            'end_date': end_date
        }

    def action_generate_date(self):
        for rec in self:
            if not rec.lock_period_item_ids:
                lock_dates = []
                for month in range(1,13):

                    values = self.action_get_date(year=rec.name, month=month)
                    lock_dates.append((0,0, values))
                rec.lock_period_item_ids = lock_dates
            else:
                month = 0
                for lock_period_item_id in rec.lock_period_item_ids:
                    month += 1
                    values = self.action_get_date(year=rec.name, month=month)
                    lock_period_item_id.write(values)

            self.env['lock.period.item'].cron_auto_lock()

class LockPeriodYear(models.Model):
    _name = 'lock.period.item'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Lock Period Item"
    _order = "start_date asc,end_date asc"

    lock_period_id = fields.Many2one('lock.period', string="Lock Date", required=True, track_visibility='onchange')
    start_date = fields.Date(string='Start Date', required=True, readonly=True, track_visibility='onchange')
    end_date = fields.Date(string='End Date', required=True, readonly=True, track_visibility='onchange')
    is_lock = fields.Boolean(string="Lock", track_visibility='onchange', help="Will lock the document donation transaction, donation refund and monthly processing.")

    def cron_auto_lock(self):
        period_obj = self.env['lock.period'].sudo()
        today = fields.Date.context_today(self.sudo())
        first_day = datetime.strptime(today, "%Y-%m-%d").replace(day=1)
        last_month = (first_day - relativedelta.relativedelta(days=1)).strftime("%Y-%m-%d")
        items = self.sudo().search([('end_date', '=', last_month)])
        items.write({
            'is_lock': True
        })

        periods = period_obj.search([('name', '=', str(first_day.year))], limit=1)
        if not periods:
            period_id = period_obj.create({'name': str(first_day.year)})
            period_id.action_generate_date()