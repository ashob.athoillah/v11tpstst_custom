# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (C) odooERP (<https://www.odoo.com>)
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import random
import string
import base64
from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from odoo.addons.v11_tpstst_custom.membership import membership_application as membership

STATE_MEMBER = [
    ('active', 'Active'),
    ('inactive', 'Not Active'),
    ('terminate', 'Terminated'),
]

class Partner(models.Model):
    _inherit = "res.partner"
    sql_constraints = [
        ('email_uniq', 'unique(email)', 'The email is already stored in system'),
        ('nric_uniq', 'unique(identity_no)', 'The NRIC is already stored in system'),
        ('mobile', 'unique(mobile)', 'The Mobile / 手机 number is already stored in system'),
    ]
    
    def _default_category(self):
        return self.env['res.partner.category'].browse(self._context.get('category_id'))

    name = fields.Char('English Name / 英文姓名', track_visibility='onchange')
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility='onchange')
    identity_no = fields.Char('NRIC No. / 新居民证号码', index=True, size=12, copy=False)
    identity_no_mask = fields.Char('NRIC No. / 新居民证号码', copy=False, store=True, compute="_compute_identity_no_mask")
    show_identity_no = fields.Char('Show ?', copy=False, default=False)
    identity_no_type = fields.Selection([
                        ('bc', 'BC'),
                        ('nric', 'NRIC'),
                        ('passport', 'Passport'),
                        ('fin', 'FIN')
                        ], default='nric')
    identity_type = fields.Selection([
                        ('sc', 'Singapore Citizen'),
                        ('pr', 'Permanent Resident'),
                        ('other', 'Others'),
                        ], string='Identity Type', required=True)
    citizenship_id = fields.Many2one('hr.citizenship', string="Citizenship", track_visibility='onchange')
    
    color_id = fields.Many2one('color.type.application', string='', track_visibility='onchange')
    mobile = fields.Char('Mobile / 手机',  copy=False, track_visibility='onchange')
    street = fields.Char(string='Street', track_visibility='onchange')
    street2 = fields.Char(string='Street 2', track_visibility='onchange')
    zip = fields.Char(string='Postal Code', track_visibility='onchange')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string="Gender / 性别", track_visibility='onchange')
    country_id = fields.Many2one('res.country', string='Country', track_visibility='onchange')
    email = fields.Char('E-mail / 电邮',copy=False, required=False, track_visibility='onchange')
    dialect_ids = fields.Many2many('dialect.list', 'res_partner_dialect_list_rel', string="Dialect / 籍贯", track_visibility='onchange')
    birthdate = fields.Date('Date of Birth / 出生日期', track_visibility='onchange')
    occupation = fields.Char('Occupation / 职业', track_visibility='onchange')
    function = fields.Char('Occupation / 职业')
    mailing_address = fields.Char('Mailing Address / 邮寄地址', track_visibility='onchange')
    title = fields.Many2one('res.partner.title', string="Title / 称号", track_visibility='onchange')
    parent_id = fields.Many2one('res.partner', string='Company / 公司', index=True, track_visibility='onchange')
    age = fields.Integer('Age', store=True, compute="_get_age")
    total_payment = fields.Monetary(string='Total Payments', store=True, compute='compute_total_amount_payment')
    # category_id = fields.Many2many('res.partner.category', column1='partner_id',
    #                                 column2='category_id', string='Tags', default=_default_category, track_visibility='onchange')
    rank_ids = fields.Many2many('partner.rank', 'partner_id', string="Rank / 秩", track_visibility='onchange')
    phone = fields.Char('Tel / 电话', track_visibility='onchange')
    comment = fields.Text(string="Remark / 备考", track_visibility='onchange')

    # @api.onchange('occupation')
    # def sync_function_with_occupation(self) :
    #     if self.occupation :
    #         self.function = self.occupation

    @api.multi
    def show_identity(self):
        for rec in self:
            rec.show_identity_no = True

    @api.multi
    def hide_identity(self):
        for rec in self:
            rec.show_identity_no = False

    @api.depends('identity_no', 'show_identity_no')
    @api.multi
    def _compute_identity_no_mask(self):
        for rec in self:
            if rec.identity_no:
                if rec.show_identity_no:
                    rec.identity_no_mask = rec.identity_no
                else:
                    rec.identity_no_mask = rec.convert_string(rec.identity_no)    
            else :
                pass
    
    @api.multi
    def convert_string(self, val):
        if val:
            res = len(val[:-4]) * "*" + val[-4:]
            return res
        return False

    @api.multi
    def compute_total_amount_payment(self):
        total_payment = 0
        for partner in self:
            payment = partner.env['account.move'].search([('partner_id', '=', partner.id)])
            for pay in payment:
                total_payment += pay.amount
            partner.total_payment = total_payment
    
    @api.multi
    @api.depends('birthdate')
    def _get_age(self):
        for rec in self :
            if rec.birthdate:
                birth = datetime.strptime(rec.birthdate, "%Y-%m-%d").date()
                today = datetime.today()

                rec.age = relativedelta(today, birth).years

    @api.onchange('birthdate')
    @api.multi
    def birthdate_validation(self):
        for rec in self :
            if rec.birthdate:
                birth = datetime.strptime(rec.birthdate, '%Y-%m-%d')
                today = datetime.today().date()
                age = relativedelta(today, birth).years
                if age < 18 :
                    rec.birthdate = False
                    return {'warning': {
                        'title': "Warning",
                        'message': "Your age is below than 18",
                        }
                    }

    @api.multi
    def random_token(self):
        """The token has an entropy of about 120 bits (6 bits/char * 20 chars)
            chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
            return (''.join(random.SystemRandom().choice(chars) for i in range(20)))"""

        # ensure there's at least 1 digit, 1 uppercase, 1 lowercase, and minimal 8 characters as required by standar website
        password_list = ([
           random.choice(string.digits),
           random.choice(string.ascii_lowercase),
           random.choice(string.ascii_uppercase),
          ]
         + [random.choice(string.ascii_lowercase
                          + string.ascii_uppercase
                          + string.digits) for i in range(12)])
        random.shuffle(password_list)
        password = ''.join(password_list)
        return password

    @api.multi
    def _create_user(self, login):
        user_obj = self.env['res.users']
        group_public_user = self.env.ref('v11_tpstst_custom.group_public_user')
        if not login :
            raise UserError(_("Login is not defined, please contact administrator or IT Support."))

        random_password = self.random_token()
        data_login = {}

        data_login['login'] = login
        data_login['password'] = random_password
        data_login['non_en_pass'] = random_password
        data_login['active'] = True
        data_login['partner_id'] = self.id
        data_login['groups_id'] = [(6, 0, [group_public_user.id])]
        data_login['portal_user'] = True


        user_ids = user_obj.search([('partner_id', '=', self.id)], limit=1)
        user_id = user_ids and user_ids[0] or False
        if user_id:
            user_id = user_ids[0] or False
        else :
            user_id = user_obj.with_context(send_email=True).create(data_login)

        return user_id


    #URL
    @api.multi
    def link_to_login(self):
        for part in self:
            url = "%s/account/login" % (part.create_uid.company_id.website)
            part.link_to_login = url

    @api.multi
    def link_to_payment(self, model, id):
        url = False
        web_param = self.env['ir.config_parameter'].sudo().get_param('web.base.url')

        record = str(id)
        url_safe_encoded_bytes = base64.urlsafe_b64encode(record.encode("utf-8"))
        url_safe_encoded_str = str(url_safe_encoded_bytes, "utf-8")
        url = "%s/%s/onlinepayment/%s" % (web_param, model, (url_safe_encoded_str))

        return url

    @api.multi
    def link_to_donation(self, id):
        url = False
        for rec in self :
            record = str(rec.id)
            url_safe_encoded_bytes = base64.urlsafe_b64encode(record.encode("utf-8"))
            url_safe_encoded_str = str(url_safe_encoded_bytes, "utf-8")
            url = "%s/donation/%s" % (rec.create_uid.company_id.website, (url_safe_encoded_str))
        return url


    @api.model
    def create(self, vals):
        # avoid sensitive login
        if vals.get('email', False) :
            vals['email'] = vals['email'].lower().strip()

        partner = super(Partner, self).create(vals)
        return partner

    @api.multi
    def write(self, vals):
        if vals.get('email', False) :
            vals['email'] = vals['email'].lower().strip()

        result = super(Partner, self).write(vals)

        return result
    
    @api.multi
    def name_get(self):
        res = []
        for partner in self:
            name = partner.name or 'New Partner'
            cn_name = partner.chinese_name
            membership_number = partner.membership_number
            
            if membership_number and name and cn_name :
                name = name + ' ( ' + cn_name + ' )' + ' - ' + membership_number
            elif membership_number and name :
                name = name + ' - ' + membership_number
            elif membership_number and cn_name :
                name = cn_name + ' - ' + membership_number
            else :
                name = name or cn_name


            res.append((partner.id, name))

        return res