# -*- encoding: utf-8 -*-
import time
from odoo.addons import decimal_precision as dp
from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from itertools import chain
from hashids import Hashids

# salt = password for unlock the url
SALT = 'Toa Payoh Seu Teck Sean Tong Tigernix'
MIN_LENGTH = 10
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

hash_url = Hashids(salt=SALT, min_length=MIN_LENGTH, alphabet=ALPHABET)

class ParticipantRegistration(models.Model):
    _name = "participant.registration"
    _description = 'Participant Registration'
    _inherit = ['mail.thread']

    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.event_journal_id and company.event_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    # General
    reg_date = fields.Date('Date of Application / 申请日期', default=lambda self: fields.datetime.now(), track_visibility='onchange')
    reg_no = fields.Char('Registration Number /  编号', track_visibility='onchange')

    remarks = fields.Text('Remark / 备考',  track_visibility='onchange')

    # Event Registration
    event_id = fields.Many2one('event.event', 'Event', track_visibility='onchange')
    event_type = fields.Selection([('free','Free'),('paid','Paid')], string="Event Type", track_visibility="onchange")
    event_start_date = fields.Date('Event Start Date', track_visibility='onchange')
    event_end_date = fields.Date('Event End Date', track_visibility='onchange')

    # Applicant
    is_existing_member = fields.Boolean("Existing Participant")
    partner_id = fields.Many2one('res.partner', domain=[('is_company', '=', False)], string="Participant", track_visibility='onchange')
    title_id = fields.Many2one('res.partner.title', string="Title / 称号", track_visibility='onchange')
    name = fields.Char('English Name / 英文姓名', track_visibility='onchange')
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility='onchange')
    identity_no = fields.Char('NRIC No. / 新居民证号码', track_visibility='onchange', index=True, size=12)
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string="Gender / 性别", track_visibility='onchange')
    identity_no_mask = fields.Char('NRIC No. / 新居民证号码', copy=False, size=12, store=True, compute="_compute_identity_no_mask")
    color_id = fields.Many2one('color.type.application', string='Color Type', track_visibility='onchange')
    mobile = fields.Char('Mobile / 手机', track_visibility='onchange')
    phone = fields.Char('Tel / 电话', track_visibility='onchange')
    street = fields.Char(string='Street', track_visibility='onchange')
    street2 = fields.Char(string='Street 2', track_visibility='onchange')
    birthdate = fields.Date('Date of Birth / 出生日期', track_visibility='onchange')
    zip = fields.Char(string='Postal Code', track_visibility='onchange')
    country_id = fields.Many2one('res.country', string='Country', track_visibility='onchange')
    company_id = fields.Many2one('res.partner', string='Company / 公司', track_visibility='onchange')
    # state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirm'), ('cancel', 'Cancelled'), ('approve', 'Approved'), ('paid', 'Paid')], string="State", default='draft', track_visibility='onchange')
    email = fields.Char('E-mail / 电邮', track_visibility='onchange')
    particular_visitors = fields.One2many('visitor.list.info', 'partreg_id', string="Particular Visitor", track_visibility='onchange')
    signature = fields.Binary("Applicant’s Signature / 申请人签名", track_visibility='onchange')
    dialect_ids = fields.Many2many('dialect.list', 'partreg_dialect_list_rel', string="Dialect / 籍贯", track_visibility='onchange')
    occupation = fields.Char('Occupation / 职业', track_visibility='onchange')
    signature = fields.Binary("Applicant’s Signature / 申请人签名", copy=False, track_visibility='onchange')
    mailing_address = fields.Char('Mailing Address / 邮寄地址', track_visibility='onchange')
    show_identity_no = fields.Char('Show ?', copy=False)
    registration_id = fields.Many2one('event.registration', string="Registration", track_visibility="onchange")
    state = fields.Selection([
            ('draft', 'Draft'), 
            ('confirm', 'Submit'), 
            ('vertified', 'Vertified'),
            ('donated', 'Donated'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'), 
            ('reject', 'Rejected'), 
            ], string="State", default='draft', copy="False", track_visibility='onchange')

    # Payment
    journal_id = fields.Many2one('account.journal', string="Journal", default=_default_journal, track_visibility='onchange')
    residual = fields.Float('Payable Amount',  track_visibility='onchange')
    amount_untaxed = fields.Float(string='Untaxed Fee', store=True,
                                  compute='_amount_all', track_visibility='onchange')
    product_ids = fields.Many2many('product.product', 'partreg_product_rel', 'partreg_id', 'product_id', string="Product Lines", track_visibility='onchange')
    total_product_amount = fields.Float('Total Amount', compute="_total_product_amount_valid_price", store=True, track_visibility='onchange')
    amount_tax = fields.Float(string='GST', store=True, readonly=False, compute='_amount_all', track_visibility='onchange')
    amount_total = fields.Float(string='Total Amount', store=True,
                                compute='_amount_all', track_visibility='onchange')
    amount_paid = fields.Float(string='Amount Paid', store=True,
                                 track_visibility='onchange')
    total_event_fee = fields.Float('Event Fee', compute="_get_event_fee_amount", store=True, track_visibility='onchange')
    funds_id = fields.Many2one('pool.of.funds','Funds', domain=[('state','=', 'confirm')])

    invoice_ids = fields.Many2many('account.invoice', 'partreg_invoice_rel', 'partreg_id', 'invoice_id', string='Invoices', track_visibility='onchange', copy=False)
    # move_line_ids = fields.Many2many('account.move.line', string='Payments Received',)
    move_line_ids = fields.One2many('account.move.line', 'event_id')
    voucher_ids = fields.One2many('account.voucher', 'event_id')
    check_invoice = fields.Boolean('Invoice created.', default=False)

    # Product
    # product_id = fields.Many2one('product.template', domain=[('columbarium_product', '=', True), ('sold_out', '=', False)],  track_visibility='onchange')
    # level_id = fields.Many2one('level.detail.list',  track_visibility='onchange')
    agreement = fields.Boolean('Agreement', track_visibility='onchange')
    vertified = fields.Boolean(string="Vertified", copy=False, help="Vertifiying user email",track_visibility='onchange')
    responsible_user_id = fields.Many2one('res.users', copy=False, string='Attended by / 经手人', track_visibility='onchange')
    login_id = fields.Many2one('res.users', copy=False, string="Login ID", track_visibility='onchange')
    button_done = fields.Boolean("Done", default=False, compute="check_button_done")
    check_button_invoice = fields.Boolean("Invoice button parameter", default=False, compute="_check_button_invoice")
    hash_id = fields.Char('Hash ID', compute="_get_hash_id", store=True, copy=False)
    url_auth_email = fields.Char('URL Authentication E-mail', compute="_link_auth_email", store=True, copy=False)
    no_of_people = fields.Integer('Total Person Accompanied', default=0, store=True, track_visibility='onchange')
    session_id = fields.Many2one('event.session', 'Session', track_visibility='onchange')
    vaccine_status = fields.Selection([('yes','Yes'),('no','No')], string="Have been Vaccinated ?", track_visibility="onchange")
    purpose_event_id = fields.Many2one('purpose.event', 'Purpose of Coming', track_visibility='onchange')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name) + '[' + str(rec.reg_no) + ']'
            result.append((rec.id, name))

        return result

    @api.one
    def _check_button_invoice(self):
        for rec in self:
            if rec.event_type == 'paid':
                if rec.state == 'vertified' and rec.check_invoice == False:
                    rec.check_button_invoice = True #show Create Invoice Button
            elif rec.event_type == 'free':
                if rec.total_product_amount > 0 and rec.state == 'vertified' and rec.check_invoice == False:
                    rec.check_button_invoice = True #Shiw Create Invoice Button

    @api.one
    def check_button_done(self):
        for rec in self:
            if rec.state == 'vertified' and rec.total_product_amount == 0 and rec.event_type == 'free':
                rec.button_done = True
            # elif rec.state == 'approve' and rec.state == 'paid':
            #     rec.button_done = False
            # elif rec.state == 'approve' and rec.check_invoice == True:
            #     rec.button_done = False

    @api.depends('birthdate')
    def _get_age(self):
        for part in self:
            if part.birthdate == datetime.now().strftime('%Y-%m-%d'):
                part.age = 0
            elif part.birthdate:
                current_date = datetime.now()
                dob = datetime.strptime(part.birthdate, '%Y-%m-%d')
                days = str(current_date - dob)
                days = int(days.split(" ", 1)[0])
                months = days / 30
                month, year = months % 12, months / 12
                part.age = year
            else:
                part.age = 0
    @api.multi
    def action_draft(self) :
        self.state = 'draft'
    
    @api.multi
    def action_confirm(self):
        state = 'confirm'
        self.get_responsible_user()
        if self.is_existing_member :
            self.prevent_double_register()
            if self.no_of_people:
                self.prevent_acc_personlimited()
            self.name_set()
            state = 'vertified'
        else :
            if self.no_of_people:
                self.prevent_acc_personlimited()
            self.prevent_duplicate_registration()
            self.prevent_duplicate_user(self.email)
            self._get_hash_id()
            self.mail_confirm_email()
        if self.event_id.seats_availability == 'limited':
            self.prevent_over_participant()

        self.state = state

    def prevent_over_participant(self):
        event = self.event_id
        seats_max = event.seats_max
        for rec in self:
            if len(self.session_id.registration_ids.filtered(lambda x: x.state != 'cancel')) == seats_max:
                raise ValidationError(_("This Session has already reach the maximum attendee(s) of participant, kindly register to another session"))


    def prevent_double_register(self):
        evreg_obj = self.env['event.registration']
        session = self.session_id
        event = self.event_id
        partner = self.partner_id
        evreg = evreg_obj.search([('session_id','=', session.id),('event_id', '=', event.id), ('partner_id', '=', partner.id)])
        if evreg:
            raise ValidationError(_("You already Register to the Event"))

    def prevent_acc_personlimited(self):

        evreg_obj = self.env['event.registration']
        session = self.session_id
        event = self.event_id
        limit_general = event.max_companied_free
        limit_member = event.max_companied_member
        partner = self.partner_id
        if self.is_existing_member:
            if partner.member:
                if self.no_of_people > limit_member:
                    raise ValidationError(_("Cannot bring people more than {}").format(limit_member))
            elif not partner.member:
                if self.no_of_people > limit_general:
                    raise ValidationError(_("Cannot bring people more than {}").format(limit_general))
        else:
            if self.no_of_people > limit_general:
                raise ValidationError(_("Cannot bring people more than {}").format(limit_general))

    @api.depends('state')
    @api.multi
    def _get_hash_id(self):
        hashid = False
        for rec in self :
            hashid = rec.id
            try :
                hashid = hash_url.encode(int(hashid))
            except :
                pass

            rec.hash_id = hashid

    @api.multi
    def get_true_id(self):
        hashid = False
        for rec in self :
            hashid = rec.hash_id

            try :
                hashid = hash_url.decode(hashid)
            except :
                pass

        return hashid

    @api.depends('hash_id', 'state')
    @api.multi
    def _link_auth_email(self):
        url = False
        web_param = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        model = 'participant'
        for rec in self :
            hashid = rec.hash_id

            url = "%s/%s/vertification/%s" % (web_param, model, hashid)

            rec.url_auth_email = url

    @api.multi
    def action_vertified(self) :
        if not self.is_existing_member :
            self.name_set()
            self.action_create_res_partner_data()
        self.state = 'vertified'

    
    @api.multi
    def action_reject(self) :
        self.get_responsible_user()
        self.state = 'reject'
    
    @api.multi
    def action_cancel(self) :
        self.get_responsible_user()
        self.state = 'cancel'

    @api.multi
    def action_create_user(self) :
        user = self.partner_id._create_user(self.email)
        user.mail_create_user()
        return user

    @api.multi
    def mail_confirm_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_confirmation_participant') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.event_email,
                'email_to' : self.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

    @api.multi
    def action_create_res_partner_data(self) :
        obj_partner = self.env['res.partner']
        partner = self.action_search_partner()

        data = {}

        data['name']            = self.name
        data['chinese_name']    = self.chinese_name
        data['identity_no']     = self.identity_no or ''
        data['color_id']        = self.color_id and self.color_id.id or ''
        data['mobile']          = self.mobile
        data['phone']           = self.phone
        data['street']          = self.street
        data['street2']         = self.street2
        data['zip']             = self.zip
        data['gender']          = self.gender
        data['country_id']      = self.country_id and self.country_id.id
        data['email']           = self.email
        data['dialect_ids']     = self.dialect_ids
        data['birthdate']       = self.birthdate
        data['occupation']      = self.occupation
        data['mailing_address'] = self.mailing_address
        data['function']        = self.occupation

        if not partner :
            partner = obj_partner.create(data)
            self.write({'partner_id' : partner.id})
        else :
            if partner.member :
                raise UserError(_("Details Information for this member already recorded in membership module"))
            else :
                partner.write(data)
                self.write({'partner_id' : partner.id})
    
    @api.multi
    def action_search_partner(self) :
        obj_partner = self.env['res.partner']

        # =ilike ignore case sentive, so it's really good to use in searching email
        partner = obj_partner.search([('email', '=ilike', self.email)])

        return partner

    @api.multi
    def action_done(self):
        self.action_paid()

    @api.multi
    def action_paid(self):
        if self.is_existing_member:
            self.login_id = self.partner_id.user_id.login
        else:
            self.login_id = self.action_create_user()

        self.create_evreg()
        if self.event_type == 'paid' :
            self.mail_paid()
        self.state = 'paid'
    
    
    @api.multi
    def mail_paid(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_ordinary_paid_participant') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.event_email,
                'email_to' : self.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()
    

    @api.multi
    def create_evreg(self):
        evreg_obj = self.env['event.registration']
        app_val = {
            'event_id': self.event_id.id,
            'session_id': self.session_id.id,
            'event_type': self.event_type,
            'event_start_date': self.event_start_date,
            'event_end_date': self.event_end_date,
            
            'partner_id': self.partner_id.id,
            'app_id': self.id,
            'name': self.partner_id.name,
            'title_id': self.title_id.id,
            'chinese_name': self.chinese_name,
            'mobile': self.mobile,
            'email': self.email,
            'no_of_people': self.no_of_people,
        }

        evreg_id = evreg_obj.create(app_val)
        evreg_id.action_confirm()

        evreg_id.confirm_registration()
        self.write({'registration_id': evreg_id.id})
        return evreg_id


    @api.multi
    def action_cancel(self) :
        self.state = 'cancel'
    
    @api.multi
    def action_create_invoice(self):
        self.create_invoice()
        self.check_invoice = True


    @api.depends('event_id', 'event_id.product_id.valid_price', 'event_type', 'no_of_people')
    @api.multi
    def _amount_all(self) :
        for rec in self :
            product = rec.event_id.product_id
            amount_untaxed = 0.00
            amount_tax= 0.00
            amount_paid = 0.00
            residual = 0.00
            list_taxes = []
            price = 0.00
            product_amount = 0.00
            total_product_amount = 0.00

            if product :
                taxes_member = []
                price = product.valid_price
                taxes_member = [float("%.2f" % (price * (taxes.amount / 100))) for taxes in product.taxes_id if product.taxes_id]
                list_taxes += taxes_member

            include_people = price * rec.no_of_people
            amount_untaxed = price + include_people
            amount_tax = sum(list_taxes)

            rec.amount_untaxed = amount_untaxed
            rec.amount_tax = amount_tax
            rec.amount_total = amount_untaxed + amount_tax


    @api.depends('product_ids', 'product_ids.valid_price')
    @api.multi
    def _total_product_amount_valid_price(self) :
        for rec in self :
            list_product = rec.product_ids
            total_product_amount = 0.00
            if list_product :
                for product in list_product :
                    total_product_amount += product.valid_price
            
            rec.total_product_amount = total_product_amount

    @api.depends('event_id', 'event_id.product_id')
    @api.multi
    def _get_event_fee_amount(self) :
        for rec in self :
            if self.event_type == 'paid':
                rec.total_event_fee = rec.event_id.product_id.valid_price
            else:
                rec.total_event_fee = 0.00

    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no :
            self.reg_no = self.env['ir.sequence'].next_by_code('participant.registration.seq')

    
    @api.onchange('is_existing_member')
    @api.multi
    def sync_partner(self) :
        if not self.is_existing_member :
            self.partner_id = False
    
    @api.onchange('partner_id')
    @api.multi
    def sync_data_partner(self) :
        partner = self.partner_id
        if partner :
            self.title_id       = partner.title and partner.title.id
            self.name           = partner.name
            self.chinese_name   = partner.chinese_name
            self.birthdate      = partner.birthdate
            self.gender         = partner.gender
            self.mobile         = partner.mobile
            self.email          = partner.email
            self.street         = partner.street
            self.street2        = partner.street2
            self.zip            = partner.zip
            self.country_id     = partner.country_id and partner.country_id.id
            self.company_id     = partner.parent_id and partner.parent_id.id
        else :
            self.title_id       = False
            self.name           = False
            self.chinese_name   = False
            self.gender         = False
            self.mobile         = False
            self.email          = False
            self.street         = False
            self.street2        = False
            self.zip            = False
            self.country_id     = False
            self.company_id     = False

    @api.onchange('event_id')
    def get_ev_info(self):
        event = self.event_id
        if event:
            self.event_type = event.event_type
            self.event_start_date = event.date_begin
            self.event_end_date = event.date_end
        else:
            self.event_type = False
            self.event_start_date = False
            self.event_end_date = False


    @api.multi
    def create_invoice(self):
        invoice_obj = self.env['account.invoice']
        inv_line_obj = self.env['account.invoice.line']

        for rec in self :
            product = rec.event_id.product_id
            event_type = self.event_type

            part_data   = {}
            if product and event_type == 'paid' :
                
                # create invoice
                part_data['partner_id']                 = rec.partner_id and rec.partner_id.id
                part_data['date_invoice']               = datetime.now().date()
                part_data['origin']                     = rec.reg_no
                part_data['name']                       = (str(product.name) or '') + ' - ' + 'Event' + ' (' + str(rec.reg_no) + ')' + ' - ' + str(rec.partner_id.name)
                part_data['state']                      = 'draft'
                part_data['type']                       = 'out_invoice'
                part_data['currency_id']                = rec.env.ref('base.SGD').id
                part_data['journal_id']                 = rec.journal_id and rec.journal_id.id
                part_data['event_application_id']       = rec and rec.id

                invoice = invoice_obj.create(part_data)

                rec.write({'invoice_ids': [(4, invoice.id)]})

                # insert membership fee into invoice line
                event_inv_line = {}
                event_fee_inv_line = False

                event_inv_line['product_id']           = product and product.id
                event_inv_line['name']                 = product.name
                event_inv_line['quantity']             = 1 + rec.no_of_people
                event_inv_line['price_unit']           = product.valid_price
                event_inv_line['account_analytic_id']  = product.product_analytic_id and product.product_analytic_id.id
                event_inv_line['account_id']           = product.property_account_income_id and product.property_account_income_id.id
                event_inv_line['invoice_id']           = invoice and invoice.id

                event_fee_inv_line = inv_line_obj.create(event_inv_line)

                product_tax = False
                if len(product.taxes_id) > 0:
                    product_tax = product.taxes_id[0] and product.taxes_id[0].id or False
              
                if event_fee_inv_line:
                    if product_tax:
                        event_fee_inv_line.invoice_line_tax_ids = [(6, 0, [product_tax])]
                    invoice.line_ids = [(4, event_fee_inv_line.id)]
                    
                invoice._onchange_invoice_line_ids()
                invoice._compute_amount()

            else :
                raise Warning(_('Product fee or product list not found!'))

 
    def prevent_duplicate_registration(self):
        dt_fmt = '%d/%m/%Y'
        if self.partner_id and self.event_id:
            previous_evreg = self.env['event.registration'].search([('event_id', '=', self.event_id.id),('email', '=ilike', self.email),('state','=','open')])
            if previous_evreg:
                raise ValidationError('The email already registered to this Event')
    @api.multi
    def prevent_duplicate_user(self, email):
        partner_obj = self.env['res.partner']
        get_partner = partner_obj.search([('email', '=ilike', email)])
        if get_partner and len(get_partner.user_ids) > 0 : 
            raise ValidationError(_("This email has already have an user, please login to continue").format(email, get_partner.name))

    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id     

    @api.depends('invoice_ids', 'invoice_ids.payment_move_line_ids')
    @api.multi
    def _get_payment_detail(self):
        for rec in self:
            invs = rec.invoice_ids
            all_mvlines = list(chain.from_iterable([list(inv.payment_move_line_ids) for inv in invs]))
            rec.move_line_ids = [(6, 0, [m.id for m in all_mvlines])]

    @api.constrains('name', 'chinese_name')
    def prevent_overlapping_sessions(self):
        if not self.name and not self.chinese_name:
            raise ValidationError(_("You should fill at least 1 field between Name and Chinese Name / 中文姓名"))


    @api.multi
    def call_event_payment_wizard(self):
        vals = {}
        journal_id = self._default_journal() or False
        vals['journal_id'] = journal_id
        return {
            'name': _('Event Payment'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.voucher.donation',
            'target': 'new',
            'context': {'default_event_id': self.id, 'default_amount': self.amount_total, 'default_funds_id': self.funds_id.id, 'default_reg_no': self.reg_no, 'default_journal_id': self.journal_id.id, 'default_partner_id':self.partner_id.id}
        }

class VisitorInfoLine(models.Model):
    _name = "visitor.list.info"
    _inherit = ['mail.thread']
    
    name = fields.Char('Name', track_visibility="onchange")
    partreg_id = fields.Many2one('participant.registration', 'Registration', track_visibility="onchange")
    evreg_id = fields.Many2one('event.registration', 'Event Registration', track_visibility="onchange")
    relationship = fields.Char("Relationship", track_visibility="onchange")
    title_id = fields.Many2one('res.partner.title', 'Title / 称号', track_visibility="onchange")
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility="onchange")
    identity_no = fields.Char('NRIC No. / 新居民证号码', index=True, size=12, track_visibility="onchange")
    color_id = fields.Many2one('color.type.application', string='Color Type', track_visibility="onchange")
    origin = fields.Char('Origin', track_visibility="onchange")

class PurposeEvent(models.Model):
    _name = "purpose.event"
    _inherit = ['mail.thread']
    
    name = fields.Char('Name', track_visibility="onchange")