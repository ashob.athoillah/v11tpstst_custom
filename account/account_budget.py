# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp 
from odoo.exceptions import UserError
from itertools import chain


class CrossoveredBudget(models.Model):
    _inherit = "crossovered.budget"

    @api.multi
    def unlink(self):
        finance_head_user = self.env.user.has_group('v11_tpstst_custom.group_accounting')
        for budget in self:
            if not finance_head_user:
                raise UserError(_("Only Finance Head Can Delete Budget!"))
        return super(CrossoveredBudget, self).unlink()

    creating_user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user, domain=[('internal','=',True)])

class CrossoveredBudgetLines(models.Model):
    _name = "crossovered.budget.lines"
    _inherit = ['crossovered.budget.lines', 'mail.thread', 'utm.mixin']

    planned_amount = fields.Float('Budget', required=True, digits=0, track_visibility='onchange')
    practical_amount = fields.Float(compute='_compute_practical_amount', string='Utilized Amount', digits=0)
    budget_balance = fields.Float(compute='_compute_budget_balance', string='Balance Amount', digits=0)
    funds_id = fields.Many2one('pool.of.funds', 'Funds', required=False, domain=[('state','=', 'confirm')])
    remarks = fields.Text('Remarks', track_visibility='onchange')
    percentage = fields.Float(compute='_compute_percentage', string='Surplus or Deficit (%)')
    date_from = fields.Date('Start Date', required=True, track_visibility='onchange')
    date_to = fields.Date('End Date', required=True, track_visibility='onchange')
    crossovered_budget_id = fields.Many2one('crossovered.budget', 'Budget', ondelete='cascade', index=True, required=True, track_visibility='onchange')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', track_visibility='onchange')
    general_budget_id = fields.Many2one('account.budget.post', 'Budgetary Position', required=True, track_visibility='onchange')
    consolidated_budgetary_position = fields.Boolean(related='general_budget_id.consolidated_budgetary_position', store=True)

    @api.multi
    def _compute_practical_amount(self):
        for line in self:
            result = 0.0
            acc_ids = line.general_budget_id.account_ids.ids
            date_to = self.env.context.get('wizard_date_to') or line.date_to
            date_from = self.env.context.get('wizard_date_from') or line.date_from
            if line.analytic_account_id.id and line.funds_id:
                self.env.cr.execute("""
                        SELECT SUM(amount)
                        FROM account_analytic_line
                        WHERE account_id=%s
                            AND tag_id=%s
                            AND (date between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))
                            AND general_account_id=ANY(%s)""",
                                    (line.analytic_account_id.id, line.funds_id.analytic_tag_id.id, date_from, date_to, acc_ids,))
                result = self.env.cr.fetchone()[0] or 0.0
            elif line.analytic_account_id.id:
                self.env.cr.execute("""
                        SELECT SUM(amount)
                        FROM account_analytic_line
                        WHERE account_id=%s
                            AND (date between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))
                            AND general_account_id=ANY(%s)""",
                                    (line.analytic_account_id.id, date_from, date_to, acc_ids,))
                result = self.env.cr.fetchone()[0] or 0.0
            line.practical_amount = result

    @api.multi
    def _compute_budget_balance(self):
        for line in self:
            balance = line.planned_amount + line.practical_amount
            line.budget_balance = balance

    @api.multi
    def _compute_percentage(self):
        for line in self:
            line.percentage = abs((line.practical_amount/line.planned_amount) * 100)

class AccountBudgetPost(models.Model):
    _inherit = "account.budget.post"

    _sql_constrains = [
        ("code_unique",
         "UNIQUE(code)",
         "Code must be unique!."),
    ]

    code = fields.Char('Code', required=True)
    consolidated_budgetary_position = fields.Boolean('Consolidated Budgetary Position')
    crossovered_budget_line_ids = fields.One2many('crossovered.budget.lines', 'general_budget_id', 'Budget Lines')

    @api.onchange('code')
    def onchange_code(self):
        if self.code:
            code = self.code.replace(' ', '')
            self.code = code
