# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import Warning, UserError
from dateutil.relativedelta import relativedelta
from datetime import datetime, date
from hashids import Hashids

# The Purpose to avoid time-wasting when new type appears
MEMBERSHIP_TYPE = [
    ('ordinary', 'Ordinary'),
    ('associate', 'Associate'),
]

DEFAULT_TYPE = 'associate'

MODEL_MEMBERSHIP_APPLICATION = 'membership/application'
MODEL_MEMBERSHIP_DONATION = 'donation/member'

# salt = password for unlock the url
SALT = 'Toa Payoh Seu Teck Sean Tong Tigernix'
MIN_LENGTH = 10
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

hash_url = Hashids(salt=SALT, min_length=MIN_LENGTH, alphabet=ALPHABET)

class MembershipApplication(models.Model):
    _name = "membership.application"
    _description = 'Membership Application'
    _inherit = ['mail.thread']

    @api.model
    def _default_account(self):
        company = self.env.user.company_id
        account_id = company.membership_income_account_id and company.membership_income_account_id.id or False

        return account_id
    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.membership_journal_id and company.membership_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    @api.model
    def _default_funds(self):
        company = self.env.user.company_id
        funds_id = company.default_membership_fund_id and company.default_membership_fund_id.id or False

        return funds_id

    # General
    reg_date = fields.Date('Date of Application / 申请日期',  default=lambda self: fields.datetime.now(), track_visibility='onchange')
    reg_no = fields.Char('Registration Number /  编号', copy=False, track_visibility='onchange')

    remarks = fields.Text('Remark / 备考', track_visibility='onchange')

    # Applicant
    title_id = fields.Many2one('res.partner.title', string="Title / 称号", track_visibility='onchange')
    parent_id = fields.Many2one('res.partner', string='Company / 公司', index=True, track_visibility='onchange')
    name = fields.Char('English Name / 英文姓名', track_visibility='onchange')
    chinese_name = fields.Char('Chinese Name / 中文姓名', track_visibility='onchange')
    mobile = fields.Char('Mobile / 手机', track_visibility='onchange')
    phone = fields.Char('Tel / 电话', track_visibility='onchange')
    street = fields.Char(string='Street', track_visibility='onchange')
    street2 = fields.Char(string='Street 2', track_visibility='onchange')
    zip = fields.Char(string='Postal Code', track_visibility='onchange')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string="Gender / 性别", track_visibility='onchange')
    country_id = fields.Many2one('res.country', string='Country', track_visibility='onchange', default=lambda self: self._get_default_country())
    email = fields.Char('E-mail / 电邮', copy=False, track_visibility='onchange')
    dialect_ids = fields.Many2many('dialect.list', 'membership_application_dialect_list_rel', string="Dialect / 籍贯", track_visibility='onchange')
    birthdate = fields.Date('Date of Birth / 出生日期', track_visibility='onchange')
    occupation = fields.Char('Occupation / 职业', track_visibility='onchange')
    signature = fields.Binary("Applicants Signature / 申请人签名", copy=False, track_visibility='onchange')
    mailing_address = fields.Char('Mailing Address / 邮寄地址', track_visibility='onchange')
    citizenship_id = fields.Many2one('hr.citizenship', string="Citizenship", track_visibility='onchange')
    password = fields.Char('Password')
    
    # NRIC Related 
    identity_no = fields.Char('NRIC No. / 新居民证号码', index=True, size=12, copy=False)
    identity_no_mask = fields.Char('NRIC No. / 新居民证号码', copy=False, size=12, store=True, compute="_compute_identity_no_mask")
    show_identity_no = fields.Char('Show ?', copy=False)
    identity_no_type = fields.Selection([
                        ('bc', 'BC'),
                        ('nric', 'NRIC'),
                        ('passport', 'Passport'),
                        ('fin', 'FIN')
                        ], default='nric')

    identity_type = fields.Selection([
                        ('sc', 'Singapore Citizen'),
                        ('pr', 'Permanent Resident'),
                        ('other', 'Others'),
                        ], string='Identity Type', required=True)

    #Membership Related
    membership_type = fields.Selection(MEMBERSHIP_TYPE, default=DEFAULT_TYPE, string="Membership Type", track_visibility='onchange')
    membership_period_id = fields.Many2one('membership.period', default=lambda self: self._get_default_membership_period(), string="Membership Period", track_visibility='onchange')
    
    donation_ids = fields.One2many('donation.donation', 'membership_application_id', copy=False, string="Donation List")
    donation_amount = fields.Float(string="Total Donation Amount", store=True, compute="_get_donation_amount")
    
    niche_application_id = fields.Many2one('niche.application', string="Niche Application", track_visibility='onchange')
    tablet_application_id = fields.Many2one('tablet.application', string="Tablet Application", track_visibility='onchange')
    
    tablet_advance_id = fields.Many2one('purchase.in.advance', string="Tablet Advance Application", track_visibility='onchange')
    niche_advance_id = fields.Many2one('purchase.in.advance', string="Niche Advance Application", track_visibility='onchange')
    
    tablet_advance_list_ids = fields.One2many('purchase.advance.list', 'membership_application_id', domain=[('purchase_advance_type','=', 'tablet')], track_visibility='onchange')
    niche_advance_list_ids = fields.One2many('purchase.advance.list', 'membership_application_id', domain=[('purchase_advance_type','=', 'niche')], track_visibility='onchange')
    
    #state
    state = fields.Selection([
            ('draft', 'Draft'), 
            ('confirm', 'Submit'), 
            # ('vertified', 'Vertified'),
            ('approve', 'Approved'), 
            ('donated', 'Process'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'), 
            ('reject', 'Rejected'), 
            ], string="State", default='draft', copy="False", track_visibility='onchange')

    #etc
    recommend_by_id = fields.Many2one('res.partner', string="Recommended By / 介绍人签名",track_visibility='onchange')
    accept_term_and_condition = fields.Boolean('I\'m willing to accept Terms & Conditions', track_visibility='onchange')
    term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self: self._get_default_term_condition(), string='Term & Conditions', track_visibility='onchange')
    associate_term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self: self._get_default_associate_term_condition(), string='Term & Conditions', track_visibility='onchange')
    partner_id = fields.Many2one('res.partner', copy=False, string="Member", track_visibility='onchange')
    responsible_user_id = fields.Many2one('res.users', copy=False, string='Attended by / 经手人', track_visibility='onchange')
    color_id = fields.Many2one('color.type.application', string="NRIC Color", track_visibility='onchange')


    # Payment
    account_id = fields.Many2one('account.account','Account', required=True, default=_default_account)
    journal_id = fields.Many2one('account.journal', string="Journal", default=_default_journal, track_visibility='onchange')
    membership_fee_id = fields.Many2one('product.product', compute='_get_membership_fee', string="Membership Fee", copy=False, track_visibility='onchange')
    product_ids = fields.Many2many('product.product', 'product_membership_application_rel', 'product_id', 'membership_application_id', string="Product Lines", track_visibility='onchange')
    total_membership_fee = fields.Float('Membership Fee', compute="_amount_all", store=True, track_visibility='onchange')
    total_product_amount = fields.Float('Total Amount', compute="_total_product_amount_valid_price", store=True, track_visibility='onchange')
    # move_line_ids = fields.One2many('account.move.line', 'memberships_id', string='Payments Received', compute="_get_payment_detail", track_visibility='onchange')
    move_line_ids = fields.One2many('account.move.line', 'memberships_id')
    funds_id = fields.Many2one('pool.of.funds','Funds', required=True, default=_default_funds, domain=[('state','=', 'confirm')])
    voucher_ids = fields.One2many('account.voucher', 'membership_id')
   
    residual = fields.Float('Payable Amount', track_visibility='onchange')
    amount_untaxed = fields.Float(string='Untaxed Fee', store=True,
                                  compute='_amount_all', track_visibility='onchange')
    amount_tax = fields.Float(string='GST', store=True, readonly=False, compute='_amount_all', track_visibility='onchange')
    amount_total = fields.Float(string='Total Amount', store=True,
                                compute='_amount_all', track_visibility='onchange')
    amount_paid = fields.Float(string='Amount Paid', store=True,
                                track_visibility='onchange')
    #user related
    login_id = fields.Many2one('res.users', copy=False, string="Login ID", track_visibility='onchange')
    # hash_id = fields.Char('Hash ID', compute="_get_hash_id", store=True, copy=False)
    link_to_donation = fields.Char('Donation URL', compute="_link_to_donation", store=True, copy=False)
    url_auth_email = fields.Char('URL Authentication E-mail', compute="_link_auth_email", store=True, copy=False)
    
    @api.multi
    def button_dummy(self) :
        for rec in self :
            # rec._link_auth_email()
            rec._link_to_donation()

    @api.multi
    def action_create_niche_application(self) :
        if not self.niche_application_id :
            self.niche_application_id = self.niche_application_id.create({'partner_id' : self.partner_id.id})
        else :
            raise UserError(_("Niche Application already created.")) 

    @api.multi
    def action_create_tablet_application(self) :
        if not self.tablet_application_id :
            self.tablet_application_id = self.tablet_application_id.create({'partner_id' : self.partner_id.id})
        else :
            raise UserError(_("Tablet Application already created.")) 
        
    @api.multi
    def action_create_tablet_in_advance(self) :
        self.tablet_advance_id = self.tablet_advance_id.create({'partner_id' : self.partner_id.id, 'purchase_advance_type' : 'tablet', 'membership_application_id': self.id})
    
    @api.multi
    def action_create_niche_in_advance(self) :
        self.niche_advance_id = self.niche_advance_id.create({'partner_id' : self.partner_id.id, 'purchase_advance_type' : 'niche', 'membership_application_id': self.id})


    # Link
    @api.multi
    def _get_hash_id(self, id):
        hashid = id
        try :
            hashid = hash_url.encode(int(hashid))
        except :
            pass

        return hashid

    @api.multi
    def get_true_id(self):
        hashid = False
        for rec in self :
            hashid = rec.hash_id

            try :
                hashid = hash_url.decode(hashid)
            except :
                pass

        return hashid
 
    # @api.multi
    # def _link_auth_email(self):
    #     url = False
    #     web_param = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
    #     model = 'membership/application'
    #     for rec in self :
    #         hashid = rec._get_hash_id(rec.id)

    #         url = "%s/page/%s/vertification/%s" % (web_param, model, hashid)

    #         rec.url_auth_email = url

    @api.multi
    def _link_to_donation(self):
        url = False
        web_param = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        model = 'donation/member'
        for rec in self :
            records = [donation.id for donation in rec.donation_ids if donation.state != 'done']
            if len(records) > 0 :
                record = records[0]
                hashid = rec._get_hash_id(record)

                url = "%s/page/%s/%s" % (web_param, model, hashid)

            rec.link_to_donation = url


    # Default Value
    @api.multi
    def _get_default_membership_period(self) :
        obj_period = self.env['membership.period']
        get_period = obj_period.search([('default', '=', True)], limit=1)
        return get_period

    @api.multi
    def _get_default_term_condition(self) :
        company = self.env.user.company_id
        term_and_condition = False
        if company.default_membership_term_id :
            term_and_condition = company.default_membership_term_id and company.default_membership_term_id.id or False

        return term_and_condition

    @api.multi
    def _get_default_associate_term_condition(self):
        company = self.env.user.company_id
        term_and_condition = False
        if company.default_associate_membership_term_id:
            term_and_condition = company.default_associate_membership_term_id and company.default_associate_membership_term_id.id or False

        return term_and_condition
    
    @api.multi
    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'SG')], limit=1)
        return country

    #Button Available 

    @api.multi
    def action_draft(self) :
        self.state = 'draft'
        
    @api.multi
    def action_confirm(self) :
        # self._link_auth_email()
        # self.mail_confirm_email()
        # self._link_auth_email()
        self.name_set()
        self.mail_confirm_email()
        self.get_responsible_user()
        self.action_create_res_partner_data()
        self.state = 'confirm'

    @api.multi
    def action_approve(self) :
        self.get_responsible_user()
        self.state = 'approve'

    @api.multi
    def action_reject(self) :
        self.get_responsible_user()
        self.state = 'reject'

    @api.multi
    def action_donated(self) :
        for rec in self :
            if rec.state == 'approve' :
                rec.state = 'donated'
            # if 

    @api.multi
    def action_paid(self) :
        pool_of_funds = self.env['pool.of.funds'].search([('name','=', self.funds_id.name)])
        funds_vals = []
        vals = {
                    'funds_id': pool_of_funds.id,
                    'amount': self.amount_total,
                    'log_for': 'donation',
                    'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                    'account': self.account_id.user_type_id.name,
                }
        funds_vals.append((0, 0, vals))
        pool_of_funds.write({'log_ids':funds_vals})
        if self.membership_type == 'associate' :
            status = self.validation_niche_tablet()
            if not status :
                raise UserError(_("You need to do payment to your niche/tablet application first."))
            
        self.state = 'paid'
        self.action_update_membership()
        self.partner_id.generate_member_id()
        self.login_id = self.action_create_user()
        
    

    @api.multi
    def action_cancel(self) :
        # if self.state == 'paid' :
        #     self.action_cancel_membership()
        # self.get_responsible_user()
        self.state = 'approve'

    @api.multi
    def validation_niche_tablet(self) :
        for rec in self :
            status = False

            if rec.state == 'donated':
                status = True

            return status

    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id

    @api.multi
    def show_identity(self):
        for rec in self:
            rec.show_identity_no = True

    @api.multi
    def hide_identity(self):
        for rec in self:
            rec.show_identity_no = False

    @api.depends('identity_no', 'show_identity_no')
    @api.multi
    def _compute_identity_no_mask(self):
        for rec in self:
            if rec.identity_no:
                if rec.show_identity_no:
                    rec.identity_no_mask = rec.identity_no
                else:
                    rec.identity_no_mask = rec.convert_string(rec.identity_no) 
    
    @api.multi
    def convert_string(self, val):
        if val:
            res = len(val[:-4]) * "*" + val[-4:]
            return res
        return False

    #Create user / Contact / Membership

    def check_nric_formula(self, nric, birthdate=False):
        sc = False
        pr = False
        identity_type = ''
        nric_len = 9
        sg_first_char = ['s', 'S', 't', 'T']
        pr_first_char = ['f', 'F', 'g', 'G']
        special_val = ['g', 'G', 't', 'T']
        digit_count = [2,7,6,5,4,3,2]
        count_amount = 0
        mod_amount = 0
        last_char = ''
        sg_check_letter = {'0' : 'J',
                           '1' : 'Z',
                           '2' : 'I',
                           '3' : 'H',
                           '4' : 'G',
                           '5' : 'F',
                           '6' : 'E',
                           '7' : 'D',
                           '8' : 'C',
                           '9' : 'B',
                           '10' : 'A',
                           }
        pr_check_letter = {'0' : 'X',
                           '1' : 'W',
                           '2' : 'U',
                           '3' : 'T',
                           '4' : 'R',
                           '5' : 'Q',
                           '6' : 'P',
                           '7' : 'N',
                           '8' : 'M',
                           '9' : 'L',
                           '10' : 'K',
                           }
        
        if len(str(nric)) != nric_len:
            raise UserError(_('Error in Identity Number1'))
            
        if birthdate:
            birthyear = datetime.strptime(birthdate, '%Y-%m-%d').year
            if birthyear > 1968:
                nric_birthyear = str(birthyear)[-2:]
                if (nric[1] + nric[2]) != nric_birthyear:
                    raise UserError(_('Error in Identity Number'))

        sc = False
        pr = False
        if nric[0] in sg_first_char:
            sc = True
        elif nric[0] in pr_first_char:
            pr = True
        else:
            raise UserError(_('Error in Identity Number'))

        count_amount += int(digit_count[0]) * int(nric[1])
        count_amount += int(digit_count[1]) * int(nric[2])
        count_amount += int(digit_count[2]) * int(nric[3])
        count_amount += int(digit_count[3]) * int(nric[4])
        count_amount += int(digit_count[4]) * int(nric[5])
        count_amount += int(digit_count[5]) * int(nric[6])
        count_amount += int(digit_count[6]) * int(nric[7])

        if nric[0] in special_val:
            mod_amount = (count_amount + 4) % 11
        else:
            mod_amount = count_amount % 11

        if sc :
            identity_type = 'sc'
            last_char = sg_check_letter[str(mod_amount)]
        elif pr :
            identity_type = 'pr'
            last_char = pr_check_letter[str(mod_amount)]

        if str(nric[8]) != str(last_char):
            raise UserError(_('Error in Identity Number'))
            
        return identity_type

    @api.onchange('identity_no')
    def onchange_identity_no(self):
        warning = {}
        vals = {}
        if self.identity_no :
            identity_no = str(self.identity_no).upper().strip()
            vals['identity_no'] = identity_no
            user_id = self.env['res.partner'].search([('identity_no', '=ilike', identity_no)])
            sg_citizen = self.env['hr.citizenship'].search([('is_singaporean', '=', True)])
            
            if not self.birthdate:
                self.identity_no = False
                return {'warning': {
                    'title': "Warning",
                    'message': "Please fill in birthdate first",
                    }
                }
            
            if self.identity_no_type in ['bc', 'nric']:
                check_nric = self.check_nric_formula(identity_no, self.birthdate)
                if check_nric[0:2] == 'sc':
                    self.citizenship_id = sg_citizen[0]
                    self.identity_type = 'sc'
                elif check_nric[0:2] == 'pr':
                    self.citizenship_id = False
                    self.identity_type = 'pr'
            else:
                self.identity_type = 'other'
            # add check by len for case user accidentally change the identity no then want to change it back.
            # if user create new partner with same identity no, system would prompt warning from constraint, so there will no duplicated partner.
            if len(user_id) > 1 : 
                title = "Warning"
                message = "Identity Number(%s) already exists!" % identity_no
                warning = {
                        'title': title,
                        'message': message,
                }
                vals['identity_no'] = False
            return {
                    'value':vals,
                    'warning':warning}
                    
        else:
            self.citizenship_id = False
            self.identity_type = False
    
    @api.multi
    def action_search_partner(self) :
        obj_partner = self.env['res.partner']

        # =ilike ignore case sentive, so it's really good to use in searching email
        partner = obj_partner.search([('email', '=ilike', self.email)])

        return partner


    @api.multi
    def action_create_res_partner_data(self) :
        obj_partner = self.env['res.partner']
        partner = self.action_search_partner()

        data = {}

        data['title']            = self.title_id and self.title_id.id
        data['name']             = self.name
        data['chinese_name']     = self.chinese_name
        data['identity_no']      = self.identity_no
        data['identity_no_type'] = self.identity_no_type
        data['identity_type']    = self.identity_type
        data['citizenship_id']   = self.citizenship_id and self.citizenship_id.id
        data['mobile']           = self.mobile
        data['phone']            = self.phone
        data['street']           = self.street
        data['street2']          = self.street2
        data['zip']              = self.zip
        data['gender']           = self.gender
        data['country_id']       = self.country_id and self.country_id.id
        data['email']            = self.email
        data['dialect_ids']      = self.dialect_ids
        data['birthdate']        = self.birthdate
        data['occupation']       = self.occupation
        data['mailing_address']  = self.mailing_address
        data['function']         = self.occupation

        if not partner :
            partner = obj_partner.create(data)
            self.write({'partner_id' : partner.id})
        else :
            if partner.member :
                raise UserError(_("Details Information for this member already recorded in membership module"))
            else :
                partner.write(data)
                self.write({'partner_id' : partner.id})

    @api.multi
    def action_create_user(self) :
        # _create_user is searching and create so don't worry about overlapping
        user = self.partner_id._create_user(self.partner_id.membership_number)
        user.mail_create_user()
        return user
    
    @api.multi
    def action_update_membership(self) :
        today = datetime.now().date()

        data = {'membership_type': self.membership_type, 'member': True, 'member_since': today,
                'membership_state': 'active'}

        if self.membership_type == 'ordinary' :
            data['membership_start']    = today
            data['membership_stop']     = today + relativedelta(years=self.membership_period_id.period)
        else :
            data['membership_previous_validity'] = today

        if self.partner_id :
            self.partner_id.write(data)
        else :
            raise UserError(_("Can't found member data, may recheck your application"))
    
    @api.multi
    def action_cancel_membership(self) :
        data = {'membership_type': False, 'member': False, 'member_since': False, 'membership_state': 'inactive'}

        if self.membership_type == 'ordinary' :
            data['membership_start']    = False
            data['membership_stop']     = False

        if self.partner_id :
            self.partner_id.write(data)
        else :
            raise UserError(_("Can't found member data, may recheck your application"))

    # Payment/donation

    @api.multi
    def action_create_donation(self) :
        data = {'partner_id': self.partner_id.id,
                'payment_method_id': self.create_uid.company_id.donation_journal_id and self.create_uid.company_id.donation_journal_id.id,
                'membership_application_id': self.id, 'pay_now': 'pay_now'}

        self.donation_ids.create(data)
        self._link_to_donation()

    @api.depends('product_ids', 'product_ids.valid_price')
    @api.multi
    def _total_product_amount_valid_price(self) :
        for rec in self :
            list_product = rec.product_ids
            total_product_amount = 0.00
            if list_product :
                for product in list_product :
                    total_product_amount += product.valid_price
            
            rec.total_product_amount = total_product_amount

    @api.multi
    def donation_amount_validation(self) :
        for rec in self :
            if rec.donation_amount >= rec.membership_fee_id.min_membership_donation and rec.state != 'paid' :
                rec.action_donated()

    @api.depends('donation_ids', 'donation_ids.total_amount_donation', 'state', 'donation_ids.state')
    @api.multi
    def _get_donation_amount(self) :
        donation_amount = 0.00
        for rec in self :
            for donation in rec.donation_ids :
                if donation.state == 'done' :
                    donation_amount += donation.total_amount_donation

            rec.donation_amount = donation_amount

    @api.onchange('donation_ids', 'donation_ids.total_amount_donation', 'state', 'donation_ids.state')
    @api.multi
    def donation_status(self) :
        if self.state == 'process' :
            self.donation_amount_validation()

    @api.depends('membership_type')
    @api.multi
    def _get_membership_fee(self):
        obj_product = self.env['product.product']
        for rec in self :
            fee = False
            membership_type = rec.membership_type
            membership_period = rec.membership_period_id
            membership_fee = False

            if membership_type and membership_type == 'ordinary' :
                fee = obj_product.search([
                    ('active', '=', True), 
                    ('membership', '=', True), 
                    ('type', '=', 'service'), 
                    ('membership_fee_type', '=', 'application') ,
                    ('membership_type', '=', membership_type),
                    ('membership_period_id', '=', membership_period.id)
                    ], limit=1)
            else :
                fee = obj_product.search([
                    ('active', '=', True),
                    ('membership', '=', True), 
                    ('type', '=', 'service'), 
                    ('membership_fee_type', '=', 'application') ,
                    ('membership_type', '=', membership_type)
                    ], limit=1)


            if fee :
                membership_fee = fee
            # else :
            #     raise UserError(_("No membership fee founds, May go Product Module to create the records."))


            rec.membership_fee_id = membership_fee

    @api.depends('membership_fee_id', 'membership_fee_id.valid_price', 'product_ids')
    @api.multi
    def _amount_all(self) :
        for rec in self :
            membership_fee = rec.membership_fee_id
            list_product = rec.product_ids
            amount_untaxed = 0.00
            amount_tax= 0.00
            amount_paid = 0.00
            residual = 0.00
            list_taxes = []
            member_amount = 0.00
            product_amount = 0.00
            total_product_amount = 0.00

            if membership_fee :
                taxes_member = []
                member_amount = membership_fee.valid_price
                taxes_member = [float("%.2f" % (member_amount * (taxes.amount / 100))) for taxes in membership_fee.taxes_id if membership_fee.taxes_id]
                list_taxes += taxes_member
            
            if list_product :
                for product in list_product :
                    taxes_product = []
                    product_amount = product.valid_price
                    total_product_amount += product_amount
                    taxes_product = [float("%.2f" % (product_amount * (taxes.amount / 100))) for taxes in product.taxes_id if product.taxes_id]
                    list_taxes += taxes_product

            amount_untaxed = member_amount + total_product_amount
            amount_tax = sum(list_taxes)

            rec.amount_untaxed = amount_untaxed
            rec.amount_tax = amount_tax
            rec.amount_total = amount_untaxed + amount_tax
            rec.total_membership_fee = membership_fee.valid_price
            # rec.write({'amount_untaxed' : amount_untaxed, 'amount_tax' : amount_tax, 'amount_total' : amount_untaxed + amount_tax})
    
    @api.onchange('birthdate')
    @api.multi
    def birthdate_validation(self):
        for rec in self :
            if rec.birthdate:
                birth = datetime.strptime(rec.birthdate, '%Y-%m-%d')
                today = datetime.today().date()
                age = relativedelta(today, birth).years
                if age < 18 :
                    rec.birthdate = False
                    return {'warning': {
                        'title': "Warning",
                        'message': "Your age is below than 18",
                        }
                    }

    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no : 
            if self.membership_type == 'associate' :
                self.reg_no = self.env['ir.sequence'].next_by_code('associate.membership.application.seq')
            elif self.membership_type == 'ordinary' :
                self.reg_no = self.env['ir.sequence'].next_by_code('ordinary.membership.application.seq')
            else :
                raise Warning(_('Please to key-in the membership type before proceed to the next step.'))
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.reg_no or _('New Membership Application')
            result.append((rec.id, name))

        return result

    @api.model
    def create(self, vals):
        if vals.get('email', False) :
            vals['email'] = vals['email'].lower().strip()
        if not vals.get('signature', False) :
            raise Warning(_('Please to key-in the membership type before proceed to the next step.'))
        if not vals.get('term') :
            pass

        return super(MembershipApplication, self).create(vals)


    @api.multi
    def write(self, vals):
        if vals.get('email', False) :
            vals['email'] = vals['email'].lower().strip()

        result = super(MembershipApplication, self).write(vals)

        return result

    #mail related

    @api.multi
    def mail_confirm_email(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_confirmation') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : self.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

    @api.multi
    def mail_approve(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_ordinary_approve') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : self.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()

    @api.multi
    def mail_paid(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_ordinary_paid') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : self.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()
    
    @api.multi
    def get_dialects(self):
        dialect_list = ''
        for rec in self:
            if rec.dialect_ids:
                dialects = [d.name for d in self.env['dialect.list'].search([('id', 'in', rec.dialect_ids.ids)])]
                dialect_list = ", ".join(dialects)
        return dialect_list

    @api.multi
    def call_membership_payment_wizard(self):
        vals = {}
        return {
            'name': _('Membership Payment'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.voucher.donation',
            'target': 'new',
            'context': {'default_membership_id': self.id, 'default_amount': self.amount_total, 'default_funds_id': self.funds_id.id, 'default_reg_no': self.reg_no, 'default_journal_id': self.journal_id.id ,'default_partner_id':self.partner_id.id}
        }