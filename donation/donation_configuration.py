from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
model_fields = fields
from datetime import datetime
from dateutil import relativedelta

DRAFT_STATE = {'draft': [('readonly', False)]}

class DonationCampaign(models.Model):
    _name = 'donation.campaign'
    _description = "Donation Campaign"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'name asc'

    name = fields.Char(string="Name", required=True, track_visibility='onchange')
    description = fields.Text(string="Description", track_visibility='onchange')
    default = fields.Boolean('Default', track_visibility='onchange', help="It will be chosen as default interval on donation module")

    @api.multi
    def write(self, vals):
        if vals.get('default', True) :
            get_default = self.env['donation.campaign'].search([('default', '=', True)], limit=1)
            if get_default :
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(DonationCampaign, self).write(vals)
    
    @api.model
    def create(self, vals):
        if vals.get('default', True) :
            get_default = self.env['donation.campaign'].search([('default', '=', True)], limit=1)
            if get_default :
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(DonationCampaign, self).create(vals)
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result

class DonationInterval(models.Model):
    _name = "donation.interval"
    _description = 'Donation Interval'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Name', track_visibility='onchange')
    interval = fields.Integer('Interval', track_visibility='onchange')
    interval_type = fields.Selection([
        ('days', 'Days'),
        ('weeks', 'Weeks'),
        ('months', 'Months'),
        ('years', 'Years')
    ], string="Interval Type", default='months', required=True, track_visibility='onchange')
    default = fields.Boolean('Default', track_visibility='onchange', help="It will be chosen as default interval on donation module")

    @api.multi
    def write(self, vals):
        if vals.get('default', True) :
            get_default = self.env['donation.interval'].search([('default', '=', True)], limit=1)
            if get_default :
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(DonationInterval, self).write(vals)
    
    @api.model
    def create(self, vals):
        if vals.get('default', True) :
            get_default = self.env['donation.interval'].search([('default', '=', True)], limit=1)
            if get_default :
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(DonationInterval, self).create(vals)

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result

class Category(models.Model):
    _name = 'donation.category'
    _description = "Donation Category"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'name asc'
    
    name = fields.Char(string="Name", track_visibility='onchange')
    type = fields.Selection([
        ('medical', 'Medical'),
        ('charity', 'Charity / Coffin'),
        ('festival', 'Festival'),
        ('other', 'Others'),
    ], string="Type", track_visibility='onchange')
    description = fields.Text(string="Description", track_visibility='onchange')
            
    @api.constrains('name')
    def unique_category(self):
        for rec in self:
            existings = self.env['donation.category'].search([('name', '=', rec.name)], limit=2)
            if len(existings._ids) > 1:
                raise ValidationError("This category already exists.")
    
    @api.multi
    def unlink(self):
        for rec in self:
            if rec.type:
                raise UserError(_("Failed to delete the category, the category is used in the system."))
        return super(Category, self).unlink()