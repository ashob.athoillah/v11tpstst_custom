# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import Warning
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DSDF
import odoo.addons.decimal_precision as dp
import time
from itertools import chain

class BankAccRecStatement(models.Model):
    _name = "bank.acc.rec.statement"
    _description = 'Bank Reconcile'
    _inherit = ['bank.acc.rec.statement','mail.thread', 'utm.mixin']
    _order = 'ending_date DESC'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(BankAccRecStatement, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        report_bank_rec = self.env.ref('v11_tpstst_custom.action_report_bank_reconcile')
        context = self._context
        if context.get('default_daily_cashier') != False:
            for print_submenu in res.get('toolbar', {}).get('print', []):
                if print_submenu['id'] == report_bank_rec.id:
                    res['toolbar']['print'].remove(print_submenu)
        return res


    name = fields.Char('Name', required = True, size = 64,
                   states={'done':[('readonly', True)]}, default='/', copy=False,
                   help="This is a unique name identifying the \
                   statement (e.g. Bank X January 2018).")
    ending_date = fields.Date('Ending Date', required = True,
                              states = {'done':[('readonly', True)]},
                              default = fields.Datetime.now,
                              help = "The ending date of your bank statement.")
    ending_balance = fields.Float('Ending Balance per Bank', required = True,
                                  digits = dp.get_precision('Account'),
                                  help = "The Ending Balance on your bank \
                                  statement.",
                                  states = {'done':[('readonly', True)]})
    ending_balance_tb = fields.Float('Ending Balance by TB',
                                  digits = dp.get_precision('Account'),
                                  help = "The Ending Balance on your Trial Balance",
                                  compute='_get_ending_balance_tb')
    total_deposit_transit = fields.Float('Total Deposit in Transit',
                                  digits = dp.get_precision('Account'),
                                  compute='_get_ending_balance_tb')
    total_outstanding_dis = fields.Float('Total Outstanding Disbursement',
                                  digits = dp.get_precision('Account'),
                                  compute='_get_ending_balance_tb')
    date = fields.Date('Date', related="ending_date")
    daily_cashier = fields.Boolean('Daily Cashier Closing',default=True)
    state = fields.Selection([('draft', 'Draft'),
        ('to_be_reviewed', 'Ready for Review'),
        ('confirm', 'Waiting  for Finance Confirm'),
        ('process', 'Process'), ('done', 'Done'),
        ('cancel', 'Cancel')], 'State',
        index = True, readonly = True, default = 'draft', track_visibility='onchange',)
    payment_method_ids = fields.Many2many('account.journal','Available Payment Method', related='company_id.daily_cashier_payment_method_ids', readonly=True)
    payment_method_id = fields.Many2one('account.journal', string="Payment Method", required=False, states = {'done':[('readonly', True)]})
    journal_id = fields.Many2one('account.journal', 'Journal', states = {'done':[('readonly', True)]}, domain=[('type','=','cash')])
    bank_in_id = fields.Many2one('bank.in.statement', 'Bank-In Statement', readonly=True)
    attachment_ids = fields.One2many('ir.attachment', 'daily_closing_id', 'Receipts', readonly=True)
    bank_in = fields.Boolean('Bank-In', readonly=True, compute='_get_bank_in_id', copy=False)
    bank_in_date = fields.Date('Bank-In Date', related='bank_in_id.bank_in_date', readonly=True)

    @api.multi
    def _get_bank_in_id(self):
        for record in self :
            bank_in = False
            if record.bank_in_id:
                bank_in =  True
            record.bank_in = bank_in

    @api.multi
    def _get_ending_balance_tb(self):
        for record in self :
            total = 0.0
            total_deposit = 0.0
            total_outstanding = 0.0
            if record.account_id and record.ending_date and record.journal_id:
                first_date = record.ending_date[:4]+'-01-01'
                query = '''
                select coalesce(sum(coalesce(debit,0.0) - coalesce(credit,0.0)),0.0)
                from account_move_line aml
                join account_move mv on mv.id=aml.move_id
                where aml.account_id=%s
                    and mv.state='posted'
                    and mv.date ='%s'
                '''%(record.account_id.id,record.ending_date)
                self._cr.execute(query)
                query_res = self._cr.fetchall()
                total = query_res and query_res[0] and query_res[0][0] or 0.0

                query = '''
                with cleared_move_lines as (
                    select move_line_id
                    from bank_acc_rec_statement_line recline
                    join bank_acc_rec_statement bankrec on bankrec.id=recline.statement_id
                    join account_move_line aml on aml.id=recline.move_line_id
                    where coalesce(recline.cleared_bank_account,False) is True
                        and coalesce(bankrec.daily_cashier,False) is not True
                        and aml.account_id=%(account_id)s
                        and bankrec.ending_date = '%(ending_date)s'
                        
                )
                select coalesce(sum(coalesce(debit,0.0) - coalesce(credit,0.0)),0.0)
                from account_move_line aml
                join account_move mv on mv.id=aml.move_id
                where aml.account_id=%(account_id)s
                    and coalesce(aml.debit,0.0) > 0
                    and mv.state='posted'
                    and coalesce(aml.force_cleared_bank_account,False) is not True
                    and aml.id not in (select move_line_id from cleared_move_lines)
                    and mv.date ='%(ending_date)s'
                    /*and mv.date >='%(first_date)s'
                    and mv.journal_id=%(journal_id)s*/
                '''%({'account_id':record.account_id.id,
                      'ending_date':record.ending_date,
                      'first_date':first_date,
                      'journal_id':record.journal_id.id,
                      })
                self._cr.execute(query)
                query_res = self._cr.fetchall()
                total_deposit = query_res and query_res[0] and query_res[0][0] or 0.0

                query = '''
                with cleared_move_lines as (
                    select move_line_id
                    from bank_acc_rec_statement_line recline
                    join bank_acc_rec_statement bankrec on bankrec.id=recline.statement_id
                    join account_move_line aml on aml.id=recline.move_line_id
                    where coalesce(recline.cleared_bank_account,False) is True
                        and coalesce(bankrec.daily_cashier,False) is not True
                        and aml.account_id=%(account_id)s
                        and bankrec.ending_date = '%(ending_date)s'
                        
                )
                select coalesce(sum(coalesce(debit,0.0) - coalesce(credit,0.0)),0.0)
                from account_move_line aml
                join account_move mv on mv.id=aml.move_id
                where aml.account_id=%(account_id)s
                    and coalesce(aml.credit,0.0) > 0
                    and mv.state='posted'
                    and coalesce(aml.force_cleared_bank_account,False) is not True
                    and aml.id not in (select move_line_id from cleared_move_lines)
                    and mv.date ='%(ending_date)s'
                    /*and mv.date >='%(first_date)s'
                    and mv.journal_id=%(journal_id)s*/
                '''%({'account_id':record.account_id.id,
                      'ending_date':record.ending_date,
                      'first_date':first_date,
                      'journal_id':record.journal_id.id,
                      })
                self._cr.execute(query)
                query_res = self._cr.fetchall()
                total_outstanding = query_res and query_res[0] and query_res[0][0] or 0.0
                total_outstanding = total_outstanding * -1
            record.ending_balance_tb = total
            record.total_deposit_transit = total_deposit
            record.total_outstanding_dis = total_outstanding

    @api.multi
    def _name_filter(self):
        same_name_statement_ids = []
        for statement in self:
            if not statement.daily_cashier:
                same_name_statement_ids = self.search([('name','=',statement.name),('company_id','=',statement.company_id.id),('account_id','=',statement.account_id.id)])
        if len(same_name_statement_ids) > 1:
            return False
        return True

    _constraints = [
        (_name_filter, 'The name of the statement must be unique per company \
        and G/L account!', ['name','company_id','account_id']),
     ]

    @api.multi
    def unlink(self):
        attachment_obj = self.env['ir.attachment']
        for statement in self:
            if statement.state != 'cancel':
                if statement.daily_cashier:
                    raise Warning(_('Please cancel before deleting this daily cashier closing.'))
                else:
                    raise Warning(_('Please cancel before deleting this record.'))
            attachment_list = attachment_obj.search([('res_model','=','bank.acc.rec.statement'),('res_id', '=', statement.id)])
            if statement.daily_cashier:
                if statement.name and statement.name != '/':
                    raise Warning(_('Cannot remove the Daily Cashier Closing.\nThe number already generated!'))
                attachment_list.with_context(ignore_it_checking=True).unlink()
            else:
                if statement.name and statement.name != '/':
                    raise Warning(_('Cannot remove Bank Reconciliation.\nThe number already generated!'))
        return super(BankAccRecStatement, self).unlink()

    @api.multi
    def unclear_bank_rec_line(self,recline_ids=[]):
        if not recline_ids:
            return False
        recline_obj = self.env['bank.acc.rec.statement.line']
        reclines = recline_obj.browse(recline_ids)
        reclines.write({'cleared_bank_account':False})


    @api.multi
    def open_wizard_unclear_debit(self):
        rec = self[0]
        self = self.with_context({'statement_ids':[rec.id],'recline_type':'dr'})
        result = self.env.ref('v11_tpstst_custom.action_bank_rec_line_unselect').read()[0]
        result['context'] = self._context
        return result


    @api.multi
    def open_wizard_unclear_credit(self):
        rec = self[0]
        self = self.with_context({'statement_ids':[rec.id],'recline_type':'cr'})
        result = self.env.ref('v11_tpstst_custom.action_bank_rec_line_unselect').read()[0]
        result['context'] = self._context
        return result

    @api.onchange('payment_method_id')
    def onchange_payment_method(self):
        val = {'value': {'journal_id': False}}
        if self.payment_method_id :
            payment_method = self.payment_method_id
            val['value']['journal_id'] = payment_method or False
        return val

    @api.onchange('journal_id')
    def onchange_journal_id(self):
        val = {'value': {'account_id': False}}
        if self.journal_id and self.daily_cashier:
            val['value']['account_id'] = self.journal_id.default_credit_account_id and self.journal_id.default_credit_account_id.id or False
        if self.journal_id and (not self.daily_cashier):
            val['value']['account_id'] = self.journal_id.default_credit_account_id or self.journal_id.default_debit_account_id or False
        return val

    @api.depends('credit_move_line_ids',
                 'credit_move_line_ids.cleared_bank_account',
                 'credit_move_line_ids.cleared_dcc_account',
                 'debit_move_line_ids',
                 'debit_move_line_ids.cleared_bank_account',
                 'debit_move_line_ids.cleared_dcc_account')
    def _get_balance(self):
        precision_obj = self.env['decimal.precision']
        account_precision = precision_obj.precision_get('Account')
        for statement in self:
            sum_of_credits = 0.0
            sum_of_debits = 0.0
            cleared_balance = 0.0
            difference = 0.0
            sum_of_credits_lines = 0.0
            sum_of_debits_lines = 0.0
            if not statement.daily_cashier:
                for line in statement.credit_move_line_ids:
                    sum_of_credits += line.cleared_bank_account and \
                    round(line.amount, account_precision) or 0.0
                    sum_of_credits_lines += line.cleared_bank_account and 1.0 or \
                    0.0
                for line in statement.debit_move_line_ids:
                    sum_of_debits += line.cleared_bank_account and \
                    round(line.amount, account_precision) or 0.0
                    sum_of_debits_lines += line.cleared_bank_account and 1.0 or 0.0
            else:
                for line in statement.credit_move_line_ids:
                    sum_of_credits += line.cleared_dcc_account and \
                    round(line.amount, account_precision) or 0.0
                    sum_of_credits_lines += line.cleared_dcc_account and 1.0 or \
                    0.0
                for line in statement.debit_move_line_ids:
                    sum_of_debits += line.cleared_dcc_account and \
                    round(line.amount, account_precision) or 0.0
                    sum_of_debits_lines += line.cleared_dcc_account and 1.0 or 0.0
            cleared_balance = round((sum_of_debits - sum_of_credits),\
                                    account_precision)
            difference = round((statement.ending_balance -
                                statement.starting_balance) - cleared_balance,\
                               account_precision)
            statement.sum_of_credits = sum_of_credits
            statement.sum_of_debits = sum_of_debits
            statement.cleared_balance = cleared_balance
            statement.difference = difference
            statement.sum_of_credits_lines = sum_of_credits_lines
            statement.sum_of_debits_lines = sum_of_debits_lines

    def _get_last_reconciliation(self, account_id, daily_cashier):
        res = self.search([('account_id', '=', account_id),
                           ('state', 'not in', ['cancel','draft']),('daily_cashier','=',daily_cashier)],
                          order = "ending_date desc", limit = 1)
        if self.id :
            res = self.search([('account_id', '=', account_id),
                               ('state', 'not in', ['cancel','draft']),('daily_cashier','=',daily_cashier),('id','!=',self.id)],
                              order = "ending_date desc", limit = 1)
        if res and res.ids:
            return res[0]
        else:
            return None

    @api.onchange('account_id')
    def onchange_account_id_zero_starting_balance(self):
        val = {'value':{}}
        last_rec = self._get_last_reconciliation(self.account_id.id, self.daily_cashier)
        if not last_rec:
            if not self.daily_cashier:
                val['value']['starting_balance'] = 0
        return val

    @api.onchange('payment_method_id','account_id', 'ending_date', 'suppress_ending_date_filter',
        'keep_previous_uncleared_entries')
    def onchange_account_id(self):
        val = {'value': {'credit_move_line_ids': [],
                         'debit_move_line_ids': [],
                         'multi_currency': False,
                         'company_currency_id': False,
                         'account_currency_id': False, }}
        last_rec = self._get_last_reconciliation(self.account_id.id, self.daily_cashier)
        if last_rec and last_rec.ending_date:
            e_date = (datetime.strptime(last_rec.ending_date, DSDF) + timedelta(days = 1)).strftime(DSDF)
            val['value']['exchange_date'] = e_date
            if not self.daily_cashier:
                val['value']['starting_balance'] = last_rec.ending_balance
        elif self.ending_date:
            dt_ending = datetime.strptime(self.ending_date, DSDF, ) + timedelta(days = -1)
            if dt_ending.month == 1:
                dt_ending = dt_ending.replace(year = dt_ending.year - 1, month = 12)
            else:
                prev_month = (dt_ending.replace(day = 1) - timedelta(days = 1))
                if dt_ending.day <= prev_month.day:
                    dt_ending = dt_ending.replace(month = dt_ending.month - 1)
                else:
                    dt_ending = prev_month

            val['value']['exchange_date'] = dt_ending.strftime(DSDF)

        move_line_ids = []
        acc_curr_id = self.account_id.currency_id
        cmpny_curr_id = self.account_id.company_id.currency_id

        if acc_curr_id and cmpny_curr_id and acc_curr_id.id != cmpny_curr_id.id:
            val['value']['multi_currency'] = True

        for statement in self:
            statement_lines = statement.credit_move_line_ids + statement.debit_move_line_ids
            move_line_ids = [line.move_line_id.id for line in statement_lines if line.move_line_id]

        domain = [('account_id', '=', self.account_id.id),
                  ('move_id.state', '=', 'posted'),
                  ('journal_id.type', '!=', 'situation'),
                 ]

        if self.daily_cashier:
            domain = [('journal_id', '=', self.payment_method_id.id),
                      ('account_id', '=', self.account_id.id),
                      ('move_id.state', '=', 'posted'),
                      ('journal_id.type', '!=', 'situation'),
                      ]
        if not self.suppress_ending_date_filter:
            domain += [('date', '=', self.ending_date)]
        line_ids = self.env['account.move.line'].search(domain)
        for line in line_ids:
            include = True
            if line.move_id.reverse_move_ids and self.daily_cashier:
                for reverse_move in (line and line.move_id and line.move_id.reverse_move_ids):
                    if reverse_move.date == self.ending_date:
                        include = False
                        break
            if not include:
                continue
            if line.id not in move_line_ids:
                res = self._get_move_line_write(line,
                                                val['value']['multi_currency'])
            else:
                res = self._get_exits_move_line(line)
                if res == {} :
                    res = self._get_move_line_write(line,
                                                val['value']['multi_currency'])
            if not self.daily_cashier:
                res['cleared_bank_account'] = True
            else:
                res['cleared_dcc_account'] = True

            if self.daily_cashier:
                if res.get('type') == 'dr':
                    val['value']['debit_move_line_ids'].insert(0, res)
            else:
                if res.get('type') == 'cr':
                    val['value']['credit_move_line_ids'].insert(0, res)
                else:
                    val['value']['debit_move_line_ids'].insert(0, res)

        return val

    @api.multi
    def refresh_record(self):
        to_write = {'credit_move_line_ids': [],
                    'debit_move_line_ids': [],
                    'multi_currency': False
                    }
        for obj in self:
            if not obj.account_id:
                continue
            account_curr_id = obj.account_id.currency_id
            cmpny_curr_id = obj.account_id.company_id.currency_id
            if account_curr_id and cmpny_curr_id and \
            account_curr_id.id != cmpny_curr_id.id:
                to_write['multi_currency'] = True
            move_line_ids = [
                line.move_line_id.id
                for line in obj.credit_move_line_ids + obj.debit_move_line_ids
                if line.move_line_id]

            for line in obj.debit_move_line_ids:
                if obj.daily_cashier and (line.move_line_id.move_id.reversed_from or line.move_line_id.move_id.reverse_move_ids.ids or (not line.move_line_id) or (line.move_line_id.advance_voucher_id and line.move_line_id.advance_voucher_id.refund_payment)) :
                    line.unlink();
                elif not line.move_line_id:
                    line.unlink()

            for line in obj.credit_move_line_ids:
                if obj.daily_cashier and (line.move_line_id.move_id.reversed_from or line.move_line_id.move_id.reverse_move_ids.ids or (not line.move_line_id) or (line.move_line_id.advance_voucher_id and line.move_line_id.advance_voucher_id.refund_payment)):
                    line.unlink();
                elif not line.move_line_id:
                    line.unlink()

            domain = [
                ('id', 'not in', move_line_ids),
                ('account_id', '=', obj.account_id.id),
                ('move_id.state', '=', 'posted'),
                ('cleared_bank_account', '=', False),
                ('journal_id.type', '!=', 'situation')]
            if not self.daily_cashier:
                domain.append(('force_cleared_bank_account', '=', False))
            if obj.daily_cashier:
                domain = [
                    ('id', 'not in', move_line_ids),
                    ('payment_method_id', '=', obj.payment_method_id.id),
                    ('account_id', '=', self.account_id.id),
                    ('move_id.state', '=', 'posted'),
                    ('cleared_dcc_account', '=', False),
                    ('draft_assigned_to_dcc', '=', False),
                    ('journal_id.type', '!=', 'situation'),
                    ('move_id.reversed_from', '=', False),
            ]
            if not obj.keep_previous_uncleared_entries:
                if not obj.daily_cashier:
                    domain += [('draft_assigned_to_statement', '=', False)]
                else:
                    domain += [('draft_assigned_to_dcc', '=', False)]
            if not obj.suppress_ending_date_filter:
                domain += [('date', '=', obj.ending_date)]
            lines = self.env['account.move.line'].search(domain)
            for line in lines:
                include = True
                if line.move_id.reverse_move_ids and self.daily_cashier:
                    for reverse_move in (line and line.move_id and line.move_id.reverse_move_ids):
                        if reverse_move.date == obj.ending_date:
                            include = False
                            break
                if not include:
                    continue
                if obj.keep_previous_uncleared_entries:
                    if not line.is_b_a_r_s_state_done():
                        continue
                res = (0, 0,
                       self._get_move_line_write(line,
                                                 to_write['multi_currency']))
                if res and res[2]:
                    if not obj.daily_cashier:
                        res[2]['cleared_bank_account'] = True
                    else:
                        res[2]['cleared_dcc_account'] = True
                if self.daily_cashier:
                    if res and res[2] :
                        if res[2].get('type') == 'dr':
                            to_write['debit_move_line_ids'].append(res)
                else:
                    if res and res[2] :
                        if res[2].get('type') == 'dr':
                            to_write['debit_move_line_ids'].append(res)
                        if res[2].get('type') == 'cr':
                            to_write['credit_move_line_ids'].append(res)
            obj.write(to_write)

    @api.multi
    def check_closing_difference_balance(self):
        """Check if difference balance is zero or not."""
        for statement in self:
            if statement.difference != 0.0:
                raise Warning(_("Prior to reconciling a statement, all \
                differences must be accounted for and the Difference balance \
                must be zero. Please review and make necessary changes."))

    @api.multi
    def action_confirm(self):
        """Change the status of statement from 'draft' to 'confirm'."""
        #  If difference balance more than zero prevent further processing
        for rec in self:
            rec.check_closing_difference_balance()
            sequence = rec.name
            if not rec.name or rec.name == '/':
                sequence = self.env['ir.sequence'].next_by_code('bank.acc.rec.statement.seq')
            self.write({'name':sequence, 'state':'confirm'})
        return True

    @api.multi
    def action_finance_confirm(self):
        """Change the status of statement to 'done'."""
        #  If difference balance more than zero prevent further processing

        # no user restriction at the moment
        # finance_ids = [finance.id for finance in self.env['res.company'].browse(self.company_id.id).cashier_closing_officer_ids]
        # if self._uid not in finance_ids:
        #     raise Warning(_('Only finance officer can confirm, please contact your administrator!'))

        self.check_closing_difference_balance()
        self.write({'state': 'done'})
        return True

    @api.multi
    def action_unconfirm(self):
        """Change the status of statement back to 'confirm'."""
        #  If difference balance more than zero prevent further processing

        # no user restriction at the moment
        # finance_ids = [finance.id for finance in self.env['res.company'].browse(self.company_id.id).cashier_closing_officer_ids]
        # if self._uid not in finance_ids:
        #     raise Warning(_('Only finance officer can unconfirm, please contact your administrator!'))

        self.write({'state': 'confirm'})
        return True

    @api.multi
    def attach_receipt(self):
        """Check if difference balance is zero or not."""
        for statement in self:
            ctx = {'default_name': 'Bank-In Receipt ' + str(statement.name),'default_res_id':statement.id,
            'default_res_model':'bank.acc.rec.statement','default_reload_view':True,
            'default_daily_closing_id':statement.id};
            return {
                'type': 'ir.actions.act_window',
                'name': 'Attachment',
                'res_model': 'ir.attachment',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'context': ctx
            }

    def _get_move_line_write(self, line, multi_currency):
        amount = 0.0
        if multi_currency:
            amount = line and line.amount_currency or 0.0
        else:
            amount = line.credit or line.debit or 0.0

        #because we dont use payment method but only journal_id
        move_line_id = line.id
        res = {}
        query = '''
            select coalesce(line.ref,'') as ref,
                line.partner_id,
                coalesce(partner.name,'') as partner_name,
                line.currency_id,
                line.name,
                line.journal_id,
                coalesce(advoucher.payment_method_id,(
                    select voucher.payment_method_id from account_voucher voucher where voucher.move_id=line.move_id limit 1)
                    ) as other_payment_method_id
            from account_move_line line
            left join res_partner partner on partner.id=line.partner_id
            left join advance_account_voucher advoucher on advoucher.id=line.advance_voucher_id
            where line.id=%s
        '''%move_line_id
        self._cr.execute(query)
        query_res = self._cr.dictfetchall()
        query_line_res = query_res and query_res[0] or {}
        if query_line_res:
            res.update({'ref': query_line_res['ref'],
               'date': line.date,
               'partner_id': query_line_res['partner_id'] or False,
               'partner_name': query_line_res['partner_name'] or '',
               'currency_id': query_line_res['currency_id'],
               'amount': abs(amount),
               'name': query_line_res['name'],
               'move_line_id': move_line_id,
               'journal_id': query_line_res['journal_id'],
               'type': line.credit and 'cr' or 'dr'}
               )
        #need trigger below query to fix old data
        '''
        with recline_data as (
        select recline.id,
            coalesce(partner.name,'') as partner_name,
            coalesce(advoucher.payee_name,'') as payee_name
        from bank_acc_rec_statement_line recline
        join bank_acc_rec_statement bankrec on bankrec.id=recline.statement_id
        join account_move_line moveline on moveline.id=recline.move_line_id
        left join res_partner partner on partner.id=moveline.partner_id
        left join advance_account_voucher advoucher on advoucher.id=moveline.advance_voucher_id
        where coalesce(bankrec.daily_cashier,False) is not True
        )
        update bank_acc_rec_statement_line recline
        set partner_name = (select partner_name from recline_data where recline_data.id=recline.id)
        where recline.id in (select id from recline_data);
        
        with recline_data as (
        select recline.id,
            coalesce(advoucher.payee_name,'') as payee_name
        from bank_acc_rec_statement_line recline 
        join bank_acc_rec_statement bankrec on bankrec.id=recline.statement_id
        join account_move_line moveline on moveline.id=recline.move_line_id
        left join advance_account_voucher advoucher on advoucher.id=moveline.advance_voucher_id
        where coalesce(bankrec.daily_cashier,False) is not True
        )
        update bank_acc_rec_statement_line recline
        set payee_name = (select payee_name from recline_data where recline_data.id=recline.id)
        where recline.id in (select id from recline_data);
        
        
        with recline_data as (
        select recline.id,
                coalesce(
                    moveline.payment_method_id,
                    coalesce(advoucher.payment_method_id,(
                        select voucher.payment_method_id from account_voucher voucher where voucher.move_id=moveline.move_id limit 1)
                    )
                ) as payment_method_id
        from bank_acc_rec_statement_line recline
        join bank_acc_rec_statement bankrec on bankrec.id=recline.statement_id
        join account_move_line moveline on moveline.id=recline.move_line_id
        left join advance_account_voucher advoucher on advoucher.id=moveline.advance_voucher_id
        where coalesce(bankrec.daily_cashier,False) is not True
            and recline.payment_method_id is null
        )
        update bank_acc_rec_statement_line recline
        set payment_method_id = (select payment_method_id from recline_data where recline_data.id=recline.id)
        where recline.id in (select id from recline_data);
        
        '''
        return res

    @api.multi
    def action_process(self):
        bank_recon_approver_ids = [finance.id for finance in self.env['res.company'].browse(self.company_id.id).bank_recon_approver_ids]
        if self._uid not in bank_recon_approver_ids:
            raise Warning(_('Only certain user can process bank reconciliation, please contact your administrator!'))

        res = super(BankAccRecStatement, self).action_process()
        for statement in self:
            statement_lines = statement.credit_move_line_ids + \
            statement.debit_move_line_ids
            for statement_line in statement_lines:
                if statement_line.move_line_id:
                    vals = {}
                    if not statement.daily_cashier:
                        vals = {'cleared_bank_account':
                                statement_line.cleared_bank_account,
                                'bank_acc_rec_statement_id': statement.id or False}
                    else:
                        vals = {'cleared_dcc_account':
                                statement_line.cleared_bank_account,
                                'dcc_id': statement.id or False}
                    statement_line.move_line_id.write(vals)

    @api.multi
    def action_cancel_draft(self):
        """Reset the statement to draft and perform resetting operations."""
        for statement in self:
            statement_lines = statement.credit_move_line_ids + \
            statement.debit_move_line_ids
            for statement_line in statement_lines:
                if statement_line and (not statement_line.statement_id.daily_cashier):
                    statement_line.write({'cleared_bank_account': False,
                                          'research_required': False
                                          })
                if statement_line.move_line_id and (not statement_line.statement_id.daily_cashier):
                    line_vals = {'cleared_bank_account': False,
                                 'bank_acc_rec_statement_id': False}
                    statement_line.move_line_id.write(line_vals)
            statement.write({'state': 'draft',
                             'verified_by_user_id': False,
                             'verified_date': False
                             })

    @api.multi
    def _get_exits_move_line(self, mv_line_rec):
        domain = [('move_line_id', '=', mv_line_rec.id),
                  ('statement_id', 'in', self.ids)]
        res = {}
        statemen_line_obj = self.env['bank.acc.rec.statement.line']
        statmnt_mv_line_ids = statemen_line_obj.search(domain)
        for statement_line in statmnt_mv_line_ids:
            if not statement_line.statement_id.daily_cashier:
                res.update({'cleared_bank_account':
                            statement_line.cleared_bank_account,
                            'ref': statement_line.ref or '',
                            'date': statement_line.date or False,
                            'partner_id': statement_line.partner_id.id or False,
                            'currency_id': statement_line.currency_id.id or False,
                            'amount': abs(statement_line.amount) or 0.0,
                            'name': statement_line.name or '',
                            'move_line_id':
                            statement_line.move_line_id.id or False,
                            'payment_method_id': statement_line.move_line_id.payment_method_id and statement_line.move_line_id.payment_method_id.id or False,
                            'type': statement_line.type})
            else:
                res.update({'cleared_dcc_account':
                            statement_line.cleared_bank_account,
                            'ref': statement_line.ref or '',
                            'date': statement_line.date or False,
                            'partner_id': statement_line.partner_id.id or False,
                            'currency_id': statement_line.currency_id.id or False,
                            'amount': abs(statement_line.amount) or 0.0,
                            'name': statement_line.name or '',
                            'move_line_id':
                            statement_line.move_line_id.id or False,
                            'payment_method_id': statement_line.move_line_id.payment_method_id and statement_line.move_line_id.payment_method_id.id or False,
                            'type': statement_line.type})
        return res

class BankAccRecStatementLine(models.Model):
    _inherit = 'bank.acc.rec.statement.line'

    partner_name = fields.Char('Partner Name')
    payee_name = fields.Char('Payee Name')
    payment_method_id = fields.Many2one('account.journal', string="Payment Method")
    cleared_dcc_account = fields.Boolean('Cleared? ',
                                          help = 'Check if the transaction \
                                          has cleared from the daily cashier closing')

    #do not show below field at one2many tree view, this in only for search purpose
    #if put at tree view, system will took a long loading time
    move_id = fields.Many2one('account.move',related='move_line_id.move_id',readonly=True,store=False,export_invisible=True,string='Number (Move)')

    @api.multi
    def write(self, vals):
        if vals and vals.get('move_line_id', False):
            move_line_obj = self.env['account.move.line']
            account_move_line_brw = move_line_obj.browse(vals['move_line_id'])
            if not self.statement_id.daily_cashier:
                account_move_line_brw.write({'draft_assigned_to_statement': True})
            else:
                account_move_line_brw.write({'draft_assigned_to_dcc': True})
        return super(BankAccRecStatementLine, self).write(vals)

    @api.multi
    def unlink(self):
        dcc_move_line_ids = []
        statement_move_line_ids = []
        for stamnt_line in self:
            if stamnt_line.move_line_id:
                if stamnt_line.statement_id.daily_cashier:
                    dcc_move_line_ids.append(stamnt_line.move_line_id.id)
                else:
                    statement_move_line_ids.append(stamnt_line.move_line_id.id)
        if dcc_move_line_ids:
            move_line_obj = self.env['account.move.line']
            account_move_line_brw = move_line_obj.browse(dcc_move_line_ids)
            account_move_line_brw.write({'draft_assigned_to_dcc': False,
                                         'cleared_dcc_account': False,
                                         'dcc_id': False,
                                            })
        if statement_move_line_ids:
            move_line_obj = self.env['account.move.line']
            account_move_line_brw = move_line_obj.browse(statement_move_line_ids)
            account_move_line_brw.write({'draft_assigned_to_statement': False,
                                         'cleared_bank_account': False,
                                         'bank_acc_rec_statement_id': False,
                                            })
        return models.Model.unlink(self)

    @api.model
    def create(self, vals):
        if not vals.get('move_line_id', False):
            raise Warning(_('You cannot add any new bank statement line \
            manually as of this revision!'))
        res = models.Model.create(self,vals)
        if res.move_line_id:
            if not res.statement_id.daily_cashier:
                res.move_line_id.write({'draft_assigned_to_statement': True})
            else:
                res.move_line_id.write({'draft_assigned_to_dcc': True})
        return res

class BankInStatement(models.Model):
    _name = "bank.in.statement"
    _description = 'Bank-In'
    _inherit = ['mail.thread', 'utm.mixin']

    name = fields.Char('Name', required = True, size = 64,
                   states = {'done':[('readonly', True)]}, copy=False, default='/')
    date = fields.Date('Generated Date', required=True, states = {'done':[('readonly', True)]}, default=lambda *a: time.strftime('%Y-%m-%d'))
    bank_in_date = fields.Date('Bank-In Date', states = {'done':[('readonly', True)]})
    company_id = fields.Many2one('res.company', 'Company', required = True,
                                 readonly = True, help = "The Company for \
                                 which the deposit ticket is made to",
                                 default =
                                 lambda self: self.env.user.company_id)
    journal_id = fields.Many2one('account.journal', 'Journal', required = True, states = {'done':[('readonly', True)]}, domain=[('type','=','bank')])
    account_id = fields.Many2one('account.account', 'Account', required = True,
        states = {'done':[('readonly', True)]}, domain = "[('company_id', '=', company_id)]")
    move_line_ids = fields.One2many('account.move.line', 'bank_in_rec_statement_id', 'Lines', readonly=True)
    daily_closing_ids = fields.Many2many('bank.acc.rec.statement', 'bank_in_closing_rel','bank_in_id','statement_id',string='Closings', states = {'done':[('readonly', True)]})
    state = fields.Selection([('draft', 'Draft'),
        ('done', 'Done'), ('cancel', 'Cancel')], 'State',
        index = True, readonly = True, default = 'draft', track_visibility='onchange')
    total_amount = fields.Float(string='Amount', readonly=True)
    remarks = fields.Text('Remark / 备考')
    payment_method_ids = fields.Many2many('custom.payment.method','Payment Method', compute="_get_payment_method", readonly=True)

    @api.multi
    @api.depends('daily_closing_ids')
    def _get_payment_method(self):
        payment_methods = []
        for record in self :
            if record.daily_closing_ids :
                for dcc in record.daily_closing_ids :
                    if dcc.payment_method_id and dcc.payment_method_id not in payment_methods :
                        payment_methods.append(dcc.payment_method_id.id)
            record.payment_method_ids = payment_methods

    @api.multi
    def unlink(self):
        for statement in self:
            if statement.state != 'cancel':
                raise Warning(_('Please cancel before remove this bank-in statement.'))
            if statement.name and statement.name != '/':
                raise Warning(_('Cannot remove the Bank-In.\nThe number already generated!'))
        return super(BankInStatement, self).unlink()

    @api.onchange('daily_closing_ids')
    def onchange_closings(self):
        total = 0
        val = {'value': {'total_amount': total}}
        if self.daily_closing_ids:
            for closing in self.daily_closing_ids:
                total += closing.ending_balance
        val['value']['total_amount'] = total
        return val

    @api.onchange('journal_id')
    def onchange_journal_id(self):
        val = {'value': {'account_id': False}}
        if self.journal_id:
            val['value']['account_id'] = self.journal_id.default_debit_account_id and self.journal_id.default_debit_account_id.id or False
        return val

    @api.multi
    def action_draft_cancel(self):
        """Cancel the the statement."""
        for statement in self:
            statement.write({'state': 'draft'})

    @api.multi
    def action_cancel(self):
        """Cancel the the statement."""
        for statement in self:
            if statement.state == 'done':
                for line in self.daily_closing_ids:
                    dcc_lines = line.credit_move_line_ids + \
                        line.debit_move_line_ids
                    for dcc_line in dcc_lines:
                        if dcc_line.move_line_id:
                            vals = {'cleared_dcc_account': dcc_line.cleared_bank_account,
                                    'dcc_id': False}
                            dcc_line.move_line_id.write(vals)
            statement.write({'state': 'cancel'})

    @api.multi
    def action_done(self):
        if not self.journal_id:
            raise Warning(_('No destination journal detected, please review and make make necessary changes.!'))

        if not self.daily_closing_ids:
            raise Warning(_('No closing statement detected, please review and make make necessary changes.!'))

        for statement in self.daily_closing_ids:
            statement_lines = statement.credit_move_line_ids + \
            statement.debit_move_line_ids
            for statement_line in statement_lines:
                if statement_line.move_line_id:
                    vals = {'cleared_dcc_account':
                            statement_line.cleared_bank_account,
                            'dcc_id': statement.id or False}
                    statement_line.move_line_id.write(vals)
            statement.write({'bank_in': True})

        sequence = self.name
        if not self.name or self.name == '/':
            bankin_sequence = False
            sequence = self.env['ir.sequence'].next_by_code('bank.acc.rec.statement.seq') or _('New Daily Cashier Closing')
        write_vals = {
            'name' : sequence,
            'state' : 'done'
        }
        if not self.bank_in_date:
            write_vals.update({'bank_in_date' : time.strftime('%Y-%m-%d')})
        self.write(write_vals)
