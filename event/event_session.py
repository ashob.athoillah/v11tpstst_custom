from odoo import api, fields, models, _
from datetime import date, datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from itertools import chain
import pytz
from pytz import timezone


def minutesrange(datetime1, datetime2):
    """
    Returns a list of time in minutes between two datetime objects
    :param datetime1: datetime
    :param datetime2: datetime
    :return list: list
    """
    total_minutes = (datetime2 - datetime1).total_seconds() / 60
    # print(total_minutes)
    for n in range(int(total_minutes)):
        yield datetime1 + timedelta(minutes=n)

def obtain_duplicates(listuple, include_key=False):
    """
    Find duplicates from a list of twin-valued tuples, grouped from
    Variables flow example:
    listuple: [(1, 'room a'), (2, 'room a'), (3, 'room b')]
    rev_multidict: {'room a': {1, 2}, 'room b': {3}} // Two ids are in the same room (room a)
    valgrouped_dict: {'room a': {1, 2}} // Get only the kvp where the set has more than 1 items
    duplicate_ids: [1, 2]
    :param listuple: List of two-valued tuples. First tuple value is the object's id, second value is the value where
                     duplicates are checked on
    :type listuple: list
    :param include_key: If need to return a collection of grouped same values.
    :type include_key: bool
    :returns: A list of object ids if not including key, else dict
    :rtype: set or dict
    """
    rev_multidict = reverse_group_values(listuple)
    if include_key:
        valgrouped_dict = {k: v for (k, v) in rev_multidict.items() if len(v) > 1}
        return valgrouped_dict
    else:
        # overlap_sets = [v for k, v in rev_multidict.items() if len(v) > 1]
        # print('overlap sets', overlap_sets)
        duplicate_ids = set(chain.from_iterable(v for k, v in rev_multidict.items() if len(v) > 1))
        # overlap_obj_ids = list(max(overlap_sets, key=len)) if overlap_sets else []
        # print('duplicate_ids', duplicate_ids)
        return duplicate_ids

def reverse_group_values(listuple):
    """ Grouping same values of the second value of a list of tuples
    listuple: [(1, 'room a'), (2, 'room a'), (3, 'room b')]
    rev_multidict: {'room a': {1, 2}, 'room b': {3}} // Two ids are in the same room (room a)
    :returns: rev_multidict
    :rtype: dict
    """
    rev_multidict = {}
    for k, v in listuple:
        rev_multidict.setdefault(v, set()).add(k)
    return rev_multidict

def hour_float_to_time(time_float, is_24hr_format=True):
    """ Converts hour floats like 9.5 into 9:30 or 09:30 if is_24_hr_format is True """
    if type(time_float) is str:
        time_float = float(time_float)
    hour, minute = divmod(time_float * 60, 60)
    # print('hour n minute', hour, minute)
    hm_string = '{0:02.0f}:{1:02.0f}'.format(hour, minute)
    if is_24hr_format:
        return hm_string
    else:
        return datetime.strptime(hm_string, '%H:%M').strftime('%-I.%M %p')

class EventSession(models.Model):
    _name = "event.session"
    _inherit = ['mail.thread']
    _order = "session_no ASC"


    name = fields.Char('Name', track_visibility='onchange')
    session_no = fields.Integer('Session No', track_visibility='onchange')
    session_activity = fields.Char("Session Activity", track_visibility="onchange")
    date = fields.Date('Session Date', track_visibility='onchange')
    start_time = fields.Float('Start Time', track_visibility='onchange')
    end_time = fields.Float('End Time', track_visibility='onchange')
    event_id = fields.Many2one('event.event', 'Event', track_visibility='onchange')
    registration_ids = fields.One2many('event.registration', 'session_id', 'Registration')
    date_begin = fields.Datetime(string='Start Date', store=True, compute="get_date_begin")
    date_end = fields.Datetime(string='End Date', store=True, compute="get_date_end")
    state = fields.Selection([('confirmed','Confirmed'),('cancel','Cancelled'),('start','Started'),('done','Done')], default='confirmed' ,string="State", track_visibility='onchange')
    replace = fields.Boolean("Replace")
    remarks = fields.Text('Remark / 备考', track_visibility='onchange')

    seat_available = fields.Integer('Seat Available', compute="_seat_count")
    minimum_attendees = fields.Integer("Minimum Attendees", related="event_id.seats_min")

    # @api.depends('registration_ids')
    @api.multi
    def _seat_count(self) :
        seat_available = 0
        seat_taken = 0
        seat_limit = 0
        for rec in self:
            event = rec.event_id
            if event.seats_availability == 'limited' :
                seat_limit = event.seats_max
                seat_taken = len(rec.registration_ids)
                seat_available = seat_limit - seat_taken
                print('seat_available = ', seat_available)

            rec.seat_available = seat_available

    @api.multi
    def start_session(self):
        if len(self.registration_ids) < self.minimum_attendees:
            raise UserError("Cannot start session when Attendee(s) less than Minimum Attendees")
        self.state = 'start'
    
    @api.multi
    def finish_session(self):
        self.state == 'done'

    # @api.multi
    # @api.depends('attendance_ids', 'attendance_ids.registration_id')
    # def get_confirm_student(self):
    #     # print(haha)
    #     for session in self:
    #         attn_obj = self.env['event.session.attendance']
    #         evregs = self.env['event.registration'].search([('event_id', '=', session.event_id.id)])
    #         if not evregs:
    #             raise ValidationError(_('You cannot generate the attendance list if no one is registered to this event!'))
    #         curr_reg_ids = []
    #         for atn in session.attendance_ids:
    #             if atn.registration_id:
    #                 curr_reg_ids.append(atn.registration_id.id)
    #         for evreg in evregs:
    #             if not evreg.id in curr_reg_ids and evreg.state == 'open':
    #                 vals = {
    #                     'session_id': session.id,
    #                     'event_id': session.event_id.id,
    #                     'registration_id': evreg.id,
    #                     'member_id': evreg.partner_id.id,
    #                     'identity_no' : evreg.identity_no,
    #                     'present': True,
    #                 }
    #                 attn_obj.create(vals)
    #                 # print(vals)
    #     # print(error)
    #     return True

    
    @api.depends('date', 'start_time')
    @api.one
    def get_date_begin(self, return_value=False):
        if not self.date or not self.start_time:
            return False
        d = datetime.strptime(self.date, '%Y-%m-%d')
        begin_string = '{0:02.0f}:{1:02.0f}'.format(*divmod(self.start_time * 60, 60))
        begin = datetime.strptime(begin_string, '%H:%M').time()
        # print(date, begin)
        date_begin = datetime.combine(d, begin)
        tz_info = self._context.get('tz', self.env.user.tz)
        if not tz_info:
            tz_info = "Asia/Singapore"
        local_tz = timezone(tz_info)
        date_begin_local = local_tz.localize(date_begin).astimezone(pytz.utc)
        # print(date_begin)
        if return_value:
            return date_begin_local

    @api.depends('date', 'end_time')
    @api.one
    def get_date_end(self, return_value=False):
        if not self.date or not self.start_time:
            return
        d = datetime.strptime(self.date, '%Y-%m-%d')
        end_string = '{0:02.0f}:{1:02.0f}'.format(*divmod(self.end_time * 60, 60))
        end = datetime.strptime(end_string, '%H:%M').time()
        # print(date, end)
        date_end = datetime.combine(d, end)
        tz_info = self._context.get('tz', self.env.user.tz)
        if not tz_info:
            tz_info = "Asia/Singapore"
            # raise ValidationError('Please set the timezone for the user!')
        local_tz = timezone(tz_info)
        date_end_local = local_tz.localize(date_end).astimezone(pytz.utc)
        # print(date_end)
        if return_value:
            return date_end_local

    def apply_all_session(self):
        """ This method is used to trigger get_date_begin() and get_date_end() on already existing event.sessions with
        no date_begin and date_end values. Useful in cases where you need the values to be written before calling
        @api.constrains (usually triggers first before @api.depends can even write the values to the event.session)
        """
        # print('apply all session start')
        sessions = self.search([])
        # print('sessions', sessions)
        for s in sessions:
            if s.date and s.start_time and s.end_time:
                # if not s.date_begin:
                begin_values = s.get_date_begin(return_value=True)
                # print('begin values', begin_values)
                s.date_begin = begin_values[0]
                # if not s.date_end:
                end_values = s.get_date_end(return_value=True)
                s.date_end = end_values[0]
        # print('apply all session finished')

    @api.constrains('date', 'start_time', 'end_time')
    def prevent_overlapping_sessions(self):
        # ensure all sessions have date_begin and date_end by re-triggering @api.depends methods
        # print('constrains begin')
        self.apply_all_session()
        if not self.replace:
            self.check_overlap_sessions(self)

    def get_overlap_same_events(self, target_session, session_pool=None):
        """ Get sessions with the same datehour within the same event """
        session_obj = self.env['event.session']
        ids = self.find_session_overlaps(target_session, session_pool, event_check='same')
        overlap_same_events = session_obj.browse(ids)
        return overlap_same_events
    
    def get_overlap_diff_events(self, target_session, session_pool=None):
        """ Get Sessions with the same hour on other events (target_session included in the list)"""
        session_obj = self.env['event.session']
        ids = self.find_session_overlaps(target_session, session_pool, event_check='different')
        overlap_diff_events = session_obj.browse(ids)
        return overlap_diff_events


    def check_overlap_sessions(self, target_session):
        ovse, ovde = self.get_all_overlap_session_modes(target_session)
        error_string = ""
        dt_fmt = '%d/%m/%Y'
        if ovse:
            error_string += 'These sessions are overlapping for the currently edited event:\n{}'.format(
                '\n'.join(['{} ({} @ {} - {})'.format(s.name, s.date_begin, hour_float_to_time(s.start_time),
                hour_float_to_time(s.end_time)) for s in ovse]))
        if error_string:
            raise ValidationError(_(error_string))

    def get_all_overlap_session_modes(self, target_session, session_pool=None):
        """ This is where most unallowed overlapping rules are defined for event.session
            1.  Two sessions in the SAME event SHOULD NOT have the SAME datehour range.
            2.  Two sessions CAN have the SAME datehour range if they are on DIFFERENT events
            :returns tuple: Four values that are stated below
        """

        overlap_same_event = self.get_overlap_same_events(target_session, session_pool)
        overlap_diff_event = self.get_overlap_diff_events(target_session, session_pool)
        return overlap_same_event, overlap_same_event
        # return overlap_same_event, overlap_diff_event
        # print('target = ', target_session)
        # print('event = ', self.event_id)
        # event = self.event_id
        # for rec in event.registration_ids:
        #     pass

            
    def find_session_overlaps(self, target_session, session_pool=None, event_check=None):
        """ Get all sessions with the same date as target_session (including itself), then filter out which of them
            has overlapping hour_from and hour_to datetimes.
            :param target_session: The session (one or many) as comparison
            :param session_pool: Limits the sessions search domain to this sessions
            :param event_check: 'same', 'different', or None, leads to seeing overlapping sessions either on the same
            event, different events, or regardless of events.
        """
        # print('target session', target_session)
        # print('session pool', session_pool)
        # Range = namedtuple('Range', ['start', 'end'])
        datetime_fmt = '%Y-%m-%d %H:%M:%S'
        date_fmt  ='%Y-%m-%d'
        # print('Target ssession', target_session)
        all_dates = []
        date_match = [ts.date for ts in target_session]
        event_ids = [ts.event_id.id for ts in target_session]
        # print('date match', date_match, event_ids)
        search_domain = [('date', 'in', date_match)]
        if event_check == 'same':
            search_domain.append(('event_id', 'in', event_ids))
        elif event_check == 'different':
            search_domain.append(('event_id', 'not in', event_ids))
        # print('final domain', search_domain)
        # print('sessions from search', sessions.mapped('name'))
        if session_pool:
            # print('session_pool', session_pool.mapped('name'))
            search_domain.append(('id', 'in', session_pool.ids))
            # sessions = sessions.filtered(lambda s: s.id in session_pool.ids)
        sessions = self.search(search_domain)
        # print('sessions from filter', sessions.mapped('name'))
        # print('obtained sessions', sessions)
        # If target session is not in sessions (different events), add it back
        # if any([ts not in sessions for ts in target_session]):
        # print('second end session result', sessions)
        sessions |= target_session
        # print('obtained sessions', sessions)
        # print('obtained session from domain', sessions)
        # print('end session result', sessions)
        for session in sessions:
            begin = session.get_date_begin(return_value=True)[0]
            end = session.get_date_end(return_value=True)[0]
            # print('begin and end', self.name, begin, end)
            # print('begin and end', begin, end)
            if not (begin and end):
                raise Exception(_('Date begin or Date end is null! Cannot perform splitting in order to get overlap '
                                  'dates'))
            dates = minutesrange(begin, end)
            for date in dates:
                all_dates.append((session.id, date.timestamp()))
        overlap_obj_ids = obtain_duplicates(all_dates)

        return overlap_obj_ids
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            if rec.session_activity:
                name = name + ' ' + rec.session_activity + ' [' + rec.date + ', ' + hour_float_to_time(rec.start_time) + ' - ' + hour_float_to_time(rec.end_time) + ']' 
            else:
                name = name + ' [' + rec.date + ', ' + hour_float_to_time(rec.start_time) + ' - ' + hour_float_to_time(rec.end_time) + ']' 
            result.append((rec.id, name))
        return result
                

class EventSessionAttendance(models.Model):
    _name = "event.session.attendance"
    _descripttion = "Session Attendance"
    _inherit = ['mail.thread']

    member_id = fields.Many2one('res.partner', 'Participant')
    session_id = fields.Many2one('event.session', 'Session')
    registration_id = fields.Many2one('event.registration', string='Registration No')
    email = fields.Char('E-mail / 电邮', copy=False)
    mobile = fields.Char('Mobile / 手机', copy=False)
    present = fields.Boolean('Present')
    state = fields.Selection([
        ('coming','Coming'),
        ('not_coming','Not Coming')
    ], string="Status", default='not_coming')

    @api.onchange('registration_id')
    def onchange_registration_id(self):
        self.ensure_one()
        reg_id = self.registration_id.id or False
        if not reg_id:
            self.member_id = False
            self.identity_no = ''
            return {'value': {'member_id': False, 'identity_no': ''}}

        reg_obj = self.env['event.registration']
        for reg in reg_obj.browse([reg_id]):
            member_id = reg.partner_id and reg.partner_id.id or False
            event_id = reg.event_id and reg.event_id.id or False
            self.member_id = member_id
            self.event_id = event_id
            self.identity_no = reg.identity_no
            return {
                'value': {'member_id': member_id, 'event_id': event_id, 'identity_no': reg.identity_no}}

    @api.onchange('present')
    def att_rule(self):
        if self.present:
            self.state = 'coming'
        else:
            self.state = 'not_coming'

    
    @api.model
    def create(self, vals):
        if vals.get('present'):
            vals.update({'state': 'coming'})
        return super(EventSessionAttendance, self).create(vals)



class SessionTimeConfiguration(models.Model):
    _name = "session.time.configuration"
    _description = "Session Time Configuration"
    _inherit = ['mail.thread']

    name = fields.Char("Name", track_visibility="onchange")
    responsible_id = fields.Many2one('res.users',"Attended by / 经手人", default=lambda self: self.env.user, track_visibility="onchange")
    session_conf_ids = fields.One2many('event.session.conf.line', 'session_conf_id', "Event Session Configuration")
    default_conf = fields.Boolean("Set as Default")

    @api.multi
    def set_default(self):
        ev_sess_line_conf_obj = self.env['session.time.configuration']
        all_ev_sess_conf = ev_sess_line_conf_obj.search([])
        for rec in all_ev_sess_conf:
            if rec.default_conf == True:
                rec.default_conf = False
        self.default_conf = True

class EventSessionConfLine(models.Model):
    _name = "event.session.conf.line"

    name = fields.Char("Session")
    session_conf_id = fields.Many2one('session.time.configuration', "Session Configuration")
    start_time = fields.Float("Start")
    end_time = fields.Float("End")

    @api.constrains('start_time', 'end_time')
    def time_constraints(self):
        if self.end_time <= self.start_time:
            raise ValidationError(_("You cannot set End time less than or equal to start time"))
