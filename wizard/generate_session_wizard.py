from datetime import datetime, timedelta
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from datetime import datetime, timedelta

class GenerateSessionWizard(models.TransientModel):
    _name = "generate.session.wizard"
    _description = 'Generate Session'

    name = fields.Char('Name')
    event_id = fields.Many2one('event.event', 'Event')
    default_conf_id = fields.Many2one('session.time.configuration', 'Default Configuration')
    exist_session = fields.Boolean("Session")
    generate_option = fields.Selection([
        ('default_conf', 'Default Configuration'),
        ('custom', 'Custom Configuration'),
    ], string="Generate Option", default="default_conf")
    generate_method = fields.Selection([
        ('add', 'Add'),
        ('replace', 'Replace')
    ], string="Generate Method")

    @api.multi
    def generate_session(self):
        FMT = '%Y-%m-%d %H:%M:%S'
        fmt = '%Y-%m-%d'
        session_obj = self.env['event.session']
        default_conf = self.default_conf_id
        event = self.event_id
        start_date = datetime.strptime(event.date_begin, FMT)
        end_date = datetime.strptime(event.date_end, FMT)
        diff_day = end_date - start_date
        delta = timedelta(days=1)
        session_name = 1 #will be used for session name
        if self.exist_session == True:
            last_session_date = event.session_ids[-1].date
            all_session = event.session_ids.ids
            if self.generate_method == 'add':

                session_len = len(event.session_ids)
                if (int(diff_day.days) + 1) * len(default_conf.session_conf_ids)  == session_len:
                    raise ValidationError (_("you cannot add more session, you should remove some session or extend your Event End Date"))
                elif (int(diff_day.days) + 1) * len(default_conf.session_conf_ids) < session_len:
                    raise ValidationError(_("Seem's like your record is missing session, please contact your System Administrator to adjust it"))
                session_name += session_len
                start_date = datetime.strptime(event.session_ids[-1].date, fmt) + delta
        if self.generate_method == 'replace':
            while start_date <= end_date:
                # if replace all the Started or Done will not be replaced, all the confirmed session will be updated according to session date and session number
                session = event.session_ids.filtered(lambda x: x.state not in ('start', 'done') and x.date == datetime.strftime(start_date, fmt)).sorted(key=lambda r: int(r.name))
                for se in session:
                    registration_to_move = []
                    match_session = ''
                    if len(session) == len(default_conf.session_conf_ids):
                        match_session = default_conf.session_conf_ids.filtered(lambda x: x.name == se.name if int(se.name) <= len(session) else x.name == str(int(se.name) % len(session)))
                    # elif len(session) < len(default_conf.session_conf_ids):
                    #     pass  
                    if match_session:
                        event.session_ids = [(3, se.id, 0)]
                        vals_default = {}
                        vals_default['name'] = str(se.name)
                        vals_default['session_no'] = int(se.name)
                        vals_default['date'] = datetime.strftime(start_date, FMT)
                        vals_default['state'] = se.state
                        vals_default['event_id'] = self.event_id.id
                        vals_default['start_time'] = match_session.start_time
                        vals_default['replace'] = True
                        vals_default['end_time'] = match_session.end_time
                        vals_default['seat_available'] = self.event_id.seats_max
                        new_session = session_obj.create(vals_default)
                        if se.registration_ids:
                            for evreg in se.registration_ids:
                                registration_to_move.append(evreg.id)
                            se.registration_ids = [(5, 0, registration_to_move)]
                            new_session.registration_ids = [(6, 0, registration_to_move)]
                start_date += delta
        else:
            while start_date <= end_date:
                for rec in self.env['event.session.conf.line'].search([('session_conf_id', '=' ,default_conf.id)], order="name asc"):
                    vals_default = {}
                    vals_default['name'] = session_name
                    vals_default['session_no'] = int(session_name)
                    vals_default['date'] = datetime.strftime(start_date, FMT)
                    vals_default['state'] = 'confirmed'
                    vals_default['event_id'] = self.event_id.id
                    vals_default['start_time'] = rec.start_time
                    vals_default['end_time'] = rec.end_time
                    vals_default['seat_available'] = self.event_id.seats_max
                    session_obj.create(vals_default)
                    session_name += 1
                start_date += delta
        return True


