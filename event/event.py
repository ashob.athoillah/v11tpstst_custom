import pytz
from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from pytz import timezone

class Event(models.Model):
    _name = "event.event"
    _inherit = ["event.event", "mail.thread"]

    @api.depends('session_ids')
    @api.multi
    def get_session_count(self):
        for event in self:
            event.session_count = len(event.session_ids)

    @api.multi
    def session_tied(self):

        return {
            'name' : _("Event Session"),
            'res_model': 'event.session',
            'type': 'ir.actions.act_window',
            'domain': [('event_id','=',self.id)],
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    event_type = fields.Selection([
        ('free','Free'),
        ('paid','Paid'),
    ],default='paid', string="Event Type", track_visibility='onchange')
    event_category = fields.Selection([
        ('alive','Alive'),
        ('deceased','Deceased')
    ], string="Event Category", default='alive', track_visibility='onchange')
    # event_categ = fields.Selection([('')])
    product_id = fields.Many2one('product.product', 'Event Product', track_visibility='onchange')
    reg_date_begin = fields.Date(string='Registration Start Date', track_visibility='onchange')
    reg_date_end = fields.Date(string='Registration End Date', track_visibility='onchange')
    event_image = fields.Binary("Image", track_visibility='onchange')
    description = fields.Text(string="Description", track_visibility='onchange')
    # registration_ids = fields.One2many('event.registration', 'event_id', 'Registration')
    session_ids = fields.One2many('event.session', 'event_id', 'Sessions')
    session_count = fields.Integer(string='Session Count', compute=get_session_count)
    seats_max = fields.Integer(
        string='Maximum Attendees Number', oldname='register_max',
        readonly=True, states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]}, track_visibility='onchange',
        help="For each event you can define a maximum registration of seats(number of attendees), above this numbers the registrations are not accepted.")
    seats_availability = fields.Selection(
        [('limited', 'Limited'), ('unlimited', 'Unlimited')],
        'Maximum Attendees', required=True, track_visibility='onchange', default='unlimited')
    seats_min = fields.Integer(
        string='Minimum Attendees', oldname='register_min', track_visibility='onchange',
        help="For each event you can define a minimum reserved seats (number of attendees), if it does not reach the mentioned registrations the event can not be confirmed (keep 0 to ignore this rule)")

    max_companied_free = fields.Integer(string='Limit Accompanied General', track_visibility='onchange')
    max_companied_member = fields.Integer(string='Limit Accompanied Member', track_visibility='onchange')

    @api.one
    def button_done(self):
        if datetime.strptime(self.date_end, '%Y-%m-%d %H:%M:%S') > datetime.now():
            raise UserError("You cannot Finish Event when the End Date is bigger than now.")
        return super(Event, self).button_done(self)

    @api.multi
    def button_cancel(self):
        for event in self :
            events = event.mapped('session_ids.state') 
            for state in events :
                if state not in ['start', 'done'] :
                    pass
                else :
                    raise UserError(_("There are already session that been started. Please reset it to draft if you want to cancel this event."))

        self.session_ids.write({'state': 'cancel'})
        self.state = 'cancel'

    @api.one
    @api.constrains('date_begin', 'date_end')
    def _check_closing_date(self):
        if self.date_end < self.date_begin:
            raise ValidationError(_('Closing Date cannot be set before Beginning Date.'))
    
    @api.one
    @api.constrains('reg_date_begin', 'reg_date_end')
    def _check_last_reg_date(self):
        if self.reg_date_end < self.reg_date_begin:
            raise ValidationError(_('Registration Closing Date cannot be set before Registration Beginning Date.'))
    
    @api.multi
    def call_wizard_session_generator(self):
        session_conf_obj = self.env['session.time.configuration']
        session_conf = session_conf_obj.search([])
        default_conf = ''
        if not session_conf:
            raise ValidationError(_("You haven't set a default Configuration for Session, kindly to set it first in Configuration -> Session Time Configuration"))
        if session_conf:
            default_conf = session_conf.filtered(lambda x: x.default_conf == True)
            if not default_conf:
                raise ValidationError(_("Seem's like you have created a Session Time Configuration, but not set one configuration as default, kindly to set Configuration you've made by click ON button in the Form"))
        exist_session = False
        add_new_session = ''
        if self.session_ids:
            exist_session = True
        return {
            'name': _('Generate Session(s)'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.session.wizard',
            'target': 'new',
            'context': {'default_event_id': self.id, 'default_exist_session': exist_session, 'default_default_conf_id': default_conf.id, 'default_generate_method': 'add'}
        }
    
    

    @api.multi
    def _track_subtype(self, init_values):
        fields_to_check = ['seats_min', 'max_companied_free', 'seats_availability', 'max_companied_member']
        if any(element in init_values for element in fields_to_check) and self.state == 'confirm':
            return 'v11_tpstst_custom.mt_seats_min_change'
        return super(Event, self)._track_subtype(init_values)
    
    @api.multi
    def write(self, vals):
        fields_to_check = ['seats_min', 'seats_max', 'max_companied_free', 'seats_availability', 'max_companied_member']
        before_change = self.read(fields=fields_to_check)
        ctx = {}
        old = 'old_'
        new = 'new_'
        if any(element in vals for element in fields_to_check) and self.state == 'confirm':
            email_template = self.env.ref('v11_tpstst_custom.event_changed_details_notification')
            for rec, val in vals.items():
                if rec in fields_to_check:
                    old_rec = old + rec
                    new_rec = new + rec
                    ctx.update({
                        old_rec: before_change[0][rec],
                        new_rec: val
                    })
                for i in filter(lambda x: x not in vals, fields_to_check):
                    old_unchanged_field = old + i
                    new_unchanged_field = new + i
                    ctx.update({
                        old_unchanged_field: before_change[0][i],
                        new_unchanged_field: before_change[0][i]
                    })
            self.with_context(ctx).mail_change_event_details(template_id=email_template)
        res = super(Event, self).write(vals)
        return res

    @api.multi
    def mail_change_event_details(self, template_id, force_send=False):
        for session in self.session_ids.filtered(lambda x: x.state not in ['cancel', 'done', 'started'] and datetime.strptime(x.date, '%Y-%m-%d') >= datetime.now()):
            for attendee in session.registration_ids:
                template = self.env['mail.template'].search([('id', '=', template_id.id)])
                template.send_mail(attendee.id, force_send=True)

                # self.env['mail.template'].browse(template_id.id).with_context(ctx).send_mail(attendee.id, force_send=force_send)
    

    


class EventType(models.Model):
    _inherit = 'event.type'

    @api.model
    def _get_default_event_type_mail_ids(self):
        return [(0, 0, {
            'interval_unit': 'now',
            'interval_type': 'after_sub',
            'template_id': self.env.ref('event.event_subscription')
        }), (0, 0, {
            'interval_unit': 'now',
            'interval_type': 'after_sub',
            'template_id': self.env.ref('v11_tpstst_custom.event_registration_notification')
        }), (0, 0, {
            'interval_nbr': 1,
            'interval_unit': 'days',
            'interval_type': 'before_event',
            'template_id': self.env.ref('event.event_reminder')
        }), (0, 0, {
            'interval_nbr': 10,
            'interval_unit': 'days',
            'interval_type': 'before_event',
            'template_id': self.env.ref('event.event_reminder')
        })]

    event_type_mail_ids = fields.One2many(
        'event.type.mail', 'event_type_id', string='Mail Schedule',
        copy=False,
        default=_get_default_event_type_mail_ids)