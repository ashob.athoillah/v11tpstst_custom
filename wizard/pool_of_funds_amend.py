# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) TigernixERP (<https://www.tigernix.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
    
from datetime import datetime, date
from odoo import _, api, fields, models
import odoo.addons.decimal_precision as dp 
from odoo.exceptions import UserError


class PoolOfFundsAmend(models.TransientModel):
    _name = 'pool.of.funds.amend'

    type= fields.Selection([('adjust','Adjust'),('move','Transfer amount')],'Type', default='adjust')
    remarks = fields.Text('Remark',default='')
    amount = fields.Float(string='Adding / Deducting Funds', digits=dp.get_precision('Account'), help='Put negative sign (-) for Deducting Funds')
    moved_amount = fields.Float(string='Amount to be Transferred', digits=dp.get_precision('Account'))
    src_funds_id = fields.Many2one('pool.of.funds','From',default=lambda self:self._context.get('active_id',False))
    dest_funds_id = fields.Many2one('pool.of.funds','To')
    transaction_date = fields.Date('Transaction Date')
    
    #Data Fixing Need
    move_id = fields.Many2one('account.move',string='Journal Entries')
    voucher_id = fields.Many2one('account.voucher', string="Sales Receipt/Direct Payment")
    inv_id = fields.Many2one('account.invoice', string="Invoice")
        
        
    @api.onchange('src_funds_id','type','dest_funds_id')
    def onchange_dest_funds_id(self):
        if self.type == 'move' and self.src_funds_id and self.dest_funds_id:
            self.remarks = 'Transfer balance from %s to %s'%(self.src_funds_id.name,self.dest_funds_id.name)
    
    @api.multi
    def amend_funds(self):
        context = self._context
        fund = self.env['pool.of.funds'].browse(context['active_id'])
        wizard = self[0]
        
        log_for = ''
        if wizard.amount < 0:
            log_for = 'utilized'
        else:
            log_for = 'donation'
        
        log = self.env['pool.of.funds.log'].create({'log_for':log_for,
                                                    'funds_id':fund.id,
                                                    'amount':wizard.amount,
                                                    'remarks':wizard.remarks,
                                                    'transaction_date':wizard.transaction_date,
                                                    })
            
        return {'type': 'ir.actions.act_window_close'}
        
    @api.multi
    def move_funds(self):
        context = self._context
        source_fund = self.env['pool.of.funds'].browse(context['active_id'])
        wizard = self[0]
        if wizard.moved_amount <= 0:
            raise UserError(_('Amount to be transferred must not be zero or less.'))
        if source_fund.total_amount < wizard.moved_amount:
            raise UserError(_('Amount to be transferred is not enough.'))
        src_funds_id = self.env['pool.of.funds'].search([('id','=', self.src_funds_id.id)])
        dest_funds_id = self.env['pool.of.funds'].search([('id','=', self.dest_funds_id.id)])
        account = self.env['account.move']
        company = self.env.user.company_id
        config = self.env['res.company'].search([('id','=',company.id)])
        line_vals = []
        if self :
            line_vals.append((0,0, {'account_id' : config.donation_journal_id.default_credit_account_id.id, 'amount_currency': 0, 'currency_id': False, 'funds_id': src_funds_id.id, 'name' : _('Funds Move Out'), 'move_id' : src_funds_id.id,'debit' : 0, 'credit' :self.moved_amount}))
            line_vals.append((0,0, {'account_id' : config.donation_journal_id.default_debit_account_id.id, 'amount_currency': 0, 'currency_id': False, 'funds_id': dest_funds_id.id, 'name' : _('Funds Move In'), 'move_id' : dest_funds_id.id,'debit' : self.moved_amount, 'credit' :0}))
            for rec in self:
                rec.name = self.env['ir.sequence'].next_by_code('transfer.funds.journal.seq')
            vals_journal = {'name': rec.name, 'date': str(date.today().strftime("%Y-%m-%d")), 'journal_id': config.donation_journal_id.id, 'ref': 'Account Move', 'state' : 'posted', 'line_ids' : line_vals, 'narration': False}
            account.create(vals_journal)
            
        dest_fund = wizard.dest_funds_id
        wizard = self[0]
        log = self.env['pool.of.funds.log'].create({'log_for':'transfer','funds_id':source_fund.id,'transfer_funds_id':dest_fund.id,'amount':-wizard.moved_amount,'remarks':wizard.remarks})
        log = self.env['pool.of.funds.log'].create({'log_for':'transfer','funds_id':dest_fund.id,'transfer_funds_id':source_fund.id,'amount':wizard.moved_amount,'remarks':wizard.remarks})          
            
        return {'type': 'ir.actions.act_window_close'}
