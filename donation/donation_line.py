# -*- coding: utf-8 -*-
import requests
import pprint
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil import relativedelta
model_fields = fields
from odoo.tools import OrderedSet, pycompat
import logging
_logger = logging.getLogger(__name__)
from itertools import chain
from ast import literal_eval


class DonationLine(models.Model):
    _name = 'donation.line'
    _description = 'Donation Line'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    funds_id = fields.Many2one('pool.of.funds','Funds',required=True, domain=[('state','=', 'confirm')])
    donation_id = fields.Many2one('donation.donation', string="Donation")
    wizard_id = fields.Many2one('generate.donation.line', string="Donation")
    donation_type = fields.Selection([
        ('medical', 'Medical'),
        ('charity', 'Charity / Coffin'),
        ('festival', 'Festival'),
        ('trunk', 'Trunk'),
        ('other', 'Others'),
    ], string="Donation Purpose", track_visibility='onchange')
    description = fields.Char('Description', track_visibility='onchange')
    amount = fields.Float('Amount', track_visibility='onchange')


    # def avoid_duplicate_donation_type(self) :

    # def write(self, vals):
    #     if vals.get('donation_type') :
    #         get_default = self.env['donation.line'].search([('donation_type', '=', vals['donation_type']), ('donation_id', '=', vals['donation_id'])], limit=1)
    #         if get_default :
    #             raise ValidationError("")

    #     return super(DonationLine, self).write(vals)
    
    # @api.model
    # def create(self, vals):
    #     if vals.get('default', True) :
    #         get_default = self.env['donation.interval'].search([('default', '=', True)], limit=1)
    #         if get_default :
    #             raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
    #     return super(DonationLine, self).create(vals)

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.donation_id.partner_id.name) + " Donation for " + str(self.donation_type).capitalize() or "" + "(%s)" % self.donation_id.date
            result.append((rec.id, name))

        return result

    @api.onchange("donation_type")
    def on_change_state(self):
        company = self.env.user.company_id
        config = self.env['res.company'].search([('id','=',company.id)])
        for record in self:
            if record.donation_type == "medical":
                record.funds_id = config.default_medical_fund_id.id
            elif record.donation_type == "trunk":
                record.funds_id = config.default_trunk_fund_id.id
            elif record.donation_type == "charity":
                record.funds_id = config.default_charity_fund_id.id
            elif record.donation_type == "festival":
                record.funds_id = config.default_festival_fund_id.id
            elif record.donation_type == "other":
                record.funds_id = config.default_other_fund_id.id