##############################################################################
import time
from datetime import datetime, date
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta

class BeneficiaryList(models.Model):
    _name = "beneficiary.list"
    _description = 'Benificiary List'

    partner_id = fields.Many2one('res.partner', string="Beneficiary")
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string="Gender / 性别")
    birthdate = fields.Date('Date of Birth / 出生日期')
    age = fields.Integer('Age', compute="_compute_age")
    relation = fields.Char('Relation')
    identity_no = fields.Char('NRIC No. / 新居民证号码', index=True, size=12, copy=False)
    identity_no_mask = fields.Char('NRIC No. / 新居民证号码', copy=False, size=12, store=True, compute="_compute_identity_no_mask")
    show_identity_no = fields.Char('Show ?', copy=False)
    color_id = fields.Many2one('color.type.application', string='Color')
    membership_application_id = fields.Many2one('membership.application', string="Membership Application")

    @api.multi
    def show_identity(self):
        for rec in self:
            rec.show_identity_no = True

    @api.multi
    def hide_identity(self):
        for rec in self:
            rec.show_identity_no = False

    @api.depends('identity_no', 'show_identity_no')
    @api.multi
    def _compute_identity_no_mask(self):
        for rec in self:
            if rec.identity_no:
                if rec.show_identity_no:
                    rec.identity_no_mask = rec.identity_no
                else:
                    rec.identity_no_mask = rec.convert_string(rec.identity_no)    
            else :
                pass
    
    @api.multi
    def convert_string(self, val):
        if val:
            res = len(val[:-4]) * "*" + val[-4:]
            return res
        return False

    @api.depends('birthdate')
    @api.multi
    def _compute_age(self):
        for rec in self :
            if rec.birthdate:
                birth = datetime.strptime(rec.birthdate, "%Y-%m-%d").date()
                today = datetime.today()

                rec.age = relativedelta(today, birth).years

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.partner_id.name
            result.append((rec.id, name))

        return result

class ColorTypeApplication(models.Model):
    _name = "color.type.application"
    _description = 'Color Type Application'

    name = fields.Char('Name')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result

class DialectList(models.Model):
    _name = "dialect.list"
    _description = 'Dialect List'

    name = fields.Char('Name')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result

class MembershipPeriod(models.Model):
    _name = "membership.period"
    _description = 'Membership Period'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'name asc'

    name = fields.Char('Name', required=True, track_visibility='onchange')
    period = fields.Integer('Period', required=True, track_visibility='onchange')
    period_type = fields.Selection([
        ('days', 'Days'),
        ('weeks', 'Weeks'),
        ('months', 'Months'),
        ('years', 'Years')
    ], string="Period Type", default='years', required=True, track_visibility='onchange')
    default = fields.Boolean('Default', track_visibility='onchange', help="It will be chosen as default period on membership module")

    @api.multi
    def write(self, vals):
        if vals.get('default', True) :
            get_default = self.env['membership.period'].search([('default', '=', True)], limit=1)
            if get_default :
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(MembershipPeriod, self).write(vals)
    
    @api.model
    def create(self, vals):
        if vals.get('default', True) :
            get_default = self.env['membership.period'].search([('default', '=', True)], limit=1)
            if get_default :
                raise ValidationError("Default has been setup, you may uncheck it first before update the new one")
        return super(MembershipPeriod, self).create(vals)

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.name)
            result.append((rec.id, name))

        return result


