import datetime

from odoo import api, fields, models, _
from odoo.tools import float_is_zero,float_round,float_compare
from odoo.exceptions import AccessError,UserError
import odoo.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
import time
import pytz
from pytz import timezone
from itertools import chain

def to_local_datetime(dt, context, from_format="%Y-%m-%d %H:%M:%S", to_format="%Y-%m-%d %H:%M:%S"):
    tz_name = context.get('tz')
    local_tz = False
    if tz_name:
        local_tz = timezone(tz_name)
    local_date = datetime.datetime.strptime(dt, from_format)
    if local_tz:
        local_date = pytz.utc.localize(local_date).astimezone(local_tz) 
    local_date = datetime.datetime.strftime(local_date, to_format)
    return local_date

def to_utc_datetime(dt, context, from_format="%Y-%m-%d %H:%M:%S", to_format="%Y-%m-%d %H:%M:%S"):
    tz_name = context.get('tz')
    local_tz = False
    if tz_name:
        local_tz = timezone(tz_name)
    local_date = datetime.datetime.strptime(dt, from_format)
    utc_date = local_date
    if local_tz:
        utc_date = local_tz.localize(utc_date).astimezone(pytz.utc)
    utc_date = datetime.datetime.strftime(utc_date, to_format)
    return utc_date

class AccountMove(models.Model):
    _inherit = 'account.move'

    # @api.multi
    # def _get_default_journal(self):
    #     res = super(AccountMove, self)._get_default_journal()
    #     if self._context.get('default_deferred_journal'):
    #         res = self.env.user.company_id.def_revenue_journal_id.id or res
    #     return res

    payment_method_id = fields.Many2one('account.journal', string="Payment Method")
    
    funds_id          = fields.Many2one('pool.of.funds','Funds',readonly=True, copy=False, domain=[('state','=', 'confirm')])
    funds_log_id      = fields.Many2one('pool.of.funds.log','Funds Log',readonly=True, copy=False)
    # journal_id = fields.Many2one('account.journal', string='Journal', required=True, states={'posted': [('readonly', True)]}, default=_get_default_journal)
    month = fields.Char('Month')
    # advance_voucher_id = fields.Many2one('advance.account.voucher', 'Advance Payment Voucher')
    total_debit = fields.Monetary(string='Total Debit', store=True, readonly=True, compute='_compute_debit_credit')
    total_credit = fields.Monetary(string='Total Credit', store=True, readonly=True, compute='_compute_debit_credit')

    #Reverse
    reversed_from = fields.Many2one('account.move', string="Reversed From", copy=False)
    reverse_move_ids = fields.One2many('account.move', 'reversed_from', string="Reverse Move", copy=False)
    debit_note = fields.Boolean('Debit Note',readonly=True)
    funds_log_id = fields.Many2one('pool.of.funds.log','Funds Log',readonly=True, copy=False)


    
    @api.depends('line_ids', 'line_ids.debit', 'line_ids.credit')
    @api.multi
    def _compute_debit_credit(self):
        for move in self:
            debit = 0
            credit = 0
            for line in move.line_ids:
                debit += line.debit
                credit += line.credit
            move.total_debit = debit
            move.total_credit = credit

    @api.multi
    def _reverse_move(self, date=None, journal_id=None):
        self.ensure_one()
           
        for line in self.line_ids:
            for credit in line.matched_credit_ids:
                line.origin_aml_id = credit.credit_move_id and credit.credit_move_id.id
                
        reversed_move = self.copy(default={
            'date': date,
            'journal_id': journal_id.id if journal_id else self.journal_id.id,
            'ref': _('reversal of: ') + self.name,
            'reversed_from': self.id})
        
        for acm_line in reversed_move.line_ids.with_context(check_move_validity=False):
            acm_line.write({
                'debit': acm_line.credit,
                'credit': acm_line.debit,
                'amount_currency': -acm_line.amount_currency
            })
        return reversed_move
    
    @api.multi
    def _check_lock_date(self):
        for move in self:
            company = move.company_id
            lock_date = max(company.period_lock_date or '0000-00-00', company.fiscalyear_lock_date or '0000-00-00')
            if self.user_has_groups('account.group_account_manager'):
                lock_date = company.fiscalyear_lock_date
#            if self.env.user.id in company.lock_date_exc_user_ids._ids:
#                lock_date = False
            if move.date <= (lock_date or '0000-00-00'):
                to_format = '%d/%m/%Y'
                lang = self.env.user.lang
                lang_obj = self.env['res.lang']
                if lang:
                    langs = lang_obj.search([('code','=',lang)])
                    if langs:
                       to_format = langs[0].date_format
                date = to_local_datetime(lock_date,self._context,from_format='%Y-%m-%d',to_format=to_format)
                if self.user_has_groups('account.group_account_manager'):
                    message = _("You cannot add/modify entries prior to and inclusive of the lock date %s") % (date)
                else:
                    message = _("You cannot add/modify entries prior to and inclusive of the lock date %s. Check the company settings or ask someone with the 'Adviser' role") % (date)
                raise UserError(message)
        return True
    
    @api.model
    def create(self, vals):
        # vals["move_id"]=self.id
        return super(AccountMove, self).create(vals)

    def adjust_entries_taxes(self):
        '''
            to adjust AR/AP taxes for reporting purpose
        '''
        company_currency = self.company_id and self.company_id.currency_id or self.env.user.company_id.currency_id
        tax_total = 0.0
        move_lines = self.line_ids
        for line in self.line_ids:
            if line.tax_line_id: #if it is journal item for taxes
                tax_total += (line.debit - line.credit)
        tax_total = abs(tax_total)
        move_lines_amount = {}
        move_lines_total = 0.0
        for line in move_lines:
            if line.account_id.user_type_id and line.account_id.user_type_id.type in ('receivable','payable'):
                move_lines_amount[line] = line.debit - line.credit
                move_lines_total += move_lines_amount[line]
        remaining_tax = tax_total
        if move_lines_total:
            for index,(line, line_amount) in enumerate(move_lines_amount.items()):
                line_tax_amount = (float(line_amount) / move_lines_total) * tax_total
                if index + 1 == len(move_lines_amount):
                    line_tax_amount = remaining_tax
                line_tax_amount = company_currency.round(float(line_tax_amount or 0.0))
                remaining_tax -= line_tax_amount
                line.write({'line_tax_amount':line_tax_amount})
    

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    value_date = fields.Date(string="Value Date")
    payment_method_id = fields.Many2one('account.journal', string="Payment Method", related="move_id.payment_method_id", readonly=True)
    payment_method_type = fields.Selection([], string="Payment Method Type", related="payment_method_id.type", readonly=True)
    voucher_id = fields.Many2one('account.voucher', string="Voucher",track_visibility='onchange')
    funds_id = fields.Many2one('pool.of.funds','Funds',required=True,ondelete='cascade')
    niches_id = fields.Many2one('niche.application','Niches',required=True,ondelete='cascade')
    event_id = fields.Many2one('participant.registration','Event',required=True,ondelete='cascade')
    advance_id = fields.Many2one('purchase.in.advance','Advance Niches',required=True,ondelete='cascade')
    tablets_id = fields.Many2one('tablet.application','Tablets',required=True,ondelete='cascade')
    memberships_id = fields.Many2one('membership.application','Memberships',required=True,ondelete='cascade')
    membership_renewal_id = fields.Many2one('membership.renewal','Memberships Renewal',required=True,ondelete='cascade')

    payment_term_type = fields.Selection([
        ('giro','Giro'),
        ('cda','CDA')], string="Is Giro / CDA")
    giro_date = fields.Date(string='Giro / CDA Date')
    bank_id   = fields.Many2one('res.partner.bank', 'Bank')
    giro_cda_bank_id   = fields.Many2one('res.bank', 'Bank')
    
    bank_in_rec_statement_id = fields.Many2one('bank.in.statement', 'Bank In Statement')
    giro_payment = fields.Boolean("Giro / CDA Payment", default=False, copy=False, readonly=True)
    giro_payment_failed = fields.Boolean("Failed Giro / CDA Payment", default=False, copy=False, readonly=True)
    pos_payment_method_id = fields.Many2one('account.journal', string="POS Payment Method")
    line_tax_amount = fields.Monetary(default=0.0, currency_field='company_currency_id',string='Tax amount')
    
    giro_collection_id = fields.Many2one('giro.collection', string="Giro Collection")
    
    #Advance Vouvher
    origin_aml_id = fields.Many2one('account.move.line', string="Origin Journal Item")
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic tags',related=None)
    analytic_tag_id = fields.Many2one('account.analytic.tag', string='Analytic tag')
    force_cleared_bank_account = fields.Boolean('Force Cleared? ',
                                          help = 'Check if the transaction \
                                          has cleared from the bank')
    
    cleared_dcc_account = fields.Boolean('Cleared? ',
                                          help = 'Check if the transaction \
                                          has cleared from the bank')
    dcc_id = fields.Many2one('bank.acc.rec.statement',
                            'Daily Cashier Closing',
                            help = "The Daily Cashier \
                            Closing linked with the \
                            journal item")
    draft_assigned_to_dcc = fields.Boolean('Assigned to DCC? ',
                                            help = 'Check if the move \
                                            line is assigned to \
                                            DCC lines')
    
    @api.model
    def create(self, vals):
        """ :context's key apply_taxes: set to True if you want vals['tax_ids'] to result in the creation of move lines for taxes and eventual
                adjustment of the line amount (in case of a tax included in price).

            :context's key `check_move_validity`: check data consistency after move line creation. Eg. set to false to disable verification that the move
                debit-credit == 0 while creating the move lines composing the move.

        """
        context = dict(self._context or {})
        amount = vals.get('debit', 0.0) - vals.get('credit', 0.0)
        if not vals.get('partner_id') and context.get('partner_id'):
            vals['partner_id'] = context.get('partner_id')
        move = self.env['account.move'].browse(vals['move_id'])
        account = self.env['account.account'].browse(vals['account_id'])
        if account.deprecated:
            raise UserError(_('The account %s (%s) is deprecated !') %(account.name, account.code))
        if 'journal_id' in vals and vals['journal_id']:
            context['journal_id'] = vals['journal_id']
        if 'date' in vals and vals['date']:
            context['date'] = vals['date']
        if 'journal_id' not in context:
            context['journal_id'] = move.journal_id.id
            context['date'] = move.date
        #we need to treat the case where a value is given in the context for period_id as a string
        if not context.get('journal_id', False) and context.get('search_default_journal_id', False):
            context['journal_id'] = context.get('search_default_journal_id')
        if 'date' not in context:
            context['date'] = fields.Date.context_today(self)
        journal = vals.get('journal_id') and self.env['account.journal'].browse(vals['journal_id']) or move.journal_id
        vals['date_maturity'] = vals.get('date_maturity') or vals.get('date') or move.date
        ok = not (journal.type_control_ids or journal.account_control_ids)

        if journal.type_control_ids:
            type = account.user_type_id
            for t in journal.type_control_ids:
                if type == t:
                    ok = True
                    break
        if journal.account_control_ids and not ok:
            for a in journal.account_control_ids:
                if a.id == vals['account_id']:
                    ok = True
                    break
        # Automatically convert in the account's secondary currency if there is one and
        # the provided values were not already multi-currency
        if account.currency_id and 'amount_currency' not in vals and account.currency_id.id != account.company_id.currency_id.id:
            vals['currency_id'] = account.currency_id.id
            if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                vals['amount_currency'] = 0.0
            else:
                ctx = {}
                if 'date' in vals:
                    ctx['date'] = vals['date']
                vals['amount_currency'] = account.company_id.currency_id.with_context(ctx).compute(amount, account.currency_id)

        if not ok:
            raise UserError(_('You cannot use this general account in this journal, check the tab \'Entry Controls\' on the related journal.'))

        # Create tax lines
        tax_lines_vals = []
        if context.get('apply_taxes') and vals.get('tax_ids'):
            # Get ids from triplets : https://www.tigernix.com/documentation/10.0/reference/orm.html#tigernix.models.Model.write
            tax_ids = [tax['id'] for tax in self.resolve_2many_commands('tax_ids', vals['tax_ids']) if tax.get('id')]
            # Since create() receives ids instead of recordset, let's just use the old-api bridge
            taxes = self.env['account.tax'].browse(tax_ids)
            currency = self.env['res.currency'].browse(vals.get('currency_id'))
            partner = self.env['res.partner'].browse(vals.get('partner_id'))
            res = taxes.with_context(dict(self._context, round=True)).compute_all(amount,
                currency, 1, vals.get('product_id'), partner)
            # Adjust line amount if any tax is price_include
            if abs(res['total_excluded']) < abs(amount):
                if vals['debit'] != 0.0: vals['debit'] = res['total_excluded']
                if vals['credit'] != 0.0: vals['credit'] = -res['total_excluded']
                if vals.get('amount_currency'):
                    vals['amount_currency'] = self.env['res.currency'].browse(vals['currency_id']).round(vals['amount_currency'] * (res['total_excluded']/amount))
            # Create tax lines
            for tax_vals in res['taxes']:
                if tax_vals['amount']:
                    tax = self.env['account.tax'].browse([tax_vals['id']])
                    account_id = (amount > 0 and tax_vals['account_id'] or tax_vals['refund_account_id'])
                    if not account_id: account_id = vals['account_id']
                    default_funds = self.env.user.company_id.default_funds_id
                    if not default_funds:
                        raise UserError(_('Please set default Funds in company configuration!'))
                    temp = {
                        'account_id': account_id,
                        'name': vals['name'] + ' ' + tax_vals['name'],
                        'tax_line_id': tax_vals['id'],
                        'move_id': vals['move_id'],
                        'partner_id': vals.get('partner_id'),
                        'statement_id': vals.get('statement_id'),
                        'debit': tax_vals['amount'] > 0 and tax_vals['amount'] or 0.0,
                        'credit': tax_vals['amount'] < 0 and -tax_vals['amount'] or 0.0,
                        'analytic_account_id': vals.get('analytic_account_id') if tax.analytic else False,
                        'funds_id': default_funds.id,
                    }
                    bank = self.env["account.bank.statement"].browse(vals.get('statement_id'))
                    if bank.currency_id != bank.company_id.currency_id:
                        ctx = {}
                        if 'date' in vals:
                            ctx['date'] = vals['date']
                        temp['currency_id'] = bank.currency_id.id
                        temp['amount_currency'] = bank.company_id.currency_id.with_context(ctx).compute(tax_vals['amount'], bank.currency_id, round=True)
                    tax_lines_vals.append(temp)

        #Toggle the 'tax_exigible' field to False in case it is not yet given and the tax in 'tax_line_id' or one of
        #the 'tax_ids' is a cash based tax.
        taxes = False
        if vals.get('tax_line_id'):
            taxes = [{'tax_exigibility': self.env['account.tax'].browse(vals['tax_line_id']).tax_exigibility}]
        if vals.get('tax_ids'):
            taxes = self.env['account.move.line'].resolve_2many_commands('tax_ids', vals['tax_ids'])
        if taxes and any([tax['tax_exigibility'] == 'on_payment' for tax in taxes]) and not vals.get('tax_exigible'):
            vals['tax_exigible'] = False

        new_line = models.Model.create(self,vals)
        for tax_line_vals in tax_lines_vals:
            # TODO: remove .with_context(context) once this context nonsense is solved
            self.with_context(context).create(tax_line_vals)

        if self._context.get('check_move_validity', True):
            move.with_context(context)._post_validate()

        return new_line
    
    @api.onchange('funds_id')
    def onchange_funds_id(self):
        analytic_tag_funds = self.funds_id.analytic_tag_id
        for funds in self:
            if funds.funds_id:
                self.analytic_tag_id = analytic_tag_funds
        
    @api.multi
    @api.depends('move_id')
    def name_get(self):
        result = []
        for line in self:
            result.append((line.id, line.move_id.name))
        return result
    
    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context
        if context and context.get('search_invoice_ids', False):
            args += [('invoice_id','in',context['search_invoice_ids'])]
        if context.get('search_non_voucher_refund'):
            #should not use parameter advance_voucher_ids, the id will only become new id object (not integer) if came from onchange
            query = '''
                select moveline.id
                from advance_account_voucher_line vline
                join account_move_line moveline on moveline.id=vline.move_line_id
                join advance_account_voucher voucher on voucher.id=vline.voucher_id
                where voucher.refund_payment = True
                    and voucher.state not in ('cancel','draft','posted')
            ''' #also exclude posted since the posted already reconciled 
            self._cr.execute(query)
            query_result = self._cr.fetchall()
            refund_moveline_ids = list(chain.from_iterable(query_result))
            args += [('id','not in',refund_moveline_ids)]
        return super(AccountMoveLine, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
    
    
    @api.model
    def _query_get(self, domain=None):
        context = dict(self._context or {})
        domain = domain or []
#        if not isinstance(domain, (list, tuple)):
#            domain = safe_eval(domain)

        if context.get('search_funds_type_ids',[]):
            #funds_ids = list(self.env['pool.of.funds'].search([('type_id','in',context['search_funds_type_ids'])])._ids)
            #domain += [('funds_id', 'in', funds_ids)]
            funds_type_obj = self.env['type.of.funds']
            #fund_types = funds_type_obj.search('id','in',context['search_funds_type_ids'])
            fund_types = funds_type_obj.browse(context['search_funds_type_ids'])
            account_ids = []
            for fund_type in fund_types:
                account_ids += list(fund_type.account_ids._ids)
            domain += [('account_id', 'in', account_ids)]

        return super(AccountMoveLine, self)._query_get(domain=domain)


    def auto_reconcile_lines(self):
        """ This function iterates recursively on the recordset given as parameter as long as it
            can find a debit and a credit to reconcile together. It returns the recordset of the
            account move lines that were not reconciled during the process.
        """
        if not self.ids:
            return self
        sm_debit_move, sm_credit_move = self._get_pair_to_reconcile()
        #there is no more pair to reconcile so return what move_line are left
        if not sm_credit_move or not sm_debit_move:
            return self
        company_currency_id = self[0].account_id.company_id.currency_id
        account_curreny_id = self[0].account_id.currency_id
        field = (account_curreny_id and company_currency_id != account_curreny_id) and 'amount_residual_currency' or 'amount_residual'
        if not sm_debit_move.debit and not sm_debit_move.credit:
            #both debit and credit field are 0, consider the amount_residual_currency field because it's an exchange difference entry
            field = 'amount_residual_currency'
        if self[0].currency_id and all([x.currency_id == self[0].currency_id for x in self]):
            #all the lines have the same currency, so we consider the amount_residual_currency field
            field = 'amount_residual_currency'
        if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
            field = 'amount_residual'
        elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
            field = 'amount_residual_currency'
        #Reconcile the pair together
        amount_reconcile = min(sm_debit_move[field], -sm_credit_move[field])
        #Remove from recordset the one(s) that will be totally reconciled
        if amount_reconcile == sm_debit_move[field]:
            self -= sm_debit_move
        if amount_reconcile == -sm_credit_move[field]:
            self -= sm_credit_move

        #Check for the currency and amount_currency we can set
        currency = False
        amount_reconcile_currency = 0
        if sm_debit_move.currency_id == sm_credit_move.currency_id and sm_debit_move.currency_id.id:
            currency = sm_credit_move.currency_id.id
            amount_reconcile_currency = min(sm_debit_move.amount_residual_currency, -sm_credit_move.amount_residual_currency)

        amount_reconcile = min(sm_debit_move.amount_residual, -sm_credit_move.amount_residual)

        if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
            amount_reconcile_currency = 0.0
        reconcile_date = self._context.get('reconcile_date')

        self.env['account.partial.reconcile'].create({
            'debit_move_id': sm_debit_move.id,
            'credit_move_id': sm_credit_move.id,
            'amount': amount_reconcile,
            'amount_currency': amount_reconcile_currency,
            'currency_id': currency,
            'reconcile_date': reconcile_date,
        })

        #Iterate process again on self
        return self.auto_reconcile_lines()

    @api.model
    def _prepare_exchange_diff_partial_reconcile(self, aml, line_to_reconcile, currency):
        reconcile_date = self._context.get('reconcile_date')
        return {
            'debit_move_id': aml.credit and line_to_reconcile.id or aml.id,
            'credit_move_id': aml.debit and line_to_reconcile.id or aml.id,
            'amount': abs(aml.amount_residual),
            'amount_currency': abs(aml.amount_residual_currency),
            'currency_id': currency.id,
            'reconcile_date': reconcile_date,
        }
    
    @api.multi
    def reconcile(self, writeoff_acc_id=False, writeoff_journal_id=False):
        # Empty self can happen if the user tries to reconcile entries which are already reconciled.
        # The calling method might have filtered out reconciled lines.
        if not self:
            return True

        #Perform all checks on lines
        company_ids = set()
        all_accounts = []
        partners = set()
        for line in self:
            company_ids.add(line.company_id.id)
            all_accounts.append(line.account_id)
            if (line.account_id.internal_type in ('receivable', 'payable')):
                partners.add(line.partner_id.id)
            # if line.reconciled:
            #     raise UserError(_('You are trying to reconcile some entries that are already reconciled!'))
        if len(company_ids) > 1:
            raise UserError(_('To reconcile the entries company should be the same for all entries!'))
        
        #if len(set(all_accounts)) > 1:
        if any(acc.internal_type != all_accounts[0].internal_type for acc in all_accounts):
            raise UserError(_('Entries are not of the same account!'))
        if not (all_accounts[0].reconcile or all_accounts[0].internal_type == 'liquidity'):
            raise UserError(_('The account %s (%s) is not marked as reconciliable !') % (all_accounts[0].name, all_accounts[0].code))

        #reconcile everything that can be
        remaining_moves = self.auto_reconcile_lines()

        #if writeoff_acc_id specified, then create write-off move with value the remaining amount from move in self
        if writeoff_acc_id and writeoff_journal_id and remaining_moves:
            all_aml_share_same_currency = all([x.currency_id == self[0].currency_id for x in self])
            writeoff_vals = {
                'account_id': writeoff_acc_id.id,
                'journal_id': writeoff_journal_id.id
            }
            if not all_aml_share_same_currency:
                writeoff_vals['amount_currency'] = False
            writeoff_to_reconcile = remaining_moves._create_writeoff(writeoff_vals)
            #add writeoff line to reconcile algo and finish the reconciliation
            remaining_moves = (remaining_moves + writeoff_to_reconcile).auto_reconcile_lines()
            return writeoff_to_reconcile
        
        account_move_ids = [l.move_id.id for l in self if float_compare(l.move_id.matched_percentage, 1, precision_digits=5) == 0]
        if account_move_ids:
            expense_sheets = self.env['hr.expense.sheet'].search([
                ('account_move_id', 'in', account_move_ids), ('state', '!=', 'done')
            ])
            expense_sheets.set_to_paid()

        return True
    
    def force_full_reconcile(self):
        reconcile_date = fields.Date.context_today(self) 
        self = self.with_context(reconcile_date=reconcile_date)
        return super(AccountMoveLine, self).force_full_reconcile()
    
    @api.one
    def _prepare_analytic_line(self):
        """ Prepare the values used to create() an account.analytic.line upon validation of an account.move.line having
            an analytic account. This method is intended to be extended in other modules.
        """
        amount = (self.credit or 0.0) - (self.debit or 0.0)
        return {
            'name': self.name,
            'date': self.date,
            'account_id': self.analytic_account_id.id,
            'tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
            'unit_amount': self.quantity,
            'product_id': self.product_id and self.product_id.id or False,
            'product_uom_id': self.product_uom_id and self.product_uom_id.id or False,
            'amount': self.company_currency_id.with_context(date=self.date or fields.Date.context_today(self)).compute(amount, self.analytic_account_id.currency_id) if self.analytic_account_id.currency_id else amount,
            'general_account_id': self.account_id.id,
            'ref': self.ref,
            'move_id': self.id,
            'user_id': self.invoice_id.user_id.id or self._uid,
            'user_id': self.invoice_id.user_id.id or self._uid,
            'tag_id': self.analytic_tag_id.id,
        }

class AccountPartialReconcile(models.Model):
    _inherit = "account.partial.reconcile"
    
    reconcile_date = fields.Date('Date Reconciled')
    
    @api.model
    def create_exchange_rate_entry(self, aml_to_fix, amount_diff, diff_in_currency, currency, move):
        """
        Automatically create a journal items to book the exchange rate
        differences that can occure in multi-currencies environment. That
        new journal item will be made into the given `move` in the company
        `currency_exchange_journal_id`, and one of its journal items is
        matched with the other lines to balance the full reconciliation.

        :param aml_to_fix: recordset of account.move.line (possible several
            but sharing the same currency)
        :param amount_diff: float. Amount in company currency to fix
        :param diff_in_currency: float. Amount in foreign currency `currency`
            to fix
        :param currency: res.currency
        :param move: account.move
        :return: tuple.
            [0]: account.move.line created to balance the `aml_to_fix`
            [1]: recordset of account.partial.reconcile created between the
                tuple first element and the `aml_to_fix`
        """
        partial_rec = self.env['account.partial.reconcile']
        aml_model = self.env['account.move.line']

        amount_diff = move.company_id.currency_id.round(amount_diff)
        diff_in_currency = currency and currency.round(diff_in_currency) or 0
        
        company_id = self.env.user.company_id
        analytic_account_id = False
        funds_id = False
        analytic_tag_id = False
        if not company_id.account_analytic_id:
            raise UserError(_("Please select default anallytic account in company configuration"))
        else:
            analytic_account_id = company_id.account_analytic_id

        if not company_id.default_funds_id:
            raise UserError(_("Please select default funds in company configuration"))
        else:
            funds_id = company_id.default_funds_id

        if not company_id.analytic_tag_id:
            raise UserError(_("Please select default analytic tag in company configuration"))
        else:
            analytic_tag_id = company_id.analytic_tag_id

        created_lines = self.env['account.move.line']
        for aml in aml_to_fix:
            #create the line that will compensate all the aml_to_fix
            line_to_rec = aml_model.with_context(check_move_validity=False).create({
                'name': _('Currency exchange rate difference'),
                'debit': amount_diff < 0 and -aml.amount_residual or 0.0,
                'credit': amount_diff > 0 and aml.amount_residual or 0.0,
                'account_id': aml.account_id.id,
                'move_id': move.id,
                'currency_id': currency.id,
                'amount_currency': diff_in_currency and -aml.amount_residual_currency or 0.0,
                'partner_id': aml.partner_id.id,
                'analytic_account_id': analytic_account_id.id or False,
                'analytic_tag_id': analytic_tag_id.id or False,
                'funds_id': funds_id.id or False,
            })
            #create the counterpart on exchange gain/loss account
            exchange_journal = move.company_id.currency_exchange_journal_id
            aml_model.with_context(check_move_validity=False).create({
                'name': _('Currency exchange rate difference'),
                'debit': amount_diff > 0 and aml.amount_residual or 0.0,
                'credit': amount_diff < 0 and -aml.amount_residual or 0.0,
                'account_id': amount_diff > 0 and exchange_journal.default_debit_account_id.id or exchange_journal.default_credit_account_id.id,
                'move_id': move.id,
                'currency_id': currency.id,
                'amount_currency': diff_in_currency and aml.amount_residual_currency or 0.0,
                'partner_id': aml.partner_id.id,
                'analytic_account_id': analytic_account_id.id or False,
                'analytic_tag_id': analytic_tag_id.id or False,
                'funds_id': funds_id.id or False,
            })

            #reconcile all aml_to_fix
            partial_rec |= self.with_context(skip_full_reconcile_check=True).create(
                self._prepare_exchange_diff_partial_reconcile(
                        aml=aml,
                        line_to_reconcile=line_to_rec,
                        currency=currency)
            )
            created_lines |= line_to_rec
        return created_lines, partial_rec

class AccountFullReconcile(models.Model):
    _inherit = "account.full.reconcile"

    @api.multi
    def unlink(self):
        """ When removing a full reconciliation, we need to revert the eventual journal entries we created to book the
            fluctuation of the foreign currency's exchange rate.
            We need also to reconcile together the origin currency difference line and its reversal in order to completly
            cancel the currency difference entry on the partner account (otherwise it will still appear on the aged balance
            for example).
        """
        # KELVYN : TO REMOVE THE REVERSE MOVE FUNCTION
        for rec in self:
            if rec.exchange_move_id:
                # reverse the exchange rate entry after de-referencing it to avoid looping
                # (reversing will cause a nested attempt to drop the full reconciliation)
                to_reverse = rec.exchange_move_id
                rec.exchange_move_id = False
                # to_reverse.reverse_moves()
        return models.Model.unlink(self)

#def reconcile
#def auto_reconcile_lines
#def create_exchange_rate_entry
#def partial_reconcile_lines
#manual reconcile from menu journal items?
#def reverse_moves? #no changes
#def process_reconciliation #no changes
#def compute_refund   at account.invoice.refund #?

class AccountMoveLineReconcileWriteoff(models.TransientModel):
    
    _inherit = 'account.move.line.reconcile.writeoff'
    
    
    @api.multi
    def trans_rec_reconcile_partial(self):
        reconcile_date = self.date_p or fields.Date.context_today(self) 
        self = self.with_context(reconcile_date=reconcile_date)
        return super(AccountMoveLineReconcileWriteoff, self).trans_rec_reconcile_partial()
        
    @api.multi
    def trans_rec_reconcile(self):
        reconcile_date = self.date_p or fields.Date.context_today(self) 
        self = self.with_context(reconcile_date=reconcile_date)
        return super(AccountMoveLineReconcileWriteoff, self).trans_rec_reconcile()
    
class AccountMoveMultiCancel(models.TransientModel):
    _name = "account.move.multi.cancel.wizard"
    
    @api.multi
    def account_move_multi_cancel_button(self):
        context = self._context
        active_ids = context.get('active_ids')
        move_obj = self.env['account.move']
        moves = []
        if active_ids:
            moves = move_obj.browse(active_ids)
            
        if moves:
            for move in moves:
                move.button_cancel()