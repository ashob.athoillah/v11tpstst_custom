# -*- encoding: utf-8 -*-
import time
from odoo.addons import decimal_precision as dp
from datetime import datetime, timedelta
from datetime import datetime, date
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from itertools import chain


class PurchaseinAdvance(models.Model):
    _name = "purchase.in.advance"
    _description = 'Purchase in Advance'
    _inherit = ['mail.thread']


    @api.model
    def _default_account(self):
        company = self.env.user.company_id
        account_id = company.purchase_adv_income_account_id and company.purchase_adv_income_account_id.id or False

        return account_id

    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.membership_journal_id and company.membership_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    @api.model
    def _default_niche_funds(self):
        company = self.env.user.company_id
        funds_id = company.default_niche_fund_id and company.default_niche_fund_id.id or False

        return funds_id

    @api.model
    def _default_tablet_funds(self):
        company = self.env.user.company_id
        funds_id = company.default_tablet_fund_id and company.default_tablet_fund_id.id or False

        return funds_id

    #general
    reg_date = fields.Date('Date of Application / 申请日期', default=lambda self: fields.datetime.now(), track_visibility='onchange')
    reg_no = fields.Char('Registration Number /  编号', copy=False, track_visibility='onchange')

    remarks = fields.Text('Remark / 备考',  track_visibility='onchange')
    responsible_user_id = fields.Many2one('res.users', copy=False, string='Attended by / 经手人', track_visibility='onchange')

    #form
    partner_id = fields.Many2one('res.partner', string="Member", track_visibility='onchange')
    purchase_advance_type = fields.Selection([('niche', 'Niche'), ('tablet', 'Tablet')], string="Purchase in Advance For", track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'), 
        ('confirm', 'Confirm'), 
        ('approve', 'Approve'), 
        ('paid', 'Paid'),
        ('reject', 'Reject'), 
        ('cancel', 'Cancel')], default="draft", string="State", track_visibility='onchange')
    
    # Payment
    journal_id = fields.Many2one('account.journal', string="Journal", default=_default_journal, track_visibility='onchange')
    
    residual = fields.Float('Payable Amount',  track_visibility='onchange')
    amount_untaxed = fields.Float(string='Untaxed Fee', store=True,
                                  compute='_amount_all', track_visibility='onchange')
    amount_tax = fields.Float(string='GST', store=True, readonly=False, compute='_amount_all', track_visibility='onchange')
    amount_total = fields.Float(string='Total Amount', store=True,
                                compute='_amount_all', track_visibility='onchange')
    amount_paid = fields.Float(string='Amount Paid', store=True,
                                 track_visibility='onchange')
    invoice_ids = fields.Many2many('account.invoice', 'tablet_application_invoice_rel', 'tablet_application_id',
                                   'invoice_id', string='Invoices', track_visibility='onchange', copy=False)
    move_line_ids = fields.One2many('account.move.line', 'advance_id')
    voucher_ids = fields.One2many('account.voucher', 'purchase_in_advance_id')
    niche_funds_id = fields.Many2one('pool.of.funds','Funds', domain=[('state','=', 'confirm')], default=_default_niche_funds, track_visibility='onchange')
    tablet_funds_id = fields.Many2one('pool.of.funds', 'Funds', domain=[('state', '=', 'confirm')], default=_default_tablet_funds, track_visibility='onchange')
    account_id = fields.Many2one('account.account','Account', required=True, default=_default_account)

    # Product
    purchase_list_ids = fields.One2many('purchase.advance.list', 'purchase_in_advance_id', string="Purchase List")
    membership_application_id = fields.Many2one('membership.application', string="Related Membership Application", track_visibility='onchange')

    #etc
    accept_term_and_condition = fields.Boolean('I\'m willing to accept Terms & Conditions', track_visibility='onchange')
    tablet_term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self :self._get_default_tablet_advance_term_condition(), string='Term & Conditions', track_visibility='onchange')
    niche_term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self :self._get_default_niche_advance_term_condition(), string='Term & Conditions', track_visibility='onchange')
    signature = fields.Binary("Applicant’s Signature / 申请人签名", copy=False, track_visibility='onchange')

    @api.multi
    def _get_default_tablet_advance_term_condition(self) :
        company = self.env.user.company_id
        term_and_condition = False
        if company.default_tablet_in_advance_term_id :
            term_and_condition = company.default_tablet_in_advance_term_id and company.default_tablet_in_advance_term_id.id or False

        return term_and_condition
    @api.multi
    def _get_default_niche_advance_term_condition(self):
        company = self.env.user.company_id
        term_and_condition = False
        if company.default_niche_in_advance_term_id:
            term_and_condition = company.default_niche_in_advance_term_id and company.default_niche_in_advance_term_id.id or False

        return term_and_condition

    @api.multi
    def action_confirm(self) :
        for rec in self :
            if rec.purchase_advance_type == 'niche':
                for item in rec.purchase_list_ids:
                    item.action_add_booked_item()
            rec.state = 'confirm'
            rec.name_set()

    @api.multi
    def action_paid(self) :
        pool_of_funds = self.env['pool.of.funds'].search([('name','=', self.funds_id.name)])
        funds_vals = []
        vals = {
                    'funds_id': pool_of_funds.id,
                    'amount': self.amount_total,
                    'log_for': 'donation',
                    'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                    'account': self.account_id.user_type_id.name,
                }
        funds_vals.append((0, 0, vals))
        pool_of_funds.write({'log_ids':funds_vals})
        for rec in self :
            for item in rec.purchase_list_ids :
                item.action_write_status_item()
            if rec.partner_id.membership_type == 'associate'  :
                rec.get_application_record()
            rec.state = 'paid'

    @api.multi
    def action_paid_advance(self,fund) :
        if fund == 'tablet':
            pool_of_funds = self.env['pool.of.funds'].search([('name','=', self.tablet_funds_id.name)])
        if fund == 'niche':
            pool_of_funds = self.env['pool.of.funds'].search([('name','=', self.niche_funds_id.name)])
        funds_vals = []
        vals = {
                    'funds_id': pool_of_funds.id,
                    'amount': self.amount_total,
                    'log_for': 'donation',
                    'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                    'account': self.account_id.user_type_id.name,
                }
        funds_vals.append((0, 0, vals))
        pool_of_funds.write({'log_ids':funds_vals})
        for rec in self :
            for item in rec.purchase_list_ids :
                item.action_write_status_item()
            if rec.partner_id.membership_type == 'associate' :
                rec.get_application_record()
            rec.state = 'paid'
            

    @api.multi
    def action_approve(self) :
        for rec in self :
            if rec.purchase_advance_type == 'tablet':
                for item in rec.purchase_list_ids:
                    item.action_add_booked_item()
            rec.state = 'approve'
    
    @api.multi
    def action_reject(self) :
        for rec in self :
            if rec.purchase_advance_type == 'niche' :
                for item in rec.purchase_list_ids :
                    item.action_remove_booked_item()
            rec.state = 'reject'

    @api.multi
    def action_cancel(self) :
        self.get_responsible_user()
        if self.state == 'confirm' and self.purchase_advance_type == 'niche' :
            self.action_remove_booked_item()
        elif self.state == 'approve' :
            self.action_remove_booked_item()

        self.state = 'cancel'

    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id
        
    @api.multi
    def get_application_record(self) :
        app_obj = self.env['membership.application']
        application = False
        if self.purchase_advance_type == 'niche' :
            application = app_obj.search([('niche_advance_id', '=', self.id)], limit=1)
        else :
            application = app_obj.search([('tablet_advance_id', '=', self.id)], limit=1)
            
        if application :
            application.action_donated()
        # else :
        #     raise Warning(_('You\'re need to became associate member to able purchase tablet'))

    @api.depends('purchase_list_ids')
    @api.multi
    def _amount_all(self) :
        for rec in self :
            amount_untaxed = []
            list_taxes = []
            for item in rec.purchase_list_ids :
                amount_untaxed.append(item.amount_untaxed)
                list_taxes.append(item.tax)

            rec.amount_untaxed = sum(amount_untaxed)
            rec.amount_tax = sum(list_taxes)
            rec.amount_total = sum(amount_untaxed) + sum(list_taxes)
    
    @api.multi
    def name_set(self) :
        #by not put this on create function, system can avoid sequence skipping while doing test import
        if not self.reg_no :
            self.reg_no = self.env['ir.sequence'].next_by_code('purchase.in.advance.seq')

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.reg_no or _('New Puchase in Advance Application')
            result.append((rec.id, name))

        return result 

    @api.multi
    def call_payment_in_advance_wizard(self):
        vals = {}
        journal_id = self._default_journal() or False
        vals['journal_id'] = journal_id
        if self.purchase_advance_type == 'niche':
            return {
                'name': _('Niche Advance Payment'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'generate.voucher.donation',
                'target': 'new',
                'context': {'default_funds_id': self.niche_funds_id.id, 'default_niche_advance_id': self.id, 'default_amount': self.amount_total, 'default_reg_no': self.reg_no, 'default_journal_id':vals['journal_id'],'default_partner_id':self.partner_id.id}
            }
        elif self.purchase_advance_type == 'tablet':
            return {
                'name': _('Tablet Advance Payment'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'generate.voucher.donation',
                'target': 'new',
                'context': {'default_funds_id': self.tablet_funds_id.id, 'default_tablet_advance_id': self.id, 'default_amount': self.amount_total, 'default_reg_no': self.reg_no, 'default_journal_id':vals['journal_id'],'default_partner_id':self.partner_id.id}
            }
        else:
            raise Warning(_('Data type is mandatory, please select the type'))
    
class PurchaseinAdvanceList(models.Model) :
    _name = "purchase.advance.list"

    #parent record
    membership_application_id = fields.Many2one('membership.application', string="Related Membership Application", related="purchase_in_advance_id.membership_application_id")

    # main
    purchase_in_advance_id = fields.Many2one('purchase.in.advance', string="Related Purchase in Advance Form")
    product_id = fields.Many2one('product.product', string="Location", track_visibility='onchange')
    purchase_advance_type = fields.Selection([('niche', 'Niche'), ('tablet', 'Tablet')], related="purchase_in_advance_id.purchase_advance_type", string="Purchase in Advance For", track_visibility='onchange')
    level_id = fields.Many2one('level.detail.list', string="Level / Allotment Area", track_visibility='onchange')
    product_template_id = fields.Many2one('product.template', string="Product", track_visibility='onchange')
    unit_id = fields.Many2one('side.detail.list', string="Unit No.", track_visibility='onchange')
    unit_level_id = fields.Many2one('side.level.list', string="Unit No. Level", track_visibility='onchange')
    tablet_ids = fields.One2many('tablet.list', 'purchase_advance_list_id', string="Deceased Info")
    deceased_info_ids = fields.One2many('niche.deceased.info', 'purchase_advance_list_id', string="Deceased Info", track_visibility='onchange')

    #payment
    amount_untaxed = fields.Float('Amount Untax',store=True, compute="_get_product_price", track_visibility='onchange')
    tax = fields.Float('Tax', compute="_get_product_price",store=True, track_visibility='onchange')
    amount_total = fields.Float('Amount Total',store=True, compute="_get_product_price", track_visibility='onchange')
    
    #etc
    tablet_applied_name_reg_no = fields.Char('Tablet Applied No.')

    @api.depends('product_id', 'level_id')
    @api.multi
    def _get_product_price(self) :
        for rec in self :
            product = rec.product_id
            level = rec.level_id
            state = rec.purchase_in_advance_id.state
            amount_untaxed = 0.00
            list_taxes = []
            amount_tax= 0.00
            amount_paid = 0.00
            residual = 0.00
            #check whether sold to avoid overlapping 
            if product and level :
                if (product.sold_out or level.is_sold) and state not in ['approve', 'paid'] :
                    raise Warning(_('Location/Level for application %s already FULL please to choose other location/level') % (str(rec.partner_id.name) + ' - ' + str(product.name) + ' - ' + str(level.name)) )
                    
                amount_untaxed = level.price
                list_taxes = [float("%.2f" % (amount_untaxed * (taxes.amount / 100))) for taxes in product.taxes_id if product.taxes_id]
                amount_tax = amount_untaxed + sum(list_taxes)


            rec.amount_untaxed = amount_untaxed
            rec.tax = sum(list_taxes)
            rec.amount_total = amount_tax

    @api.onchange('product_id', 'level_id', 'purchase_in_advance_id', 'purchase_advance_type')
    @api.multi
    def action_filter_product(self):
        product_domain = []
        for rec in self :
            if rec.product_id :
                rec.product_template_id = rec.product_id.product_tmpl_id and rec.product_id.product_tmpl_id.id
            else :
                rec.product_template_id = False

            if not rec.level_id.product_template_id.id == rec.product_id.product_tmpl_id.id :
                rec.level_id = False
                
            invalid_product = []
            listed_id = []
            dic_id = {}
            list_dic = []
            for purchase in rec.purchase_in_advance_id.purchase_list_ids :
                if 1 >=(purchase.product_id.slot_left) :
                    invalid_product.append(purchase.product_id.id)
                else :
                    if (purchase.product_id.id) in listed_id :
                        for dic in list_dic :
                            if int(purchase.product_id.id) == dic['id'] :
                                current_slot = dic.get('total_slot')
                                if ((current_slot + 1) >= (purchase.product_id.slot_left)) :
                                    invalid_product.append(purchase.product_id.id)
                                else :
                                    dic['total_slot'] = current_slot + 1
                                    if 1 >= purchase.product_id.slot_left :
                                        invalid_product.append(purchase.product_id.id)
                    else :
                        dic_id['id'] = purchase.product_id.id
                        dic_id['total_slot'] = 1
                        list_dic.append(dic_id)
                        listed_id.append(purchase.product_id.id)
                        if 1 >= purchase.product_id.slot_left :
                            invalid_product.append(purchase.product_id.id)

            if rec.purchase_advance_type == 'niche' :
                product_domain = [('columbarium_product', '=', True), ('sold_out', '=', False), ('id', 'not in', invalid_product)]
            elif rec.purchase_advance_type == 'tablet':
                product_domain = [('tablet_product', '=', True), ('sold_out', '=', False), ('id', 'not in', invalid_product)]
                
        if product_domain :
            return {'domain' : {'product_id' : product_domain}}

    @api.onchange('product_id', 'level_id', 'purchase_advance_type')
    @api.multi
    def action_filter_level(self):
        level_domain = []
        for rec in self :
            invalid_level = []
            listed_id = []
            dic_id = {}
            list_dic = []
            for purchase in rec.purchase_in_advance_id.purchase_list_ids :
                if (purchase.level_id.slot_left - 1) < 0 :
                    invalid_level.append(purchase.level_id.id)
                else :
                    if (purchase.level_id.id) in listed_id :
                        for dic in list_dic :
                            if int(purchase.level_id.id) == dic['id'] :
                                current_slot = dic.get('total_slot')
                                if ((current_slot + 1) >= (purchase.level_id.slot_left)) :
                                    invalid_level.append(purchase.level_id.id)
                                else :
                                    dic['total_slot'] = current_slot + 1
                    else :
                        dic_id['id'] = purchase.level_id.id
                        dic_id['total_slot'] = 1
                        list_dic.append(dic_id)
                        listed_id.append(purchase.level_id.id)
                        if 1 >= purchase.level_id.slot_left :
                            invalid_level.append(purchase.level_id.id)

            if rec.purchase_advance_type == 'niche' :
                level_domain = [('is_sold', '=', False), ('product_template_id', '=', rec.product_id.product_tmpl_id.id), ('id', 'not in', invalid_level)]
            elif rec.purchase_advance_type == 'tablet':
                level_domain = [('is_sold', '=', False), ('product_template_id', '=', rec.product_id.product_tmpl_id.id), ('id', 'not in', invalid_level)]
            else:
                raise Warning(_('Please select type first!'))
            
        if level_domain :
            return {'domain' : {'level_id' : level_domain}}

    @api.multi
    def action_add_booked_item(self) :
        for rec in self :
            product = rec.product_template_id
            level = rec.level_id
            unit = rec.unit_id
            unit_level = rec.unit_level_id
            if rec.purchase_advance_type == 'niche' :
                if not rec.product_id.sold_out :
                    if level.slot > (level.slot_used + level.slot_booked) and not level.is_sold :
                        level.slot_booked += 1
                        level._compute_overall_tablet_slot()
                        product._compute_overall_slot()
                    else :
                        raise Warning(_('This level is not available anymore.')) 
                else :
                    raise Warning(_('This location and level is not available anymore.')) 
                
            elif rec.purchase_advance_type == 'tablet' :
                if not rec.product_id.sold_out :
                    if level.slot >= (level.slot_used + level.slot_booked) and not level.is_sold :
                        if unit.slot >= (unit.slot_used + unit.slot_booked) and not unit.is_sold :
                            if unit_level.slot >= (unit_level.slot_used + unit_level.slot_booked) and not unit_level.is_sold :
                                unit_level.slot_booked +=1
                                unit_level._compute_overall_slot()
                            else :
                                raise Warning(_('This unit level is not available anymore.')) 
                        else :
                            raise Warning(_('This unit is not available anymore.')) 
                        product._compute_overall_slot()
                    else :
                        raise Warning(_('This level is not available anymore.'))
                else :
                    raise Warning(_('This location and level is not available anymore.'))  
            else :
                raise Warning(_('System not able recognize this purchase, please contact the technical support!')) 

    @api.multi
    def action_remove_booked_item(self) :
        for rec in self :
            product = rec.product_template_id
            level = rec.level_id
            unit = rec.unit_id
            unit_level = rec.unit_level_id
            
            if rec.purchase_advance_type == 'niche' :
                level.slot_booked -= 1
            elif rec.purchase_advance_type == 'tablet' :
                unit_level.slot_booked -= 1
                unit._compute_slot_tablet()
                product._compute_overall_slot()
            else :
                raise Warning(_('System not able recognize this purchase, please contact the technical support!'))

    @api.multi
    def action_write_status_item(self) :
        for rec in self :
            product = rec.product_template_id
            level = rec.level_id
            unit = rec.unit_id
            unit_level = rec.unit_level_id
            if rec.purchase_advance_type == 'niche' :
                if not rec.product_id.sold_out :
                    if level.slot >= (level.slot_used + level.slot_booked) and not level.is_sold :
                        level.slot_used += 1
                        level.slot_booked -= 1
                        level._compute_overall_tablet_slot()
                        product._compute_overall_slot()
                    else :
                        raise Warning(_('This level is not available anymore.')) 
                else :
                    raise Warning(_('This location and level is not available anymore.')) 
                
            elif rec.purchase_advance_type == 'tablet' :
                if not rec.product_id.sold_out :
                    if level.slot >= (level.slot_used + level.slot_booked) and not level.is_sold :
                        if unit.slot >= (unit.slot_used + unit.slot_booked) and not unit.is_sold :
                            if unit_level.slot >= (unit_level.slot_used + unit_level.slot_booked) and not unit_level.is_sold :
                                unit_level.slot_used += 1
                                unit_level.slot_booked -= 1
                                unit_level._compute_slot_tablet()
                            else :
                                raise Warning(_('This unit level is not available anymore.'))
                        else :
                            raise Warning(_('This unit is not available anymore.')) 
                        product._compute_overall_slot()
                    else :
                        raise Warning(_('This level is not available anymore.'))
                else :
                    raise Warning(_('This location and level is not available anymore.'))  
            else :
                raise Warning(_('System not able recognize this purchase, please contact the technical support!')) 