from datetime import datetime, timedelta
from datetime import datetime, date
from odoo import api, fields, models, http, tools, SUPERUSER_ID, _
from odoo.exceptions import Warning, AccessError, UserError, ValidationError

class GenerateVoucherDonation(models.Model):
    _name = "generate.voucher.donation"
    _description = 'Generate Voucher Donation'

    donation_id = fields.Many2one('donation.donation', string="Donation")
    event_id = fields.Many2one('participant.registration', string="Event")
    niche_advance_id = fields.Many2one('purchase.in.advance', string="Niche")
    tablet_advance_id = fields.Many2one('purchase.in.advance', string="Adv Tablet")
    tablet_id = fields.Many2one('tablet.application', string="Tablet")
    niche_id = fields.Many2one('niche.application', string="Niche")
    membership_renewal_id = fields.Many2one('membership.renewal', string="Renew Membership")
    membership_id = fields.Many2one('membership.application', string="Membership")
    reg_no = fields.Char('Registration Number', copy=False, track_visibility='onchange')
    amount = fields.Float('Amount')
    fee = fields.Float('Fee')
    journal_id = fields.Many2one('account.journal', string="Journal")
    funds_id = fields.Many2one('pool.of.funds','Funds', domain=[('state','=', 'confirm')])
    partner_id = fields.Many2one('res.partner', string="Partner")
    ext_transaction_number = fields.Char('Transaction Number')
    date = fields.Date(string="Date", default=str(date.today().strftime("%Y-%m-%d")))
    hide = fields.Boolean('Hide', track_visibility='onchange')

    @api.onchange('date')
    def _check_date(self):
        if str(self.date) < str(date.today().strftime("%Y-%m-%d")):
            raise UserError(_("Please fix your date, date value must be more or same as today!"))

    @api.multi
    def action_create_voucher(self) :
        account = self.env['account.move']
        company = self.env.user.company_id
        config = self.env['res.company'].search([('id','=',company.id)])
        line_vals = []
        sequence = self.journal_id.sequence_id
        name_sequence = sequence.with_context(ir_sequence_date=account.date).next_by_id()
        
        if self.donation_id :
            if self.amount < 0 :
                raise ValidationError(_("Amount must be more than 0"))
            line_vals = []
            if self.donation_id :
                line_vals.append((0,0, {'partner_id': self.donation_id.partner_id.id, 'account_id' : config.donation_receivable_account_id.id, 'amount_currency': 0, 'currency_id': False, 'name' : _('Automatic Balancing Line'), 'move_id' : self.donation_id.id,'debit' : 0, 'credit' :self.amount}))
                for donation_list in self.donation_id.donation_line_ids:
                    funds_vals = []
                    vals_funds = {
                        'funds_id': self.funds_id.id,
                        'amount': donation_list.amount,
                        'log_for': 'donation',
                        'remarks': self.donation_id.remarks,
                        'transaction_date': str(date.today().strftime("%Y-%m-%d")),
                        'account_id': config.donation_receivable_account_id.id,
                    }
                    funds_vals.append((0, 0, vals_funds))
                    line_vals.append((0,0, {'partner_id': self.donation_id.partner_id.id, 'account_id' : config.donation_income_account_id.id, 'amount_currency': 0, 'currency_id': False, 'name' : donation_list.donation_type, 'funds_id': self.funds_id.id, 'move_id' : self.donation_id.id,'debit' : donation_list.amount, 'credit' : 0}))
                    self.funds_id.write({'log_ids':funds_vals})
                vals_journal = {'name': name_sequence, 'date': str(date.today().strftime("%Y-%m-%d")), 'journal_id': self.journal_id.id, 'ref': 'Donation', 'state' : 'posted', 'line_ids' : line_vals, 'narration': False}
                self.donation_id.create_voucher(self.amount, self.fee, self.ext_transaction_number,self.date)
                # self.donation_id.write({'state':'done'})
                self.donation_id.action_done()
                account.create(vals_journal)

        else:
            vals_credit = {'account_id' : config.membership_receivable_account_id.id, 'amount_currency': 0, 'currency_id': False, 'name' : self.reg_no, 'funds_id': self.funds_id.id, 'move_id' : self.id,'debit' : 0, 'credit' : self.amount}
            vals_debit = {'account_id' : config.membership_income_account_id.id, 'amount_currency': 0, 'currency_id': False, 'name' : self.reg_no, 'funds_id': self.funds_id.id, 'move_id' : self.id,'debit' : self.amount, 'credit' : 0}
            
            voucher_vals = {
                'account_id': config.donation_receivable_account_id.id,
                'name': self.reg_no,
                'number': name_sequence,
                'partner_id': self.partner_id.id, 
                'voucher_type': 'sale', 
                'custom_voucher_type': 'donation', 
                'pay_now': 'pay_now', 
                'payment_method_id': self.journal_id.id, 
                'payment_journal_id': self.journal_id.id, 
                'journal_id': self.journal_id.id, 
                'donation_interval_id': 1, 
                'date': self.date, 
                'line_ids': [(0, 0, {'name': self.reg_no, 'account_id':config.membership_receivable_account_id.id, 'price_unit': self.amount, 'type': 'donation'})], 
                'account_date': date.today().strftime("%Y-%m-%d"),
                'type_other_donation' : True,
                'ext_transaction_number' : self.ext_transaction_number 
            }
            
            if self.membership_id:
                vals_credit.update({'memberships_id': self.membership_id.id,'partner_id': self.partner_id.id,})
                vals_debit.update({'memberships_id': self.membership_id.id,'partner_id': self.partner_id.id})
                voucher_vals.update({'memberships_id': self.membership_id.id})
                ref = self.reg_no
            elif self.membership_renewal_id:
                vals_credit.update({'membership_renewal_id': self.membership_renewal_id.id,'partner_id': self.partner_id.id})
                vals_debit.update({'membership_renewal_id': self.membership_renewal_id.id,'partner_id': self.partner_id.id})
                voucher_vals.update({'memberships_renewal_id': self.membership_renewal_id.id})
                ref = self.reg_no
            elif self.niche_id:
                vals_credit.update({'niches_id': self.niche_id.id,'partner_id': self.partner_id.id})
                vals_debit.update({'niches_id': self.niche_id.id,'partner_id': self.partner_id.id})
                voucher_vals.update({'niche_id': self.niche_id.id})
                ref = self.reg_no
            elif self.tablet_id:
                vals_credit.update({'tablets_id': self.tablet_id.id,'partner_id': self.partner_id.id})
                vals_debit.update({'tablets_id': self.tablet_id.id,'partner_id': self.partner_id.id})
                voucher_vals.update({'tablet_id': self.tablet_id.id})
                ref = self.reg_no
            elif self.tablet_advance_id or self.niche_advance_id:
                object = self.tablet_advance_id or self.niche_advance_id
                vals_credit.update({'advance_id': object.id,'partner_id': self.partner_id.id})
                vals_debit.update({'advance_id': object.id,'partner_id': self.partner_id.id})
                voucher_vals.update({'advance_id': object.id})
                ref = self.reg_no
            elif self.event_id:
                vals_credit.update({'event_id': self.event_id.id,'partner_id': self.partner_id.id})
                vals_debit.update({'event_id': self.event_id.id,'partner_id': self.partner_id.id})
                voucher_vals.update({'event_id': self.event_id.id})
                ref = self.reg_no
            line_vals.append((0,0,vals_debit))
            line_vals.append((0,0,vals_credit))
            vals_journal = {'name': name_sequence, 'date': str(date.today().strftime("%Y-%m-%d")), 'journal_id': self.journal_id.id, 'ref': ref, 'state' : 'posted', 'line_ids' : line_vals, 'narration': False}
            
            voucher = self.env['account.voucher']
            voucher_id = voucher.create(voucher_vals)
            voucher_id.proforma_voucher()
            
            object = self.membership_id or self.membership_renewal_id or self.niche_id or self.tablet_id or self.tablet_advance_id or self.niche_advance_id or self.event_id
            account.create(vals_journal)
            if self.tablet_advance_id:
                object.action_paid_advance('tablet')
            elif self.niche_advance_id:
                object.action_paid_advance('niche')
            else:
                object.action_paid()

    @api.onchange('journal_id')
    def _compute_hide(self):
        if self.journal_id.transaction_no == True:
            self.hide = True
        else:
            self.ext_transaction_number = ""
            self.hide = False