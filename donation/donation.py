# -*- coding: utf-8 -*-
import requests
import pprint
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil.relativedelta import relativedelta
model_fields = fields
from odoo.tools import OrderedSet, pycompat
import logging
_logger = logging.getLogger(__name__)
from itertools import chain
from ast import literal_eval

class Donation(models.Model):
    _name = 'donation.donation'
    _description = 'Donation'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'name desc'
    _sql_constraints = [('name_unique', 'unique(name)', 'The donation no. is already exist.')]

    @api.model
    def _default_journal(self):
        company = self.env.user.company_id
        company_id = self._context.get('company_id', company.id)
        # not fix yet for domain
        domain = [
            ('type', '=', 'general'),
            ('company_id', '=', company_id),
        ]
        journal_id = company.donation_journal_id and company.donation_journal_id.id or False
        if not journal_id:
            journal_id = self.env['account.journal'].search(domain, limit=1)
        return journal_id

    name = fields.Char(string="Donation No.", copy=False, index=True,
                       track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('process', 'Process'),
        # ('approve', 'Approve'),
        ('done', 'Done'),
        ('cancel', 'Cancel'),
    ], string="Status", required=True, copy=False, default='draft', track_visibility='onchange')

    partner_id = fields.Many2one('res.partner', string="Name / 姓名", track_visibility='onchange')
    is_anonymous = fields.Boolean(string="Anonymous")
    date = fields.Date(string="Date", default=lambda *a: datetime.today(),   required=True,
                       track_visibility='onchange', index=True)
    currency_id = fields.Many2one('res.currency', compute='_get_company_currency', string='Currency', 
                                  required=True, default=lambda self: self._get_company_currency(), track_visibility=None)
    company_id = fields.Many2one('res.company', 'Company / 公司', required=True, 
                                 states={'draft': [('readonly', False)]}, default=lambda self: self._get_company(),
                                 track_visibility=None)

    campaign_id = fields.Many2one('donation.campaign', string="Campaign", default=lambda self: self._get_default_campaign(), track_visibility='onchange')

    total_amount = fields.Monetary(string="Donation Amount", compute="_compute_donation_amount",
                                            store=True, track_visibility='onchange')

    residual = fields.Monetary(string="Residual", compute="_compute_donation_amount", store=True, track_visibility='onchange')
    fee_amount = fields.Monetary(string="Fee Amount", compute="_compute_donation_amount", store=True, track_visibility='onchange')
    total_received_amount = fields.Monetary(string="Received Amount", compute="_compute_donation_amount",
                                            store=True, track_visibility='onchange')

    total_amount_donation = fields.Monetary(string="Total Amount Donation", compute="_get_total_amount_of_donation", store=True, track_visibility='onchange')

    active = fields.Boolean(string="Active", default=True, store=True, track_visibility='onchange')

    remarks = fields.Text(string="Remark / 备考", track_visibility='onchange')

    donation_interval_id = fields.Many2one('donation.interval', default=lambda self: self._get_default_interval(), string="Donation Interval", track_visibility='onchange')
    voucher_ids = fields.One2many('account.voucher', 'donation_id', string="Voucher", track_visibility='onchange')
    start_date = fields.Date('Start Date', help='to determine when donation start', track_visibility='onchange')
    end_date = fields.Date('End Date', help='to determine when donation end', track_visibility='onchange')
    billing_date = fields.Date('Billing Date', store=True, compute="_generate_billing_date", inverse="_set_billing_date", track_visibility='onchange')
    prev_billing_date = fields.Date('Previous Billing Date', compute="_get_billing_date", store=True)
    interval = fields.Integer('Interval')
    donation_line_ids = fields.One2many('donation.line', 'donation_id', string="Donation Line", track_visibility='onchange')
    responsible_user_id = fields.Many2one('res.users', string='Responsible', track_visibility='onchange')
    membership_application_id = fields.Many2one('membership.application', string="Related Membership Application", track_visibility='onchange')
    paynow_no = fields.Char('PayNow No.', track_visibility='onchange')
    cheque_no = fields.Char('Cheque No.', track_visibility='onchange')
    first_deduction_date = fields.Date('First Deduction Date', track_visibility='onchange')
    payment_method_id = fields.Many2one('account.journal', string="Payment Method", track_visibility='onchange')
    pay_now = fields.Selection([('pay_now', 'Pay Directly'), ('pay_later','Pay Later')], default="pay_now", string="Payment Type",track_visibility='onchange')
    payment_method_type = fields.Selection([
        ('paynow','PayNow'),
        ('cash', 'Cash'),
        ('net', 'Nets'),
        ('cheque', 'Cheque'),
        ('other', 'Miscellaneous')
    ], string="Type", required=True, related='payment_method_id.type')
    accept_term_and_condition = fields.Boolean('I\'m willing to accept Terms & Conditions', track_visibility='onchange')
    term_and_condition_id = fields.Many2one('term.and.condition', default=lambda self: self._get_default_term_condition(), string='Term & Conditions', track_visibility='onchange')
    fee_membership = fields.Float('Fee Membership', store=True, compute="_get_fee_membership", track_visibility='onchange')


    @api.multi
    def call_donation_line_generator_wizard(self):
        return {
            'name': _('Generate Donation List'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.donation.line',
            'target': 'new',
            'context': {'default_donation_id': self.id}
        }

    @api.multi
    def call_voucher_donation_generator_wizard(self,):
        vals = {}
        journal_id = self._default_journal() or False
        vals['journal_id'] = journal_id
        return {
            'name': _('Generate Voucher Donation'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'generate.voucher.donation',
            'target': 'new',
            'context': {'default_donation_id': self.id, 'default_amount': self.total_amount_donation, 'default_journal_id': vals['journal_id']}
        }

    @api.multi
    def action_confirm(self) :
        self.name_set()
        self.get_responsible_user()
        # self.generate_donation_list(True, True, True, 100)
        self.state = 'confirm'

    @api.multi
    def action_process(self) :
        self.get_responsible_user()
        self.state = 'process'

    # @api.multi
    # def action_approve(self) :
    #     self.get_responsible_user()
    #     self.state = 'approve'
    #     self.check_current_status()

    @api.multi
    def action_paid(self):
        self.action_done()

    @api.multi
    def action_done(self) :
        self.get_responsible_user()
        membership_id = self.membership_application_id
        if membership_id :
            membership_id._get_donation_amount()
            membership_id.action_donated()
        else :
            self.mail_done()
        self.state = 'done'

    @api.multi
    def action_cancel(self) :
        self.get_responsible_user()
        self.state = 'cancel'
    
    # @api.multi
    # def action_voucher(self) :
    #     self.get_responsible_user()
    #     self.create_voucher(self.total_amount_donation, 0, self.payment_method_id)

    @api.multi
    def action_generate_next_billing_date(self):
        self.generate_next_billing_date()


    # Get something function 

    @api.multi
    def get_responsible_user(self) :
        self.responsible_user_id = self.env.user.id
    
    @api.multi
    def generate_start_date(self) :
        self.start_date = datetime.today()

    @api.multi
    def generate_end_date(self) :
        self.end_date = datetime.today()

    @api.multi
    def generate_next_billing_date(self) :
        for rec in self :
            if rec.prev_billing_date and rec.interval > 0 and not rec.end_date and rec.start_date:
                billing_date = rec.prev_billing_date + relativedelta(months=+rec.interval) 
                rec.billing_date = billing_date

    @api.depends('start_date')
    @api.multi
    def _generate_billing_date(self) :
        for rec in self :
            billing_date = False
            if not rec.prev_billing_date and rec.start_date and rec.interval > 0 and not rec.end_date:
                billing_date = rec.start_date + relativedelta(months=+rec.interval)

            rec.billing_date = billing_date

    @api.multi
    def _set_billing_date(self) :
        if self.end_date and self.billing_date :
            if self.billing_date > self.end_date :
                raise ValidationError(_("The billing date is invalid. Please set it before the end date"))
        elif self.start_date and self.billing_date :
            if self.billing_date < self.start_date :
                raise ValidationError(_("The billing date is invalid. Please set it after the start date"))

    @api.depends('billing_date')
    @api.multi
    def _get_billing_date(self) :
        for rec in self :
            prev_billing_date = False
            if rec.billing_date :
                prev_billing_date = rec.billing_date
            rec.prev_billing_date = prev_billing_date


    @api.onchange('donation_interval_id')
    @api.multi
    def get_interval(self) :
        if self.donation_interval_id :
            self.interval = self.donation_interval_id.interval
        else :
            self.interval = 0

        if self.start_date :
            self.start_date = False

        if self.billing_date :
            self.billing_date = False

    @api.multi
    def _get_default_interval(self) :
        obj_interval = self.env['donation.interval']
        get_interval = obj_interval.search([('default', '=', True)], limit=1)
        return get_interval

    @api.multi
    def _get_default_campaign(self) :
        obj_campaign = self.env['donation.campaign']
        get_campaign = obj_campaign.search([('default', '=', True)], limit=1)
        return get_campaign

    @api.depends('company_id')
    @api.multi
    def _get_company_currency(self):
        for rec in self :
            rec.currency_id = rec.company_id.currency_id.id

    @api.model
    def _get_company(self):
        return self._context.get('company_id', self.env.user.company_id.id)

    @api.multi
    def _get_default_term_condition(self) :
        company = self.env.user.company_id

        term_and_condition = False
        
        if company.default_donation_term_id :
            term_and_condition = company.default_donation_term_id and company.default_donation_term_id.id or False

        return term_and_condition

    @api.depends('membership_application_id', 'membership_application_id.membership_fee_id', 'membership_application_id.membership_fee_id.valid_price')
    @api.multi
    def _get_fee_membership(self) :
        fee = 0.00
        for rec in self :
            if rec.membership_application_id :
                fee = rec.membership_application_id.membership_fee_id.valid_price

            rec.fee_membership = fee

    # Payment Related

    # def _check_lock_date(self):
    #     company_id = self.env.user.company_id
    #     for rec in self:
    #         lock_date = max(company_id.period_lock_date or '0000-00-00', company_id.fiscalyear_lock_date or '0000-00-00')
    #         if self.env.user.has_group('base.group_erp_manager'):
    #             lock_date = company_id.fiscalyear_lock_date
    #         if rec.date <= (lock_date or '0000-00-00'):
    #             if self.env.user.has_group('base.group_erp_manager'):
    #                 message = _("You cannot add/modify entries prior to and inclusive of the lock date %s") % (lock_date)
    #             else:
    #                 message = _("You cannot add/modify entries prior to and inclusive of the lock date %s. Check the company settings or ask someone with the 'Administrator' role") % (lock_date)
    #             raise UserError(message)
    #     return True

    @api.multi
    def action_create_contact(self, name, email) :
        obj_partner = self.env['res.partner']
        partner = False
        email = email.lower().strip()


        check_email = obj_partner.search([('email', '=ilike', email)], limit=1)

        if not check_email :
            partner = obj_partner.create({'name' : name, 'email' : email})
        else :
            partner = check_email

        if partner :
            self.partner_id = partner.id
        else :
            raise ValidationError(_("Something Went Wrong!!!"))

    @api.multi
    def generate_donation_list(self, fund_charity, fund_festival, fund_medical, fund_trunk, fund_other, charity, festival, medical, trunk, other, total_amount) :
        #parameter for chairty festival and medical is boolean value
        self.write({'donation_line_ids': [(5, 0, 0)]})
        obj_donation_line = self.env['donation.line']
        allocation = 0
        amount = 0.00
        divided_amount = 0.00
        result = 0.00
        different_amt = 0.00
        remaining_amt = 0.00
        list_bool = [charity, festival, medical, trunk, other]
        if total_amount :
            # count donation purpose categ chosen by user
            allocation = len([value for value in list_bool if value])
            if allocation > 1 :
                amount = total_amount / allocation
                divided_amount = float(("%.2f" % amount))

                result = divided_amount * allocation
                # avoid amount that can't divide by category count
                if result != total_amount :
                    #allocate remaining amount into var
                    remaining_amt = total_amount - result
                    different_amt += round(remaining_amt, 2)
            else :
                amount = total_amount

                
        data_list = []
        if charity :
            data = {
                'donation_type' : 'charity',
                'amount' : round((divided_amount + different_amt), 2) if divided_amount > 0 else amount,
                'donation_id' : self.id,
                'funds_id' : fund_charity
            }
            data_list.append(data)

        if festival :
            data = {
                'donation_type' : 'festival',
                'amount' : divided_amount if divided_amount > 0 else amount,
                'donation_id' : self.id,
                'funds_id' : fund_festival
            }
            data_list.append(data)

        if medical :
            data = {
                'donation_type' : 'medical',
                'amount' : divided_amount if divided_amount > 0 else amount,
                'donation_id' : self.id,
                'funds_id' : fund_medical
            }
            data_list.append(data)

        if trunk :
            data = {
                'donation_type' : 'trunk',
                'amount' : divided_amount if divided_amount > 0 else amount,
                'donation_id' : self.id,
                'funds_id' : fund_trunk
            }
            data_list.append(data)

        if other :
            data = {
                'donation_type' : 'other',
                'amount' : divided_amount if divided_amount > 0 else amount,
                'donation_id' : self.id,
                'funds_id' : fund_other
            }
            data_list.append(data)
        for data in data_list :
            obj_donation_line.create(data)
    

    @api.depends('donation_line_ids', 'donation_line_ids.amount')
    @api.multi
    def _get_total_amount_of_donation(self) :
        for rec in self :
            list_donation = rec.donation_line_ids
            total_amount_donation = 0.00
            if list_donation :
                for donation in list_donation :
                    total_amount_donation += donation.amount
            
            rec.total_amount_donation = total_amount_donation

    @api.depends('total_amount_donation', 'voucher_ids', 'voucher_ids.state', 'state')
    @api.multi
    def _compute_donation_amount(self) :
        residual = 0.00
        for rec in self :
            agreed_amount = rec.total_amount_donation
            valid_donation = sum([voucher.donation_amount for voucher in rec.voucher_ids if voucher.donation_state == 'success'])
            valid_amount = sum([voucher.total_donation_amount for voucher in rec.voucher_ids if voucher.donation_state == 'success'])
            fee_amount = sum([voucher.fee_amount for voucher in rec.voucher_ids if voucher.donation_state == 'success'])

            if agreed_amount > valid_amount :
                residual = agreed_amount - valid_amount

            rec.residual = residual
            rec.fee_amount = fee_amount
            rec.total_amount = valid_donation
            rec.total_received_amount = valid_amount
            
            
    # check something 
    @api.multi
    def check_current_status(self) :
        for rec in self :
            rec._compute_donation_amount()
            agreed_amount = rec.residual
            receive_amount = self.total_received_amount
            status = False
            if rec.state == 'confirm' :
                if receive_amount > 0 :
                    rec.action_process()
                else :
                    status == 'confirm'

            if rec.state == 'process' :
                if receive_amount >= agreed_amount :
                    rec.action_done()
                else :
                    status == 'process'


    @api.constrains('total_amount_donation')
    @api.multi
    def check_donation_amount(self):
        for rec in self:
            total_amount = rec.total_amount_donation
            if total_amount < 0 and self.state in ['draft', 'confirm'] :
                raise ValidationError(_("The donation amount is invalid."))


    @api.constrains('end_date','date')
    @api.multi
    def check_end_date(self):
        for rec in self:
            if not rec.date:
                raise ValidationError("Please fill the start date first.")
            elif rec.end_date and rec.date > rec.end_date:
                raise ValidationError("End date must be above the start date.")    

    @api.multi
    def name_get(self):
        res = []
        for item in self:
            name = item.name
            if not name:
                name = "New Donation"
            res.append((item.id, name))
        return res

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.name:
                raise ValidationError("Unable to delete the donation has number.")
            # rec.move_ids.unlink()
            super(Donation, rec).unlink()
        return True

    @api.multi
    def name_set(self):
        for rec in self:
            if not rec.name:
                rec.name = self.env['ir.sequence'].next_by_code('donation.donation') or _('/')


    @api.multi
    def create_voucher(self, amount, fee, transaction_number,other_date):
        voucher_obj = self.env['account.voucher']
        context = self._context
        payment_method = self.payment_method_id
        for rec in self:
            vals = {
                'name': rec.name,
                'donation_id': rec.id,
                'partner_id': rec.partner_id and rec.partner_id.id,
                'voucher_type': 'sale',
                'custom_voucher_type': 'donation',
                'narration': rec.remarks,
                'type_donation':True,
                'ext_transaction_number': transaction_number
            }

            if not vals['partner_id']:
                raise ValidationError(_("Please select a partner before create the donation."))

            #Temp is only allow pay_now
            pay_now = self.pay_now

            vals['pay_now'] = pay_now

            vals['payment_method_id'] = payment_method and payment_method.id or False
            vals['payment_journal_id'] = payment_method and payment_method.id or False
            if not vals['payment_method_id'] and not vals['payment_journal_id']:
                if rec.name :
                    raise UserError(_("Please select payment method for donation with donation number %s." % (rec.name)))
                else:
                    raise UserError(_("Please select payment method for donation."))

            journal_id = rec._default_journal() or False
            vals['journal_id'] = journal_id
            if not vals['journal_id']:
                raise UserError(_("Please configuration donation journal in the company profile configuration."))

            vals['donation_interval_id'] = rec.donation_interval_id and rec.donation_interval_id.id or False
            if not vals['donation_interval_id']:
                raise UserError(_("Please select interval of donation"))

            # date = datetime.today()
            # date = rec.date and datetime.strptime(rec.date, '%Y-%m-%d') or date
            # print(type(date),type(other_date),"============================================11")
            # if rec.interval > 0:
            #     date = datetime.strptime(rec.billing_date)
            #     if not rec.voucher_ids:
            #         date = rec.first_deduction_date or (rec.date and datetime.strptime(rec.date, '%Y-%m-%d') or date)
            vals['date'] = other_date

            if payment_method.type == 'cheque':
                vals['cheque_no'] = rec.cheque_no
            elif payment_method.type == 'paynow':
                vals['paynow_no'] = rec.paynow_no

            # DONATION AMOUNT
            line_id = {}
            line_id['name'] = "Donation Amount: {0}".format(rec.name)
            if rec.company_id and rec.company_id.donation_income_account_id:
                account_id = rec.company_id.donation_income_account_id and rec.company_id.donation_income_account_id.id or False
            else:
                raise UserError(_('Please select donation income account in company configuration.'))

            line_id['account_id'] = account_id
            line_id['quantity'] = 1
            line_id['price_unit'] = amount

            line_ids = []
            line_donation_amount = (0, 0, {
                'name': "Donation: {0}".format(rec.name),
                'account_id': account_id,
                'quantity': 1,
                'price_unit': amount,
                'type': 'donation'
            })

            line_ids.append(line_donation_amount)

            if fee > 0 :
                line_transaction_fee = (0, 0, {
                    'name': "Transaction Fee: {0}".format(rec.name),
                    'account_id': account_id,
                    'quantity': 1,
                    'price_unit': fee,
                    'type': 'transaction_fee'
                })

                line_ids.append(line_transaction_fee)

            vals['line_ids'] = line_ids

            vals.update(context.get('voucher_values', {}))

            if vals.get('pay_now', pay_now) == 'pay_now':
                account_id = payment_method.id and payment_method.default_debit_account_id or False
                if not account_id:
                    payment_method_journal_name = payment_method and payment_method.name or ""
                    raise UserError(_('Please define default credit/debit accounts on the journal "%s".') % (
                        payment_method_journal_name))
            elif rec.company_id and rec.company_id.donation_receivable_account_id:
                account_id = rec.company_id.donation_receivable_account_id
            elif rec.partner_id and rec.partner_id.property_account_receivable_id:
                account_id = rec.partner_id.property_account_receivable_id
            else:
                account_id = False
            vals['account_id'] = account_id and account_id.id
            if not vals['account_id']:
                raise UserError(_("Please configuration donation receivable account in the company profile configuration."))
                
            vals['account_date'] = vals['date']
            voucher_id = voucher_obj.create(vals)
            if pay_now == 'pay_now':
                account_id = payment_method and payment_method.default_debit_account_id or False
                if not account_id:
                    payment_method_journal_name = payment_method and payment_method.name or ""
                    raise UserError(_('Please define default credit/debit accounts on the journal "%s".') % (payment_method_journal_name))
            elif rec.company_id and rec.company_id.donation_receivable_account_id:
                account_id = rec.company_id.donation_receivable_account_id
            elif rec.partner_id and rec.partner_id.property_account_receivable_id:
                account_id = rec.partner_id.property_account_receivable_id
            else:
                account_id = False
            vals['account_id'] = account_id and account_id.id
            if not vals['account_id']:
                raise UserError(_("Please configuration donation receivable account in the company profile configuration."))

            if voucher_id.pay_now == 'pay_now' and voucher_id.donation_state == 'cancel':
                account_id = rec.company_id and rec.company_id.bad_debts_account_id or False
                if not account_id:
                    raise UserError(_("Please configure account for failed donation first under company configuration."))
                voucher_id.write({
                    'account_id': account_id.id
                })
            
            #create account voucher from here for data from all other donation
            voucher_id.proforma_voucher()
            rec.check_current_status()
            rec._compute_donation_amount()
            return voucher_id

    @api.multi
    def mail_done(self):
        # Searching SMTP Server
        obj_mail_server = self.env['ir.mail_server']
        company_mail_server = obj_mail_server.search([])
        # Object for Emails
        obj_mail_mail = self.env['mail.mail']
        # Generating Template
        ir_model_data = self.env['ir.model.data']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        template_id = ir_model_data.get_object_reference('v11_tpstst_custom', 'mail_template_portal_user_notification_donation_done') [1]

        template_obj = self.env['mail.template'].browse(template_id)

        values = template_obj.with_context(lang=user.lang).generate_email(self.id)

        if self.partner_id.email:
            vals = {
                'res_id' : self.id,
                'model' : self._name,
                'author_id' : self.env.user.partner_id.id or False,
                'email_from': self.create_uid.company_id.membership_email,
                'email_to' : self.partner_id.email,
                'subject' : values['subject'],
                'body_html' : values['body_html'],
                'body'  : values['body_html'],
                'mail_server_id' : company_mail_server[0].id,
                'auto_delete' : False,
            }
            email = obj_mail_mail.create(vals)

            if email :
                email.send()