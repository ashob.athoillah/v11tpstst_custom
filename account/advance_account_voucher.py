# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import fields, models, api, _, tools
from odoo.exceptions import UserError, Warning
from odoo.tools import float_round


class AdvanceAccountVoucher(models.Model):
    _inherit = 'advance.account.voucher'

    donation_id = fields.Many2one('donation.donation', string="Donation")
    payment_method_id = fields.Many2one('account.journal', string="Payment Method")

    @api.multi
    def amount_to_text(self, amount):
        self.ensure_one()
        # 200 = Administrative fee
        # 259 = GST at 7%
#        invoice_subtotal = self.invoice_line_ids.price_subtotal + 200 + 259
        amount_due = amount
        amount_i, amount_d = divmod(amount_due, 1)
        words = self.currency_id.with_context(lang=self.partner_id.lang or 'es_ES').amount_to_text(amount_i)
        invoice_words = '%(words)s' % dict(words=words)
        return invoice_words

    @api.model
    def default_get(self, fields):
        rec = super(AdvanceAccountVoucher, self).default_get(fields)
        invoice_obj = self.env['account.invoice']
        invoice_ids = self._context.get('invoice_ids',[]) or []
        if not invoice_ids and self._context.get('active_model','') == 'account.invoice':
            invoice_ids = self._context.get('active_ids',[]) or []
        if invoice_ids:
            invoices = invoice_obj.browse(invoice_ids)
            invoice_defaults = invoices.read([])
            if any(invoice.state != 'open' for invoice in invoices):
                raise UserError(_("You can only register payments for open invoices"))
            if any(inv.currency_id != invoices[0].currency_id for inv in invoices):
                raise UserError(_("In order to pay multiple invoices at once, they must use the same currency."))

            if invoice_defaults and len(invoice_defaults) == 1:
                currency_defaults = self.resolve_2many_commands('currency_id', [(6,0,[invoice_defaults[0]['currency_id'][0]])])
                invoice = invoice_defaults[0]
                currency = currency_defaults[0]
                rec['currency_id'] = invoice['currency_id'][0]
                rec['partner_id'] = invoice['partner_id'][0]
                rec['journal_id'] = invoice['journal_id'][0]
                sign = 1
                if invoice['type'] not in ['out_invoice','in_invoice']:
                    sign = -1
                rec['amount'] = invoice['residual'] * currency['rate'] * sign
            elif invoices:
                partner_ids = []
                for invoice in invoices:
                    if invoice.partner_id and invoice.partner_id.id not in partner_ids:
                        partner_ids.append(invoice.partner_id.id)
                if partner_ids and len(partner_ids) > 1:
                    raise UserError(_("In order to pay multiple invoices at once, they must belong to the same Partner."))
                rec['partner_id'] = partner_ids and partner_ids[0]
        return rec