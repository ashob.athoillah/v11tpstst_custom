# -*- coding: utf-8 -*-
{
    'name': 'TPSTST Custom',
    'version': '1.0',
    'category': 'Custom',
    'author': 'TigernixERP',
    'website': 'https://www.tigernix.com',
    'summary': 'TPSTST Custom',
    'sequence': 2,
    'description': """
                    Module used for developing TPSTST Project, which contain membership, tablet, niche, and event management. Other default included such as
                    Accounting, Contacts, and many more.
                    """,
    # 'external_dependencies': {
    #     'python': [
    #         'xlsxwriter',
    #         'xlrd',
    #     ],
    # },
    'depends':  [
                'base', 'product', 'point_of_sale', 'contacts', 'account', 'account_invoicing', 'account_accountant' ,'stock', 'crm', 'purchase', 'hr_payroll', 'hr', 'hr_expense', 'hr_holidays',
                'board', 'hr_holidays', 'mail', 'stock_barcode', 'calendar', 'event', 'v11_advance_voucher', 'v11_sg_bank_reconcile','v11_recurring_entries', 'v11_tigernix_default', 'web', 'website',
                'l10n_sg', 'v11_custom_ui'],

    'data': [
        #security
        'security/ir.model.access.csv',
        'security/security.xml',

        #report
        'report/report_associate_member_application_form.xml',
        'report/report_paper_views.xml',
        'report/report_custom_header_views.xml',
        'report/report_invoice_views.xml',
        'report/report_donation_receipt_views.xml',
        'report/report_associate_receipt_views.xml',
        'report/report_niche_marble_views.xml',
        'report/report_ordinary_member_receipt.xml',
        'report/report_ordinary_membership_form.xml',
        'report/report_memo_views.xml',
        'report/report_trunk_box_views.xml',
        'report/report_views.xml',
        'report/report_tablet_application_form_report_view.xml',
        'report/report_columbarium_application_form.xml',



        #data
        'data/sequence.xml',
        'data/assets.xml',
        'data/scheduler.xml',


        #edi
    
        #Wizard
        'wizard/account_funds_report_view.xml',
        # 'wizard/breakdown_funds_report_view.xml',
        'wizard/generate_donation_lines_views.xml',
        'wizard/generate_voucher_donation_views.xml',
        # 'wizard/columbarium_lot_no_wizard_view.xml',
        'wizard/generate_session_wizard_view.xml',
        'wizard/pool_of_funds_amend_view.xml',
        'wizard/closing_bank_in_views.xml',

        #views

        #Base
        'base/partner_views.xml',
        'base/company_views.xml',
        'base/users_views.xml',
        'base/mail_views.xml',
        'base/partner_configuration_views.xml',
        'base/menu_views.xml',

        #account
        'account/account_invoice_views.xml',
        'account/account_voucher_views.xml',
        'account/account_view.xml',
        'account/payment_method_views.xml',
        'account/lock_period_views.xml',
        'account/bank_account_reconciliation_views.xml',
        # 'account/bank_rec_line_unselect_views.xml',
        'account/account_move_views.xml',
        # 'account/giro_collection_views.xml',
        'account/menu_views.xml',

        #product
        'product/product_views.xml',
        'product/configuration_views.xml',
        'product/menu_views.xml',
        
        #Donation
        'donation/donation_transaction_views.xml',
        'donation/donation_views.xml',
        'donation/donation_line_views.xml',
        'donation/donation_configuration_views.xml',
        'donation/donation_dashboard.xml',
        'donation/menu_views.xml',

        #membership
        'membership/niche_application_views.xml',
        'membership/membership_application_views.xml',
        'membership/membership_renewal_views.xml',
        'membership/tablet_application_views.xml',
        'membership/purchase_in_advance_views.xml',
        'membership/membership_views.xml',
        'membership/membership_configuration_views.xml',
        'membership/menu_views.xml',

        #Event
        'event/event_view.xml',
        'event/event_registration_view.xml',
        'event/event_session_view.xml',
        'event/menu_views.xml',

        #Participant
        'participant/participant_registration_view.xml',
        'participant/menu_views.xml',

        #template
        'template/tpstst_email_template.xml',

    ],
    'demo': [
    ],
    'qweb': [
        'base/base.xml',
        'static/src/xml/digital_sign.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
